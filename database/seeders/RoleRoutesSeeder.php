<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleRoutesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_routes')->delete();
        
        \DB::table('role_routes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Admin Home',
                'url' => 'superAdminHome',
                'module_id' => 0,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Download Pdf',
                'url' => 'downloadPdf',
                'module_id' => 0,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'View Pdf',
                'url' => 'viewPdf',
                'module_id' => 0,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Admin Home',
                'url' => 'adminHome',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Site Setting',
                'url' => 'siteSetting',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Logo Setting',
                'url' => 'logoSetting',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Social Setting',
                'url' => 'socialSetting',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Application Setting',
                'url' => 'applicationSetting',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Admin Settings Save',
                'url' => 'adminSettingsSave',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Command Settings',
                'url' => 'commandSettings',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Run Command',
                'url' => 'runCommand',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Create Command',
                'url' => 'createCommand',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Roles',
                'url' => 'roles',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Save Role',
                'url' => 'saveRole',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Edit Role',
                'url' => 'editRole',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Delete Role',
                'url' => 'deleteRole',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Update Route List',
                'url' => 'updateRouteList',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Get Route By Type',
                'url' => 'getRouteByType',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Users',
                'url' => 'users',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Save User',
                'url' => 'saveUser',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Edit User',
                'url' => 'editUser',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Delete User',
                'url' => 'deleteUser',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'User Slug Check',
                'url' => 'userSlugCheck',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Admin Logs',
                'url' => 'adminLogs',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Product Manager Home',
                'url' => 'productManagerHome',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Supplier',
                'url' => 'supplier',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Edit Supplier',
                'url' => 'editSupplier',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Save Supplier',
                'url' => 'saveSupplier',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Delete Supplier',
                'url' => 'deleteSupplier',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Delivery Type',
                'url' => 'deliveryType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Edit Delivery Type',
                'url' => 'editDeliveryType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Store Delivery Type',
                'url' => 'storeDeliveryType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Delete Delivery Type',
                'url' => 'deleteDeliveryType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Tags',
                'url' => 'tags',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Edit Tag',
                'url' => 'editTag',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Store Tag',
                'url' => 'storeTag',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Delete Tag',
                'url' => 'deleteTag',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Brands',
                'url' => 'brands',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Edit Brand',
                'url' => 'editBrand',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Save Brand',
                'url' => 'saveBrand',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Brand Slug Check',
                'url' => 'brandSlugCheck',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Delete Brand',
                'url' => 'deleteBrand',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Categories',
                'url' => 'categories',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Edit Category',
                'url' => 'editCategory',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Show Category',
                'url' => 'showCategory',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Save Category',
                'url' => 'saveCategory',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Delete Category',
                'url' => 'deleteCategory',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Category Order Update',
                'url' => 'categoryOrderUpdate',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Category Order Save',
                'url' => 'categoryOrderSave',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Category Slug Check',
                'url' => 'categorySlugCheck',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Combination Type',
                'url' => 'combinationType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Edit Combination Type',
                'url' => 'editCombinationType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Store Combination Type',
                'url' => 'storeCombinationType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Delete Combination Type',
                'url' => 'deleteCombinationType',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Combination',
                'url' => 'combination',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Edit Combination',
                'url' => 'editCombination',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Store Combination',
                'url' => 'storeCombination',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Delete Combination',
                'url' => 'deleteCombination',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Product',
                'url' => 'product',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Edit Product',
                'url' => 'editProduct',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Init Product',
                'url' => 'initProduct',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Save Product',
                'url' => 'saveProduct',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Delete Product',
                'url' => 'deleteProduct',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Product Slug Check',
                'url' => 'productSlugCheck',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Product Reference Check',
                'url' => 'productReferenceCheck',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Product Status Change',
                'url' => 'productStatusChange',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Save Product Categories And Tags',
                'url' => 'saveProductCategoriesAndTags',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Get Product Supplier',
                'url' => 'getProductSupplier',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Save Product Pricing',
                'url' => 'saveProductPricing',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Save Product Combinations',
                'url' => 'saveProductCombinations',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Save Product Media',
                'url' => 'saveProductMedia',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Save Product Featured Media',
                'url' => 'saveProductFeaturedMedia',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Duplicate Product',
                'url' => 'duplicateProduct',
                'module_id' => 2,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Production Manager Home',
                'url' => 'productionManagerHome',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Production Order List',
                'url' => 'productionOrderList',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'Production Order Details',
                'url' => 'productionOrderDetails',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Production Order Product Purchase',
                'url' => 'productionOrderProductPurchase',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Production Order Save',
                'url' => 'productionOrderSave',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Production Purchase List',
                'url' => 'productionPurchaseList',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Production List',
                'url' => 'productionList',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Production Completed List',
                'url' => 'productionCompletedList',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Production Shipment List',
                'url' => 'productionShipmentList',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Crm Manager Home',
                'url' => 'crmManagerHome',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Crm Payment Condition',
                'url' => 'crmPaymentCondition',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Edit Crm Payment Condition',
                'url' => 'editCrmPaymentCondition',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Store Crm Payment Condition',
                'url' => 'storeCrmPaymentCondition',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Delete Crm Payment Condition',
                'url' => 'deleteCrmPaymentCondition',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Crm Printing Option',
                'url' => 'crmPrintingOption',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Edit Crm Printing Option',
                'url' => 'editCrmPrintingOption',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Store Crm Printing Option',
                'url' => 'storeCrmPrintingOption',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Delete Crm Printing Option',
                'url' => 'deleteCrmPrintingOption',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Crm Customer List',
                'url' => 'crmCustomerList',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Crm Customer Add',
                'url' => 'crmCustomerAdd',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Crm Customer Edit',
                'url' => 'crmCustomerEdit',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Crm Customer Save',
                'url' => 'crmCustomerSave',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Crm Customer Delete',
                'url' => 'crmCustomerDelete',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => ' Crm Quotation List',
                'url' => 'CrmQuotationList',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => ' Crm Assign Salesman',
                'url' => 'CrmAssignSalesman',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Crm Quotation View',
                'url' => 'crmQuotationView',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Crm Budget List',
                'url' => 'crmBudgetList',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Crm Budget Details',
                'url' => 'crmBudgetDetails',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Crm Save Budget',
                'url' => 'crmSaveBudget',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Crm Save Budget Product',
                'url' => 'crmSaveBudgetProduct',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Crm Update Budget Product',
                'url' => 'crmUpdateBudgetProduct',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Crm Duplicate Budget Product',
                'url' => 'crmDuplicateBudgetProduct',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Crm Delete Budget Product',
                'url' => 'crmDeleteBudgetProduct',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Crm Send Budget Mail',
                'url' => 'crmSendBudgetMail',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Crm Budget Product Search',
                'url' => 'crmBudgetProductSearch',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Crm Budget Product Edit',
                'url' => 'crmBudgetProductEdit',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Crm Print Budget',
                'url' => 'crmPrintBudget',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Crm Make Budget Order',
                'url' => 'crmMakeBudgetOrder',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Website Manager Home',
                'url' => 'websiteManagerHome',
                'module_id' => 5,
                'created_at' => '2021-01-27 10:07:30',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Production Purchase Details',
                'url' => 'productionPurchaseDetails',
                'module_id' => 4,
                'created_at' => '2021-01-29 12:46:51',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Save Arrival Quantity',
                'url' => 'saveArrivalQuantity',
                'module_id' => 4,
                'created_at' => '2021-01-29 12:46:51',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Send To Production',
                'url' => 'sendToProduction',
                'module_id' => 4,
                'created_at' => '2021-01-29 12:46:51',
                'updated_at' => '2021-01-29 12:46:51',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Production Details',
                'url' => 'productionDetails',
                'module_id' => 4,
                'created_at' => '2021-01-29 12:46:51',
                'updated_at' => '2021-01-29 12:46:51',
            ),
        ));

        
    }
}