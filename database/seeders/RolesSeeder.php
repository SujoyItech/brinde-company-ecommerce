<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Admin',
                'actions' => '4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24',
                'module_id' => 1,
                'created_at' => '2021-01-27 10:21:58',
                'updated_at' => '2021-01-27 10:21:58',
            ),
            1 => 
            array (
                'id' => 20,
                'title' => 'Product Admin',
                'actions' => '25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73',
                'module_id' => 2,
                'created_at' => '2021-01-29 11:39:58',
                'updated_at' => '2021-01-29 11:40:01',
            ),
            2 => 
            array (
                'id' => 21,
                'title' => 'Product Entry Manager',
                'actions' => '25|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73',
                'module_id' => 2,
                'created_at' => '2021-01-29 11:39:58',
                'updated_at' => '2021-01-29 12:15:56',
            ),
            3 => 
            array (
                'id' => 30,
                'title' => 'CRM Admin',
                'actions' => '83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:24:43',
                'updated_at' => '2021-01-27 10:24:43',
            ),
            4 => 
            array (
                'id' => 31,
                'title' => 'Sales Manager',
                'actions' => '83|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:24:43',
                'updated_at' => '2021-01-29 12:48:32',
            ),
            5 => 
            array (
                'id' => 32,
                'title' => 'Sales Man',
                'actions' => '83|92|93|94|95|96|100|101|102|103|104|105|106|107|108|109|110|111',
                'module_id' => 3,
                'created_at' => '2021-01-27 10:25:08',
                'updated_at' => '2021-01-29 13:19:06',
            ),
            6 => 
            array (
                'id' => 40,
                'title' => 'Production Admin',
                'actions' => '74|75|76|77|78|79|80|81|82',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:26:19',
                'updated_at' => '2021-01-27 10:26:19',
            ),
            7 => 
            array (
                'id' => 41,
                'title' => 'Purchase Manager',
                'actions' => '74|75|76|77|78|79|113',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:26:19',
                'updated_at' => '2021-01-29 13:48:00',
            ),
            8 => 
            array (
                'id' => 42,
                'title' => 'Logistic Manager',
                'actions' => '74|79|80|81|82|113|114|115|116',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:26:19',
                'updated_at' => '2021-01-29 13:49:28',
            ),
            9 => 
            array (
                'id' => 43,
                'title' => 'Production Manager',
                'actions' => '74|80|81',
                'module_id' => 4,
                'created_at' => '2021-01-27 10:26:19',
                'updated_at' => '2021-01-29 13:56:31',
            ),
        ));

        
    }
}