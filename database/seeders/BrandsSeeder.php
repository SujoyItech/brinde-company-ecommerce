<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BrandsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Brinde Companhia',
                'slug' => 'BC',
                'brand_url' => 'https://www.brinde-companhia.pt',
                'icon' => NULL,
                'status' => 1,
                'is_reseller' => 0,
                'created_at' => '2021-01-29 12:14:47',
                'updated_at' => '2021-01-29 13:35:58',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Top Brinde',
                'slug' => 'TB',
                'brand_url' => 'https://www.topbrinde.pt',
                'icon' => NULL,
                'status' => 1,
                'is_reseller' => 1,
                'created_at' => '2021-01-29 12:28:28',
                'updated_at' => '2021-01-29 12:28:28',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Brindese Brindes',
                'slug' => 'BB',
                'brand_url' => 'https://www.brindesebrindes.pt',
                'icon' => NULL,
                'status' => 1,
                'is_reseller' => 0,
                'created_at' => '2021-01-29 12:28:28',
                'updated_at' => '2021-01-29 12:28:28',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Bestof Gifts',
                'slug' => 'BG',
                'brand_url' => 'https://www.bestofgifts.pt',
                'icon' => NULL,
                'status' => 1,
                'is_reseller' => 0,
                'created_at' => '2021-01-29 12:28:28',
                'updated_at' => '2021-01-29 12:28:28',
                'deleted_at' => NULL,
            ),
        ));

        
    }
}