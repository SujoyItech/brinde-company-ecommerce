<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('roles')->delete();

        \DB::table('roles')->insert(array (
            0 =>
            array (
                'actions' => '4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24',
                'created_at' => '2021-01-27 10:21:58',
                'id' => 1,
                'module_id' => 1,
                'title' => 'Admin',
                'updated_at' => '2021-01-27 10:21:58',
            ),
            1 =>
            array (
                'actions' => '25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73',
                'created_at' => '2021-01-27 10:23:41',
                'id' => 2,
                'module_id' => 2,
                'title' => 'Product Manager',
                'updated_at' => '2021-01-27 10:23:41',
            ),
            2 =>
            array (
                'actions' => '83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111',
                'created_at' => '2021-01-27 10:24:43',
                'id' => 3,
                'module_id' => 4,
                'title' => 'Sales Manager',
                'updated_at' => '2021-01-27 10:24:43',
            ),
            3 =>
            array (
                'actions' => '83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111',
                'created_at' => '2021-01-27 10:25:08',
                'id' => 4,
                'module_id' => 4,
                'title' => 'Sales Man',
                'updated_at' => '2021-01-27 10:25:08',
            ),
            4 =>
            array (
                'actions' => '74|75|76|77|78|79|80|81|82',
                'created_at' => '2021-01-27 10:25:35',
                'id' => 5,
                'module_id' => 3,
                'title' => 'Purchase Manager',
                'updated_at' => '2021-01-27 10:25:35',
            ),
            5 =>
            array (
                'actions' => '74|75|76|77|78|79|80|81|82',
                'created_at' => '2021-01-27 10:25:57',
                'id' => 6,
                'module_id' => 3,
                'title' => 'Logistic Manager',
                'updated_at' => '2021-01-27 10:25:57',
            ),
            6 =>
            array (
                'actions' => '74|75|76|77|78|79|80|81|82',
                'created_at' => '2021-01-27 10:26:19',
                'id' => 7,
                'module_id' => 3,
                'title' => 'Production Manager',
                'updated_at' => '2021-01-27 10:26:19',
            ),
        ));


    }
}
