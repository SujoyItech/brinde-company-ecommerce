<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call(UsersSeeder::class);
        $this->call(CombinationTypeSeeder::class);
        $this->call(RoleRouteSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(RoleRoutesSeeder::class);
        $this->call(BrandsSeeder::class);
    }
}
