<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use phpseclib\Crypt\Hash;

class DemoUsersSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'created_at' => '2021-01-26 11:53:42',
                'current_team_id' => NULL,
                'email' => 'itech@gmail.com',
                'email_verified_at' => NULL,
                'id' => 1,
                'module_id' => 0,
                'name' => 'Super Admin',
                'password' => \Illuminate\Support\Facades\Hash::make('123456'),
                'profile_photo_path' => NULL,
                'remember_token' => NULL,
                'role' => 0,
                'slug' => 'SA',
                'status' => 1,
                'updated_at' => '2021-01-26 11:53:42',
            ),
            1 =>
            array (
                'created_at' => '2021-01-27 10:27:19',
                'current_team_id' => NULL,
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'id' => 2,
                'module_id' => 1,
                'name' => 'Admin',
                'password' => '$2y$10$OWRMlfWgCAPLZnZxnTcjlO4F5r/JWojOQhKtLBC52.QOdd9eI4QZS',
                'profile_photo_path' => NULL,
                'remember_token' => '285f70eac9a40dbde59a3d64096c655b',
                'role' => 1,
                'slug' => 'AD',
                'status' => 1,
                'updated_at' => '2021-01-27 10:27:19',
            ),
            2 =>
            array (
                'created_at' => '2021-01-27 10:28:23',
                'current_team_id' => NULL,
                'email' => 'productmanager@gmail.com',
                'email_verified_at' => NULL,
                'id' => 3,
                'module_id' => 2,
                'name' => 'Product Manager',
                'password' => '$2y$10$ESNW0ttfsscMRR0QjtLFtOnbNSxQ4pHaJZGyyG6KedYL.4KP8vDNa',
                'profile_photo_path' => NULL,
                'remember_token' => '78d7cc7b8be38b344040d18c82457cab',
                'role' => 2,
                'slug' => 'PM',
                'status' => 1,
                'updated_at' => '2021-01-27 10:28:23',
            ),
            3 =>
            array (
                'created_at' => '2021-01-27 10:29:58',
                'current_team_id' => NULL,
                'email' => 'salesmanager@gmail.com',
                'email_verified_at' => NULL,
                'id' => 4,
                'module_id' => 4,
                'name' => 'Sales Manager',
                'password' => '$2y$10$jQFaHkxPsvFbO.94G2vR3u0.jMnVMlIOJ9kSpIYx7ldT6ii45VIW.',
                'profile_photo_path' => NULL,
                'remember_token' => '3d52f5d536a7ea81d22537de021a4d9c',
                'role' => 3,
                'slug' => 'SM',
                'status' => 1,
                'updated_at' => '2021-01-27 10:29:58',
            ),
            4 =>
            array (
                'created_at' => '2021-01-27 10:31:10',
                'current_team_id' => NULL,
                'email' => 'salesman@gmail.com',
                'email_verified_at' => NULL,
                'id' => 5,
                'module_id' => 4,
                'name' => 'Sales Man',
                'password' => '$2y$10$CsUzS62oUW8nJ5G5y5Gi0OBiayk2lI6tiC3jymGNSp/hneSiAov.S',
                'profile_photo_path' => NULL,
                'remember_token' => 'e2c72b35b6f171102b93dfbb40dcf6c1',
                'role' => 4,
                'slug' => 'SMAN',
                'status' => 1,
                'updated_at' => '2021-01-27 10:31:10',
            ),
            5 =>
            array (
                'created_at' => '2021-01-27 10:32:23',
                'current_team_id' => NULL,
                'email' => 'purchasemanager@gmail.com',
                'email_verified_at' => NULL,
                'id' => 6,
                'module_id' => 3,
                'name' => 'Purchase Manager',
                'password' => '$2y$10$Q.iPYAhd1O2n1NgRIPTaTuvWrTxehIWFfLfK3DGiv59PjqiTr7I.O',
                'profile_photo_path' => NULL,
                'remember_token' => 'a71d9a36f2c7e89b71200fbc03b4040a',
                'role' => 5,
                'slug' => 'PCM',
                'status' => 1,
                'updated_at' => '2021-01-27 10:32:23',
            ),
            6 =>
            array (
                'created_at' => '2021-01-27 10:33:30',
                'current_team_id' => NULL,
                'email' => 'logisticmanager@gmail.com',
                'email_verified_at' => NULL,
                'id' => 7,
                'module_id' => 3,
                'name' => 'Logistic Manager',
                'password' => '$2y$10$AepjosoXQO9qrnKYzk4Z5.f3lWBB5YfHGezXR/vrnmZU84a3Vy60O',
                'profile_photo_path' => NULL,
                'remember_token' => '9b4a7204fb0425669674e10b0e8ff024',
                'role' => 6,
                'slug' => 'LM',
                'status' => 1,
                'updated_at' => '2021-01-27 10:33:30',
            ),
            7 =>
            array (
                'created_at' => '2021-01-27 10:34:26',
                'current_team_id' => NULL,
                'email' => 'productionmanager@gmail.com',
                'email_verified_at' => NULL,
                'id' => 8,
                'module_id' => 3,
                'name' => 'Production Manager',
                'password' => '$2y$10$W.fl81LYIcFx8eWmhENG4eApSL/7wi/fGweNpctI7GVkBBp8PS6Jm',
                'profile_photo_path' => NULL,
                'remember_token' => 'acefbc075365bf6fd0282cb4e0ead9e7',
                'role' => 7,
                'slug' => 'PRM',
                'status' => 1,
                'updated_at' => '2021-01-27 10:34:26',
            ),
        ));


    }
}
