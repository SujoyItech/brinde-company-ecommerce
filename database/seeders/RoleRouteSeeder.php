<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleRouteSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('role_routes')->delete();

        \DB::table('role_routes')->insert(array (
            0 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 1,
                'module_id' => 0,
                'name' => 'Super Admin Home',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'superAdminHome',
            ),
            1 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 2,
                'module_id' => 0,
                'name' => 'Download Pdf',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'downloadPdf',
            ),
            2 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 3,
                'module_id' => 0,
                'name' => 'View Pdf',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'viewPdf',
            ),
            3 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 4,
                'module_id' => 1,
                'name' => 'Admin Home',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'adminHome',
            ),
            4 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 5,
                'module_id' => 1,
                'name' => 'Site Setting',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'siteSetting',
            ),
            5 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 6,
                'module_id' => 1,
                'name' => 'Logo Setting',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'logoSetting',
            ),
            6 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 7,
                'module_id' => 1,
                'name' => 'Social Setting',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'socialSetting',
            ),
            7 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 8,
                'module_id' => 1,
                'name' => 'Application Setting',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'applicationSetting',
            ),
            8 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 9,
                'module_id' => 1,
                'name' => 'Admin Settings Save',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'adminSettingsSave',
            ),
            9 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 10,
                'module_id' => 1,
                'name' => 'Command Settings',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'commandSettings',
            ),
            10 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 11,
                'module_id' => 1,
                'name' => 'Run Command',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'runCommand',
            ),
            11 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 12,
                'module_id' => 1,
                'name' => 'Create Command',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'createCommand',
            ),
            12 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 13,
                'module_id' => 1,
                'name' => 'Roles',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'roles',
            ),
            13 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 14,
                'module_id' => 1,
                'name' => 'Save Role',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveRole',
            ),
            14 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 15,
                'module_id' => 1,
                'name' => 'Edit Role',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editRole',
            ),
            15 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 16,
                'module_id' => 1,
                'name' => 'Delete Role',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteRole',
            ),
            16 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 17,
                'module_id' => 1,
                'name' => 'Update Route List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'updateRouteList',
            ),
            17 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 18,
                'module_id' => 1,
                'name' => 'Get Route By Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'getRouteByType',
            ),
            18 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 19,
                'module_id' => 1,
                'name' => 'Users',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'users',
            ),
            19 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 20,
                'module_id' => 1,
                'name' => 'Save User',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveUser',
            ),
            20 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 21,
                'module_id' => 1,
                'name' => 'Edit User',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editUser',
            ),
            21 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 22,
                'module_id' => 1,
                'name' => 'Delete User',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteUser',
            ),
            22 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 23,
                'module_id' => 1,
                'name' => 'User Slug Check',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'userSlugCheck',
            ),
            23 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 24,
                'module_id' => 1,
                'name' => 'Admin Logs',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'adminLogs',
            ),
            24 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 25,
                'module_id' => 2,
                'name' => 'Product Manager Home',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productManagerHome',
            ),
            25 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 26,
                'module_id' => 2,
                'name' => 'Supplier',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'supplier',
            ),
            26 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 27,
                'module_id' => 2,
                'name' => 'Edit Supplier',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editSupplier',
            ),
            27 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 28,
                'module_id' => 2,
                'name' => 'Save Supplier',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveSupplier',
            ),
            28 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 29,
                'module_id' => 2,
                'name' => 'Delete Supplier',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteSupplier',
            ),
            29 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 30,
                'module_id' => 2,
                'name' => 'Delivery Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deliveryType',
            ),
            30 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 31,
                'module_id' => 2,
                'name' => 'Edit Delivery Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editDeliveryType',
            ),
            31 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 32,
                'module_id' => 2,
                'name' => 'Store Delivery Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'storeDeliveryType',
            ),
            32 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 33,
                'module_id' => 2,
                'name' => 'Delete Delivery Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteDeliveryType',
            ),
            33 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 34,
                'module_id' => 2,
                'name' => 'Tags',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'tags',
            ),
            34 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 35,
                'module_id' => 2,
                'name' => 'Edit Tag',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editTag',
            ),
            35 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 36,
                'module_id' => 2,
                'name' => 'Store Tag',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'storeTag',
            ),
            36 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 37,
                'module_id' => 2,
                'name' => 'Delete Tag',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteTag',
            ),
            37 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 38,
                'module_id' => 2,
                'name' => 'Brands',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'brands',
            ),
            38 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 39,
                'module_id' => 2,
                'name' => 'Edit Brand',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editBrand',
            ),
            39 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 40,
                'module_id' => 2,
                'name' => 'Save Brand',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveBrand',
            ),
            40 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 41,
                'module_id' => 2,
                'name' => 'Brand Slug Check',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'brandSlugCheck',
            ),
            41 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 42,
                'module_id' => 2,
                'name' => 'Delete Brand',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteBrand',
            ),
            42 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 43,
                'module_id' => 2,
                'name' => 'Categories',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'categories',
            ),
            43 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 44,
                'module_id' => 2,
                'name' => 'Edit Category',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editCategory',
            ),
            44 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 45,
                'module_id' => 2,
                'name' => 'Show Category',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'showCategory',
            ),
            45 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 46,
                'module_id' => 2,
                'name' => 'Save Category',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveCategory',
            ),
            46 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 47,
                'module_id' => 2,
                'name' => 'Delete Category',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteCategory',
            ),
            47 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 48,
                'module_id' => 2,
                'name' => 'Category Order Update',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'categoryOrderUpdate',
            ),
            48 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 49,
                'module_id' => 2,
                'name' => 'Category Order Save',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'categoryOrderSave',
            ),
            49 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 50,
                'module_id' => 2,
                'name' => 'Category Slug Check',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'categorySlugCheck',
            ),
            50 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 51,
                'module_id' => 2,
                'name' => 'Combination Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'combinationType',
            ),
            51 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 52,
                'module_id' => 2,
                'name' => 'Edit Combination Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editCombinationType',
            ),
            52 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 53,
                'module_id' => 2,
                'name' => 'Store Combination Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'storeCombinationType',
            ),
            53 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 54,
                'module_id' => 2,
                'name' => 'Delete Combination Type',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteCombinationType',
            ),
            54 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 55,
                'module_id' => 2,
                'name' => 'Combination',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'combination',
            ),
            55 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 56,
                'module_id' => 2,
                'name' => 'Edit Combination',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editCombination',
            ),
            56 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 57,
                'module_id' => 2,
                'name' => 'Store Combination',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'storeCombination',
            ),
            57 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 58,
                'module_id' => 2,
                'name' => 'Delete Combination',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteCombination',
            ),
            58 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 59,
                'module_id' => 2,
                'name' => 'Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'product',
            ),
            59 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 60,
                'module_id' => 2,
                'name' => 'Edit Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editProduct',
            ),
            60 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 61,
                'module_id' => 2,
                'name' => 'Init Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'initProduct',
            ),
            61 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 62,
                'module_id' => 2,
                'name' => 'Save Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveProduct',
            ),
            62 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 63,
                'module_id' => 2,
                'name' => 'Delete Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteProduct',
            ),
            63 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 64,
                'module_id' => 2,
                'name' => 'Product Slug Check',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productSlugCheck',
            ),
            64 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 65,
                'module_id' => 2,
                'name' => 'Product Reference Check',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productReferenceCheck',
            ),
            65 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 66,
                'module_id' => 2,
                'name' => 'Product Status Change',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productStatusChange',
            ),
            66 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 67,
                'module_id' => 2,
                'name' => 'Save Product Categories And Tags',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveProductCategoriesAndTags',
            ),
            67 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 68,
                'module_id' => 2,
                'name' => 'Get Product Supplier',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'getProductSupplier',
            ),
            68 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 69,
                'module_id' => 2,
                'name' => 'Save Product Pricing',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveProductPricing',
            ),
            69 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 70,
                'module_id' => 2,
                'name' => 'Save Product Combinations',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveProductCombinations',
            ),
            70 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 71,
                'module_id' => 2,
                'name' => 'Save Product Media',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveProductMedia',
            ),
            71 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 72,
                'module_id' => 2,
                'name' => 'Save Product Featured Media',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'saveProductFeaturedMedia',
            ),
            72 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 73,
                'module_id' => 2,
                'name' => 'Duplicate Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'duplicateProduct',
            ),
            73 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 74,
                'module_id' => 3,
                'name' => 'Production Manager Home',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionManagerHome',
            ),
            74 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 75,
                'module_id' => 3,
                'name' => 'Production Order List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionOrderList',
            ),
            75 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 76,
                'module_id' => 3,
                'name' => 'Production Order Details',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionOrderDetails',
            ),
            76 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 77,
                'module_id' => 3,
                'name' => 'Production Order Product Purchase',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionOrderProductPurchase',
            ),
            77 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 78,
                'module_id' => 3,
                'name' => 'Production Order Save',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionOrderSave',
            ),
            78 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 79,
                'module_id' => 3,
                'name' => 'Production Purchase List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionPurchaseList',
            ),
            79 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 80,
                'module_id' => 3,
                'name' => 'Production List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionList',
            ),
            80 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 81,
                'module_id' => 3,
                'name' => 'Production Completed List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionCompletedList',
            ),
            81 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 82,
                'module_id' => 3,
                'name' => 'Production Shipment List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'productionShipmentList',
            ),
            82 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 83,
                'module_id' => 4,
                'name' => 'Crm Manager Home',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmManagerHome',
            ),
            83 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 84,
                'module_id' => 4,
                'name' => 'Crm Payment Condition',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmPaymentCondition',
            ),
            84 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 85,
                'module_id' => 4,
                'name' => 'Edit Crm Payment Condition',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editCrmPaymentCondition',
            ),
            85 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 86,
                'module_id' => 4,
                'name' => 'Store Crm Payment Condition',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'storeCrmPaymentCondition',
            ),
            86 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 87,
                'module_id' => 4,
                'name' => 'Delete Crm Payment Condition',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteCrmPaymentCondition',
            ),
            87 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 88,
                'module_id' => 4,
                'name' => 'Crm Printing Option',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmPrintingOption',
            ),
            88 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 89,
                'module_id' => 4,
                'name' => 'Edit Crm Printing Option',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'editCrmPrintingOption',
            ),
            89 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 90,
                'module_id' => 4,
                'name' => 'Store Crm Printing Option',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'storeCrmPrintingOption',
            ),
            90 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 91,
                'module_id' => 4,
                'name' => 'Delete Crm Printing Option',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'deleteCrmPrintingOption',
            ),
            91 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 92,
                'module_id' => 4,
                'name' => 'Crm Customer List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmCustomerList',
            ),
            92 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 93,
                'module_id' => 4,
                'name' => 'Crm Customer Add',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmCustomerAdd',
            ),
            93 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 94,
                'module_id' => 4,
                'name' => 'Crm Customer Edit',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmCustomerEdit',
            ),
            94 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 95,
                'module_id' => 4,
                'name' => 'Crm Customer Save',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmCustomerSave',
            ),
            95 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 96,
                'module_id' => 4,
                'name' => 'Crm Customer Delete',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmCustomerDelete',
            ),
            96 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 97,
                'module_id' => 4,
                'name' => ' Crm Quotation List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'CrmQuotationList',
            ),
            97 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 98,
                'module_id' => 4,
                'name' => ' Crm Assign Salesman',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'CrmAssignSalesman',
            ),
            98 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 99,
                'module_id' => 4,
                'name' => 'Crm Quotation View',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmQuotationView',
            ),
            99 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 100,
                'module_id' => 4,
                'name' => 'Crm Budget List',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmBudgetList',
            ),
            100 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 101,
                'module_id' => 4,
                'name' => 'Crm Budget Details',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmBudgetDetails',
            ),
            101 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 102,
                'module_id' => 4,
                'name' => 'Crm Save Budget',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmSaveBudget',
            ),
            102 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 103,
                'module_id' => 4,
                'name' => 'Crm Save Budget Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmSaveBudgetProduct',
            ),
            103 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 104,
                'module_id' => 4,
                'name' => 'Crm Update Budget Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmUpdateBudgetProduct',
            ),
            104 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 105,
                'module_id' => 4,
                'name' => 'Crm Duplicate Budget Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmDuplicateBudgetProduct',
            ),
            105 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 106,
                'module_id' => 4,
                'name' => 'Crm Delete Budget Product',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmDeleteBudgetProduct',
            ),
            106 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 107,
                'module_id' => 4,
                'name' => 'Crm Send Budget Mail',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmSendBudgetMail',
            ),
            107 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 108,
                'module_id' => 4,
                'name' => 'Crm Budget Product Search',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmBudgetProductSearch',
            ),
            108 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 109,
                'module_id' => 4,
                'name' => 'Crm Budget Product Edit',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmBudgetProductEdit',
            ),
            109 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 110,
                'module_id' => 4,
                'name' => 'Crm Print Budget',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmPrintBudget',
            ),
            110 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 111,
                'module_id' => 4,
                'name' => 'Crm Make Budget Order',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'crmMakeBudgetOrder',
            ),
            111 =>
            array (
                'created_at' => '2021-01-27 10:07:30',
                'id' => 112,
                'module_id' => 5,
                'name' => 'Website Manager Home',
                'updated_at' => '2021-01-27 10:07:30',
                'url' => 'websiteManagerHome',
            ),
        ));


    }
}
