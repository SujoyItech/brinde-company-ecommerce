<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Admin',
                'slug' => 'SA',
                'email' => 'itech@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$pLK9SyIi7dJtkzmWTg6oNuxoIfIfT.V0srsNT5FXWwkL1ECzPSXl.',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 0,
                'role' => 0,
                'created_at' => '2021-01-26 11:53:42',
                'updated_at' => '2021-01-26 11:53:42',
                'status' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Admin',
                'slug' => 'AD',
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$pLK9SyIi7dJtkzmWTg6oNuxoIfIfT.V0srsNT5FXWwkL1ECzPSXl.',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => 'Hh0UW6I2DtMluSGkXJ5p5zfqOquq5MRQuI4eXNysl0KfnN0UVpRVS6dOpBqp',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 1,
                'role' => 1,
                'created_at' => '2021-01-27 10:27:19',
                'updated_at' => '2021-01-27 10:27:19',
                'status' => 1,
            ),
            2 => 
            array (
                'id' => 9,
                'name' => 'Product Admin',
                'slug' => 'PA',
                'email' => 'productadmin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$LI3HBoUrsabr.DKWJ1GZkunyRd/PS.oJwplAadj9pI54w4C.XeGiC',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => '217db6039a1b40986dd5151f5b1a5068',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 2,
                'role' => 20,
                'created_at' => '2021-01-29 10:56:55',
                'updated_at' => '2021-01-29 12:09:13',
                'status' => 1,
            ),
            3 => 
            array (
                'id' => 10,
                'name' => 'Product Entry Manager',
                'slug' => 'PEM',
                'email' => 'productentrymanager@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$h5TwWkFQm3.8flQobhHkQepxbpA9hSXr1XAac/7r6iD.3dBnalf9y',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => '5c7de6b32d9b693460cebab343ffeea7',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 2,
                'role' => 21,
                'created_at' => '2021-01-29 11:00:35',
                'updated_at' => '2021-01-29 12:09:03',
                'status' => 1,
            ),
            4 => 
            array (
                'id' => 11,
                'name' => 'CRM Admin',
                'slug' => 'CA',
                'email' => 'crmadmin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$dlqmeQumt/SyX1EVU2GJTeGo95oSJWe5oKl5.Hb02WEH5sHZjz4ye',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => 'a9940861a6fd2998d6802b6544b8f7f2',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 3,
                'role' => 30,
                'created_at' => '2021-01-29 11:01:20',
                'updated_at' => '2021-01-29 11:01:20',
                'status' => 1,
            ),
            5 => 
            array (
                'id' => 12,
                'name' => 'Sales Manager',
                'slug' => 'SM',
                'email' => 'salesmanager@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$jMcqprOXZMy65f43pVNu..CSZ7Kl9/fcUy4pCj9XigQEEffqTednK',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => 'b1a7038fc4980f5fe0ffebd9400d0e96',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 3,
                'role' => 31,
                'created_at' => '2021-01-29 11:26:23',
                'updated_at' => '2021-01-29 11:26:23',
                'status' => 1,
            ),
            6 => 
            array (
                'id' => 13,
                'name' => 'Sales Man',
                'slug' => 'SMAN',
                'email' => 'salesman@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$fAQRuxlnSeOf7Jy2hsjFKusrOCJb7jU2vZsaYH56GsR2KWwt7LkR.',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => '599967d78ba20ecb12dd5f1a2c0dc8b4',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 3,
                'role' => 32,
                'created_at' => '2021-01-29 11:27:06',
                'updated_at' => '2021-01-29 11:27:06',
                'status' => 1,
            ),
            7 => 
            array (
                'id' => 14,
                'name' => 'Production Admin',
                'slug' => 'PDA',
                'email' => 'productionadmin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$kQCwhhb5IgopSx1J3Scz3ObizM0iig2MJRJ0gHZ.avNmE2tiGwwvO',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => '21726ddfd322362899630a91fb212428',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 4,
                'role' => 20,
                'created_at' => '2021-01-29 11:27:58',
                'updated_at' => '2021-01-29 11:29:38',
                'status' => 1,
            ),
            8 => 
            array (
                'id' => 15,
                'name' => 'Purchase Manager',
                'slug' => 'PM',
                'email' => 'purchasemanager@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$GJLoJg43JdgFv3KuchIV5u6j/qQYbCxVGa7irf84TRhpArJhjwTh6',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => 'b46816f34b0368138def265da0fc44ae',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 4,
                'role' => 41,
                'created_at' => '2021-01-29 11:29:14',
                'updated_at' => '2021-01-29 11:29:14',
                'status' => 1,
            ),
            9 => 
            array (
                'id' => 16,
                'name' => 'Logistic Manager',
                'slug' => 'LM',
                'email' => 'logisticmanager@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JRP43MB0qGL1QhX.rBoaH.n0es.2GxMEm6oMxb.JCwNapP0RTev.C',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => 'a082e4ed3423d1c9ec5e60bd119f950c',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 4,
                'role' => 42,
                'created_at' => '2021-01-29 11:32:20',
                'updated_at' => '2021-01-29 11:32:20',
                'status' => 1,
            ),
            10 => 
            array (
                'id' => 19,
                'name' => 'Production Manager',
                'slug' => 'PDM',
                'email' => 'productionmanager@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vcdISjRm.yjzcbnITMhMY.bjxTIOMHRpZCGGoTYO.a8E6fpgJ4JCC',
                'two_factor_secret' => NULL,
                'two_factor_recovery_codes' => NULL,
                'remember_token' => '55cb0c4ec771cde00560dfe79b2a5967',
                'current_team_id' => NULL,
                'profile_photo_path' => NULL,
                'module_id' => 4,
                'role' => 43,
                'created_at' => '2021-01-29 11:35:55',
                'updated_at' => '2021-01-29 11:35:55',
                'status' => 1,
            ),
        ));

        
    }
}