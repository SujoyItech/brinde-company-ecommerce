<?php

namespace Database\Seeders;

use App\Models\Product\CombinationType;
use Illuminate\Database\Seeder;

class CombinationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CombinationType::firstOrCreate(['id'=> COLOR_ID],
            ['name'=>['en'=> COLORS_EN, 'pt'=>COLORS_PT]]);

        CombinationType::firstOrCreate(['id'=> SIZE_ID],
            ['name'=>['en'=> SIZE_EN, 'pt'=>SIZE_PT]]);
    }
}
