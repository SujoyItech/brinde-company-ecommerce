<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmCustomerVerificationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_customer_verification_codes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id');
            $table->boolean('type')->default(1);
            $table->string('code')->nullable();
            $table->date('expired_at')->nullable();
            $table->boolean('status')->default(INACTIVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_customer_verification_codes');
    }
}
