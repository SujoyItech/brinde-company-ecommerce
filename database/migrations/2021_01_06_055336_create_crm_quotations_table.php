<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_quotations', function (Blueprint $table) {
            $table->id();
            $table->string('quote_reference',30)->nullable()->comment('BRAND_SLUG - YM - PrimaryKey --> BRAND_SLUG - YM - PrimaryKey');
            $table->string('budget_reference',30)->nullable()->comment('when budget create then it will create -> BRAND_SLUG - SALESMAN_SLUG - YM - PrimaryKey');
            $table->string('order_reference',30)->nullable()->comment('ENC - YM - PrimaryKey');
            $table->string('purchase_reference',30)->nullable()->comment('PRC - YM - PrimaryKey');
            $table->string('production_reference',30)->nullable()->comment('PRD - YM - PrimaryKey');
            $table->bigInteger('customer_id')->nullable()->comment('relation with customer table');
            $table->string('customer_code',30)->nullable()->comment('generated after assigning to a salesman SALESMAN->CUSTOMER_NAME->YM-PRIMARY');
            $table->string('company_name')->nullable()->comment('clients company name');
            $table->string('contact_name')->nullable()->comment('clients name');
            $table->string('email')->nullable()->comment('clients email');
            $table->string('phone')->nullable()->comment('clients phone');
            $table->string('attached_file')->nullable()->comment('clients attach file if needed');
            $table->text('comments')->nullable()->comment('clients requirement or comment for the quotation');
            $table->integer('brand_id')->nullable()->comment('from which site the quotation came');
            $table->integer('assignee_sales_admin')->nullable()->comment('salesman/sales admin from user table');
            $table->integer('assigned_salesman')->nullable()->comment('salesman/sales admin from user table');
            $table->string('mb_reference')->nullable()->comment('payment reference ');
            $table->tinyInteger('need_payment')->default(0)->comment('0 = no | 1 = yes');
            $table->tinyInteger('budget_show_total_enable')->default(0)->comment('0 = no | 1 = yes');
            $table->tinyInteger('budget_masking_enable')->default(0)->comment('0 = no | 1 = yes');
            $table->tinyInteger('budget_printing_enable')->default(0)->comment('0 = no | 1 = yes');
            $table->string('budget_printing_option')->nullable()->comment('printing option ids in string format');
            $table->text('budget_printing_comments')->nullable()->comment('printing details info');
            $table->longText('budget_modification_comments')->nullable()->comment('budget modification details info from customer');
            $table->string('budget_model')->nullable()->comment('Budget model');
            $table->date('expired_at')->nullable()->comment('Quotation expired date');
            $table->tinyInteger('in_staging')->default(1)->comment('In Sales = 1 | In Purchase = 2 | In Arrival = 3 | In production = 4 | In Shipment = 5');
            $table->tinyInteger('payment_status')->default(0)->comment('0 = Due || 1 = Paid || 2 = Reference generated');
            $table->tinyInteger('status')->default(11)->comment('Cancelled = 2 | Expired = 3 | Rejected = 4 | New = 11 | Assigned = 12 | Budget_created = 13 | Budget_Modify_Request = 14 | Budget_Approved = 15 | Ordered = 16	');
            $table->decimal('total_price',13,2)->default(0);
            $table->decimal('total_vat',13,2)->default(0);
            $table->decimal('grand_total',13,2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_quotations');
    }
}
