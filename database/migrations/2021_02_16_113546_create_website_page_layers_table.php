<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsitePageLayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_pages_layers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_pages_id')->nullable();
            $table->string('layer_name')->nullable();
            $table->enum('layer_type',['banner','description','category','product','gallery'])->default('banner');
            $table->string('layer_title')->nullable();
            $table->string('layer_subtitle')->nullable();
            $table->smallInteger('layout_number')->nullable();
            $table->smallInteger('padding_x')->default(0);
            $table->smallInteger('padding_top')->default(0);
            $table->smallInteger('padding_bottom')->default(0);
            $table->integer('order')->default(1);
            $table->boolean('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_pages_layers');
    }
}
