<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategoryProductSlugsToWebsitePagesLayersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('website_pages_layers_details', function (Blueprint $table) {
            $table->text('category_product_slugs')->after('category_id')->nullable()->comment('for layer type : product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('website_pages_layers_details', function (Blueprint $table) {
            $table->dropColumn('category_product_slugs');
        });
    }
}
