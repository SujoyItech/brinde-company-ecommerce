<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IfthenpayRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ifthenpay_records', function (Blueprint $table) {
            $table->integer('crm_quotation_id');
            $table->string('budget_reference');
            $table->string('order_id', 4)->comment('A unique id for Ifthenpay');
            $table->string('entity', 5);
            $table->string('sub_entity', 3);
            $table->string('key_backoffice', 20);
            $table->decimal('amount', 10);
            $table->string('reference', 9);
            $table->string('showable_reference', 11);
            $table->tinyInteger('notice')->default(INACTIVE);
            $table->tinyInteger('callback_status')->default(NOT_CALLED_YET);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ifthenpay_records');
    }
}
