<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrandsTable extends Migration {

	public function up()
	{
		Schema::create('brands', function(Blueprint $table) {
			$table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255)->unique();
            $table->string('brand_url', 255)->nullable();
            $table->string('icon', 255)->nullable();
            $table->tinyInteger('status')->default('1');
            $table->boolean('is_reseller')->default('0');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('brands');
	}
}
