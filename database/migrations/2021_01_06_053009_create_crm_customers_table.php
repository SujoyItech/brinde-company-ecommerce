<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_customers', function (Blueprint $table) {
            $table->id();
            $table->string('customer_code',30)->nullable()->comment('CUSTOMER_NAME->YM-PRIMARY');
            $table->string('name')->nullable()->comment('customer company name');
            $table->string('contact_name')->nullable()->comment('customer name');
            $table->string('email',180)->nullable()->unique()->comment('customer email');
            $table->string('phone')->nullable()->comment('customer phone');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('crm_payment_condition_id')->nullable()->comment('payment condition id');
            $table->string('address')->nullable()->comment('customer address');
            $table->string('postcode')->nullable()->comment('customer postal code');
            $table->string('locality')->nullable()->comment('customer state');
            $table->string('country')->nullable()->comment('customer country');
            $table->string('tin_number',30)->nullable()->comment('customer tin number');
            $table->string('tax_address')->nullable()->comment('customer tax address');
            $table->string('fiscal_postcode')->nullable()->comment('fiscal postal code');
            $table->string('tax_location')->nullable()->comment('tax location');
            $table->string('tax_country')->nullable()->comment('tax country');
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default(1)->comment(' 1 = active , 0 = inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_customers');
    }
}
