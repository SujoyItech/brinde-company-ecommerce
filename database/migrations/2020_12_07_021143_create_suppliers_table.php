<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->bigInteger('created_by');
			$table->string('name', 255);
            $table->decimal('discount', 4,2)->default('0');
			$table->tinyInteger('status')->default('1');
		});
	}

	public function down()
	{
		Schema::drop('suppliers');
	}
}
