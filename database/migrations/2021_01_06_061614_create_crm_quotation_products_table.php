<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmQuotationProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_quotation_products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('quotation_id')->comment('relation with crm quotation table');
            $table->tinyInteger('is_shipped_item')->default(0)->comment('1 = Enrolled product || 0 = no shipping item');
            $table->bigInteger('product_id')->nullable();
            $table->string('product_name',190)->nullable();
            $table->string('product_reference',190)->nullable()->comment('product old reference');
            $table->string('product_mask_reference',190)->nullable()->comment('product new generated reference');
            $table->integer('combination_id')->nullable();
            $table->integer('combination_type_id')->nullable();
            $table->integer('quantity')->default(0);
            $table->decimal('unit_price',13,2)->default(0);
            $table->decimal('vat',13,2)->default(23);
            $table->decimal('total_price',13,2)->default(0);
            $table->decimal('total_vat',13,2)->default(0);
            $table->text('additional_info')->nullable();
            $table->text('comments')->nullable();
            $table->tinyInteger('status')->default(1)->comment('0 = inactive | 1 = Active');
            $table->tinyInteger('purchased')->default(0)->comment('0 = not purchased | 1 = Purchased | 2 = Partially purchase');
            $table->tinyInteger('arrived')->default(0)->comment('0 = not arrived | 1 = Arrived | 2 = Partially arrived');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_quotation_products');
    }
}
