<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsitePageLayerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_pages_layers_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_pages_layers_id');
            $table->string('layer_details_title')->nullable();
            $table->string('description_title')->nullable()->comment('for layer type : description|banner');
            $table->text('description_body')->nullable()->comment('for layer type : description|banner');
            $table->string('category_id')->nullable()->comment('for layer type : category|product');
            $table->string('featured_image')->nullable()->comment('for layer type : category|product|banner');
            $table->string('url_slug')->nullable()->comment('for layer type : category|product|banner');
            $table->enum('url_target_type',['_blank','_self','_parent','_top'])->default('_self');
            $table->boolean('layout_is_image')->default(1)->comment('for layer type : banner -||||- if 1, this is image nor its text');
            $table->integer('order')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_pages_layers_details');
    }
}
