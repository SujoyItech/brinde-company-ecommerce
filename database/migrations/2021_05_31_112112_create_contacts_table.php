<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('contact_type');
            $table->string('title')->nullable();
            $table->text('address')->nullable();
            $table->text('business_days')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('gps')->nullable();
            $table->string('fax')->nullable();
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
