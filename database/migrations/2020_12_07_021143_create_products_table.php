<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->text('description')->nullable();
			$table->string('slug', 255);
			$table->string('reference', 255)->unique();
			$table->decimal('min_price', 10,2)->default(0);
			$table->text('additional_info')->nullable();
			$table->integer('supplier_id')->nullable();
			$table->integer('delivery_type_id')->nullable();
			$table->tinyInteger('status')->default('1');
			$table->string('meta_title', 255)->nullable();
			$table->text('meta_keywords')->nullable();
			$table->text('meta_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}
