<?php

return array (
  'add' => 
  array (
    'added' => 'Added',
    'folder' => 'Add Folder',
    'list' => 'Add To List',
    'main' => 'Add',
    'more' => 'Add More Files',
    'new_folder' => 'Add New Folder',
    'url' => 'Url ...',
  ),
  'ajax_error' => 'Oh NO! Something Went Wrong',
  'application' => 'Application',
  'are_you_sure_delete' => 'Are You Sure You Want To Delete ?!!',
  'audio' => 
  array (
    'album' => 'Album',
    'artist' => 'Artist',
    'genre' => 'Genre',
    'main' => 'Audio',
    'support' => 'Your Browser Does Not Support The Audio Element',
    'title' => 'Title',
    'track' => 'Track Nº',
    'year' => 'Year',
  ),
  'backspace' => 'BackSpace',
  'bookmarks' => 
  array (
    'add' => 'Save Current Path',
    'main' => 'Bookmarks',
  ),
  'cancel' => 'Cancel',
  'clear' => 'Clear :attr',
  'close' => 'Close',
  'copy' => 
  array (
    'copied' => 'Copied',
    'file_folder' => 'Copy File/Folder',
    'files' => 'Copy Files Instead ?',
    'main' => 'Copy',
    'success' => 'Successfully Copied',
    'to_cp' => 'Copy Link To Clipboard',
  ),
  'create_success' => 'Successfully Created',
  'crop' => 
  array (
    'apply' => 'Apply Changes',
    'flip_horizontal' => 'Flip Horizontal',
    'flip_vertical' => 'Flip Vertical',
    'main' => 'Crop',
    'presets' => 'Presets',
    'reset' => 'Reset All',
    'reset_filters' => 'Clear Filters',
    'rotate_left' => 'Rotate Left',
    'rotate_right' => 'Rotate Right',
    'zoom_in' => 'Zoom In',
    'zoom_out' => 'Zoom Out',
  ),
  'delete' => 
  array (
    'confirm' => 'Yes, Delete It !',
    'folder' => 'Deleting A Folder Will Also Remove All Of Its Content',
    'main' => 'Delete',
    'success' => 'Successfully Deleted',
  ),
  'description' => 'Description',
  'destination_folder' => 'Destination Folder',
  'dimension' => 'Dimensions',
  'download' => 
  array (
    'downloaded' => 'Downloaded',
    'file' => 'Download File',
    'folder' => 'Download Folder',
    'sep' => 'Folders Should Be Downloaded Separately',
  ),
  'editor' => 
  array (
    'brightness' => 'Brightness',
    'clarity' => 'Clarity',
    'clip' => 'Clip',
    'concentrate' => 'Concentrate',
    'contrast' => 'Contrast',
    'cross_process' => 'Cross Process',
    'diff' => 'Toggle Diff',
    'exposure' => 'Exposure',
    'gamma' => 'Gamma',
    'glowing_sun' => 'Glowing Sun',
    'greyscale' => 'Greyscale',
    'grungy' => 'Grungy',
    'hazy_days' => 'Hazy Days',
    'hemingway' => 'Hemingway',
    'her_majesty' => 'Her Majesty',
    'hue' => 'Hue',
    'invert' => 'Invert',
    'jarques' => 'Jarques',
    'lomo' => 'Lomo',
    'love' => 'Love',
    'main' => 'Editor',
    'noise' => 'Noise',
    'nostalgia' => 'Nostalgia',
    'old_boot' => 'Old Boot',
    'orange_peel' => 'Orange Peel',
    'pinhole' => 'Pinhole',
    'saturation' => 'Saturation',
    'sepia' => 'Sepia',
    'sharpen' => 'Sharpen',
    'sin_city' => 'Sin City',
    'stack_blur' => 'Stack Blur',
    'sunrise' => 'Sunrise',
    'vibrance' => 'Vibrance',
    'vintage' => 'Vintage',
  ),
  'error' => 
  array (
    'already_exists' => 'A File/Folder Already Exists With That Name',
    'altered_fwli' => 'Can\'t Be Moved, Renamed Or Deleted Because It Has Some Locked Items',
    'cant_upload' => 'Sorry File Can\'t Be Uploaded',
    'creating_dir' => 'Something Gone Wrong While Creating The Directory, Please Check Your Permissions',
    'deleting_file' => 'Something Gone Wrong While Deleting This File/Folder, Please Check Your Permissions',
    'doesnt_exist' => 'Folder ":attr" Doesnt Exist',
    'move_into_self' => 'Folder Can\'t Be "Moved Or Copied" Into It Self',
    'moving' => 'There Seems To Be A Problem Moving This File/Folder, Make Sure You Have The Correct Permissions',
    'moving_cloud' => 'Cloud Folders Can\'t Be "Renamed, Moved Or Copied"',
  ),
  'filter' => 
  array (
    'by' => 'Filter By :attr',
    'filtration' => 'Filters & Sorting',
    'main' => 'Filter',
  ),
  'find' => 'Find...',
  'focals' => 'Focal Points',
  'folder' => 'Folder',
  'found' => 'Found',
  'go_to_folder' => 'Go To Folder !',
  'image' => 'Image',
  'items' => 'Item(s)',
  'last_modified' => 'Last Modified',
  'library' => 'Home',
  'loading' => 'Loading Files',
  'lock' => 'Lock Item',
  'lock_success' => '":attr" Lock Was Updated',
  'locked' => 'Locked',
  'move' => 
  array (
    'clear_list' => 'Movable List Successfully Cleared',
    'file_folder' => 'Move File/Folder',
    'main' => 'Move',
    'success' => 'Successfully Moved',
  ),
  'name' => 'Name',
  'new' => 
  array (
    'create_folder' => 'Create New Folder',
    'create_folder_notif' => 'New Folder Was Created',
    'file_folder' => 'New File/Folder Name',
    'folder_name' => 'New Folder Name',
  ),
  'no_files_in_folder' => 'No Files In This Folder',
  'no_val' => 'Maybe You Should Add Something First !!!',
  'non' => 'Non',
  'not_allowed_file_ext' => 'Files Of Type ":attr" Are Not Allowed',
  'nothing_found' => 'Nothing Found',
  'open' => 'Open',
  'options' => 'Options',
  'paste' => 'Paste Here',
  'pdf' => 'Your Browser Does Not Support Pdfs, Please Download The Pdf To View It',
  'preview' => 'Preview',
  'public_url' => 'Public URL',
  'refresh_notif' => 'Press The Refresh Button To View Them',
  'remove' => 'Remove',
  'rename' => 
  array (
    'file_folder' => 'Rename File/Folder',
    'main' => 'Rename',
    'success' => 'Successfully Renamed',
  ),
  'save' => 
  array (
    'link' => 'Upload Image From Url',
    'main' => 'Save',
    'success' => 'Successfully Saved As',
  ),
  'search' => 
  array (
    'glbl' => 'Global Search',
    'glbl_avail' => 'Global Search Is Now Available.',
    'main' => 'Search',
  ),
  'select' => 
  array (
    'all' => 'Select All',
    'bulk' => 'Bulk Select',
    'non' => 'Select Non',
    'nothing' => 'No File Or Folder Selected',
    'selected' => 'Selected',
  ),
  'size' => 'Size',
  'sort_by' => 'Sort By',
  'space' => 'Space',
  'stand_by' => 'Please Stand By ...',
  'text' => 'Text',
  'title' => 'Media Manager',
  'too_many_files' => 'Items Will Be Deleted !',
  'total' => 'Total',
  'type' => 'Type',
  'unlock' => 'UnLock Item',
  'upload' => 
  array (
    'in_progress' => 'Upload Is Already In Progress',
    'main' => 'Upload',
    'new_uploads_notif' => 'New Items Were Uploaded',
    'success' => 'Successfully Uploaded',
    'text' => 'Drag & Drop Files<br>Or<br>Click To Upload',
    'use_random_names' => 'Use Random Names ?',
  ),
  'video' => 'Video',
  'video_support' => 'Your Browser Does Not Support The Video Tag',
  'visibility' => 
  array (
    'error' => '":attr" Visibility Couldn\'t Be Changed',
    'main' => 'Visibility',
    'set' => 'Change Visibility',
    'success' => '":attr" Visibility Was Updated',
  ),
  'voice' => 
  array (
    'start' => 'Say Something',
    'stop' => 'Turn Microphone Off',
  ),
);
