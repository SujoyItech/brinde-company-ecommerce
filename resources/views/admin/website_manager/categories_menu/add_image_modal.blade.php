<div class="modal-header">
    <h4 class="modal-title">{{$category->name ?? ''}} - {{__('Add Category Image')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form class="category-image-form" novalidate method="post" action="{{route('saveWebsiteCategoryImage')}}" id="category-image-id" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-2">
                    <label for="featured_image">{{__('Category Image')}}</label>
                    <input type="file" name="image" id="image" class="dropify category_add_image" data-max-file-size="80K"
                           data-default-file="{{isset($category_image) ? check_image_exists(get_image_path('category'), $category_image->image ?? '') : ''}}"/>
                    <p>{{__('Maximum allowed image size will be less than 80K')}}</p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group mb-2">
                    <label for="url">{{__('Url ')}}</label>
                    <input type="text" class="form-control" name="url" id="url" placeholder="{{__('Url')}}" value="{{$category_image->url ?? ''}}"/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="category_id" value="{{$category->id}}">
            <input type="hidden" name="brand_id" value="{{$brand_id}}">
            <input type="hidden" name="id" value="{{$category_image->id ?? ''}}">
            @if (isset($category_image->id))
                <button type="button" class="btn btn-danger category-image-delete" data-id="{{$category_image->id}}"><i class="fe-trash"></i>&nbsp;{{__('Remove Image')}}</button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            <button type="submit" class="btn btn-primary category_image_submit"><i class="fe-save"></i>&nbsp;{{__('Save')}}</button>
        </div>
    </form>
</div>

<script>
    $('.category_add_image').dropify();
</script>
