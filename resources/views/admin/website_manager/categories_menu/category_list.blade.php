<div class="ajax-load">
    @if (isset($categories))
        @foreach($categories as $category)
            <div class="list-group-item list-group-item-action h-25">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{check_image_exists(get_image_path('category'),$category['icon'])}}" class="mr-2 avatar-xxl" />
                    </div>
                    <div class="col-md-3">
                        <h5 class="mb-1">{{$category['name']}}</h5><br>
                        <button class="btn btn-sm btn-dark add_category_image" data-category-id="{{$category['id']}}"><i class="fe-plus"></i>&nbsp;{{__('Add Image')}}</button>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            @if (isset($category['category_image']) && !empty($category['category_image']))
                                @foreach($category['category_image'] as $image)
                                    <div class="col-md-3 d-flex">
                                        <img class="img-thumbnail zoom category_image_edit my-auto" src="{{check_image_exists(get_image_path('category'),$image['image'])}}" data-id="{{$image['id']}}"
                                             data-brand-id="{{$image['brand_id']}}" data-category-id="{{$category['id']}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    @endif
</div>

<script>
    $('.category_edit_image').dropify();
</script>
