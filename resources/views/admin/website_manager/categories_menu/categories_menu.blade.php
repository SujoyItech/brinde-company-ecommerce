@extends('admin.layouts.app',['menu'=>'website','sub_menu'=>'website_categories'])
@section('content')
    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="header-title">{{__('Categories Images')}}</h4>
                        <p class="sub-header">{{__('Add one or more images of categories.')}}</p>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('Select Brand')}}</label>
                            <select class="form-control" name="brand_id" id="brand_id">
                                <option value="">{{__('Select Brand')}}</option>
                                @if(isset($brands))
                                    @foreach($brands as $key => $brand)
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="list-group show_list">
                   @include('admin.website_manager.categories_menu.category_list')
                </div>
            </div>
        </div>
    </div>
    <div id="category-image-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content" id="category-image-content">
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(function (){
            getCategoryData();
        })
        $(document).on('change','#brand_id',function (){
            add_loader();
            getCategoryData();
        })
        function getCategoryData(){
            if ($('#brand_id').val().length > 0){
                let brand_data = {
                    brand_id : $('#brand_id').val()
                }
                makeAjaxTextWithData(brand_data,"{{route("websiteCategories")}}",null).done(function (response){
                    $('.show_list').html(response)
                })
            }else {
                $('.show_list').html('')
            }
        }
        $(document).on('click','.add_category_image',function (){
            let category_image_data = {
                category_id : $(this).data('category-id'),
                brand_id: $('#brand_id').val()
            }
            makeAjaxPostText(category_image_data,"{{route('showCategoryImageAddModal')}}",null).done(function (response){
                $('#category-image-content').html(response);
                $('#category-image-modal').modal('show');
            })
        })
        submitOperation(submitResponse, 'category_image_submit');
        function submitResponse(response, this_form){
            if (response.success == true) {
                swalSuccess(response.message);
                $("#category-image-modal").modal('hide');
                getCategoryData();
            } else {
                swalError(response.message);
            }
        }

        $(document).on('click','.category_image_edit',function (){
            let category_image_data = {
                id: $(this).data('id'),
                category_id : $(this).data('category-id'),
                brand_id: $(this).data('brand-id')
            }
            makeAjaxPostText(category_image_data,"{{route('showCategoryImageAddModal')}}",null).done(function (response){
                $('#category-image-content').html(response);
                $('#category-image-modal').modal('show');
            })
        });
        $(document).on('click','.category-image-delete',function (){

            let category_image_id = $(this).data('id');
            swalConfirm("{{__('Do you really want to delete this ?')}}").then(function (s) {
                if(s.value){
                    let category_delete_data = {
                        id : category_image_id
                    };
                    makeAjaxPost(category_delete_data, "{{route('categoryImageDelete')}}", null).done(function (response) {
                        if (response.success == true){
                            swalSuccess(response.message);
                            $("#category-image-modal").modal('hide');
                            getCategoryData();
                        }else {
                            swalError(response.message)
                        }
                    });
                }
            })
        })
    </script>
@endsection
