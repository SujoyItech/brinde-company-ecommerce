<ul class="sortable-list list-unstyled" id="layer-list"></ul>
<script>
    $(document).ready(function (){
        getLayerList();
    })
    function getLayerList(){
        let layer_url = "{{route("websitePageDetails",['id'=>$website_pages_id])}}";
        makeAjaxText(layer_url,null).done(function (response){
            $('#layer-list').html(response);
            tippy('[data-plugin="tippy"]');
        })
    }
    $(document).on('click','.layer-status',function (){
        let id = $(this).data('id');
        let status = $(this).data('status');
        let type = $(this).data('type');
        let layout_number = $(this).data('layout');
        let layout_count = $(this).data('layout-count');
        let status_msg = status == 1 ? "{{__('Inactive')}}" : "{{__('Active')}}" ;
        if (statusActivePermission(status,type,layout_number,layout_count)){
            swalConfirm("{{__('Do you really want to')}} "+status_msg+" {{__('this ?')}}").then(function (s) {
                if(s.value){
                    let data = {
                        id : id,
                        status : status
                    };
                    makeAjaxPost(data, "{{route('websitePageLayerStatusChange')}}", null).done(function (response) {
                        if (response.success == true){
                            swalSuccess(response.message)
                            getLayerList();
                        }else {
                            swalError(response.message)
                        }
                    });
                }
            })
        }else {
            let warning = '';
            if (type == 'product'){
                warning = "{{__('You must add at lest five product to active this.')}}";
            }else {
                warning = "{{__('You must add all content before active this.')}}";
            }
            swalWarning(warning);
        }

    }) ;

    function statusActivePermission(status,type,layout_number,total_layout){
        if (status == 0){
            if (type == 'banner'){
                if (parseInt(total_layout) == parseInt(layout_number/10)){
                    return true;
                }
                return false;
            }else if(type == 'product'){
                if (total_layout >=5){
                    return true;
                }
                return false;
            }else {
                return true;
            }
        }else {
            return true;
        }
    }
    $(document).on('click','.layer-delete',function (){
        var id = $(this).data('id');

        swalConfirm("{{__('Do you really want to delete this ?')}}").then(function (s) {
            if(s.value){
                var data = {
                    id : id
                };
                makeAjaxPost(data, "{{route('websitePageLayerDelete')}}", null).done(function (response) {
                    if (response.success == true){
                        swalSuccess(response.message)
                        $('.modal').modal('hide');
                        getLayerList();
                    }else {
                        swalError(response.message)
                    }
                });
            }
        })
    })


    $(document).on('click','.layer-add-content',function (){
        let layer_type = $(this).data('type');
        let layer_count = $(this).data('count');
        let layout_content_data = {
            type : layer_type,
            website_pages_layers_id : $(this).data('id')
        }
        let layout_content_url = "{{route('showLayerContentModal')}}";
        let modal_id = '#'+$(this).data('type')+'-'+'modal';
        let modal_content = '#'+$(this).data('type')+'-'+'content';
        if (checkLayerContent(layer_type,layer_count)){
            makeAjaxPostText(layout_content_data,layout_content_url,null).done(function (response){
                $(modal_content).html(response)
                $(modal_id).modal('show')
            })
        }else {
            swalWarning("{{__('You already added maximum content!')}}")
        }
    });

    $(document).on('click','.layer-content-edit',function (){
        let layer_type =  $(this).data('layer-type');
        let layout_content_data = {
            id : $(this).data('id'),
            type : layer_type,
            website_pages_layers_id : $(this).data('layer-id'),
            order : $(this).data('order'),
        }
        let layout_content_url = "{{route('showLayerContentModal')}}";
        let modal_id = '#'+layer_type+'-'+'modal';
        let modal_content = '#'+layer_type+'-'+'content';
        makeAjaxPostText(layout_content_data,layout_content_url,null).done(function (response){
            $(modal_content).html(response)
            $(modal_id).modal('show')
        })
    })

    $(document).on('click','.layer-content-delete',function (){
        let layer_content_id = $(this).data('id');
        swalConfirm("{{__('Do you really want to delete this ?')}}").then(function (s) {
            if(s.value){
                var layer_content_data = {
                    id : layer_content_id
                };
                makeAjaxPost(layer_content_data, "{{route('layerContentDelete')}}", null).done(function (response) {
                    if (response.success == true){
                        swalSuccess(response.message)
                        $('.modal').modal('hide');
                        getLayerList();
                    }else {
                        swalError(response.message)
                    }
                });
            }else{
                load.ladda('stop');
            }
        })
    })

    function checkLayerContent(layer_type,layer_count){
        let check = false;
        if (layer_type == 'description' && layer_count < 4){
            check = true;
        }else if (layer_type == 'banner'){
            check = true;
        }else if (layer_type == 'category' && layer_count < 12){
            check = true;
        }else if (layer_type == 'product'){
            check = true;
        }else if (layer_type == 'gallery' && layer_count < 6){
            check = true;
        }
        return check;
    }

</script>
