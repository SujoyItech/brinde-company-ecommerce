@if(isset($layers))
    @foreach($layers as $layer)
        <li class="layer-item" id="{{$layer->id}}">
            <div class="card-box border-radius-0 p-2 @if($layer->status == INACTIVE) bg-soft-danger @endif">
                @if($layer->layer_type !== 'banner')
                    @if($layer->layer_type == 'product' && $layer->layers_content_counts > 0)
                        <button class="btn btn-xs btn-warning float-right layer-content-edit"  data-id="{{$layer->layers_content->id ?? ''}}" data-layer-id="{{$layer->id}}"
                                data-layer-type="{{$layer->layer_type}}" data-order="" ><i class="fe-edit"></i> {{__('Edit Content')}}</button>
                    @else
                        <button class="btn btn-xs btn-dark float-right layer-add-content"  data-type="{{$layer->layer_type}}"  data-id="{{$layer->id}}"
                                data-count="{{$layer->layers_content_counts}}" ><i class="fe-plus"></i> {{__('Add Content')}}</button>
                    @endif
                @endif
                @if ($layer->layer_type == 'description')
                    @if($layer->status == STATUS_ACTIVE)
                        @include('admin.website_manager.layers.layer_status_button.inactive_button')
                    @else
                        @include('admin.website_manager.layers.layer_status_button.active_button')
                    @endif
                    @if ($layer->layers_content_counts == 0)
                        @include('admin.website_manager.layers.layer_status_button.delete_button')
                    @endif
                @else
                    @if ($layer->layers_content_counts > 0 )
                        @if($layer->status == STATUS_ACTIVE)
                            @include('admin.website_manager.layers.layer_status_button.inactive_button')
                        @else
                            @include('admin.website_manager.layers.layer_status_button.active_button')
                        @endif
                    @else
                        @include('admin.website_manager.layers.layer_status_button.delete_button')
                    @endif
                @endif
                <span class="badge badge-soft-secondary float-left mr-2 p-2 font-12 text-uppercase layer_edit cursor-pointer"
                      data-id="{{$layer->id}}" data-style="zoom-in"><i class="fe-edit"></i>&nbsp;{{$layer->layer_type}}</span>
                <h5 class="my-0">{{__($layer->layer_name ?? $layer->layer_title)}}</h5>
                {{$layer->layer_subtitle ?? ''}}
                <div class="clearfix"></div>
                <div class="row">
                    @includeWhen($layer->layer_type == LAYER_BANNER, 'admin.website_manager.layers.layer_items.banner_layer')
                    @includeWhen($layer->layer_type == LAYER_DESCRIPTION, 'admin.website_manager.layers.layer_items.description_layer')
                    @includeWhen($layer->layer_type == LAYER_CATEGORY, 'admin.website_manager.layers.layer_items.category_layer')
                    @includeWhen($layer->layer_type == LAYER_PRODUCT, 'admin.website_manager.layers.layer_items.product_layer')
                    @includeWhen($layer->layer_type == LAYER_GALLERY, 'admin.website_manager.layers.layer_items.gallery_layer')
                </div>
            </div>
        </li>
    @endforeach
@endif
