@if(isset($layer->layers_content))
    @php($lang = app()->getLocale())
    @foreach($layer->layers_content as $category_content)
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 p-2">
            <div class="card-box border border-soft-secondary p-1 m-0">
                <img loading="lazy" class="h-10 zoom" src="{{check_image_exists('admin/images/website_manager/category/', $category_content->featured_image)}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                <a href="javascript:void(0)" class="text-info float-right layer-content-edit" data-id="{{$category_content->id}}" data-layer-id="{{$layer->id}}" data-layer-type="{{$layer->layer_type}}"><i class="fe-edit font-18"></i></a>
                <a href="javascript:void(0)" class="text-danger float-right pr-2 layer-content-delete" data-id="{{$category_content->id}}"><i class="fe-trash font-18"></i></a>
                <h6 class="card-title my-1">{{ isset($category_content->category_name) && isset($category_content->translations['category_name'][app()->getLocale()] ) ? $category_content->translations['category_name'][app()->getLocale()] : ''}}</h6>
                <h6 class="card-title my-1"><strong>{{__('Url slug: ')}}</strong> {{$category_content->url_slug}}</h6>
            </div>
        </div>
    @endforeach
@endif

