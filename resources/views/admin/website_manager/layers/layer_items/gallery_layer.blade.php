@if(isset($layer->layers_content))
    @foreach($layer->layers_content as $gallery_content)
        <div class="col-xs-12 col-sm-12 col-md-{{$layer->segment}} col-lg-{{$layer->segment}} col-xl-{{$layer->segment}} p-2">
            <div class="card-box border border-soft-secondary p-1 m-0">
                <img loading="lazy" class="h-10 zoom" src="{{check_image_exists('admin/images/website_manager/gallery/', $gallery_content->featured_image)}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                <a href="javascript:void(0)" class="text-info float-right layer-content-edit" data-id="{{$gallery_content->id}}" data-layer-id="{{$layer->id}}" data-layer-type="{{$layer->layer_type}}"><i class="fe-edit font-18"></i></a>
                <a href="javascript:void(0)" class="text-danger float-right pr-2 layer-content-delete" data-id="{{$gallery_content->id}}"><i class="fe-trash font-18"></i></a>
                <h6 class="card-title my-1">{{ $gallery_content->description_title ?? ''}}</h6>
                @if (isset($gallery_content->url_slug) && !empty($gallery_content->url_slug))
                    <h6 class="card-title my-1"><strong>{{__('Url slug: ')}}</strong> {{$gallery_content->url_slug}}</h6>
                @endif
            </div>
        </div>
    @endforeach
@endif
