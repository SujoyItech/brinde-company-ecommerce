@if(isset($layer->layers_content))
    @if(isset($layer->layers_content['product_lists']))
        @foreach($layer->layers_content['product_lists'] as $product)
            <div class="col-2 p-2">
                <div class="card-box border border-soft-secondary p-1 m-0">
                    <img loading="lazy" class="h-10 zoom" src="{{check_storage_image_exists($product->media_url)}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                    <h6 class="card-title my-1">{{$product->name}}</h6>
                    <h6 class="card-title my-1"><strong>{{__('Ref: ')}}</strong> {{$product->reference}}</h6>
                </div>
            </div>
        @endforeach
    @endif
@endif
@if (isset($layer->layers_content->featured_image ))
    <div class="col-2 p-2 text-right">
        <div class="card-box border border-soft-secondary p-1 m-0">
            <img loading="lazy" class="h-16 zoom" src="{{check_image_exists('admin/images/website_manager/product/',$layer->layers_content->featured_image)}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
        </div>
    </div>
@endif

