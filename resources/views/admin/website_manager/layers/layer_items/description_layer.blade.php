@if(isset($layer->layers_content))
    @foreach($layer->layers_content as $description_content)
        <div class="col-xs-12 col-sm-12 col-md-{{$layer->segment}} col-lg-{{$layer->segment}} col-xl-{{$layer->segment}} p-2">
            <div class="card-box border border-soft-secondary p-1 m-0">
                <img loading="lazy" class="h-10 zoom" src="{{check_image_exists('admin/images/website_manager/description/', $description_content->featured_image)}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                <a href="javascript:void(0)" class="text-info float-right layer-content-edit" data-id="{{$description_content->id}}" data-layer-id="{{$layer->id}}"
                   data-layer-type="{{$layer->layer_type}}" data-order=""><i class="fe-edit font-18"></i></a>
                <a href="javascript:void(0)" class="text-danger float-right pr-2 layer-content-delete" data-id="{{$description_content->id}}"><i class="fe-trash font-18"></i></a>
                <h6 class="card-title my-1">{{$description_content->description_title}}</h6>
            </div>
        </div>
    @endforeach
@endif
