
<form class="create-layer-form" novalidate method="post" action="{{route('saveWebsitePageLayer')}}" id="create_layer_form_id">
    <div class="modal-header">
        <h4 class="modal-title" id="standard-modalLabel">{{__('Create new layer')}}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label for="layer_name">{{__('Name')}}</label>
            <input class="form-control" type="text" name="layer_name" id="layer_name" value="{{$page_layer->layer_name ?? ''}}" required placeholder="{{__('Name')}}">
        </div>

        @if (!isset($page_layer))
            <div class="form-group">
                <label>{{__('Type')}}</label>
                <select name="layer_type" class="form-control" id="layer_type" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="banner">{{__('Banner')}}</option>
                    <option value="description">{{__('Description')}}</option>
                    <option value="category">{{__('Category')}}</option>
                    <option value="product">{{__('Product')}}</option>
                    <option value="gallery">{{__('Gallery')}}</option>
                </select>
            </div>
            <div class="banner-layout d-none">
                <div class="form-group">
                    <label>{{__('Select Layout')}}</label>
                    <div class="text-center check-layout-image">
                        @foreach(getLayouts() AS $layout_name => $layouts)
                            <div>
                                <input type="radio" name="layout_number" value="{{$layout_name}}" id="l-{{$layout_name}}" {{ $layout_name == 10 ? 'checked' : ''}}>
                                <label for="l-{{$layout_name}}"><span class="icon"></span><img src="{{adminAsset('images').'/'.$layouts}}" class="h-10 m-1 zoom" title="Layout-{{$layout_name}}" data-plugin="tippy" data-tippy-followCursor="true" data-tippy-arrow="true" data-placement="right" data-tippy-animation="fade"></label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif


        <div class="form-group">
            <label for="layer_title">{{__('Title')}}</label>
            <input class="form-control" type="text"  id="layer_title" name="layer_title" placeholder="{{__('Title')}}" value="{{$page_layer->layer_title ?? ''}}">
        </div>
        <div class="form-group">
            <label for="layer_subtitle">{{__('Sub title')}}</label>
            <input class="form-control" type="text"  id="layer_subtitle" name="layer_subtitle" placeholder="{{__('Sub title')}}" value="{{$page_layer->layer_subtitle ?? ''}}">
        </div>
        <div class="form-group">
            <label for="padding_x">{{__('Padding X')}}</label>
            <input class="form-control" type="text"  id="padding_x" name="padding_x" placeholder="{{__('Padding x')}}" value="{{$page_layer->padding_x ?? 0}}">
        </div>
        <div class="form-group">
            <label for="padding_top">{{__('Padding Top')}}</label>
            <input class="form-control" type="text"  id="padding_top" name="padding_top" placeholder="{{__('Padding top')}}" value="{{$page_layer->padding_top ?? 0}}">
        </div>
        <div class="form-group">
            <label for="padding_bottom">{{__('Padding Bottom')}}</label>
            <input class="form-control" type="text"  id="padding_bottom" name="padding_bottom" placeholder="{{__('Padding bottom')}}" value="{{$page_layer->padding_bottom ?? 0}}">
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id" value="{{$page_layer->id ?? ''}}">
        <input type="hidden" name="website_pages_id" value="{{$website_pages_id}}">
        <button type="button" class="btn btn-light" data-dismiss="modal">{{__('Close')}}</button>
        <button type="submit" class="btn btn-primary submit_basic" data-style="zoom-in">{{__('Save')}}</button>
    </div>
</form>
<script>
    $(document).ready(function (){
        resetValidation('create-layer-form');
    })
    $(document).on('change','#layer_type',function (){
       if ($(this).val() == 'banner'){
           $('.banner-layout').removeClass('d-none');
       }else {
           $('.banner-layout').addClass('d-none');
       }
    })
</script>
