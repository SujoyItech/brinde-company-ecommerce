<button class="btn btn-xs btn-outline-danger float-right mr-1 layer-delete"
        data-id="{{$layer->id}}"
        data-plugin="tippy"
        data-tippy-followCursor="true"
        data-tippy-arrow="true"
        data-tippy-placement=""
        data-tippy-animation="fade"
        data-tippy-theme="gradient"
        title="{{__('Delete')}}"><i class="fe-trash"></i></button>
