<div class="col-6 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-12 bg-secondary h-16', 'banner_layout_obj'=> isset($b_32_1) ? $b_32_1 : null])
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-12 bg-success h-16', 'banner_layout_obj'=> isset($b_32_2) ? $b_32_2 : null])
</div>
@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-6 bg-secondary h-32', 'banner_layout_obj'=> isset($b_32_3) ? $b_32_3 : null])
