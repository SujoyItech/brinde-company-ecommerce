@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-3 bg-secondary h-16', 'banner_layout_obj'=> isset($b_40_1) ? $b_40_1 : null])
@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-3 bg-success h-16', 'banner_layout_obj'=> isset($b_40_2) ? $b_40_2 : null])
@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-3 bg-danger h-16', 'banner_layout_obj'=> isset($b_40_3) ? $b_40_3 : null])
@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>4,'div_class'=>'col-3 bg-info h-16', 'banner_layout_obj'=> isset($b_40_4) ? $b_40_4 : null])

