<div class="col-4 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-12 bg-secondary h-16', 'banner_layout_obj'=> isset($b_51_1) ? $b_51_1 : null])
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-12 bg-success h-16', 'banner_layout_obj'=> isset($b_51_2) ? $b_51_2 : null])
</div>
<div class="col-4 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-12 bg-danger h-32', 'banner_layout_obj'=> isset($b_51_3) ? $b_51_3 : null])
</div>
<div class="col-4 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>4,'div_class'=>'col-12 bg-info h-16', 'banner_layout_obj'=> isset($b_51_4) ? $b_51_4 : null])
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>5,'div_class'=>'col-12 bg-warning h-16', 'banner_layout_obj'=> isset($b_51_5) ? $b_51_5 : null])
</div>

