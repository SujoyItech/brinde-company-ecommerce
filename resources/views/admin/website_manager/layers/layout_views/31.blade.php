@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-6 bg-secondary h-32', 'banner_layout_obj'=> isset($b_31_1) ? $b_31_1 : null])
<div class="col-6 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-12 bg-success h-16', 'banner_layout_obj'=> isset($b_31_2) ? $b_31_2 : null])
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-12 bg-danger h-16', 'banner_layout_obj'=> isset($b_31_3) ? $b_31_3 : null])
</div>

