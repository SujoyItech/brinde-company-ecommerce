<div class="col-6 p-0">
    <div class="col-12">
        <div class="row">
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1, 'div_class'=>'col-6 bg-secondary h-16', 'banner_layout_obj'=> isset($b_63_1) ? $b_63_1 : null])
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2, 'div_class'=>'col-6 bg-success h-16', 'banner_layout_obj'=> isset($b_63_2) ? $b_63_2 : null])
        </div>
    </div>
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3, 'div_class'=>'col-12 bg-danger h-16', 'banner_layout_obj'=> isset($b_63_3) ? $b_63_3 : null])
</div>
<div class="col-6 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>4, 'div_class'=>'col-12 bg-info h-16', 'banner_layout_obj'=> isset($b_63_4) ? $b_63_4 : null])
    <div class="col-12">
        <div class="row">
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>5, 'div_class'=>'col-6 bg-warning h-16', 'banner_layout_obj'=> isset($b_63_5) ? $b_63_5 : null])
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>6, 'div_class'=>'col-6 bg-teal h-16', 'banner_layout_obj'=> isset($b_63_6) ? $b_63_6 : null])
        </div>
    </div>
</div>




