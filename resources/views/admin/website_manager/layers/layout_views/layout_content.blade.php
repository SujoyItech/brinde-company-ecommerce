<div class="{{$div_class}} layer-content-edit"
     data-id="{{!empty($banner_layout_obj) ? $banner_layout_obj->id : ''}}"
     data-layer-id="{{$layer->id}}"
     data-layer-type="{{$layer->layer_type}}"
     data-order="{{$order}}">
    <span class="text-light align-middle mr-2 font-26">{{$order}} {!! !empty($banner_layout_obj) ? '<i class="fe-check-circle"></i>' : '<i class="fe-edit"></i>' !!}</span>
    <img loading="lazy"
         src="{{check_image_exists('admin/images/website_manager/banner/',!empty($banner_layout_obj) ? $banner_layout_obj->featured_image : '')}}"
         class="h-100" title="">
</div>
