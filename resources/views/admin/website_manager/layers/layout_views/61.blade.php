<div class="col-6 p-0">
    <div class="col-12">
        <div class="row">
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-6 bg-secondary h-16', 'banner_layout_obj'=> isset($b_61_1) ? $b_61_1 : null])
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-6 bg-success h-16', 'banner_layout_obj'=> isset($b_61_2) ? $b_61_2 : null])
        </div>
    </div>
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-12 bg-danger h-16', 'banner_layout_obj'=> isset($b_61_3) ? $b_61_3 : null])
</div>

<div class="col-6 p-0">
    <div class="col-12">
        <div class="row">
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>4,'div_class'=>'col-6 bg-info h-16', 'banner_layout_obj'=> isset($b_61_4) ? $b_61_4 : null])
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>5,'div_class'=>'col-6 bg-warning h-16', 'banner_layout_obj'=> isset($b_61_4) ? $b_61_4 : null])
        </div>
    </div>
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>6,'div_class'=>'col-12 bg-teal h-16', 'banner_layout_obj'=> isset($b_61_4) ? $b_61_4 : null])
</div>







