<div class="col-6 p-0">
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-12 bg-secondary h-32', 'banner_layout_obj'=> isset($b_43_1) ? $b_43_1 : null])
</div>
<div class="col-6 p-0">
    <div class="col-12">
        <div class="row">
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-6 bg-success h-16', 'banner_layout_obj'=> isset($b_43_2) ? $b_43_2 : null])
            @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-6 bg-danger h-16', 'banner_layout_obj'=> isset($b_43_3) ? $b_43_3 : null])
        </div>
    </div>
    @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>4,'div_class'=>'col-12 bg-info h-16', 'banner_layout_obj'=> isset($b_43_4) ? $b_43_4 : null])
</div>




