<div class="col-12">
    <div class="row">
        @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-6 bg-secondary h-16', 'banner_layout_obj'=> isset($b_34_1) ? $b_34_1 : null])
        @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-6 bg-success h-16', 'banner_layout_obj'=> isset($b_34_2) ? $b_34_2 : null])
    </div>
</div>
@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-12 bg-danger h-16', 'banner_layout_obj'=> isset($b_34_3) ? $b_34_3 : null])

