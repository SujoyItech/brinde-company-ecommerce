@include('admin.website_manager.layers.layout_views.layout_content', ['order'=>1,'div_class'=>'col-12 bg-secondary h-16', 'banner_layout_obj'=> isset($b_32_1) ? $b_32_1 : null])
<div class="col-12">
    <div class="row">
        @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>2,'div_class'=>'col-6 bg-success h-16', 'banner_layout_obj'=> isset($b_33_2) ? $b_33_2 : null])
        @include('admin.website_manager.layers.layout_views.layout_content', ['order'=>3,'div_class'=>'col-6 bg-danger h-16', 'banner_layout_obj'=> isset($b_33_3) ? $b_33_3 : null])
    </div>
</div>
