<div class="modal-header">
    <h4 class="modal-title">{{$layer->layer_title ?? ''}} - {{__('Add Category')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form class="category-form" novalidate method="post" action="{{route('saveLayerContent')}}" id="category_form_id">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-2">
                    <label for="featured_image">{{__('Category Image')}}</label>
                    <input type="file" name="featured_image" id="featured_image" class="dropify featured_image" data-default-file="{{isset($layer_content->featured_image) ? check_image_exists('admin/images/website_manager/category/', $layer_content->featured_image) : ''}}"/>
                </div>
                <div class="form-group mb-2">
                    <label>{{__('Category List')}}</label>
                    <select class="form-control selectpicker" required name="category_id" id="category_id">
                        @php
                            $tree_number = 0;
                        @endphp
                        @if(isset($categories) && !empty($categories))
                            @foreach($categories as $category)
                                @if(isset($category->children) && !empty($category->children[0]))
                                    <option {{is_selected($category->id,isset($layer_content->category_id) ? intval($layer_content->category_id): '')}} value="{{$category->id}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$category->name}}</option>
                                    @include('admin.website_manager.layers.layer_forms.child_category_tree',['sub_categories'=>$category->children, 'tree_number' => $tree_number+1])
                                @else
                                    <option {{is_selected($category->id,isset($layer_content->category_id) ? intval($layer_content->category_id): '')}} value="{{$category->id}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$category->name}}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
{{--                    <select class="form-control" name="category_id" id="category_id" required>--}}
{{--                        <option value="">{{__('Select')}}</option>--}}
{{--                        @if(isset($categories))--}}
{{--                            @foreach($categories as $category)--}}
{{--                                <option value="{{$category->id}}" {{is_selected($category->id,isset($layer_content->category_id) ? intval($layer_content->category_id): '')}}>{{$category->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    </select>--}}
                    <div class="valid-feedback">
                        {{__('Looks good!')}}
                    </div>
                </div>
                <div class="form-group mb-2">
                    <label for="url_slug">{{__('Url slug')}}</label>
                    <input type="text" class="form-control" name="url_slug" id="url_slug"  value="{{$layer_content->url_slug ?? ''}}" required readonly>
                    <div class="valid-feedback">
                        {{__('Looks good!')}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="website_pages_layers_id" value="{{$website_pages_layers_id}}">
            <input type="hidden" name="id" value="{{$layer_content->id ?? ''}}">
            <input type="hidden" name="type" value="{{$type}}">
            @if (isset($layer_content->id))
                <button type="button" class="btn btn-danger layer-content-delete" data-id="{{$layer_content->id}}"><i class="fe-trash"></i>&nbsp;{{__('Remove Data')}}</button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            <button type="submit" class="btn btn-primary layer_content_submit"><i class="fe-save"></i>&nbsp;{{__('Save')}}</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function (){
        resetValidation('category-form');
        $(function () {
            $('.selectpicker').selectpicker();
        });

    })
    $('.featured_image').dropify();
    $(document).on('change','#category_id',function (){
        let category_data = {
            id : $(this).val()
        }
        makeAjaxPost(category_data,"{{route('getCategorySlugById')}}",null).done(function (response){
            if (response.success == true){
                $('#url_slug').val(response.data.slug)
            }else {
                swalError(response.message);
            }
        })
    });

</script>
