<div class="modal-header">
    <h4 class="modal-title">{{$layer->layer_title ?? ''}} - {{__('Add Banner')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form class="banner-form" novalidate method="post" action="{{route('saveLayerContent')}}" id="banner_form_id">
        @include('admin.website_manager.layers.layer_forms.banner_details',['index'=>$order])
        <input type="hidden" name="website_pages_layers_id" value="{{$website_pages_layers_id}}">
        <input type="hidden" name="order" value="{{$order}}">
        <input type="hidden" name="id" value="{{$layer_content->id ?? ''}}">
        <input type="hidden" name="type" value="{{$type}}">
        <div class="modal-footer">
            @if (isset($layer_content->id))
                <button type="button" class="btn btn-danger layer-content-delete" data-id="{{$layer_content->id}}"><i class="fe-trash"></i>&nbsp;{{__('Remove Data')}}</button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            <button type="submit" class="btn btn-primary banner_layer_content_submit"><i class="fe-save"></i>&nbsp;{{__('Save')}}</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function (){
        resetValidation('banner-form');
        $('.layout_image').dropify();
        $('[data-plugin="switchery"]').each(function (e, a) {
            new Switchery($(this)[0], $(this).data())
        })
    })
</script>
