@foreach($sub_categories as $sub_category)
    @if(isset($sub_category->children) && !empty($sub_category->children[0]))
        <option {{is_selected($category->id,isset($layer_content->category_id) ? intval($layer_content->category_id): '')}} value="{{$sub_category->id}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$sub_category->name}}</option>
        @include('admin.website_manager.layers.layer_forms.child_category_tree',['sub_categories'=>$sub_category->children,'tree_number' => $tree_number+1])
    @else
        <option {{is_selected($category->id,isset($layer_content->category_id) ? intval($layer_content->category_id): '')}} value="{{$sub_category->id}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$sub_category->name}}</option>
    @endif
@endforeach
