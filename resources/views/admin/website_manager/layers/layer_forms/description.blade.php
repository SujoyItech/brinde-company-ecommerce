<div class="modal-header">
    <h4 class="modal-title">{{$layer->layer_title ?? ''}} - {{__('Add Description')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form class="description-form" novalidate method="post" action="{{route('saveLayerContent')}}" id="description_form_id">
        <div class="form-group mb-2">
            <label for="name">{{__('Description Title')}}</label>
            <input type="text" class="form-control" name="description_title" id="description_title" placeholder="{{__('Description Title')}}" value="{{$layer_content->description_title ?? ''}}" required>
            <div class="valid-feedback">
                {{__('Looks good!')}}
            </div>
        </div>
        <div class="col-12 p-0">
            <div class="row">
                <div class="col-4">
                    <div class="form-group mb-2">
                        <label for="featured_image">{{__('Description Image')}}</label>
                        <input type="file" name="featured_image" id="featured_image" class="dropify featured_image" data-default-file="{{isset($layer_content->featured_image) ? check_image_exists('admin/images/website_manager/description/', $layer_content->featured_image) : ''}}"/>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group mb-2">
                        <label for="description_body">{{__('Description')}}</label>
                        <textarea class="description_body" name="description_body" id="description_body">{{$layer_content->description_body ?? ''}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="website_pages_layers_id" value="{{$website_pages_layers_id}}">
            <input type="hidden" name="id" value="{{$layer_content->id ?? ''}}">
            <input type="hidden" name="type" value="{{$type}}">
            @if (isset($layer_content->id))
                <button type="button" class="btn btn-danger layer-content-delete" data-id="{{$layer_content->id}}"><i class="fe-trash"></i>&nbsp;{{__('Remove Data')}}</button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            <button type="submit" class="btn btn-primary layer_content_submit"><i class="fe-save"></i>&nbsp;{{__('Save')}}</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function (){
        resetValidation('description-form');
    });

    $(".description_body").summernote({
        placeholder: "Write something...",
        height: 150,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('.featured_image').dropify();
</script>
