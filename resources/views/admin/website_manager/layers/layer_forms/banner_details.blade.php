<div class="col-12 py-1 border-bottom">
    <div class="row">
        <div class="col-1 m-auto">
            <h2>{{$index}}</h2>
        </div>
        <div class="col-4">
            <div class="form-group mb-2">
                <label>{{__('Banner Image')}}</label>
                <input type="file" name="featured_image" class="featured_image"
                       data-default-file="{{isset($layer_content->featured_image) ? check_image_exists('admin/images/website_manager/banner/', $layer_content->featured_image) : ''}}"/>
            </div>
        </div>
        <div class="col-7">
            <div class="form-group mb-2">
                <label>{{__('Banner Link')}}</label>
                <input type="text" class="form-control" name="url_slug" placeholder="{{__('Banner link')}}" value="{{$layer_content->url_slug ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label>{{__('Item Text')}}</label>
                <input type="text" class="form-control" name="description_title" placeholder="{{__('Item Text')}}" value="{{$layer_content->description_title ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <div class="switchery-demo">
                        <b>{{__('Text show? ')}}&nbsp;</b>
                        <input type="checkbox" name="layout_is_image" {{isset($layer_content->layout_is_image) && $layer_content->layout_is_image == TRUE ? 'checked': ''}} data-plugin="switchery" data-color="#1bb99a" data-secondary-color="#ff5d48" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.featured_image').dropify();
</script>
