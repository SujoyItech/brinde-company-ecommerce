<link href="{{adminAsset('libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css"/>
<div class="modal-header">
    <h4 class="modal-title">{{$layer->layer_title ?? ''}} - {{__('Add Product')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form class="product-form" novalidate method="post" action="{{route('saveLayerContent')}}" id="product_form_id">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mb-2">
                    <label for="featured_image">{{__('Featured Image')}}</label>
                    <input type="file" name="featured_image" id="featured_image" class="dropify featured_image"
                           data-default-file="{{isset($layer_content->featured_image) ? check_image_exists('admin/images/website_manager/product/', $layer_content->featured_image) : ''}}"/>
                </div>
                <div class="form-group mb-2">
                    <label>{{__('Category List')}}</label>
                    <select class="form-control" name="category_id" id="category_id" required>
                        <option value="">{{__('Select')}}</option>
                        @if(isset($categories))
                            @foreach($categories as $category)
                                <option
                                    value="{{$category->id}}" {{is_selected($category->id,isset($layer_content->category_id) ? intval($layer_content->category_id): '')}}>{{$category->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    <div class="valid-feedback">
                        {{__('Looks good!')}}
                    </div>
                </div>
                <div class="form-group product-show">
                    <label>{{__('Select Product')}}</label>
                    <select id="selectproduct" name="category_product_slugs[]" multiple required>
                        <option value="">{{__('Select')}}</option>
                        @if(isset($layer_content['products_by_category']) && !empty($layer_content['products_by_category']))
                            @foreach($layer_content['products_by_category'] as $product)
                                <option value="{{$product->slug}}" selected>{{$product->reference}}</option>
                            @endforeach
                        @endif
                    </select>
                    <p>{{__('You must include at least five product to active this layer.')}}</p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="website_pages_layers_id" value="{{$website_pages_layers_id}}">
            <input type="hidden" name="id" value="{{$layer_content->id ?? ''}}">
            <input type="hidden" name="type" value="{{$type}}">
            @if (isset($layer_content->id))
                <button type="button" class="btn btn-danger layer-content-delete" data-id="{{$layer_content->id}}"><i class="fe-trash"></i>&nbsp;{{__('Remove Data')}}</button>
            @endif
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            <button type="submit" class="btn btn-primary layer_content_submit"><i class="fe-save"></i>&nbsp;{{__('Save')}}</button>
        </div>
    </form>
</div>
<script src="{{adminAsset('libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script>

    $(document).ready(function (){
        resetValidation('product-form');
    });

    callSelectizeFunction();
    showHideProduct();
    $(document).on('change', '#category_id', function () {
        callSelectizeFunction();
        showHideProduct();
    });

    $('.featured_image').dropify();

    function callSelectizeFunction() {
        let category_id = $('#category_id').val();
        let url = "{{url('admin/render-product-by-category')}}" + '/' + category_id
        let obj = {
            targetDom: '#selectproduct',
            url: url,
            valueField: 'slug',
            labelField: 'reference',
            searchField: ['reference','name'],
            create: false
        }
        selectizeFunction(function (item,escape) {
            let STORAGE_URL = '{{Storage::url('/')}}';
            let DEFAULT_IMAGE_URL = "{{adminAsset('images/no-image.png')}}";
            let html = '';
            html += '<div class="media bg-light p-2 border-bottom">';
            html += '<img src="' + STORAGE_URL + escape(item.media_url) + '" class="avatar-sm mr-2 rounded-circle" onerror=\'this.src="'+DEFAULT_IMAGE_URL+'"\'>';
            html += '<div class="media-body">';
            html += '<h5 class="mt-0 mb-0 font-14">' + escape(item.reference) + '</h5>';
            html += '<p class="mt-1 mb-0 text-muted font-14"><span class="w-75">' + escape(item.name) + '</span></p>';
            html += '</div>';
            html += '</div>';
            return html;
        }, obj);
    }


    function showHideProduct() {
        let category_id = $('#category_id').val();
        if (category_id.length != 0) {
            $('.product-show').removeClass('d-none');
        } else {
            $('.product-show').addClass('d-none');
        }
    }


</script>
