<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Website Page  Add/Edit')}}</h4>
        <form class="website-pages-form" novalidate method="post" action="{{route('saveWebsitePage')}}" id="website_page_form">
            <div class="form-group mb-2">
                <label>{{__('Brand')}}</label>
                <select class="form-control" name="brand_id" required>
                    <option value="">{{__('Select')}}</option>
                    @if(isset($brands))
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}" {{is_selected($brand->id,$page->brand_id ?? '')}}>{{$brand->name}}</option>
                        @endforeach
                    @endif
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label>{{__('Type')}}</label>
                <select class="form-control" name="page_type" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="home" {{is_selected('home',$page->page_type ?? '')}}>{{__('Home')}}</option>
                    <option value="special" {{is_selected('special',$page->page_type ?? '')}}>{{__('Special')}}</option>
                    <option value="static" {{is_selected('static',$page->page_type ?? '')}}>{{__('Static')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="page_name">{{__('Name')}}</label>
                <input type="text" class="form-control" name="page_name" id="page_name" placeholder="{{__('Name')}}" value="{{$page->page_name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="title">{{__('Title')}}</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="{{__('Title')}}" value="{{$page->title ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="subtitle">{{__('Sub title')}}</label>
                <input type="text" class="form-control" name="subtitle" id="subtitle" placeholder="{{__('Sub title')}}" value="{{$page->subtitle ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="url">{{__('Url slug')}}</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="{{__('Url slug')}}" value="{{$page->url ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="description">{{__('Description')}}</label>
                <textarea class="form-control" name="description" id="description" rows="2" placeholder="{{__('Description')}}">{{$page->description ?? ''}}</textarea>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$page->id ?? ''}}" id="id">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(false,function (){ })"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('website-pages-form');
    });
</script>
