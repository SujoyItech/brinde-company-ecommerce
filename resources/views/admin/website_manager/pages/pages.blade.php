@extends('admin.layouts.app',['menu'=>'website','sub_menu'=>'pages'])
@section('style')
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 add_edit">
        @include('admin.website_manager.pages.pages_add')
    </div>
    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-12 col-xs-12 list">
        @include('admin.website_manager.pages.pages_list')
    </div>
@endsection
@section('script')
    <script src="{{adminAsset('libs/tippy.js/tippy.all.min.js')}}"></script>
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            var data_url = "{{route('websitePages')}}";
            var delete_url = "{{route('deleteWebsitePage')}}";
            let order_route = "{{route('order')}}"+"?table=website_pages"+"&column=order";
            var data_column =  [
                {"data": "order", visible: false},
                {"data": "move"},
                {"data": "page_type"},
                {"data": "page_name"},
                {"data": "url"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderOrderAbleDataTable($('#page_table'),data_url,data_column,order_route);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,"{{route('editWebsitePage')}}",false);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    renderBrandWiseData();
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderBrandWiseData();
                }
            }
            $(document).on('change','.status_change',function (){
                let status_data = {
                    id : $(this).data('id'),
                    brand_id : $(this).data('brand-id'),
                    status:$(this).data('status')
                }
                let status_change_url = "{{route('websitePageStatusChange')}}";
                swalConfirm("{{__('Do you really want to change this status ?')}}").then(function (s) {
                    if(s.value){
                        makeAjaxPost(status_data,status_change_url).done(function (response){
                            if (response.success == true){
                                swalSuccess(response.message);
                                renderBrandWiseData();
                            }else {
                                swalError(response.message);
                            }
                        });
                    }else {
                        let brand_id = $('#brand_id').val();
                        renderBrandWiseData();
                    }
                });
            });

            $(document).on('change','#brand_id',function (){
                let brand_id = $(this).val();
                if (brand_id.length > 0){
                    let website_data_url = "{{route('websitePages')}}"+'/'+brand_id;
                    renderOrderAbleDataTable($('#page_table'),website_data_url,data_column,order_route);
                }else {
                    renderOrderAbleDataTable($('#page_table'),data_url,data_column,order_route);
                }
            });

            function renderBrandWiseData(){
                let brand_id = $('#brand_id').val();
                let web_data_url = brand_id.length > 0 ? data_url+'/'+brand_id : data_url;
                renderOrderAbleDataTable($('#page_table'),web_data_url,data_column,order_route);
            }
        });

    </script>
@endsection

