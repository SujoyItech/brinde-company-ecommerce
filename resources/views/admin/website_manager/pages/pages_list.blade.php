<div class="card-box">
    <div class="row">
        <div class="col-md-8">
            <h4 class="header-title">{{__('Page List')}}</h4>
            <p class="sub-header">
                {{__('Here goes the page list')}}
            </p>
        </div>
        <div class="col-md-4 text-right">
            <select class="form-control" name="brand_id" id="brand_id">
                <option value="">{{__('All')}}</option>
                @if(isset($brands))
                    @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="table-responsive">
        <table id="page_table" class="table table-sm table-striped nowrap">
            <thead>
            <tr>
                <th></th>
                <th class="text-center">{{__('Move')}}</th>
                <th>{{__('Type')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Url slug')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody class="sortable"></tbody>
        </table>
    </div>
</div>
<script>


</script>
