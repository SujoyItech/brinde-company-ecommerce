@extends('admin.layouts.app',['menu'=>'website','sub_menu'=>'website_pages'])
@section('style')
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{adminAsset('libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0">
        @include('admin.website_manager.pages.page_details_show')
    </div>
    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12 p-0">
        @include('admin.website_manager.layers.layer_lists')
    </div>
    @include('admin.website_manager.layers.layer_forms.modal_container')
@endsection
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="{{adminAsset('libs/selectize/js/standalone/selectize.min.js')}}"></script>
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            let newSortArr = {};
            let allSortArr = [];
            $(".list-unstyled").sortable({
                connectWith: "#layer-list",
                update: function (event, ui) {
                    if (!ui.sender) {
                        newSortArr  = $("#layer-list").sortable("toArray");
                        let data = {
                            website_pages_id : "{{$website_pages_id}}",
                            data: JSON.stringify(newSortArr)
                        };
                        makeAjaxPostText(data,"{{route('websitePageLayerOrderUpdate')}}").done(function (){

                        });
                    }
                }
            }).disableSelection();
        });

        $(document).on('click','#create_layer',function (){
            Ladda.bind(this);
            var load = $(this).ladda();
            let layout_data = {
                website_pages_id: {{$website_pages_id}}
            }
            let layout_data_url = "{{route('editWebsitePageLayer')}}";

            makeAjaxPostText(layout_data,layout_data_url,load).done(function (response){
                $('#create_layer_modal_content').html(response)
                $('#create-layer-modal').modal('show')
            })
        })

        $(document).on('click','.layer_edit',function (){
            Ladda.bind(this);
            var load = $(this).ladda();
            let layout_data = {
                id : $(this).data('id'),
                website_pages_id: {{$website_pages_id}}
            }
            let layout_data_url = "{{route('editWebsitePageLayer')}}";

            makeAjaxPostText(layout_data,layout_data_url,load).done(function (response){
                $('#create_layer_modal_content').html(response)
                $('#create-layer-modal').modal('show')
            })
        });

        submitOperation(submitResponse, 'submit_basic');

        submitOperation(submitResponse, 'layer_content_submit');
        submitOperation(submitResponse, 'banner_layer_content_submit');
        function submitResponse(response, this_form){
           if (response.success == true) {
               swalSuccess(response.message)
               $('.modal').modal('hide');
               getLayerList();
           } else {
               swalError(response.message);
           }
        }
    </script>

@endsection
