<div class="card">
    <div class="card-header bg-soft-secondary text-dark p-1">
        <div class="card-widgets">
            <button class="btn btn-sm btn-dark" id="create_layer" data-style="zoom-in"><i class="fa fa-plus-circle"></i>&nbsp;{{__('Add Layer')}}</button>
        </div>
        <h5 class="card-title my-1 pl-2 text-dark"><img src="{{asset(get_image_path('brand').'/'.$page->brand_icon)}}" class="avatar-xs rounded-circle mr-1"> {{$page->page_name ?? 'Page Details'}}</h5>
    </div>
    <div class="card-box p-2 mb-0">
        <div class="row">
            <div class="col-md-6">
                <h6>{{__('Url slug: ')}} {{$page->url ?? ''}} </h6>
                <p class="m-0">{{$page->description ?? ''}}</p>
            </div>
            <div class="col-md-6">
                <h6>{{__('Title: ')}} {{$page->title ?? ''}} </h6>
                <h6>{{__('Subtitle: ')}} {{$page->subtitle ?? ''}} </h6>
            </div>
        </div>
    </div>
</div>
