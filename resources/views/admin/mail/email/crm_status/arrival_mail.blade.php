<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
    xmlns="http://www.w3.org/1999/xhtml"
    style="
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    margin: 0;
  "
>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ $body['brand_name'] ?? '' }} - {{__('full arrival status')}}</title>
</head>

<body
    itemscope
    itemtype="http://schema.org/EmailMessage"
    style="
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      box-sizing: border-box;
      font-size: 14px;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: none;
      width: 100% !important;
      height: 100%;
      line-height: 1.6em;
      background-color: #f6f6f6;
      margin: 0;
    "
    bgcolor="#f6f6f6"
>
<table
    class="body-wrap"
    style="
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
        width: 100%;
        background-color: #f6f6f6;
        margin: 0;
      "
    bgcolor="#f6f6f6"
>
    <tr
        style="
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          box-sizing: border-box;
          font-size: 14px;
          margin: 0;
        "
    >
        <td
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            margin: 0;
          "
            valign="top"
        ></td>
        <td
            class="container"
            width="600"
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            display: block !important;
            max-width: 600px !important;
            clear: both !important;
            margin: 0 auto;
          "
            valign="top"
        >
            <div
                class="content"
                style="
              font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
              box-sizing: border-box;
              font-size: 14px;
              max-width: 600px;
              display: block;
              margin: 0 auto;
              padding: 20px;
            "
            >
                <table
                    class="main"
                    width="100%"
                    cellpadding="0"
                    cellspacing="0"
                    style="
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                box-sizing: border-box;
                font-size: 14px;
                border-radius: 3px;
                margin: 0;
                border: none;
              "
                >
                    <tr
                        style="
                  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                  box-sizing: border-box;
                  font-size: 14px;
                  margin: 0;
                "
                    >
                        <td
                            class="content-wrap aligncenter"
                            style="
                    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                    box-sizing: border-box;
                    font-size: 14px;
                    vertical-align: top;
                    margin: 0;
                    padding-left: 30px;
                    padding-right: 30px;
                    border-top: 2px solid {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                    border-bottom: 2px solid {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                    border-radius: 7px;
                    background-color: #fff;
                  "
                            align="center"
                            valign="top"
                        >
                            <table
                                width="100%"
                                cellpadding="0"
                                cellspacing="0"
                                style="
                      font-family: 'Helvetica Neue', Helvetica, Arial,
                        sans-serif;
                      box-sizing: border-box;
                      font-size: 14px;
                      margin: 0;
                    "
                            >
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        valign="top"
                                    >
                                        <h2
                                            class="aligncenter"
                                            style="
                            font-family: 'Helvetica Neue', Helvetica, Arial,
                              'Lucida Grande', sans-serif;
                            box-sizing: border-box;
                            font-size: 24px;
                            color: #000;
                            line-height: 1.2em;
                            font-weight: 400;
                            text-align: center;
                            margin: 40px 0 0;
                          "
                                            align="center"
                                        >
                                            {{__('Order has arrived')}}
                                        </h2>
                                    </td>
                                </tr>
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        valign="top"
                                    >
                                        <p>{{__('Hi,')}}</p>
                                        <p>
                                            {{__('You are receiving this email because your order items
                                            related to the production order')}}  <b>{{ $body['order_reference'] ?? '' }}</b> {{__('have been received.')}}
                                        </p>
                                        <p>
                                            {{__('To access directly to the order request')}}, <a href="{{$body['arrival_page_link'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{__('click here.')}}</a>
                                        </p>
                                        <p> {{__('Please find below the order details:')}}</p>
                                        <p>
                                            {{__('NOTE: the production should be ready by')}}
                                        </p>
                                    </td>
                                </tr>
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block aligncenter"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          text-align: center;
                          margin: 0;
                        "
                                        align="center"
                                        valign="top"
                                    >
                                        <h4>Articles</h4>
                                    </td>
                                </tr>
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block aligncenter"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          text-align: center;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        align="center"
                                        valign="top"
                                    >
                                        <table
                                            class="invoice"
                                            style="
                            font-family: 'Helvetica Neue', Helvetica, Arial,
                              sans-serif;
                            box-sizing: border-box;
                            font-size: 14px;
                            text-align: left;
                            width: 100%;
                            margin: 10px auto;
                          "
                                        >
                                            <tr
                                                style="
                              font-family: 'Helvetica Neue', Helvetica, Arial,
                                sans-serif;
                              box-sizing: border-box;
                              font-size: 14px;
                              margin: 0;
                            "
                                            >
                                                <td
                                                    style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                margin: 0;
                                padding: 5px 0;
                                width: 70%;
                              "
                                                    valign="top"
                                                >
                                                    <b>{{__('Article')}}</b>
                                                </td>
                                                <td
                                                    style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                margin: 0;
                                padding: 5px 0;
                                float: right;
                                width: 30%;
                              "
                                                    valign="top"
                                                >
                                                    <b>{{__('Quantity')}}</b>
                                                </td>
                                            </tr>

                                            @if(isset($body['products']))
                                                @foreach($body['products'] as $product)
                                                    <tr
                                                        style="
                              font-family: 'Helvetica Neue', Helvetica, Arial,
                                sans-serif;
                              box-sizing: border-box;
                              font-size: 14px;
                              margin: 0;
                            "
                                                    >
                                                        <td
                                                            style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                border-top-width: 1px;
                                border-top-color: #eee;
                                border-top-style: solid;
                                margin: 0;
                                padding: 5px 0;
                              "
                                                            valign="top"
                                                        >
                                                          <span><a href="">{{$product->product->name}} - {{$product->product_reference}}</a></span><br />
                                                            <br />
                                                            <span>{{$product->comments}}</span>
                                                        </td>
                                                        <td
                                                            class="alignright"
                                                            style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                text-align: right;
                                border-top-width: 1px;
                                border-top-color: #eee;
                                border-top-style: solid;
                                margin: 0;
                                padding: 5px 0;
                              "
                                                            align="right"
                                                            valign="top"
                                                        >
                                                            {{__('Necessary qty :')}} {{ $product->quantity - (int)$product->total_arrival}} <br />
                                                            {{__('Ordered qty :')}} {{$product->quantity}} <br />
                                                            {{__('Received qty :')}} {{$product->total_arrival}} <br />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                        </table>
                                    </td>
                                </tr>
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          margin: 0;
                          padding: 0 0 40px;
                        "
                                        valign="top"
                                    >
                                        <span><b>{{__('Salesman:')}} </b> {{$body['salesman_name'] ?? ''}}</span><br />
                                        <span><b>{{__('Client:')}} </b> {{$body['customer_name'] ?? ''}}</span><br />
                                        <span><b>{{__('Production: ')}}</b> {{$body['order_reference'] ?? ''}}</span><br />
                                        <span><b>{{__('Comments:')}} </b> {{$body['comments'] ?? ''}}</span><br />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
        </td>
        <td
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            margin: 0;
          "
            valign="top"
        ></td>
    </tr>
</table>
</body>
</html>
