<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
    xmlns="http://www.w3.org/1999/xhtml"
    style="
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    margin: 0;
  "
>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{{$body['brand_name'] ?? ''}} - {{__('new shipment request email')}}</title>
</head>

<body
    itemscope
    itemtype="http://schema.org/EmailMessage"
    style="
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      box-sizing: border-box;
      font-size: 14px;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: none;
      width: 100% !important;
      height: 100%;
      line-height: 1.6em;
      background-color: #f6f6f6;
      margin: 0;
    "
    bgcolor="#f6f6f6"
>
<table
    class="body-wrap"
    style="
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
        width: 100%;
        background-color: #f6f6f6;
        margin: 0;
      "
    bgcolor="#f6f6f6"
>
    <tr
        style="
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          box-sizing: border-box;
          font-size: 14px;
          margin: 0;
        "
    >
        <td
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            margin: 0;
          "
            valign="top"
        ></td>
        <td
            class="container"
            width="600"
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            display: block !important;
            max-width: 600px !important;
            clear: both !important;
            margin: 0 auto;
          "
            valign="top"
        >
            <div
                class="content"
                style="
              font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
              box-sizing: border-box;
              font-size: 14px;
              max-width: 600px;
              display: block;
              margin: 0 auto;
              padding: 20px;
            "
            >
                <table
                    class="main"
                    width="100%"
                    cellpadding="0"
                    cellspacing="0"
                    style="
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                box-sizing: border-box;
                font-size: 14px;
                border-radius: 3px;
                margin: 0;
                border: none;
              "
                >
                    <tr
                        style="
                  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                  box-sizing: border-box;
                  font-size: 14px;
                  margin: 0;
                "
                    >
                        <td
                            class="content-wrap aligncenter"
                            style="
                                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                margin: 0;
                                padding: 20px;
                                border-top: 2px solid {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                                border-bottom: 2px solid {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                                border-radius: 7px;
                                background-color: #fff;
                                "
                            align="center"
                            valign="top"
                        >
                            <table
                                width="100%"
                                cellpadding="0"
                                cellspacing="0"
                                style="
                                  font-family: 'Helvetica Neue', Helvetica, Arial,
                                    sans-serif;
                                  box-sizing: border-box;
                                  font-size: 14px;
                                  margin: 0;
                                "
                            >
                                <tr
                                    style="
                                    font-family: 'Helvetica Neue', Helvetica, Arial,
                                      sans-serif;
                                    box-sizing: border-box;
                                    font-size: 14px;
                                    margin: 0;
                                  "
                                >
                                    <td
                                        class="content-block"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        valign="top"
                                    >
                                        <h2
                                            class="aligncenter"
                                            style="
                            font-family: 'Helvetica Neue', Helvetica, Arial,
                              'Lucida Grande', sans-serif;
                            box-sizing: border-box;
                            font-size: 24px;
                            color: #000;
                            line-height: 1.2em;
                            font-weight: 400;
                            text-align: center;
                            margin: 40px 0 0;
                          "
                                            align="center"
                                        >
                                            {{__('New shipment request')}}
                                        </h2>
                                    </td>
                                </tr>
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block aligncenter"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          text-align: center;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        align="center"
                                        valign="top"
                                    >
                                        <table
                                            class="invoice"
                                            style="
                            font-family: 'Helvetica Neue', Helvetica, Arial,
                              sans-serif;
                            box-sizing: border-box;
                            font-size: 14px;
                            text-align: left;
                            width: 90%;
                            margin: 10px auto;
                          "
                                        >
                                            <tr
                                                style="
                              font-family: 'Helvetica Neue', Helvetica, Arial,
                                sans-serif;
                              box-sizing: border-box;
                              font-size: 14px;
                              margin: 0;
                            "
                                            >
                                                <td
                                                    style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                margin: 0;
                                padding: 5px 0;
                              "
                                                    valign="top"
                                                >
                                                    <p>{{__('Hi ')}},{{$body['salesman_name']}}</p>
                                                    <p>
                                                      {{__('Your order ID# ')}}<b>{{$body['order_reference']}}</b> {{__(' is in shipment')}}
                                                    </p>

                                                    <p>
                                                        {{__('Click ')}} <a href="{{$body['shipment_page_link'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{__('here ')}}</a> {{__('to visit shipment page')}}
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr
                                                style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                            >
                                                <td
                                                    class="content-block"
                                                    style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          margin: 0;
                          padding: 0 0 40px;
                        "
                                                    valign="top"
                                                >
                                                    <span><b>{{__('Salesman:')}} </b> {{$body['salesman_name'] ?? ''}}</span><br />
                                                    <span><b>{{__('Client:')}} </b> {{$body['customer_name'] ?? ''}}</span><br />
                                                    <span><b>{{__('Order Reference: ')}}</b> {{$body['order_reference'] ?? ''}}</span><br />
                                                    <span><b>{{__('Comments:')}} </b> {{$body['comments'] ?? ''}}</span><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            margin: 0;
          "
            valign="top"
        ></td>
    </tr>
</table>
<div
    class="footer"
    style="
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
        width: 100%;
        clear: both;
        color: #999;
        margin: 0;
        padding: 20px;
      "
>
    <table
        width="100%"
        style="
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          box-sizing: border-box;
          font-size: 14px;
          margin: 0;
        "
    >
        <tr
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            margin: 0;
          "
        >
            <td
                class="aligncenter content-block"
                style="
              font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
              box-sizing: border-box;
              font-size: 12px;
              vertical-align: top;
              color: #999;
              text-align: center;
              margin: 0;
              padding: 0 0 20px;
            "
                align="center"
                valign="top"
            >
                <span><a href="{{$body['brand_url'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['brand_url'] ?? 'www.brindesebrindes.pt'}}</a></span> <br/>
                <span><b>{{$body['brand_address'] ?? ''}}</b></span><br/>
                <span><a href="https://mail.google.com/mail/?view=cm&fs=1&to={{$body['brand_email'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['brand_email'] ?? 'info@brindesebrindes.pt'}}</a></span
                ><br/>
                <span> {{$body['brand_phone'] ?? '(+351) 252 860 337'}}</span><br/>
                <br/>

                <span>{{__('All the frequent questions')}} <a href="{{$body['brand_faq_link']}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{__('Faq Link')}} </a></span
                ><br/>
                <span>{{__('Our privacy policy')}} <a href="{{$body['brand_privacy_policy_link']}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{__('Privacy Policy Link')}}</a></span
                ><br/>
                <br/>
                <span>© 2006-2021 {{$body['brand_name'] ?? ''}}</span><br/>
                <span>{{__('This email was sent to')}}:</span><br/>
                <span>{{$body['salesman_email']}} - {{$body['salesman_name']}} at {{$body['date'] ?? ''}}</span>
                <br/>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
