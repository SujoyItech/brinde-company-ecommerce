<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
    xmlns="http://www.w3.org/1999/xhtml"
    style="
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    margin: 0;
  "
>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{{$body['brand_name'] ?? ''}} - {{__('budget email')}}</title>
</head>

<body
    itemscope
    itemtype="http://schema.org/EmailMessage"
    style="
      font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      box-sizing: border-box;
      font-size: 14px;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: none;
      width: 100% !important;
      height: 100%;
      line-height: 1.6em;
      background-color: #f6f6f6;
      margin: 0;
    "
    bgcolor="#f6f6f6"
>
<table
    class="body-wrap"
    style="
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
        width: 100%;
        background-color: #f6f6f6;
        margin: 0;
      "
    bgcolor="#f6f6f6"
>
    <tr
        style="
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          box-sizing: border-box;
          font-size: 14px;
          margin: 0;
        "
    >
        <td
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            margin: 0;
          "
            valign="top"
        ></td>
        <td
            class="container"
            width="600"
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            display: block !important;
            max-width: 600px !important;
            clear: both !important;
            margin: 0 auto;
          "
            valign="top"
        >
            <div
                class="content"
                style="
              font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
              box-sizing: border-box;
              font-size: 14px;
              max-width: 600px;
              display: block;
              margin: 0 auto;
              padding: 20px;
            "
            >
                <table
                    class="main"
                    width="100%"
                    cellpadding="0"
                    cellspacing="0"
                    style="
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                box-sizing: border-box;
                font-size: 14px;
                border-radius: 3px;
                margin: 0;
                border: none;
              "
                >
                    <tr
                        style="
                  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                  box-sizing: border-box;
                  font-size: 14px;
                  margin: 0;
                "
                    >
                        <td
                            class="content-wrap aligncenter"
                            style="
                    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                    box-sizing: border-box;
                    font-size: 14px;
                    vertical-align: top;
                    margin: 0;
                    padding: 20px;
                    border-top: 2px solid {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                    border-bottom: 2px solid {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                    border-radius: 7px;
                    background-color: #fff;
                  "
                            align="center"
                            valign="top"
                        >
                            <table
                                width="100%"
                                cellpadding="0"
                                cellspacing="0"
                                style="
                                  font-family: 'Helvetica Neue', Helvetica, Arial,
                                    sans-serif;
                                  box-sizing: border-box;
                                  font-size: 14px;
                                  margin: 0;
                                "
                            >
                                <tr
                                    style="
                                    font-family: 'Helvetica Neue', Helvetica, Arial,
                                      sans-serif;
                                    box-sizing: border-box;
                                    font-size: 14px;
                                    margin: 0;
                                  "
                                >
                                    <td
                                        class="content-block"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        valign="top"
                                    >
                                        <h2
                                            class="aligncenter"
                                            style="
                            font-family: 'Helvetica Neue', Helvetica, Arial,
                              'Lucida Grande', sans-serif;
                            box-sizing: border-box;
                            font-size: 24px;
                            color: #000;
                            line-height: 1.2em;
                            font-weight: 400;
                            text-align: center;
                            margin: 40px 0 0;
                          "
                                            align="center"
                                        >
                                            {{__('Your estimate request')}}
                                        </h2>
                                    </td>
                                </tr>
                                <tr
                                    style="
                        font-family: 'Helvetica Neue', Helvetica, Arial,
                          sans-serif;
                        box-sizing: border-box;
                        font-size: 14px;
                        margin: 0;
                      "
                                >
                                    <td
                                        class="content-block aligncenter"
                                        style="
                          font-family: 'Helvetica Neue', Helvetica, Arial,
                            sans-serif;
                          box-sizing: border-box;
                          font-size: 14px;
                          vertical-align: top;
                          text-align: center;
                          margin: 0;
                          padding: 0 0 20px;
                        "
                                        align="center"
                                        valign="top"
                                    >
                                        <table
                                            class="invoice"
                                            style="
                            font-family: 'Helvetica Neue', Helvetica, Arial,
                              sans-serif;
                            box-sizing: border-box;
                            font-size: 14px;
                            text-align: left;
                            width: 90%;
                            margin: 10px auto;
                          "
                                        >
                                            <tr
                                                style="
                              font-family: 'Helvetica Neue', Helvetica, Arial,
                                sans-serif;
                              box-sizing: border-box;
                              font-size: 14px;
                              margin: 0;
                            "
                                            >
                                                <td
                                                    style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                margin: 0;
                                padding: 5px 0;
                              "
                                                    valign="top"
                                                >
                                                    <p>{{__('Hi')}}, {{$body['customer_name'] ?? ''}},</p>
                                                    <p>
                                                        {{__('Thank you for the request made in')}}
                                                        <a href="{{$body['brand_url'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['brand_url'] ?? ''}}</a>
                                                    </p>

                                                    <p>
                                                        {{__('My name is ')}}{{$body['salesman_name'] ?? ''}}{{__(' and i am your sales
                                                        representative. I am available to help. If you
                                                        have any doubt, don’t hesitate to contact me
                                                        directly:')}}

                                                        <a href="https://mail.google.com/mail/?view=cm&fs=1&to={{$body['salesman_email'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['salesman_email'] ?? ''}}</a>
                                                    </p>
                                                    <p>
                                                        {{__('Your request was registered with the reference number')}}
                                                        <b>{{$body['budget_reference'] ?? ''}}</b>.
                                                    </p>
                                                    <p>
                                                        {{__('Click on the button below to see your estimate.
                                                        If you prefer, you can check it on the PDF in
                                                        attachment.')}}
                                                    </p>
                                                    <span style="text-align: center"><a href="{{$body['approve_link'] ?? ''}}" class="btn-primary" style="
                                                        font-family: 'Helvetica Neue', Helvetica,
                                                          Arial, sans-serif;
                                                        box-sizing: border-box;
                                                        font-size: 14px;
                                                        color: #fff;
                                                        text-decoration: none;
                                                        line-height: 2em;
                                                        font-weight: bold;
                                                        text-align: center;
                                                        cursor: pointer;
                                                        display: inline-block;
                                                        border-radius: 5px;
                                                        text-transform: capitalize;
                                                        background-color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                                                        margin: 0;
                                                        border-color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}};
                                                        border-style: solid;
                                                        border-width: 8px 16px;
                                                      ">{{__('View estimate')}}</a></span>
                                                    <p>
                                                        {{__('If you’re unable to use the button above, click on the following link:')}}
                                                    </p>
                                                    <span><a href="{{$body['approve_link'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['approve_link'] ?? ''}}</a></span>
                                                </td>
                                            </tr>
                                            <tr
                                                style="
                              font-family: 'Helvetica Neue', Helvetica, Arial,
                                sans-serif;
                              box-sizing: border-box;
                              font-size: 14px;
                              margin: 0;
                            "
                                            >
                                                <td
                                                    style="
                                font-family: 'Helvetica Neue', Helvetica, Arial,
                                  sans-serif;
                                box-sizing: border-box;
                                font-size: 14px;
                                vertical-align: top;
                                margin: 0;
                                padding: 5px 0;
                              "
                                                    valign="top"
                                                >
                                                    <h4>{{__('Additional information:')}}</h4>

                                                    <ul style="list-style-type: ' - '">
                                                        <li>
                                                            {{__('If the estimate has more than one line, you
                                                            can approve only the parts that you are
                                                            interested in.')}}
                                                        </li>
                                                        <li>
                                                            {{__('If necessary, after approval of the estimate,
                                                            follow the instructions to send the files for
                                                            customization or your articles.')}}
                                                        </li>
                                                        <li>
                                                            {{__('After approval of the estimate, a draft will
                                                            be created and sent to you to approve.')}}
                                                        </li>
                                                        <li>
                                                            {{__('Nothing will be sent to production without
                                                            your approval.')}}

                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            vertical-align: top;
            margin: 0;
          "
            valign="top"
        ></td>
    </tr>
</table>
<div
    class="footer"
    style="
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
        width: 100%;
        clear: both;
        color: #999;
        margin: 0;
        padding: 20px;
      "
>
    <table
        width="100%"
        style="
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
          box-sizing: border-box;
          font-size: 14px;
          margin: 0;
        "
    >
        <tr
            style="
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            margin: 0;
          "
        >
            <td
                class="aligncenter content-block"
                style="
              font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
              box-sizing: border-box;
              font-size: 12px;
              vertical-align: top;
              color: #999;
              text-align: center;
              margin: 0;
              padding: 0 0 20px;
            "
                align="center"
                valign="top"
            >
                <span><a href="{{$body['brand_url'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['brand_url'] ?? 'www.brindesebrindes.pt'}}</a></span> <br/>
                <span><b>{{$body['brand_address'] ?? ''}}</b></span><br/>
                <span><a href="https://mail.google.com/mail/?view=cm&fs=1&to={{$body['brand_email'] ?? ''}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{$body['brand_email'] ?? 'info@brindesebrindes.pt'}}</a></span
                ><br/>
                <span> {{$body['brand_phone'] ?? '(+351) 252 860 337'}}</span><br/>
                <br/>

                <span>{{__('All the frequent questions')}} <a href="{{$body['brand_faq_link']}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{__('Faq Link')}} </a></span
                ><br/>
                <span>{{__('Our privacy policy')}} <a href="{{$body['brand_privacy_policy_link']}}" style="color: {{$body['color_code'] ?? BRAND_COLORS[$body['brand_id']]}}">{{__('Privacy Policy Link')}}</a></span
                ><br/>
                <br/>
                <span>© 2006-2021 {{$body['brand_name'] ?? ''}}</span><br/>
                <span>{{__('This email was sent to')}}:</span><br/>
                <span>{{$body['salesman_email']}} - {{$body['salesman_name']}} at {{$body['date'] ?? ''}}</span>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
