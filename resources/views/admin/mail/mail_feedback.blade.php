<!-- CTA : BEGIN -->
<tr>
    <td bgcolor="#17A2B8">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 40px 40px 5px 40px; text-align: center;">
                    <h1 style="margin: 0; font-family: 'IBM Plex Sans', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: bold;">YOUR FEEDBACK IS IMPORTANT</h1>
                </td>
            </tr>
            <tr>
                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 17px; line-height: 23px; color: #aad4ea; text-align: center; font-weight:normal;">
                    <p style="margin: 0;">Let us know what you think of our latest updates</p>
                </td>
            </tr>
            <tr>
                <td valign="middle" align="center" style="text-align: center; padding: 0px 20px 40px 20px;">

                    <!-- Button : BEGIN -->
                    <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
                        <tr>
                            <td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
                                <a href="http://www.google.com" style="background: #ffffff; border: 15px solid #ffffff; font-family: 'IBM Plex Sans', sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 50px; font-weight: bold;" class="button-a">
                                    <span style="color:#17A2B8;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;GIVE FEEDBACK&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </a>
                            </td>
                        </tr>
                    </table>
                    <!-- Button : END -->

                </td>
            </tr>

        </table>
    </td>
</tr>
<!-- CTA : END -->
