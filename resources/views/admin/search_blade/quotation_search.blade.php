<form method="post" id="purchase_list_form">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label>{{__('Store')}}</label>
            <select name="brand_id" class="form-control">
                <option value="">{{__('All')}}</option>
                @if(isset($brands[0]))
                    @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="reference">{{__('Reference')}}</label>
                <input type="text" name="{{$reference ?? 'order_reference'}}" placeholder="{{__('Reference')}}" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>{{__('Client')}}</label>
                <input type="text" name="customer_code" placeholder="{{__('Client reference')}}" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>{{__('Commercial')}}</label>
                <select name="assigned_salesman" class="form-control">
                    <option value="">{{__('All')}}</option>
                    @if(isset($salesmans[0]))
                        @foreach($salesmans as $salesman)
                            <option value="{{$salesman->id}}">{{$salesman->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group col-md-3">
            <label>{{__('Created')}}</label>
            <input type="date" name="created_at" placeholder="{{__('Created At')}}" class="form-control">
        </div>
        <div class="form-group col-md-3">
            <label>{{__('Expired')}}</label>
            <input type="date" name="expired_at" placeholder="{{__('Expired At')}}" class="form-control">
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>{{__('Status')}}</label>
                <select name="status" class="form-control">
                    <option value="">{{__('All')}}</option>
                    @foreach(quotation_status() as $key => $value)
                        <option value="{{$key}}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 listing-search">
            <div class="col-8 px-0">
                <button type="submit" class="btn btn-primary btn-block purchase_list"><i class="fa fa-search"></i>&nbsp; {{__('Apply Filter')}}</button>
            </div>
            <div class="col-4">
                <button type="button" class="btn btn-soft-secondary btn-block text-dark clear_search"><i class="fe-refresh-ccw"></i></button>
            </div>
        </div>
    </div>
</form>

