@extends('admin.layouts.app',['menu'=>'production','sub_menu'=>'productionList'])
@section('title', isset($title) ? $title : '')
@section('style')
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{__('Production Reference:')}} <span id="production_reference"> {{ $item->production_reference ?? ''}} </span></h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="float-right">{{__('Current Status ')}} : <span id="complete_status" class="badge badge-success"> {{quotation_status($item->status)}} </span></h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-3">
                    <div class="col-md-4">
                        <p><b>{{__('Brand Name : ')}}</b> <span>{{$item->brand->name ?? ''}}</span></p>
                        <p><b>{{__('Salesman : ')}}</b> <span>{{isset($item->assigned_salesman) ? $item->salesman->name : ''}}</span></p>
                        <p><b>{{__('Is Printing Enabled ? : ')}}</b>
                            @if ($item->budget_printing_enable == TRUE)
                                <span class="badge badge-success">{{__('Yes')}}</span>
                            @else
                                <span class="badge badge-danger">{{__('No')}}</span>
                            @endif
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p><b>{{__('Start Date : ')}}</b> <span>{{date('d M Y', strtotime($item->created_at))}}</span></p>
                        <p><b>{{__('Expire date : ')}}</b> <span>{{!empty($item->expired_at) ? date('d M Y', strtotime($item->expired_at)) : ''}}</span></p>
                    </div>
                    <div class="col-md-4">
                        <p><b>{{__('In Stage : ')}}</b> <span>{{quotation_stage($item->in_staging)}}</span></p>
                        <p><b>{{__('Payment Status : ')}}</b> <span>{{payment_status($item->payment_status)}}</span></p>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-12 col-xl-12 col-md-6">
                        <form class="update_complete_form" novalidate method="post" action="{{route('productionCompleteSave')}}" id="update_complete_form_id">
                            <div class="row mb-1">
                                <div class="col-md-12">
                                    <h4>{{__('Production List')}}</h4>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name="quotation_id" value="{{$item->id}}">
                                <div class="col-md-12">
                                    <div id="product_items">
                                        @include('admin.production.production.production_item_list')
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div class="button-list">
                                        <a href="{{route('productionList')}}" class="btn btn-warning waves-effect waves-light mr-1"><i class="fa fa-arrow-circle-left mr-1"></i>
                                            {{__('Go back')}}
                                        </a>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right update_complete"><i class="fa fa-save mr-1"></i>{{__('Save Record')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
    <script>
        resetValidation('update_complete_form');
        $(document).on('click', '.update_complete', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#' + form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        var order_status = response.data.order_status;
                        if(order_status === parseInt('{{STATUS_PRODUCTION_PARTIALLY_COMPLETED}}')) {
                            $('#complete_status').html('{{__('Partially Completed')}}');
                        } else if(order_status === parseInt('{{STATUS_PRODUCTION_COMPLETED}}')) {
                            $('#complete_status').html('{{__('Production Completed')}}');
                        } else {
                            $('#complete_status').html('{{__('Not Completed')}}');
                        }
                        $('#product_items').html(response.data.view);
                        $('[data-plugin="switchery"]').each(function (e, a) {
                            new Switchery($(this)[0], $(this).data())
                        })
                        swalSuccess(response.message);
                        this_form.removeClass('was-validated');
                    });
                }
            });
        });
        /*-----------------****-----------------------*/
    </script>
@endsection
