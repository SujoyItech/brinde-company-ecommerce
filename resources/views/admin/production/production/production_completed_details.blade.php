@extends('admin.layouts.app',['menu'=>'production','sub_menu'=>$sub_menu ?? ''])
@section('title', isset($title) ? $title : '')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Order ##')}} {{$item->order_reference}}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-xl-6 col-md-6">
                        <p><b>{{__('Order Reference')}}: </b> <span id="budget_reference">{{$item->order_reference ?? ''}}</span></p>
                        <p><b>{{__('Customer Code')}}: </b> <span id="customer_code">{{$item->customer_code ?? ''}}</span></p>
                        <p><b>{{__('Company Name')}}: </b> {{$item->company_name}}</p>
                        <p><b>{{__('Contact Name')}}: </b> {{$item->contact_name}}</p>
                        <p><b>{{__('Email')}}: </b> {{$item->email}}</p>
                        <p><b>{{__('Phone Number')}}: </b> {{$item->phone}}</p>
                        <p><b>{{__('Stage')}}: </b> {{quotation_stage($item->in_staging)}}</p>
                        <p><b>{{__('Status')}}: </b> {{quotation_status($item->status)}}</p>
                        <p><b>{{__('Price')}}: </b> {{__('Total price: ')}} {{getBrindeMoney($item->total_price)}} + {{__('vat : ')}} {{getBrindeMoney($item->total_vat)}} = {{getBrindeMoney($item->grand_total)}}</p>
                        <p><b>{{__('Payment Status')}}: </b> {{payment_status($item->payment_status)}}</p>
                        <p><b>{{__('Quotation Date')}}: </b> {{$item->created_at}}</p>
                        <p><b>{{__('Additional Information')}}: </b> {{$item->comments}}</p>
                        @if(!empty($item->attached_file) && file_exists( public_path().'/'.get_image_path('quotation').$item->attached_file))
                            <a href="{{asset(get_image_path('quotation').$item->attached_file)}}" target="_blank">
                                <button class="btn btn-outline-secondary btn-xl"><i class="fe-paperclip"></i> {{__('View Attached File')}} </button>
                            </a>
                        @endif
                        <hr/>
                        @if(check_route_permission('sendToShipment',TRUE))
                            @include('admin.production.logistic.send_shipment_button')
                        @endif
                    </div>
                    <div class="col-lg-6 col-xl-6 col-md-6">
                        @if(isset($products[0]))
                            @foreach($products as $product)
                                <div class="card-body p-0 mb-2">
                                    <div class="card-header bg-secondary py-2 text-white">
                                        <div class="card-widgets">
                                            <a data-toggle="collapse" href="#cardCollpase{{$product->id}}" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">{{$product->product_name}} - {{$product->product_reference}}</h5>
                                    </div>
                                    @php $combination = get_product_combination($product->product_id, $product->combination_id, $product->combination_type_id) @endphp
                                    <div id="cardCollpase{{$product->id}}" class="collapse show">
                                        <div class="row">
                                            <div class="col-2 p-2">
                                                <img width="" class="img-fluid img-thumbnail" src="{{$combination['image']}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                                            </div>
                                            <div class="col-9 p-2">
                                                <p><b>{{__('Quantity')}}:</b> {{getBrindeMoney($product->quantity)}} &ensp;<b>{{__('Price')}}:</b> {{getBrindeMoney($product->unit_price)}} &ensp;
                                                @if(!empty($combination['combination']))
                                                    <b>{{$combination['combination_type']}}:</b> {{$combination['combination']}}
                                                @endif
                                                </p>
                                            </div>
                                            <div class="col-12">
                                                <p><b>{{__('Extra Information')}}:</b> {{$product->additional_info}}</p>
                                                <p><b>{{__('Description')}}:</b> {{$product->comments}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
