@extends('admin.layouts.app',['menu'=>'production','sub_menu'=>'purchase_list'])
@section('title', isset($title) ? $title : '')
@section('content')
    @include('admin.production.logistic.quantity_modal')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Purchase Reference:')}} <span id="purchase_reference"> {{$item->purchase_reference}} </span></h4>
                <h4 class="float-right">{{__('Current Status ')}} : <span id="order_status" class="badge badge-success"> {{quotation_status($item->status)}} </span></h4>
            </div>
            <div class="card-body">
                <div class="row mt-3">
                    <div class="col-md-4">
                        <p><b>{{__('Brand Name : ')}}</b> <span>{{$item->brand->name ?? ''}}</span></p>
                        <p><b>{{__('Salesman : ')}}</b> <span>{{isset($item->assigned_salesman) ? $item->salesman->name : ''}}</span></p>
                        <p><b>{{__('Is Printing Enabled ? : ')}}</b>
                            @if ($item->budget_printing_enable == TRUE)
                                <span class="badge badge-success">{{__('Yes')}}</span>
                            @else
                                <span class="badge badge-danger">{{__('No')}}</span>
                            @endif
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p><b>{{__('Start Date : ')}}</b> <span>{{date('d M Y', strtotime($item->created_at))}}</span></p>
                        <p><b>{{__('Expire date : ')}}</b> <span>{{!empty($item->expired_at) ? date('d M Y', strtotime($item->expired_at)) : ''}}</span></p>
                    </div>
                    <div class="col-md-4">
                        <p><b>{{__('In Stage : ')}}</b> <span>{{quotation_stage($item->in_staging)}}</span></p>
                        <p><b>{{__('Payment Status : ')}}</b> <span>{{payment_status($item->payment_status)}}</span></p>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-12 col-xl-12 col-md-6">
                        <h4>{{__('Product List')}}</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="product_items">
                                    @include('admin.production.logistic.purchase_product_list')
                                </div>
                            </div>
                        </div>
                        <a href="{{route('productionPurchaseList')}}" class="btn btn-warning waves-effect waves-light"><i class="fa fa-arrow-circle-left mr-1"></i>
                            {{__('Go back')}}
                        </a>
                        @if (check_route_permission('sendToProduction',TRUE))
                            <div class="float-right">
                                @if(isset($item->budget_printing_enable) && $item->budget_printing_enable == TRUE)
                                    @include('admin.production.logistic.send_production_button')
                                @else
                                    @include('admin.production.logistic.send_shipment_button')
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function initFlatpickr(selector) {
            flatpickr(selector, {
                enableTime: true,
                //altInput: true,
                //altFormat: "F j, Y H:i",
                dateFormat: "Y-m-d H:i",
            });
        }
        function initFlatpickrWithDate(selector) {
            flatpickr(selector, {
                enableTime: true,
                //altInput: true,
                //altFormat: "F j, Y H:i",
                dateFormat: "Y-m-d H:i",
                defaultDate: "today"
            });
        }
        $(window).on('load', function (){
            resetValidation('arrival-quantity-form');
            submitOperation(saveQuantityResponse, 'save-arrival-quantity');
            function saveQuantityResponse(response, this_form){
                // this_form.removeClass('was-validated');
                if (response.success === true) {
                    // $("#product_items").html(response.data.product_list);
                    // $("#send-production-button").html(response.data.send_production_button);
                    // $("#quantity-modal").modal('hide');
                    swalRedirect("{{Request::url()}}",response.message,'success');
                } else {
                    swalError(response.message);
                }
            }

            submitOperation(sendProductionResponse, 'send-production');
            function sendProductionResponse(response, this_form){
                this_form.removeClass('was-validated');
                if (response.success === true) {
                    swalSuccess(response.message);
                    window.location.href = response.data.redirect;
                } else {
                    swalError(response.message);
                }
            }
        });


        function openArrivalQuantityModal(crmProduct, arrivalLogs) {
            //clear prev inputs
                /*$("#arrival-quantity-form textarea").val("");
                $("#arrival-quantity-form input").val("");*/
                $(".clear-arrival-tr").remove();
            //

            $('[name="crm_product_id"]').val(crmProduct.id)
            $('#modal-head').text(crmProduct.product_name);
            $('#purchase-quantity').text(crmProduct.quantity);
            let arrivedQuantity = calculateArrivedQuantity(arrivalLogs);
            $('#arrived-quantity').text(arrivedQuantity);
            renderQuantityModal(crmProduct.quantity, arrivedQuantity, arrivalLogs);
            $("#quantity-modal").modal('show');
        }

        function calculateArrivedQuantity(arrivalLogs = []) {
            let sum = 0;
            for(let i=0; i < arrivalLogs.length; i++) {
                sum += arrivalLogs[i].arrival_quantity;
            }
            return sum;
        }

        function deleteArrivalLog(id) {
            $('#arrival-tr-'+id).remove();
            if($("#arrival-tr").length === 0){
                $('#arrival-tbody').append(getEmptyQuantityInputRow());
                initFlatpickr('.flatpickr');
            }
        }

        function renderQuantityModal(purchaseQuantity, arrivedQuantity, arrivalLogs = []) {
            let html = '';
            for (let i=0; i<arrivalLogs.length; i++) {
                html += `<tr id=arrival-tr-`+i+` class="clear-arrival-tr">`;
                html += `<td style="">
                              <input type="text"
                                  id="arrival-quantity-`+i+`"
                                  name="arrival_quantity[]"
                                  class="form-control"
                                  placeholder="" required="" value="`+arrivalLogs[i].arrival_quantity+`">
                              </td>`;
                html += `<td style="">
                                    <input type="text" id="arrived-at`+i+`" name="arrived_at[]" class="form-control flatpickr-filled"
                                               placeholder="" required="" value="`+(arrivalLogs[i].arrived_at ?? '')+`">
                              </td>`;
                html += `<td style=""><textarea id="note-`+i+`" name="note[]" rows="1" class="form-control"
                                        placeholder="{{__('Enter a Note')}}">`+(arrivalLogs[i].note ?? "")+`</textarea></td>`;
                html += `<td><button onclick="deleteArrivalLog(`+i+`)" class="btn btn-xs btn-danger"><i
                                                class="fa fa-trash"></i></button></td>`;
                html += `</tr>`;
            }
            if(purchaseQuantity > arrivedQuantity) {
                html += `<tr id="arrival-tr">
                                <td style=""><input type="text" id="arrival_quantity" name="arrival_quantity[]" class="form-control"
                                               placeholder="`+'{{__('Enter Quantity')}}'+`"></td>
                                <td style=""><input type="text" id="arrived_at" name="arrived_at[]" class="form-control flatpickr"
                                               placeholder="`+'{{__('Enter Arrival Time')}}'+`"></td>
                                <td style=""><textarea id="note" name="note[]" rows="1" class="form-control"
                                                  placeholder="`+'{{__('Enter a Note')}}'+`"></textarea></td>
                                <td></td>
                            </tr>`;
            }
            $('#arrival-tbody').html(html);
            initFlatpickr('.flatpickr-filled');
            initFlatpickr('.flatpickr');
        }

    </script>
@endsection
