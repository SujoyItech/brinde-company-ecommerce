@if(isset($products[0]))
    @php $total_price = 0; $total_vat = 0; @endphp
    @foreach($products as $product)
        @php
            $total_price = $total_price + $product->total_price;
            $total_vat = $total_vat + $product->total_vat;
        @endphp
        <div class="card-body mb-2 p-0" id="product-card-{{$product->id}}">
            <div class="card-header bg-soft-secondary text-dark p-1">
                <div class="card-widgets">
                        <span id="purchase_status_id-{{$product->id}}"
                              class="badge @if($product->arrived == STATUS_NOT_ARRIVED) badge-danger @elseif($product->arrived == STATUS_PARTIALLY_ARRIVED) badge-warning  @else badge-success @endif">
                            {{arrival_status($product->arrived)}} </span>
                </div>
                <h5 class="card-title my-1 pl-2 text-dark">@if(!empty($product->product_id)) {{$product->product->name}}
                    - {{$product->product_reference}} @else {{$product->product_name}} @endif</h5>
            </div>
            @if (!empty($product->product_id ))
                @php $combination = get_product_combination($product->product_id, $product->combination_id, $product->combination_type_id) @endphp
            @endif
            <div id="cardCollpase{{$product->id}}" class="collapse show">
                <div class="row">
                    <div class="col-1 p-2">
                        <img width="" class="img-fluid img-thumbnail"
                             src="{{isset($combination) ? $combination['image'] : ''}}"
                             onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                    </div>
                    <div class="col-6 p-1">
                        <p><b><i class="fa fa-calculator"></i> {{$product->quantity}} * {{getBrindeMoney($product->unit_price)}}
                                = {{getBrindeMoney($product->total_price)}} ( + {{getBrindeMoney($product->total_vat)}} of VAT at {{getBrindeMoney($product->vat)}}
                                %)</b></p>
                        <p>
                            <b>{{__('Quantity')}}:</b> {{$product->quantity}} &emsp;
                            <b>{{__('Price')}}:</b> {{getBrindeMoney($product->unit_price)}}  &emsp;
                            @if(isset($combination) && !empty($combination['combination']))
                                <b>{{$combination['combination_type']}}:</b> {{$combination['combination']}}
                            @endif
                        </p>
                        <input type="hidden" name="product_id[]" value="{{$product->id}}">
                        <a href="javascript:void(0)"
                           onclick="openArrivalQuantityModal({{$product}}, {{$product->arrival_logs}})"
                           class="btn btn-info waves-effect waves-light"><i class="fa fa-plus mr-1"></i>
                            {{__('Add arrival quantity')}}
                        </a>

                    </div>
                    <div class="col-5 p-1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="list-group">
                                        @foreach($product->arrival_logs as $log)
                                            <li class="list-group-item list-group-item-action">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1"><strong>{{__('Arrived: ')}} </strong>{{$log->arrival_quantity}}</h5>
                                                    <small class="text-muted">{{isset($log->arrived_at) ? \Carbon\Carbon::parse($log->arrived_at)->format('d F Y ') : ''}}</small>
                                                </div>
                                                <p class="mb-1">{{$log->note}}</p>
                                            </li>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <p><b>{{__('Additional Info')}}:</b> {{$product->additional_info}}</p>
                        <p><b>{{__('Description')}}:</b> {{$product->comments}}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif

