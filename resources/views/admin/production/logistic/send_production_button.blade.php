<div class="col-md-12">
    <div class="button-list">
        @if($item->status == STATUS_ORDER_ARRIVED_DONE)
            <form method="get" action="{{route('sendToProduction', $item->purchase_reference)}}" id="send-production-form" class="send-production-form">
                <button type="submit" class="btn btn-pink send-production waves-effect waves-light"><i class="fa fa-play mr-1"></i>
                    {{__('Send to production')}}
                </button>
            </form>
        @endif
    </div>
</div>
