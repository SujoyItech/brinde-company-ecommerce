<div id="quantity-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
     aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content p-3">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">{{__('Add Arrival Quantity of ')}} <span id="modal-head"></span></h4>
                <button type="button" class="close" onclick="clearForm()" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form novalidate method="post" id="arrival-quantity-form" action="{{route('saveArrivalQuantity')}}" class="arrival-quantity-form">
                    <p><b>{{__('Purchase quantity: ')}}<span id="purchase-quantity"></span></b></p>
                    <p><b>{{__('Arrived quantity: ')}}<span id="arrived-quantity"></span></b></p>
                    <input type="hidden" name="crm_product_id" value="">
                    <input type="hidden" name="quotation_id" value="{{$item->id}}">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th style="width: 20%">{{__('Arrived Quantity')}}</th>
                            <th style="width: 30%">{{__('Arrived At')}}</th>
                            <th style="width: 40%">{{__('Note')}}</th>
                            <th style="width: 10%"></th>
                        </tr>
                        </thead>
                        <tbody id="arrival-tbody">

                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-dark save-arrival-quantity"  data-style="zoom-in">Save</button>
                </form>
            </div>

        </div>
    </div>
</div>


<script>
    function clearForm() {
        $("#arrival-quantity-form").removeClass('was-validated');
        /*$('.invalid-feedback').remove();
        $("#arrival-quantity-form textarea").removeClass("is-invalid");
        $("#arrival-quantity-form input").removeClass("is-invalid");*/
    }
</script>
