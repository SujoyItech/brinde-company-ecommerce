<div class="col-md-12">
    <div class="button-list">
        @if($item->status >= STATUS_ORDER_ARRIVED_DONE)
            <button type="submit" class="btn btn-info send-production waves-effect waves-light send_shipment " data-id="{{$item->id ?? ''}}"><i class="fa fa-truck mr-1"></i>
                {{__('Send to shipment')}}
            </button>
        @endif
    </div>
</div>
<script>
    $(document).on('click','.send_shipment',function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var id = $(this).data('id');
        swalConfirm("{{__('Do you really want to send this to shipment ?')}}").then(function (s) {
            if(s.value){
                var url = "{{route('sendToShipment')}}";
                var data = {
                    id : id
                };
                makeAjaxPost(data, url, load).done(function (response) {
                    if (response.success == true){
                        swalRedirect("{{route('productionShipmentList')}}",response.message,'success')
                    }else {
                        swalError(response.message);
                    }
                });
            }else{
                load.ladda('stop');
            }
        })
    });
</script>
