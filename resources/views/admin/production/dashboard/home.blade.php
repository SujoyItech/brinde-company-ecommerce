@extends('admin.layouts.app',['menu'=>'home','sub_menu'=>'home'])
@section('content')
    <div class="col-md-12">
        <h4>{{__('Production Admin')}}</h4>
    </div>
    @if (isset($production) && !empty($production))
        <div class="col-md-3">
            <div class="widget-rounded-circle card-box bg-warning">
                <div class="row">
                    <div class="col-3">
                        <div class="avatar-md rounded-circle bg-light">
                            <i class="fe-cloud-lightning font-28 avatar-title text-danger"></i>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$production->expire_today}}</span></h3>
                            <p class="text-dark mb-1">{{__('Expire today')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-rounded-circle card-box bg-secondary">
                <div class="row">
                    <div class="col-3">
                        <div class="avatar-md rounded-circle bg-light">
                            <i class="fe-bell font-28 avatar-title text-warning"></i>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="text-right">
                            <h3 class="text-light mt-1"><span data-plugin="counterup">{{$production->expire_tomorrow}}</span></h3>
                            <p class="text-light mb-1">{{__('Expire tomorrow')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-rounded-circle card-box bg-pink">
                <div class="row">
                    <div class="col-3">
                        <div class="avatar-md rounded-circle bg-light">
                            <i class="fe-play-circle font-28 avatar-title text-dark"></i>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="text-right">
                            <h3 class="text-light mt-1"><span data-plugin="counterup">{{$production->production_running}}</span></h3>
                            <p class="text-light mb-1">{{__('Production running')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-rounded-circle card-box bg-danger">
                <div class="row">
                    <div class="col-3">
                        <div class="avatar-md rounded-circle bg-light">
                            <i class="fe-alert-circle font-28 avatar-title text-danger"></i>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="text-right">
                            <h3 class="text-light mt-1"><span data-plugin="counterup">{{$production->expired}}</span></h3>
                            <p class="text-light mb-1">{{__('Expired')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script src="{{adminAsset('libs/jquery.counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{adminAsset('libs/waypoints/lib/jquery.waypoints.min.js')}}"></script>
@endsection

