<div id="product_items">
    @if(isset($products[0]))
        @php $total_price = 0; $total_vat = 0; @endphp

        @foreach($products as $product)
            @php
                $total_price = $total_price + $product->total_price;
                $total_vat = $total_vat + $product->total_vat;
            @endphp
            <div class="card-body mb-2 p-0" id="product-card-{{$product->id}}">
                <div class="card-header bg-soft-secondary text-dark p-1">
                    <div class="card-widgets">
{{--                        @if($product->purchased != STATUS_PURCHASED)--}}
{{--                            <button id="ordered_product-{{$product->id}}" class="btn btn-warning waves-effect waves-light btn-xs purchase_item" data-id="{{$product->id}}"><i class="fa fa-clone mr-1"></i>{{__('Purchase')}} </button>--}}
{{--                        @endif--}}
                        <span id="purchase_status_id-{{$product->id}}"
                              class="badge @if($product->purchased == STATUS_NOT_PURCHASED) badge-danger @elseif($product->purchased == STATUS_PARTIALLY_PURCHASED) badge-warning  @else badge-success @endif">
                            {{purchase_status($product->purchased)}} </span>
                    </div>
                    <h5 class="card-title my-1 pl-2 text-dark">@if(!empty($product->product_id)) {{$product->product->name}} - {{$product->product_reference}} @else {{$product->product_name}} @endif</h5>
                </div>
                @if (!empty($product->product_id ))
                    @php $combination = get_product_combination($product->product_id, $product->combination_id, $product->combination_type_id) @endphp
                @endif
                <div id="cardCollpase{{$product->id}}" class="collapse show">
                    <div class="row">
                        <div class="col-2 p-2">
                            <img width="" class="img-fluid img-thumbnail" src="{{isset($combination) ? $combination['image'] : ''}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                        </div>
                        <div class="col-10 p-1">
                            <p><b><i class="fa fa-calculator"></i> {{$product->quantity}} * {{getBrindeMoney($product->unit_price)}} = {{getBrindeMoney($product->total_price)}} ( + {{getBrindeMoney($product->total_vat)}} of VAT at {{getBrindeMoney($product->vat)}}%)</b></p>
                            <p>
                                <b>{{__('Quantity')}}:</b> {{$product->quantity}} &emsp;
                                <b>{{__('Price')}}:</b> {{getBrindeMoney($product->unit_price)}}  &emsp;
                                @if(isset($combination) && !empty($combination['combination']))
                                    <b>{{$combination['combination_type']}}:</b> {{$combination['combination']}}
                                @endif
                            </p>
                            <input type="hidden" name="product_id[]" value="{{$product->id}}">

                            <div class="switchery-demo">
                                <b>{{__('Purchase : ')}}</b>
                                <input type="checkbox" @if($product->purchased == STATUS_PURCHASED) checked @endif value="1" name="purchased[{{$product->id }}]" data-plugin="switchery" data-color="#1bb99a" data-secondary-color="#ff5d48" />
                            </div>
                        </div>
                        <div class="col-12">
                            <p><b>{{__('Additional Info')}}:</b> {{$product->additional_info}}</p>
                            <p><b>{{__('Description')}}:</b> {{$product->comments}}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
