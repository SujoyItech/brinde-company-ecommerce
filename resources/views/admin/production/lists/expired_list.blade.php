@extends('admin.layouts.app',['menu'=>'production','sub_menu'=>'productionExpired'])
@section('title', isset($title) ? $title : '')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">
                        <h4 class="header-title">{{__('Expired List')}}</h4>
                    </div>
                </div>
                <hr>
                @include('admin.search_blade.quotation_search')
                <hr>
                <table id="orderTable" class="table table-sm dt-responsive">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{__('Reference')}}</th>
                        <th>{{__('Commercial')}}</th>
                        <th>{{__('Store')}}</th>
                        <th>{{__('Products')}}</th>
                        <th>{{__('Date')}}</th>
                        <th>{{__('Stage')}}</th>
                        <th>{{__('Status')}}</th>
                        <th>{{__('Payment Status')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('productionExpired')}}";
            var data_column = [
                {"data": "updated_at", visible: false},
                {"data": "order_reference"},
                {"data": "assigned_salesman"},
                {"data": "brand_id"},
                {"data": "products", orderable: false, searchable: false},
                {"data": "expired_at"},
                {"data": "in_staging"},
                {"data": "status"},
                {"data": "payment_status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#orderTable'), data_url, data_column);
            searchDataTable($('#orderTable'), data_url, data_column);
            clearSearch($('#orderTable'), data_url, data_column);
        });
    </script>

@endsection
