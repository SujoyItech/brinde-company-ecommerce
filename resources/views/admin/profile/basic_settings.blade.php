<form action="{{route('updateProfile')}}" class="parsley-examples" method="POST" id="user_profile_update" enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">{{__('Name')}}</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter full name" value="{{$profile->name ?? ''}}" required >
        <div class="valid-feedback">
            {{__('Looks good!')}}
        </div>
    </div>
    <div class="form-group">
        <label for="email">{{__('Email')}}</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$profile->email ?? ''}}" required >
        <div class="valid-feedback">
            {{__('Looks good!')}}
        </div>
    </div>
    <div class="form-group">
        <label>{{__('Avatar')}}</label>
        <input type="file" name="profile_photo_path" data-plugins="dropify"
               data-default-file="{{getUserAvatar(\Illuminate\Support\Facades\Auth::user())}}"
               data-allowed-file-extensions="png jpg jpeg jfif"
               data-max-file-size="2M" />
        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
        <div class="valid-feedback">
            {{__('Looks good!')}}
        </div>
    </div>
    <input type="hidden" name="id" value="{{$profile->id ?? ''}}">
    <button type="submit" class="btn btn-dark waves-effect waves-light mt-2 submit_basic" data-style="zoom-in"><i class="fa fa-save"></i> {{__('Save')}}</button>
</form>
