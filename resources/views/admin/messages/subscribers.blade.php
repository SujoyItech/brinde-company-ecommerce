@extends('admin.layouts.app',['menu'=>'messages','sub_menu'=>'subscriberList'])
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4><i class="fas fa-boxes"></i> {{__('Subscriber List')}}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" class="send-subscription-message" id="send-subscription-message-id" novalidate action="{{route('sendMessageToAllSubscribers')}}">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="subject">{{__('Subject')}}</label>
                            <input name="subject" class="form-control" placeholder="{{__('Subject')}}" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="message">{{__('Messages')}}</label>
                            <textarea name="message" class="form-control" required placeholder="{{__('Write your message here.')}}"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary send-message"><i class="fa fa-reply-all"></i>&nbsp; {{__('Send')}}</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table id="subscriberTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('subscriberList')}}";
            var delete_url = "{{route('deleteSubscriber')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "name"},
                {"data": "email"},
                {"data": "status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#subscriberTable'), data_url, data_column,[[ 0, "desc" ]]);
            deleteOperation(function (response){
                if (response.success == true){
                    swalSuccess(response.message);
                    renderDataTable($('#subscriberTable'), data_url, data_column,[[ 0, "desc" ]]);
                }else {
                    swalError(response.message);
                }
            },'delete_item',delete_url);

            submitOperation(submitResponse, 'send-message');
            function submitResponse(response, this_form){
                if (response.success === true) {
                    swalRedirect("{{Request::url()}}",response.message,'success');
                } else {
                    swalError(response.message);
                }
            }
        });
    </script>

@endsection
