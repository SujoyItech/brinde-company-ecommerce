<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('User Add/Edit')}}</h4>
        <form class="user-form" novalidate action="{{route('saveUser')}}" method="post" id="user_save">
            <div class="form-group mb-3">
                <label for="name">{{__('Name')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$user->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="slug">{{__('Slug')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('userSlugCheck')}}" name="slug" id="slug" value="{{$user->slug ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="email">{{__('Email')}}<span class="text-danger">*</span></label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Module')}}<span class="text-danger">*</span></label>
                <select class="form-control" name="module_id" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{MODULE_USER_ADMIN}}" {{is_selected(MODULE_USER_ADMIN,$user->module_id ?? '')}}>{{__('Admin')}}</option>
                    <option value="{{MODULE_PRODUCT_MANAGEMENT}}" {{is_selected(MODULE_PRODUCT_MANAGEMENT,$user->module_id ?? '')}}>{{__('Product Manager')}}</option>
                    <option value="{{MODULE_PRODUCTION_MANAGEMENT}}" {{is_selected(MODULE_PRODUCTION_MANAGEMENT,$user->module_id ?? '')}}>{{__('Production Manager')}}</option>
                    <option value="{{MODULE_CRM_MANAGEMENT}}" {{is_selected(MODULE_CRM_MANAGEMENT,$user->module_id ?? '')}}>{{__('CRM Manager')}}</option>
                    <option value="{{MODULE_WEBSITE_MANAGEMENT}}" {{is_selected(MODULE_WEBSITE_MANAGEMENT,$user->module_id ?? '')}}>{{__('Website Manager')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Role')}}<span class="text-danger">*</span></label>
                <select class="form-control" name="role" required>
                    <option value="">{{__('Select')}}</option>
                    @if(isset($roles) && !empty($roles))
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" {{is_selected($role->id,$user->role ?? '')}}>{{$role->title}}</option>
                        @endforeach
                    @endif
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Status')}} <span class="text-danger">*</span></label>
                <select class="form-control" required name="status">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{INACTIVE}}" {{is_selected(INACTIVE,$user->status ?? '')}}>{{__('Inactive')}}</option>
                    <option value="{{ACTIVE}}" {{is_selected(ACTIVE,$user->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{USER_BLOCKED}}" {{is_selected(USER_BLOCKED,$user->status ?? '')}}>{{__('Block')}}</option>
                    <option value="{{USER_SUSPENDED}}" {{is_selected(USER_SUSPENDED,$user->status ?? '')}}>{{__('Suspend')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Brands')}}</label>
                <select class="selectpicker" multiple data-style="btn-light" name="brand_id[]">
                    <option value="">{{__('Select')}}</option>
                    @if(isset($brands))
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}" {{in_array($brand->id,$user_brands ?? []) ? 'selected': ''}}>{{$brand->name}}</option>
                        @endforeach
                    @endif
                </select>

                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$user->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form()"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        $('.selectpicker').selectpicker({

        });
        resetValidation('user-form');
        checkSlugVlaidity('user');
    })
</script>
