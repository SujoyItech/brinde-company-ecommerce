<div class="card-box">
    <h4 class="header-title">{{__('User List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the user list')}}
    </p>

    <div class="table-responsive">
        <table id="user_brand_table" class="table table-sm table-striped dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Email')}}</th>
                <th>{{__('Role')}}</th>
                <th>{{__('Brands')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).on('click','.check_brands',function (){
        let user_brand_data = {
            user_id : $(this).data('user-id'),
            brand_id : $(this).data('brand-id'),
            checked : $(this).prop('checked') == true ? 1 : 0
        }
        let user_brand_url = "{{route('userBrandCheck')}}";
        makeAjaxPost(user_brand_data,user_brand_url,null).done(function (){

        })
    })
</script>
