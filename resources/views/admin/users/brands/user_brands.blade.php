@extends('admin.layouts.app',['menu'=>'users','sub_menu'=>'user_brands'])
@section('content')
    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12 list">
        @include('admin.users.brands.user_brand_list')
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('userBrands')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "name"},
                {"data": "email"},
                {"data": "role_name",orderable: false, searchable: false},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#user_brand_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                } else {
                    swalError(response.message);
                }
            }

        });
    </script>
@endsection
