<div class="left-side-menu">
    <div class="h-100" >
        <div class="sidebar-content">
            <div class="sidebar-icon-menu h-100" data-simplebar>
                <!-- LOGO -->
                <a href="{{url('/')}}" class="logo">
                    <span>
                        <img src="{{isset($settings->app_logo_small) && !empty($settings->app_logo_small) ?  asset(get_image_path('settings').'/'.$settings->app_logo_small) : adminAsset('images/logo-sm.png')}}" alt="" height="28">
                    </span>
                </a>
                <nav class="nav flex-column" id="two-col-sidenav-main">
                    {!! mainMenuAppend('#product-management', __('Product Management'), 'fas fa-boxes fa-2x', $menu, 'product', [MODULE_SUPER_ADMIN, MODULE_USER_ADMIN, MODULE_PRODUCT_MANAGEMENT]) !!}
                    {!! mainMenuAppend('#crm-management', __('CRM Management'), 'fas fa-handshake fa-2x', $menu, 'crm', [MODULE_SUPER_ADMIN, MODULE_USER_ADMIN, MODULE_CRM_MANAGEMENT]) !!}
                    {!! mainMenuAppend('#production-management', __('Production Management'), 'fas fa-paint-roller fa-2x', $menu, 'production', [MODULE_SUPER_ADMIN, MODULE_USER_ADMIN, MODULE_PRODUCTION_MANAGEMENT]) !!}
                    {!! mainMenuAppend('#website-management', __('Website Management'), 'fas fa-globe fa-2x', $menu, 'website', [MODULE_SUPER_ADMIN, MODULE_USER_ADMIN, MODULE_WEBSITE_MANAGEMENT]) !!}
                    {!! mainMenuAppend('#user-management', __('User Management'), 'fas fa-users fa-2x', $menu, 'user', [MODULE_SUPER_ADMIN, MODULE_USER_ADMIN]) !!}
                    {!! mainMenuAppend('#settings', __('Settings'), 'fa fa-cogs fa-2x', $menu, 'settings') !!}
                    {!! mainMenuAppend('#messages', __('Messages'), 'fas fa-envelope fa-2x', $menu, 'messages',[MODULE_SUPER_ADMIN, MODULE_USER_ADMIN]) !!}
                </nav>
            </div>
            <!--- Sidemenu -->
            <div class="sidebar-main-menu">
                <div id="two-col-menu" class="h-100" data-simplebar>
                    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN) || check_permission(MODULE_PRODUCT_MANAGEMENT))
                        <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'product') d-block @endif" id="product-management">
                            @include('admin.includes.left_menus.product_management')
                        </div>
                    @endif
                    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN) || check_permission(MODULE_CRM_MANAGEMENT))
                        <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'crm') d-block @endif" id="crm-management">
                            @include('admin.includes.left_menus.crm_management')
                        </div>
                    @endif
                    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN) || check_permission(MODULE_PRODUCTION_MANAGEMENT))
                        <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'production') d-block @endif" id="production-management">
                            @include('admin.includes.left_menus.production_management')
                        </div>
                    @endif
                    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN) || check_permission(MODULE_WEBSITE_MANAGEMENT))
                        <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'website') d-block @endif" id="website-management">
                            @include('admin.includes.left_menus.website_management')
                        </div>
                    @endif
                    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN))
                        <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'user') d-block @endif" id="user-management">
                            @include('admin.includes.left_menus.user_management')
                        </div>
                    @endif
                    <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'settings') d-block @endif" id="settings">
                        @include('admin.includes.left_menus.settings')
                    </div>
                    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN))
                        <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'messages') d-block @endif" id="messages">
                            @include('admin.includes.left_menus.message')
                        </div>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- End Sidebar -->

    </div>
    <!-- Sidebar -left -->

</div>

<script>
    $(function (){
        getProductionCount();
    })
    function getProductionCount(){

        let in_shipment = "{{$getProductionCount->in_shipment ?? 0}}";
        let completed = "{{$getProductionCount->production_completed ?? 0}}";
        let production_running = "{{$getProductionCount->production_running ?? 0}}";
        let expire_today = "{{$getProductionCount->expire_today ?? 0}}";
        let expire_tomorrow = "{{$getProductionCount->expire_tomorrow ?? 0}}";
        let expired = "{{$getProductionCount->expired ?? 0}}";

        $('.in_shipment').html(in_shipment);
        $('.completed').html(completed);
        $('.production_running').html(production_running);
        $('.expire_today').html(expire_today);
        $('.expire_tomorrow').html(expire_tomorrow);
        $('.expired').html(expired);
    }
</script>
