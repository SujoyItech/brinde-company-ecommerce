<h5 class="menu-title"><i class="fas fa-envelope fa-2x"></i> {{__('Messages')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    {!! menuLiAppend('subscriberList', __('Subscriber'), 'fas fa fa-bell', $sub_menu,'subscriberList',TRUE) !!}
    {!! menuLiAppend('messages', __('Messages'), 'fa fa-inbox', $sub_menu,'messages',TRUE) !!}
</ul>
