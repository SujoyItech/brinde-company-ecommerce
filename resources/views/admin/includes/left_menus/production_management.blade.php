<h5 class="menu-title"><i class="fas fa-paint-roller fa-2x"></i> {{__('Production')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif">
        <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif" href="{{route('productionManagerHome')}}"><i class="fa fa-home"></i>&nbsp;{{__('Dashboard')}}</a>
    </li>
    {{-- route_name -- menu_title -- icon_class -- $sub_menu(key to check) -- submenu_to_compare -- admin_permission --}}
    {!! menuLiAppend('productionOrderList', __('Order List'), 'fa fa-shopping-cart', $sub_menu, 'order_list', TRUE) !!}
    {!! menuLiAppend('productionPurchaseList', __('Purchase List'), 'fas fa-cash-register', $sub_menu, 'purchase_list', TRUE) !!}
    {!! menuLiAppend('productionList', __('In Production List'), 'fa fa-play', $sub_menu, 'productionList', TRUE, 'badge-pink production_running') !!}
    {!! menuLiAppend('productionCompletedList', __('Completed'), 'fa fa-check-circle', $sub_menu, 'productionCompletedList', TRUE,'badge-success completed') !!}
    {!! menuLiAppend('productionShipmentList', __('In Shipment'), 'fa fa-truck', $sub_menu, 'productionShipmentList', TRUE, 'badge-info in_shipment' ) !!}
    {!! menuLiAppend('productionExpireToday', __('Expire Today'), 'fa fa-fire', $sub_menu, 'productionExpireToday', TRUE, 'badge-warning expire_today' ) !!}
    {!! menuLiAppend('productionExpireTomorrow', __('Expire Tomorrow'), 'fa fa-bell', $sub_menu, 'productionExpireTomorrow', TRUE, 'badge-secondary expire_tomorrow' ) !!}
    {!! menuLiAppend('productionExpired', __('Expired'), 'fa fa-exclamation-circle', $sub_menu, 'productionExpired', TRUE, 'badge-danger expired' ) !!}
</ul>
