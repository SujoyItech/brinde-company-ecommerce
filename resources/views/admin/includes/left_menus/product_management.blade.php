<h5 class="menu-title"><i class="fas fa-boxes fa-2x"></i> {{__('Products')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif">
        <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif" href="{{route('productManagerHome')}}"><i class="fa fa-home"></i>&nbsp;{{__('Dashboard')}}</a>
    </li>
    {!! menuLiAppend('categories', __('Categories'), 'fa fa-box', $sub_menu, 'categories', TRUE) !!}
    {!! menuLiAppend('supplier', __('Supplier'), 'fab fa-blackberry', $sub_menu, 'supplier', TRUE) !!}
    {!! menuLiAppend('deliveryType', __('Delivery Type'), 'fas fa-tags', $sub_menu, 'deliveryType', TRUE) !!}
    {!! menuLiAppend('combinationType', __('Combination Type'), 'fas fa-tags', $sub_menu, 'combinationType', TRUE) !!}
    {!! menuLiAppend('combination', __('Combination'), 'fas fa-tags', $sub_menu, 'combination', TRUE) !!}
    {!! menuLiAppend('tags', __('Tags'), 'fas fa-tag', $sub_menu, 'tags', TRUE) !!}
    {!! menuLiAppend('brands', __('Brands'), 'fas fa-store', $sub_menu, 'brands', TRUE) !!}
    {!! menuLiAppend('product', __('Products'), 'fas fa-boxes', $sub_menu, 'product', TRUE) !!}
    {!! menuLiAppend('productUpload', __('Products Upload'), 'fas fa-upload', $sub_menu, 'productUpload', TRUE) !!}

</ul>
