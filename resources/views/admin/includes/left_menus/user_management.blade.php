<h5 class="menu-title"><i class="fas fa-users fa-2x"></i> {{__('Users')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    {!! menuLiAppend('users', __('Admin Users'), 'fas fa-user-tie', $sub_menu,'users',TRUE) !!}
    {!! menuLiAppend('roles', __('Roles + Permission'), 'fa fa-user-secret', $sub_menu,'roles',TRUE) !!}
    {!! menuLiAppend('userBrands', __('User Brands'), 'fas fa-store', $sub_menu,'userBrands',TRUE) !!}
</ul>
