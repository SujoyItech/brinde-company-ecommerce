<h5 class="menu-title"><i class="fa fa-cogs fa-2x"></i> {{__('Settings')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    {!! menuLiAppend('profile', __('Profile Settings'), 'fa fa-user',TRUE) !!}
    @if(check_permission(MODULE_SUPER_ADMIN)|| check_permission(MODULE_USER_ADMIN))
    {!! menuLiAppend('commandSettings', __('Commands'), 'fa fa-terminal', $sub_menu, 'command_setting',TRUE) !!}
    @endif
</ul>
