<h5 class="menu-title"><i class="fas fa-handshake fa-2x"></i> {{__('CRM')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif">
        <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif" href="{{route('crmManagerHome')}}"><i class="fa fa-home"></i>&nbsp;{{__('Dashboard')}}</a>
    </li>
    {{-- route_name -- menu_title -- icon_class -- $sub_menu(key to check) -- submenu_to_compare -- admin_permission --}}
    {!! menuLiAppend('crmPaymentCondition', __('Payment condition'), 'fa fa-box', $sub_menu, 'payment_condition', TRUE) !!}
    {!! menuLiAppend('crmPrintingOption', __('Printing Option'), 'fa fa-box', $sub_menu, 'printing_option', TRUE) !!}
    {!! menuLiAppend('crmCustomerList', __('Customer List'), 'fa fa-box', $sub_menu, 'customer', TRUE) !!}
    {!! menuLiAppend('CrmQuotationList', __('Quotations'), 'fa fa-box', $sub_menu, 'quotation', TRUE) !!}
    {!! menuLiAppend('crmBudgetList', __('Budgets'), 'fa fa-box', $sub_menu, 'budget', TRUE) !!}
    {!! menuLiAppend('crmAllList', __('All'), 'fa fa-box', $sub_menu, 'crmAllList', TRUE) !!}
</ul>
