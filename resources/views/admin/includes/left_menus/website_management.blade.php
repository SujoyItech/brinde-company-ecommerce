<h5 class="menu-title"><i class="fas fa-globe fa-2x"></i> {{__('Website')}}</h5>
<hr class="my-1">
<ul class="nav flex-column">
    {!! menuLiAppend('websitePages', __('Website Pages'), 'fa fa-box', $sub_menu, 'website_pages', TRUE) !!}
    {!! menuLiAppend('websiteCategories', __('Website Categories'), 'fa fa-box', $sub_menu, 'website_categories', TRUE) !!}
    <li class="nav-item">
        <a href="#web-contents" data-toggle="collapse" class="nav-link">
            <span><i class="fa fa-file"></i>  {{__('Web Contents')}} </span>
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse" id="web-contents">
            <ul class="nav-second-level">
                {!! menuLiAppend('termsConditionSettings', __('Terms & Condition'), 'fa fa-file', $sub_menu, 'termsConditionSettings',TRUE) !!}
                {!! menuLiAppend('privacyPolicySettings', __('Privacy Policy'), 'fa fa-file',$sub_menu, 'privacyPolicySettings',TRUE) !!}
                {!! menuLiAppend('aboutUsSettings', __('About Us'), 'fa fa-file',$sub_menu, 'aboutUsSettings',TRUE) !!}
                {!! menuLiAppend('whyChooseUsSettings', __('Agent Wanted'), 'fa fa-file',$sub_menu, 'whyChooseUsSettings',TRUE) !!}
                {!! menuLiAppend('faqsSettings', __('FAQs'), 'fa fa-file',$sub_menu, 'faqsSettings',TRUE) !!}
                {!! menuLiAppend('teamSettings',__('Teams'),'fa fa-file',$sub_menu, 'teamSettings',TRUE,) !!}
                {!! menuLiAppend('contactSettings',__('Contacts'),'fa fa-file',$sub_menu, 'contactSettings',TRUE,) !!}
            </ul>
        </div>
    </li>
    <li class="nav-item">
        <a href="#general-settings @if(!empty($sub_menu) && in_array($sub_menu,['siteSettings','logoSettings','applicationSetting','socialSettings'])) show @endif" data-toggle="collapse" class="nav-link">
            <span><i class="fa fa-cog"></i>  {{__('General Settings')}} </span>
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse" id="general-settings">
            <ul class="nav-second-level">
                {{-- route_name -- menu_title -- icon_class -- $sub_menu(key to check) -- submenu_to_compare --}}
                {!! menuLiAppend('applicationSetting', __('Application Settings'), 'fa fa-link', $sub_menu,'applicationSetting',TRUE) !!}
                {!! menuLiAppend('siteSetting', __('Admin Site Settings'), 'fa fa-globe', $sub_menu,'siteSetting',TRUE) !!}
                {!! menuLiAppend('logoSetting', __('Logo Settings'), 'fab fa-react', $sub_menu,'logoSetting',TRUE) !!}
                {!! menuLiAppend('socialSetting', __('Social Settings'), 'fa fa-link', $sub_menu, 'socialSettings',TRUE) !!}
            </ul>
        </div>
    </li>

</ul>
