<div class="right-bar">
    <div data-simplebar class="h-100">
        <div class="p-3">
            <img src="{{getUserAvatar(\Illuminate\Support\Facades\Auth::user())}}" class="rounded avatar-xxl img-thumbnail" alt="profile-image" id="profile_image">
            <h4 class="">{{\Illuminate\Support\Facades\Auth::user()->name ?? ''}}</h4>
            <h5 class="">{{\Illuminate\Support\Facades\Auth::user()->email ?? ''}}</h5>
            <hr/>
            <h5><a href="{{ route('profile') }}" class="text-dark"> <i class="fe-user"></i> {{__('Profile Settings')}} </a></h5>
            <h5><a href="{{ route('profile') }}" class="text-dark"> <i class="fe-lock"></i> {{__('Password Change')}} </a></h5>
            <div class="btn-group">
                @php
                    $user = Auth::user()
                @endphp
                <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if($user->language == 'en')
                        <img src="{{adminAsset('images/flags/gb.svg')}}" alt="" width="15"> {{__('English')}} <span class="caret text-right"></span>
                    @elseif($user->language == 'pt')
                        <img src="{{adminAsset('images/flags/pt.svg')}}" alt="" width="15"> {{__('Portuguese')}} <span class="caret text-right"></span>
                    @else
                        <img src="{{adminAsset('images/flags/gb.svg')}}" alt="" width="15"> {{__('English')}} <span class="caret text-right"></span>
                    @endif
                    <i class="mdi mdi-chevron-down"></i>
                </button>
                <div class="dropdown-menu" style="">
                    <a class="dropdown-item" href="{{url('admin/lang/en')}}"><img src="{{adminAsset('images/flags/gb.svg')}}" alt="" width="15"> {{__('English')}}</a>
                    <a class="dropdown-item" href="{{url('admin/lang/pt')}}"><img src="{{adminAsset('images/flags/pt.svg')}}" alt="" width="15"> {{__('Portuguese')}}</a>
                </div>
            </div>
            <hr/>
            <h5>
                <div class="py-2 border-t border-theme-40 dark:border-dark-3">
                    <a href="{{ route('logout') }}" class="text-danger"> <i class="fa fa-sign-out-alt"></i> {{__('Logout')}} </a>
                </div>
            </h5>
        </div>
    </div> <!-- end slimscroll-menu-->
</div>

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
