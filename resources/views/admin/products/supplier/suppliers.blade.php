@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'supplier'])
@section('content')
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 add_edit">
        @include('admin.products.supplier.supplier_add')
    </div>
    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-12 col-xs-12">
        @include('admin.products.supplier.supplier_list')
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('supplier')}}";
            var delete_url = "{{route('deleteSupplier')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "name"},
                {"data": "discount"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#supplier_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,"{{route('editSupplier')}}");

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    renderDataTable($('#supplier_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#supplier_table'),data_url,data_column);
                }
            }
        });
    </script>
@endsection
