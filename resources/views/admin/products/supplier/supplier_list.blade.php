<div class="card-box">
    <h4 class="header-title">{{__('Supplier List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the supplier list')}}
    </p>

    <div class="table-responsive">
        <table id="supplier_table" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Discount')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
