<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Supplier Add/Edit')}}</h4>
        <form class="supplier-form" novalidate action="{{route('saveSupplier')}}" method="post" id="supplier_save">
            <div class="form-group mb-3">
                <label for="name">{{__('Supplier Name')}}</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Supplier Name" value="{{$supplier->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="validationCustom02">{{__('Discount Percentage')}}</label>
                <input type="number" class="form-control" id="validationCustom02" placeholder=" Ex.0.01"  required step="0.01" min="0" max="99.99" name="discount" value="{{$supplier->discount ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="validationCustom03">{{__('Status')}}</label>
                <select class="form-control" id="validationCustom03" required name="status">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$supplier->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$supplier->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$supplier->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form()"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('supplier-form');
    })
</script>
