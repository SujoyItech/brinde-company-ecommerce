<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Brand  Add/Edit')}}</h4>
        <form class="brands-form" novalidate method="post" action="{{route('saveBrand')}}" id="brand_form">
            <div class="form-group mb-2">
                <label for="name">{{__('Name')}}</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Name')}}" value="{{$brand->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="slug">{{__('Slug')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('brandSlugCheck')}}" name="slug" id="slug" value="{{$brand->slug ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="store_url">{{__('Brand url')}}</label>
                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="email">{{__('Email')}}</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="{{__('Email')}}" value="{{$brand->email ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="phone">{{__('Phone')}}</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="{{__('Phone')}}" value="{{$brand->phone ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="address">{{__('Address')}}</label>
                <input type="text" class="form-control" name="address" id="address" placeholder="{{__('Address')}}" value="{{$brand->address ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="privacy_policy_link">{{__('Privacy Policy Link')}}</label>
                <input type="text" class="form-control" name="privacy_policy_link" id="privacy_policy_link" placeholder="{{__('Privacy policy link')}}" value="{{$brand->privacy_policy_link ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="faq_link">{{__('Faq Link')}}</label>
                <input type="text" class="form-control" name="faq_link" id="faq_link" placeholder="{{__('Faq link')}}" value="{{$brand->faq_link ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label class="col-form-label" for="icon">{{__('Icon')}}</label>
                <input type="file" name="icon" id="icon" class="dropify" data-allowed-file-extensions="png jpg jpeg jfif" data-max-file-size="1M"
                       data-default-file="{{isset($brand->icon) ? asset(get_image_path('brand').'/'.$brand->icon) : ''}}"/>
            </div>
            <div class="form-group mb-2 color">
                <label>{{__('Color code (only for colors)')}}</label>
                <input type="color" class="form-control" name="color_code" id="color_code" value="{{$brand->color_code ?? ''}}">
            </div>
            <div class="form-group mb-2">
                <label>{{__('Status')}}</label>
                <select class="form-control" name="status" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$brand->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$brand->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="checkbox checkbox-success mb-2">
                    <input id="isReseller" type="checkbox" name="is_reseller" {{isset($brand->is_reseller) && $brand->is_reseller == TRUE ? 'checked' : ''}}>
                    <label for="isReseller">
                        {{__('Is reseller')}}
                    </label>
                </div>
            </div>
            @if(isset($brand->id))
            <input type="hidden" name="id" value="{{$brand->id ?? ''}}" id="id">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
{{--            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(true,function (){ $('input:checkbox').removeAttr('checked') })"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>--}}
            @endif

        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('brands-form');
        checkSlugVlaidity('user');
    });
</script>
