@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="card-box">
            <h4 class="header-title">{{__('Product Review List')}}</h4>
            <p class="sub-header">
                Here goes the product category list
            </p>
            <div class="row mb-3">
                <div class="col-lg-4">
                    <select class="selectpicker mb-0" data-style="btn-outline-secondary">
                        <option>Search By Products</option>
                        <option>Black</option>
                        <option>Silver</option>
                        <option>Blue</option>
                    </select>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-sm mb-0">
                    <thead class="bg-secondary text-light">
                    <tr>
                        <th>User Information</th>
                        <th>Ratings</th>
                        <th width="70%">Comments</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-4.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">4.5</td>
                        <td>Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-2.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">3</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-3.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">4</td>
                        <td>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release versions of Lorem Ipsum.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-4.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">4.5</td>
                        <td>Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-2.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">3</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-3.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">4</td>
                        <td>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release versions of Lorem Ipsum.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-4.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">4.5</td>
                        <td>Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-2.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">3</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    <tr>
                        <td scope="row">
                            <img src="http://127.0.0.1:8000/admin/images/users/user-3.jpg" alt="user-image" class="avatar-xs"> Nirjhar Mandal <span class="text-muted">nirjhar@gmail.com</span>
                        </td>
                        <td scope="row">4</td>
                        <td>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release versions of Lorem Ipsum.</td>
                        <td class="text-center"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fas fa-ban"></i>&nbsp;&nbsp;<i class="mdi mdi-android-messages"></i></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
