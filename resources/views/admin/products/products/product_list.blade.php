@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'product'])
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4><i class="fas fa-boxes"></i> {{__('Product List')}}</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="text-right">
                            <button class="btn btn-dark add_product"><i class="fa fa-plus-square"></i> {{__('Product-insert')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" id="search_product_form">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="reference">{{__('Reference')}}</label>
                            <input type="text" name="reference" class="form-control">
                        </div>
                        <div class="form-group col-md-3">
                            <label>{{__('Name/Description')}}</label>
                            <input type="text" name="name_description" placeholder="" class="form-control">
                        </div>
                        <div class="form-group col-md-3">
                            <label>{{__('Special cases')}}</label>
                            <select name="special_case" class="form-control">
                                <option value="">{{__('none')}}</option>
                                <option value="n_ct">{{__('No categories')}}</option>
                                <option value="n_t">{{__('No tags')}}</option>
                                <option value="n_cr">{{__('No colors')}}</option>
                                <option value="n_m">{{__('No media')}}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>{{__('Supplier')}}</label>
                            <select name="supplier_id" class="form-control">
                                <option value="">{{__('All')}}</option>
                                @if(isset($suppliers) && !empty($suppliers[0]))
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>{{__('Categories')}}</label>
                            <select name="category_id" class="form-control">
                                <option value="">{{__('All')}}</option>
                                @if(isset($categories) && !empty($categories[0]))
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>{{__('Active')}}</label>
                            <select name="status" class="form-control">
                                <option value="">{{__('All')}}</option>
                                <option value="{{STATUS_ACTIVE}}">{{__('Active')}}</option>
                                <option value="{{STATUS_PROCESSING}}">{{__('Inactive')}}</option>
                            </select>
                        </div>
                        <div class="col-md-4 listing-search">
                            <div class="col-md-7">
                                <button type="submit" class="btn btn-primary btn-block search_product"><i class="fa fa-search"></i>&nbsp; {{__('Apply Filter')}}</button>
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-soft-secondary btn-block text-dark clear_search"><i class="fe-refresh-ccw"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table id="productTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Reference')}}</th>
                            <th>{{__('Description')}}</th>
                            <th>{{__('Supplier')}}</th>
                            <th>{{__('Tags')}}</th>
                            <th>{{__('Colors')}}</th>
{{--                            <th>{{__('Size')}}</th>--}}
                            <th>{{__('Active?')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('admin.products.products.add_product_modal')
    @include('admin.products.products.duplicate_product_modal')
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('product')}}";
            var delete_url = "{{route('deleteProduct')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "icon", orderable: false, searchable: false},
                {"data": "name",orderable:true},
                {"data": "reference"},
                {"data": "description"},
                {"data": "supplier_name", searchable: false},
                {"data": "tag_names", searchable: false},
                {"data": "colors", searchable: false},
                /*{"data": "size", searchable: false},*/
                {"data": "status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#productTable'), data_url, data_column,[[ 0, "desc" ]],10);
            $(document).on('click','.search_product',function (){
                var form_id = $(this).closest('form').attr('id');
                var this_form = $('#' + form_id);
                $(this_form).on('submit', function (e) {
                    if (!e.isDefaultPrevented()) {
                        e.preventDefault();
                        var formData = $(this).serializeArray();
                        renderDataTableWithData($('#productTable'),data_url,data_column,formData,[[ 0, "desc" ]],10);
                    }
                });
            })

            $(document).on('click','.clear_search',function (){
                var this_form = $(this).closest('form')[0];
                this_form.reset();
                renderDataTable($('#productTable'), data_url, data_column,[[ 0, "desc" ]],10);
            });

            statusChangeOperation(function (response){
                if (response.success === true){
                    swalSuccess(response.message);
                }else {
                    swalError(response.message);
                }
                renderDataTable($('#productTable'), data_url, data_column,[[ 0, "desc" ]],10);
            },'status_change',"{{route('productStatusChange')}}")

            $(document).on('click','.add_product',function (){
                $('#addProductModal').modal('show');
            });

            $(document).on('click','.duplicate_item',function (){
                let old_product_id = $(this).data('id');
                let name = $(this).data('name');
                $("#old_product_id_in").val(old_product_id);
                $("#dup-name").val(name);
                $('#duplicateProductModal').modal('show');
            });

            submitOperation(submitResponse, 'create-product');
            submitOperation(submitResponse, 'duplicate-product');
            function submitResponse(response, this_form){
                if (response.success === true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    this_form.removeClass('was-validated');
                    window.location.href = '{{route('editProduct', ':reference')}}'.replace(':reference', response.data.reference);
                } else {
                    swalError(response.message);
                }
            }

        });
    </script>

@endsection
