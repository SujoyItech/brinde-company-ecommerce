<div id="duplicateProductModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form novalidate class="duplicate_product-form" method="post" action="{{route('duplicateProduct')}}" id="duplicate_product">
                <input type="hidden" id="old_product_id_in" name="old_product_id">
                <div class="modal-header pl-4 pb-0">
                    <h4 class="modal-title text-center">{{__('Duplicate product')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-style="zoom-in">×</button>
                </div>
                <hr>
                <div class="pl-4 pr-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="control-label">{{__('Name')}} <span class="text-danger">*</span></label>
                                <input type="text" id="dup-name" name="name" class="form-control" required placeholder="{{__('Name')}}">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="slug">{{__('Reference')}} <span class="text-danger">*</span></label>
                                <input type="text" class="form-control check_reference_validity" data-referenceforid="dup-name" data-exceptvalueid="id"
                                       data-referencevalidateurl="{{route('productReferenceCheck')}}" name="reference" id="dup-reference"
                                       placeholder="{{__('Enter a unique reference string. Can\'t use space and special characters.')}}"
                                       required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light text-dark waves-effect" data-dismiss="modal" data-style="zoom-in">{{__('Cancel')}}</button>
                    <button type="submit" class="duplicate-product btn btn-dark waves-effect waves-light" data-style="zoom-in"><i class="fa fa-save"></i> {{__('Save')}}</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        resetValidation('duplicate_product-form')
        checkReferenceValidity();
    });
</script>

