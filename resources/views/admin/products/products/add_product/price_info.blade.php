<form novalidate action="{{route('saveProductPricing')}}" method="post" id="pricing-form" class="pricing-form">
    <input type="hidden" name="product_id" value="{{$product->id}}">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{__('Quantities and prices')}}</h4>
                </div>
                <div class="col-md-6">
                    <div class="text-right">
                        <button class="btn btn-dark submit-pricing" type="submit"><i class="fa fa-save"></i> {{__('Save quantities and price')}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if(count($brands) > 0)
                @foreach($brands as $index => $brand)
                    @if($index === 0)
                        @include('admin.products.products.add_product.price.first_brand_pricing')
                    @else
                        @include('admin.products.products.add_product.price.other_brands_pricing')
                    @endif
                @endforeach
            @else
                <p class="card-text mt-2 mb-3">
                    <strong>{{__('Note: ')}}</strong>{{__('No Brands found. Please add Brands first.')}}
                </p>
            @endif
        </div>
    </div>
</form>
<script>
    $(document).ready(function () {
        resetValidation('pricing-form');
    });
</script>
