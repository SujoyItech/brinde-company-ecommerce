<form novalidate class="product-general-form" method="post" action="{{route('saveProduct')}}" id="product-general-form">
    <input type="hidden" name="id" value="{{$product->id ?? ''}}">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{__('General information')}}</h4>
                </div>
                <div class="col-md-6">
                    <div class="text-right">
                        <button class="btn btn-dark submit-general" type="submit" data-style="zoom-in"><i class="fa fa-save"></i> {{__('Save general information')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                <h5 class="card-title">{{__('Name (required)')}}</h5>
                <input type="text" id="name" name="name" class="form-control" value="{{$product->name}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <div class="form-group">
                <h5 class="card-title">{{__('Description')}}</h5>
                <textarea class="form-control" name="description" rows="3" placeholder="{{__('Description here.')}}">{{$product->description}}</textarea>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Complementary information')}}</h5>
                <textarea class="form-control" name="additional_info" rows="3" placeholder="{{__('Complementary information here.')}}">{{$product->additional_info}}</textarea>
                <p class="card-text mt-2">{{__('Measurements sizes,weights and more technical information.')}}</p>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Supplier')}}</h5>
                <select class="form-control" name="supplier_id" required>
                    <option value="">--Select--</option>
                    @foreach($suppliers as $supplier)
                        <option @if($product->supplier_id === $supplier->id) selected @endif value="{{$supplier->id}}">{{$supplier->name}}</option>
                    @endforeach
                </select>
                <p class="card-text mt-2"><strong>{{__('Note: ')}}</strong>{{__('the supplier has an influence on the prices of the item in the Re-seller type store.')}}</p>
                <p class="card-text"><strong class="text-warning">{{__('WARNING: ')}}</strong>{{__('If you change the supplier, save the general information and after
                that reload the page so that the price values of the stores are updated.')}}</p>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Delivery type')}}</h5>
                <select class="form-control" name="delivery_type_id" required>
                    <option value="">--Select--</option>
                    @foreach($deliveryTypes as $deliveryType)
                        <option @if($product->delivery_type_id === $deliveryType->id) selected @endif value="{{$deliveryType->id}}">{{$deliveryType->name}}</option>
                    @endforeach
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Status')}}</h5>
                <input type="checkbox" @if($product->status === ACTIVE) checked @endif data-plugin="switchery" name="status" data-color="#1bb99a"/>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <h5><strong>{{__('Product status for')}}</strong></h5>
            <p class="card-text mt-2"><strong>{{__('Note: ')}}</strong>{{__('This product will be active for the following brand.')}}</p>
            <div class="row">
                @if (isset($brands) && !empty($brands))
                    @foreach($brands as $brand)
                        <div class="col-md-6">
                            <div class="form-group">
                                <h5 class="card-title">{{$brand->name}}</h5>
                                <input type="checkbox" data-plugin="switchery" name="brand_id[]" data-color="#1bb99a" value="{{$brand->id}}" @if (in_array($brand->id,$product_brands ?? [])) checked @endif/>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</form>
<script>
    $(document).ready(function (){
        resetValidation('product-general-form');
    });
</script>

