<div>
    <h5>{{__($brand->name)}}</h5>
    <p class="card-text mt-2 mb-3">
        <strong>{{__('Note: ')}}</strong>{{__('All other store prices are based on this one. They will be change if they are.')}}
    </p>
    <div id="brand-div-{{$brand->id}}">
        @foreach($pricings as $index => $pricing)
            <div class="row pricing-row-{{$index}}">
                <div class="col-md-3">
                    <input type="hidden" name="compare[]" id="compare-input-{{$index}}"
                           value="{{$pricing->compare}}">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" data-compare="more" data-index="{{$index}}"
                                class="more-button-{{$index}} compare-clickable btn
                            @if($pricing->compare === MORE) btn-primary @else btn-white @endif">+
                        </button>
                        <button type="button" data-compare="equal" data-index="{{$index}}"
                                class="equal-button-{{$index}} compare-clickable btn
                            @if($pricing->compare === EQUAL) btn-primary @else btn-white @endif">=
                        </button>
                        <button type="button" data-compare="less" data-index="{{$index}}"
                                class="less-button-{{$index}} compare-clickable btn
                            @if($pricing->compare === LESS) btn-primary @else btn-white @endif">-
                        </button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i
                                        class="fas fa-tachometer-alt"></i></span>
                        </div>
                        <input type="number" required name="quantity[]" min="0" data-index="{{$index}}"
                               id="quantity-input-{{$index}}" class="form-control quantity-input-{{$index}}"
                               placeholder="{{__('Enter quantity')}}" value="{{$pricing->quantity}}"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-euro-sign"></i></span>
                        </div>
                        @if($brand->is_reseller)
                            @php
                                $discountAmount = $pricing->price * $supplier->discount/100;
                                $price = number_format($pricing->price - $discountAmount, 2, '.','');
                            @endphp
                        @else
                            @php
                                $price = $pricing->price;
                            @endphp
                        @endif
                        <input type="number" required name="price[]" min="0" step="0.01" data-brand="{{$brand}}" data-index="{{$index}}"
                               id="price-input-{{$index}}"
                               class="form-control price-input-{{$index}}"
                               placeholder="{{__('Enter price')}}" value="{{$price}}"
                               aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-md-1">
                    <a href="javascript:" data-index="{{$index}}" class="btn btn-xs btn-danger delete_row"><i
                            class="fa fa-trash"></i></a>
                </div>
            </div>
        @endforeach
    </div>
    <a href="javascript:" class="btn btn-secondary add_more"><i class="fa fa-plus"></i> {{__('Add more')}}</a>
</div>
<hr>

<script>
    let indexCount = {{count($pricings)}};
    let supplier = {};
    let brands = JSON.parse('{!!$brands!!}');

    function loadSupplier() {
        $.ajax({
            url: '{{route('getProductSupplier')}}',
            method: 'post',
            async: false,
            data: {
                id: {{$product->id}}
            }
        }).done(function (response) {
            if (response.success) {
                supplier = response.supplier;
            }
        }).fail(function (error) {
            console.log(error.responseText);
        });
    }


    $(document).on('click', '.add_more', function () {
        loadSupplier();
        if (supplier.name === undefined) {
            alert("{{__('Add a supplier to the product first.')}}");
            return
        }

        indexCount++;

        //generate first brand row
        let firstBrandPricingRowHtml = `<div class="row pricing-row-` + indexCount + `">
                                            <div class="col-md-3">
                                                <input type="hidden" name="compare[]" id="compare-input-` + indexCount + `" value="more">
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="button" data-compare="more" data-index="` + indexCount + `" class="more-button-` + indexCount + ` compare-clickable btn
                                                    btn-primary ">+</button>
                                                    <button type="button" data-compare="equal" data-index="` + indexCount + `" class="equal-button-` + indexCount + ` compare-clickable btn
                                                    btn-white ">=</button>
                                                    <button type="button" data-compare="less" data-index="` + indexCount + `" class="less-button-` + indexCount + ` compare-clickable btn
                                                    btn-white ">-</button>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-tachometer-alt"></i></span>
                                                    </div>
                                                    <input type="number" required name="quantity[]" min="0" data-index="` + indexCount + `" id="quantity-input-` + indexCount + `"
                                                    class="form-control quantity-input-` + indexCount + `" placeholder="`+'{{__('Enter quantity')}}'+`"
                                                    aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-euro-sign"></i></span>
                                                    </div>
                                                    <input type="number" step="0.01" required name="price[]"  min="0" data-brand='`+JSON.stringify(brands[0])+`' data-index="` + indexCount + `"
                                                    id="price-input-` + indexCount + `" class="form-control price-input-` + indexCount + `"
                                                    placeholder="`+'{{__('Enter price')}}'+`" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <a href="javascript:" data-index="` + indexCount + `" class="btn btn-xs btn-danger delete_row">
                                                <i class="fa fa-trash"></i></a>
                                            </div>
                                        </div>`;
        $('#brand-div-'+brands[0].id).append(firstBrandPricingRowHtml);


        //generate others brand row
        for(let i=1; i<brands.length; i++) {
            let firstBrandPricingRowHtml = `<div class="row justify-content-center pricing-row-` + indexCount + `">
                                                <div class="col-md-4">
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <button type="button" data-compare="more" data-index="` + indexCount + `" class="more-button-` + indexCount + ` compare-disabled btn
                                                        btn-primary ">+</button>
                                                        <button type="button" data-compare="equal" data-index="` + indexCount + `" class="equal-button-` + indexCount + ` compare-disabled btn
                                                        btn-white ">=</button>
                                                        <button type="button" data-compare="less" data-index="` + indexCount + `" class="less-button-` + indexCount + ` compare-disabled btn
                                                        btn-white ">-</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-tachometer-alt"></i></span>
                                                        </div>
                                                        <input type="text" disabled data-index="` + indexCount + `" class="form-control quantity-input-`
                                                            + indexCount + `" placeholder="`+'{{__('Enter quantity')}}'+`" aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-euro-sign"></i></span>
                                                        </div>
                                                        <input type="text" disabled data-brand='`+JSON.stringify(brands[i])+`' data-index="` + indexCount + `"
                                                            class="form-control price-input-` + indexCount + `"
                                                        placeholder="`+'{{__('Enter price')}}'+`" aria-describedby="basic-addon1">
                                                    </div>
                                                </div>

                                            </div>`;
            $('#brand-div-'+brands[i].id).append(firstBrandPricingRowHtml);
        }

    });

    $(document).on('click', '.delete_row', function () {
        let index = $(this).data('index');
        $(".pricing-row-"+index).remove();
    })

    $(document).on("click", ".compare-clickable", function () {
        //find previous active compare button and make inactive
        let prevActiveCompareElem = $(this).siblings(".btn-primary");
        let currentIndex = prevActiveCompareElem.data('index');
        let prevActiveCompareType = prevActiveCompareElem.data('compare');
        let prevActiveCompareClass = "." + prevActiveCompareType + "-button-" + currentIndex;
        $(prevActiveCompareClass).removeClass("btn-primary");
        $(prevActiveCompareClass).addClass("btn-white");

        //make this active
        let thisCompareElem = $(this);
        let thisCompareType = thisCompareElem.data('compare');
        let thisCompareClass = "." + thisCompareType + "-button-" + currentIndex;
        $(thisCompareClass).removeClass("btn-white");
        $(thisCompareClass).addClass("btn-primary");

        //change compare input values
        $("#compare-input-" + currentIndex).val($(this).data('compare'));
    });

    $(document).on('input', '[name="quantity[]"]', function () {
        let index = $(this).data('index');
        $(".quantity-input-" + index).val($(this).val());
    });

    $(document).on('input', '[name="price[]"]', function () {
        loadSupplier();
        let index = $(this).data('index');
        let price_original = Number($(this).val());
        let price = price_original;
        let priceInputElems = $(".price-input-" + index);
        for (let i = 0; i < priceInputElems.length; i++) {
            let brand = $(priceInputElems[i]).data('brand');
            if (price > 0 && brand.is_reseller) {
                let discountedPrice = price * parseFloat(supplier.discount)/ 100.0
                price = (price - discountedPrice).toFixed(2);
            } else if (price === 0) {
                price = '';
            }
            $(priceInputElems[i]).val(price);
            price = price_original;
        }
    });


</script>
