<h5>{{__($brand->name)}}</h5>
<div id="brand-div-{{$brand->id}}">
    @foreach($pricings as $index => $pricing)
        <div class="row justify-content-center pricing-row-{{$index}}">
            <div class="col-md-4">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="more-button-{{$index}} compare-disabled btn
                            @if($pricing->compare === MORE) btn-primary @else btn-white @endif">+</button>
                    <button type="button" class="equal-button-{{$index}} compare-disabled btn
                            @if($pricing->compare === EQUAL) btn-primary @else btn-white @endif">=</button>
                    <button type="button" class="less-button-{{$index}} compare-disabled btn
                            @if($pricing->compare === LESS) btn-primary @else btn-white @endif">-</button>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-tachometer-alt"></i></span>
                    </div>
                    <input type="text" disabled class="form-control quantity-input-{{$index}}"
                           value="{{$pricing->quantity}}" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-euro-sign"></i></span>
                    </div>
                    @if($brand->is_reseller && isset($supplier->discount))
                        @php
                            $discountAmount = $pricing->price * $supplier->discount/100;
                            $price = number_format($pricing->price - $discountAmount, 2, '.','');
                        @endphp
                    @else
                        @php
                            $price = $pricing->price;
                        @endphp
                    @endif
                    <input type="text" disabled data-brand="{{$brand}}" class="form-control price-input-{{$index}}"
                           value="{{$price}}" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    @endforeach
</div>
<hr>
