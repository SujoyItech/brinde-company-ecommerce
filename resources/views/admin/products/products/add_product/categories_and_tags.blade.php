<form novalidate class="product-category-form" method="post" action="{{route('saveProductCategoriesAndTags')}}" id="product-category-form">
    <input type="hidden" name="id" value="{{$product->id}}">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4>{{__('Categories/Sub-Categories and Tags')}}</h4>
                </div>
                <div class="col-md-6">
                    <div class="text-right">
                        <button class="btn btn-dark submit-category"  data-style="zoom-in"><i class="fa fa-save"></i> {{__('Save categories and tags')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
                <h5 class="card-title">{{__('Categories/Sub-Categories')}}</h5>
                <select class="form-control selectpicker" multiple="true" required name="category_id[]">
                    @php
                        $tree_number = 0;
                    @endphp
                    @if(isset($categories) && !empty($categories))
                        @foreach($categories as $category)
                            @if(isset($category->children) && !empty($category->children[0]))
                                <option @if(in_array($category->id,$productCategories)) selected @endif value="{{$category->id}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$category->name}}</option>
                                @include('admin.products.products.add_product.child_category_tree',['sub_categories'=>$category->children, 'tree_number' => $tree_number+1])
                            @else
                                <option @if(in_array($category->id,$productCategories)) selected @endif value="{{$category->id}}" style="margin-left: {{$tree_number*20}}px" class="product-category-tree">{{$category->name}}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <h5 class="card-title">{{__('Tags ')}}</h5>
                <select class="form-control selectpicker" multiple="true" required name="tag_id[]">
                    @foreach($tags as $tag)
                        <option
                            @foreach($productTags as $productTag)
                                 @if($productTag->tag_id === $tag->id) selected @endif
                            @endforeach
                        value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function (){
        //resetValidation('product-category-form');

    });
</script>
