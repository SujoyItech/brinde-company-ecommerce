<div id="media-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
     aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content p-3">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">{{__('Media manager')}} <span id="comb-head"></span></h4>
                <button type="button" class="close" onclick="clearFormValidation()" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form novalidate method="post" id="product-media-form" action="{{route('saveProductMedia')}}"
                      class="product-media-form">
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    {{--    <input type="hidden" id="c-t-id-in" name="combination_type_id" value="">--}}
                    {{--    <input type="hidden" id="c-id-in" name="combination_id" value="">--}}
                    <input type="hidden" id="p-c-id-in" name="id" value="">
                    <div class="form-group" id="media-type-div">
                        <h5 class="card-title">{{__('Select Media type')}}</h5>
                        <select class="form-control" id="media_type" name="media_type" required="">
                            <option value="">--{{__('Select')}}--</option>
                            @foreach(get_product_media_type() as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        <div class="valid-feedback">
                            {{__('Looks good!')}}
                        </div>
                    </div>
                    <div class="form-group" id="url-div">
                        <h5 class="card-title">{{__('Enter Url')}}</h5>
                        <input type="text" id="media_url" name="media_url" class="clean-input form-control" placeholder="" required="">
                        <div class="valid-feedback">
                            {{__('Looks good!')}}
                        </div>
                    </div>
                    <div class="form-group" id="uploader-div">
                        <label class="col-form-label"
                        for="image">{{__('Upload an image. Max size: ')}}
                            {{isset(__options(['application_settings'])->product_image_max_size_in_mb)?__options(['application_settings'])->product_image_max_size_in_mb:1}}
                            {{__(' MB and Types: jpg, jpeg, png (required)')}}</label>
                        <div class="valid-feedback">
                            {{__('Looks good!')}}
                        </div>
                        <input type="file" name="image" class="clean-input" required id="image"
                               data-default-file="">
                    </div>
                    <button type="submit" class="btn btn-dark submit-media"  data-style="zoom-in">Save</button>
                </form>
            </div>

        </div>
    </div>
</div>


<script>
    $(document).ready(function (){
        $(document).on('change','#media_type',function (){
            clearFormValidation();
            let media_type = $(this).val();
            if(media_type === '{{INTERNAL_IMAGE}}') {
                $("#url-div").hide();
                $("#media_url").attr("disabled","");

                let imageElem = $("#image");
                imageElem.removeAttr("disabled");
                //imageElem.dropify();
                $("#uploader-div").show();

            } else if (media_type === "") {
                $("#uploader-div").hide();
                let imageElem = $("#image");
                imageElem.attr("disabled","");

                $("#url-div").hide();
                $("#media_url").attr("disabled","");

            } else {
                $("#uploader-div").hide();
                let imageElem = $("#image");
                imageElem.attr("disabled","");

                let mediaUrlElem = $("#media_url");
                mediaUrlElem.removeAttr("disabled");
                $("#url-div").show();
            }
        });
    });

    function clearFormValidation() {
        $("#product-media-form").removeClass('was-validated');
        $('.invalid-feedback').remove();
        $("#product-media-form select").removeClass("is-invalid");
        $("#product-media-form input").removeClass("is-invalid");
    }
</script>
