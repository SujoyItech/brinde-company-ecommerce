@foreach($productCombinations as $prodComb)
    @if($prodComb->media_type or $prodComb->combination_type_id == COLOR_ID)
        <div class="col-md-3 media_item">
            @if($prodComb->combination_type_id == NULL && $prodComb->combination_id == NULL)
                <span class="text-danger float-right delete_item cursor-pointer" data-id="{{$prodComb->id}}"><i class="fa fa-window-close"></i></span>
            @endif
            <div class="text-center tippy" data-plugin="tippy"
                 data-tippy-followCursor="true"
                 data-tippy-arrow="true"
                 data-tippy-placement=""
                 data-tippy-animation="fade"
                 data-tippy-theme="gradient"
                 title="{{__('Click to edit')}}">
                <img onclick="showMediaModal({{$prodComb}}, '{!! isset($prodComb->combination->name) ? $prodComb->combination->name : '' !!}')"
                    class="avatar-lg zoom mb-1" src="{{get_product_combination_thumbnail_and_media_url($prodComb)['thumbnail']}}"
                     onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                <br/>
                <span>{!! !empty($prodComb->combination->name) ? $prodComb->combination->name : $prodComb->product->name !!}</span>
                <div class="radio mb-3 ml-3">
                    <input type="radio" name="product_combination_id" data-product_id="{{$prodComb->product_id}}" id="{{$prodComb->id}}"
                           value="{{$prodComb->id}}" {{$prodComb->is_featured == ACTIVE ? 'checked' : ''}}>
                    <label for="{{$prodComb->id}}"> {{__('Featured')}} </label>
                </div>
            </div>
        </div>
    @endif
@endforeach
<script>
    $(document).on('click','.delete_item',function (e) {
        Ladda.bind(this);
        let load = $(this).ladda();
        let id = $(this).data('id');
        const this_item = $(this);
        swalConfirm("Do you really want to delete this ?").then(function (s) {
            if(s.value){
                let url = "{{route('deleteProductCombination')}}";
                const data = {
                    id : id
                };
                makeAjaxPost(data, url, load).done(function (response) {
                    if (response.success == true){
                        swalSuccess(response.message);
                        this_item.closest('.media_item').remove();
                    }else {
                        swalError(response.message);
                    }
                });
            }else{
                load.ladda('stop');
            }
        })
    });
</script>
