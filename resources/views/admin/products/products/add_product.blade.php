@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'product'])
@section('style')
    <link href="{{adminAsset('libs/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{adminAsset('multiselect/tagsinput.css')}}" rel="stylesheet" type="text/css">
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('admin.products.products.duplicate_product_modal')
    <div class="col-12">
        <div class="row">
            <div class="col-8 text-left">
                <h3><b>{{__('Product information of ')}}{{$product->name}} ({{$product->reference}})</b></h3>
            </div>
            <div class="col-4 text-right">
                <button class="btn btn-dark duplicate_item" data-id="{{$product->id}}" data-name="{{$product->name}}">
                    <i class="fa fa-clone"></i> {{__('Duplicate Product')}}</button>
            </div>
        </div>
    </div><br><br><br><br>
    <div class="col-6">
        @include('admin.products.products.add_product.general_info')
        @include('admin.products.products.add_product.categories_and_tags')
    </div>
    <div class="col-6">
        @include('admin.products.products.add_product.combinations')
        @include('admin.products.products.add_product.price_info')
    </div>

@endsection
@section('script')
    {{--    for gallery--}}
    <script src="{{adminAsset('libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{adminAsset('js/pages/gallery.init.js')}}"></script>
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
    <script src="{{adminAsset('libs/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{adminAsset('multiselect/tagsinput.js')}}"></script>

    {{-----------------------------------------}}

    <script>
        submitOperation(submitResponse, 'submit-general');
        submitOperation(submitResponse, 'submit-category');
        submitOperation(submitResponse, 'submit-pricing');
        function submitResponse(response, this_form){
            if (response.success === true) {
                swalSuccess(response.message);
                //$('form :input').val('');
                this_form.removeClass('was-validated');
                {{--window.location.href = '{{route('editProduct', ':reference')}}'.replace(':reference', response.data.reference);--}}
            } else {
                swalError(response.message);
            }
        }

        $(document).on('click','.duplicate_item',function (){
            let old_product_id = $(this).data('id');
            let name = $(this).data('name');
            $("#old_product_id_in").val(old_product_id);
            $("#dup-name").val(name);
            $('#duplicateProductModal').modal('show');
        });

        submitOperation(duplicate, 'duplicate-product');
        function duplicate(response, this_form){
            if (response.success === true) {
                swalSuccess(response.message);
                $('form :input').val('');
                this_form.removeClass('was-validated');
                window.location.href = '{{route('editProduct', ':reference')}}'.replace(':reference', response.data.reference);
            } else {
                swalError(response.message);
            }
        }
    </script>
@endsection
