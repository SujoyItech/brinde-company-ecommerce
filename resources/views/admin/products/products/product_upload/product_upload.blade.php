@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'productUpload'])
@section('content')
    @include('admin.products.products.product_upload.product_upload_form')
    @include('admin.products.products.product_upload.product_upload_list')
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            let data_url = "{{route('productUpload')}}";
            let data_column = [
                {"data": "id", visible: false},
                {"data": "file"},
                {"data": "supplier_name"},
                {"data": "uploader_name"},
                {"data": "uploaded_product"},
                {"data": "upload_time"},
                {"data": "status"},
                {"data": "action"}
            ];
            renderDataTable($('#productUploadTable'), data_url, data_column);
            resetValidation('product_import_class')
            submitOperation(submitResponse,'product_file_upload');
            deleteOperation(submitResponse,'delete_item',"{{route('deleteProductExcel')}}");
            function submitResponse(response){
                if (response.success == true){
                    swalRedirect("{{Request::url()}}",response.message,'success');
                }else {
                    swalError(response.message);
                }
            }

            $(document).on('click','.product_upload_process',function (){
                let data = {
                    id : $(this).data('id')
                }
                makeAjaxPost(data,"{{route('productUploadProcess')}}").done(function (response){
                    if (response.success == true){
                        swalRedirect("{{Request::url()}}",response.message,'success');
                    }else {
                        swalError(response.message);
                    }
                })
            })
        });

        $(document).on('change', '.custom-file-input', function () {
            let changedlabel = '<i class="fa fa-check"></i> '+"{{__('File Selected')}}";
            $(this).next('.custom-file-label').addClass("selected").html(changedlabel);
        });





    </script>
@endsection
