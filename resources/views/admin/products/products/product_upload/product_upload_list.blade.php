<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h4><i class="fas fa-boxes"></i> {{__('Product Upload List')}}</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="productUploadTable" class="table table-sm table-striped dt-responsive">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{__('File')}}</th>
                        <th>{{__('Supplier')}}</th>
                        <th>{{__('Uploader')}}</th>
                        <th>{{__('Uploaded Products')}}</th>
                        <th>{{__('Date')}}</th>
                        <th>{{__('Status')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
