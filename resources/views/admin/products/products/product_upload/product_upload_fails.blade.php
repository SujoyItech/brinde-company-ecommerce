@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'productUploadFailedList'])
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4><i class="fas fa-boxes"></i> {{__('Product Upload Fails List')}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="productUploadFailTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Image')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Reference')}}</th>
                            <th>{{__('Description')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            let data_url = "{{route('productUploadFailedList',['payload'=>$payload ?? ''])}}";
            let data_column = [
                {"data": "id", visible: false},
                {"data": "image",searchable: false, orderable: false},
                {"data": "name"},
                {"data": "reference"},
                {"data": "description"},
                {"data": "status"},
                {"data": "action",searchable: false}
            ];
            renderDataTable($('#productUploadFailTable'), data_url, data_column,[]);
            deleteOperation(submitResponse,'delete_item',"{{route('deleteUploadFailProduct',['payload'=>$payload ?? ''])}}");
            function submitResponse(response){
                if (response.success == true){
                    swalSuccess(response.message)
                    renderDataTable($('#productUploadFailTable'), data_url, data_column,[]);
                }else {
                    swalError(response.message);
                }
            }
        });
    </script>
@endsection
