<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h4><i class="fas fa-boxes"></i> {{__('Product Upload')}}</h4>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{route('productExport')}}" class="btn btn-dark product_download"><i class="fa fa-download"></i>&nbsp; {{__('Download Format')}}</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form method="POST" id="product_import" novalidate class="product_import_class" action="{{route('productImport')}}" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('Supplier')}}</label>
                            <select name="supplier_id" class="form-control" required>
                                <option value="">{{__('Select')}}</option>
                                @if(isset($suppliers) && !empty($suppliers[0]))
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-0">
                            <label>{{__('Upload XLXS')}}</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input" id="inputGroupFile04" required>
                                    <label class="custom-file-label" for="inputGroupFile04"><i class="fa fa-paperclip"></i> {{__('Choose file')}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark product_file_upload"><i class="fa fa-upload"></i>&nbsp; {{__('Upload')}}</button>
            </form>
        </div>
    </div>
</div>
