<div class="card-box">
    <h4 class="header-title">{{__('Combination type List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the combination type list')}}
    </p>
    <div class="table-responsive">
        <table id="comination_type_table" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
