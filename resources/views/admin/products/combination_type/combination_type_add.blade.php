<div class="card ajax-load">
    <div class="card-body">
        <h4 class="card-title">{{__('Combination type Add/Edit')}}</h4>
        <form class="combination-type-form" novalidate method="post" action="{{route('storeCombinationType')}}" id="combination_type_form">
            <div class="form-group mb-3">
                <label for="name">{{__('Name')}} <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" name="name"  placeholder="{{__('Color/Size')}}" value="{{$combination_type->translations['name']['en'] ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$combination_type->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(false,function (){})"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('combination-type-form');
    });

</script>

