@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'deliveryType'])
@section('content')
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 col-xs-12 add_edit">
        @include('admin.products.delivery.delivery_type_add')
    </div>
    <div class="col-lg-8 col-xl-8 col-md-8 col-sm-12 col-xs-12 list">
        @include('admin.products.delivery.delivery_type_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('deliveryType')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "icon",orderable: false, searchable: false},
                {"data": "name"},
                {"data": "time"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#delivery_type'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            editOperation(editResponse,"{{route('editDeliveryType')}}",true);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    clearDropify();
                    this_form.removeClass('was-validated')
                    renderDataTable($('#delivery_type'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                $('.dropify').dropify();
                submitOperation(submitResponse, 'submit_basic');
            }
        });


    </script>
@endsection
