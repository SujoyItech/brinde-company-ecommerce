<div class="card ajax-load">
    <div class="card-body">
        <h4 class="card-title">{{__('Delivery type Add/Edit')}}</h4>
        <form class="delivery-form" novalidate method="post" action="{{route('storeDeliveryType')}}" id="delivery_form">

            <div class="form-group mb-3">
                <label for="name">{{__('Name')}}</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Flash/Express/Normal')}}" value="{{$delivery_type->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="time">{{__('Delivery Hour(24/48/72)')}}</label>
                <input type="number" class="form-control" name="time" id="time" min="0" placeholder="{{__('24/48/72')}}" value="{{$delivery_type->time ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label class="col-form-label" for="icon">{{__('File')}}</label>
                <input type="file" name="icon" id="icon" class="dropify"  data-allowed-file-extensions="png jpg jpeg jfif" data-max-file-size="1M"
                       data-default-file="{{isset($delivery_type->icon) ? asset(get_image_path('delivery').'/'.$delivery_type->icon) : ''}}"/>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Status')}}</label>
                <select class="form-control" name="status" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$delivery_type->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$delivery_type->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$delivery_type->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(true)"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function (){
        resetValidation('delivery-form');
    });
</script>
