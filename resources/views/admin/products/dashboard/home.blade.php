@extends('admin.layouts.app',['menu'=>'home','sub_menu'=>'home'])
@section('content')
    <div class="col-md-12">
        <h4>{{__('Product Admin')}}</h4>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-success">
                        <i class="fe-image font-22 avatar-title text-success"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$product['no_image_query']}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('No images')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-primary">
                        <i class="fe-paperclip font-22 avatar-title text-primary"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="mt-1"><span data-plugin="counterup">{{$product['no_color_query']}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('No colors')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-danger">
                        <i class="fe-list font-22 avatar-title text-danger"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$product['no_category_query']}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('No categories')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="widget-rounded-circle card-box">
            <div class="row">
                <div class="col-6">
                    <div class="avatar-lg rounded-circle bg-soft-warning">
                        <i class="fe-tag font-22 avatar-title text-warning"></i>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text-right">
                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$product['no_tags_query']}}</span></h3>
                        <p class="text-muted mb-1 text-truncate">{{__('No tags')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{adminAsset('libs/jquery.counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{adminAsset('libs/waypoints/lib/jquery.waypoints.min.js')}}"></script>
@endsection

