<div class="card ajax-load">
    <div class="card-body">
        <h4 class="card-title">{{__('Combination Add/Edit')}}</h4>
        <form class="combination-form" novalidate method="post" action="{{route('storeCombination')}}" id="combination_form">
            <div class="form-group mb-3">
                <label>{{__('Combination Type')}} <span class="text-danger">*</span></label>
                <select class="form-control" name="combination_type_id" required id="combination_type_id">
                    <option value="">{{__('Select')}}</option>
                    @if(isset($combination_types) && !empty($combination_types[0]))
                        @foreach($combination_types as $combination_type)
                            <option value="{{$combination_type->id}}" {{is_selected($combination_type->id,$combination->combination_type_id ?? '')}}>{{$combination_type->name}}</option>
                        @endforeach
                    @endif
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="name">{{__('Combination Name')}}  <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="name" name="name"  placeholder="Combination Name ( Red )" value="{{$combination->translations['name']['en'] ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Combination class')}}</label>
                <input type="text" class="form-control" name="class_name" value="{{$combination->class_name ?? ''}}">
            </div>
            <div class="form-group mb-3 color {{isset($combination->combination_type_id) && $combination->combination_type_id == COLOR_ID ? '' : 'd-none'}}">
                <label>{{__('Color code (only for colors)')}}</label>
                <input type="color" class="form-control" name="color_code" id="color_code" value="{{$combination->color_code ?? ''}}">
            </div>
            <input type="hidden" name="id" value="{{$combination->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form()"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function (){
        resetValidation('combination-form');
    });
    $(document).on('change','#combination_type_id',function (){
        if($(this).val() == "{{COLOR_ID}}"){
            $('.color').removeClass('d-none');
        }else {
            $('.color').addClass('d-none');
            $('#color_code').val('');
        }
    })

</script>
