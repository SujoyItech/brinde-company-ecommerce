<div class="card-box">
    <h4 class="header-title">{{__('Combination List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the combination list')}}
    </p>
    <div class="table-responsive">
        <table id="combination_table" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th>{{__('Combination Type')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Class name')}}</th>
                <th>{{__('Color')}}</th>

{{--                <th>{{__('Status')}}</th>--}}
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
