<div class="card-box">
    <h4 class="header-title">{{__('Tag List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the tag list')}}
    </p>

    <div class="table-responsive">
        <table id="tags_table" class="table dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
