@extends('admin.layouts.app',['menu'=>'product','sub_menu'=>'tags'])
@section('content')
    <div class="col-4 add_edit">
        @include('admin.products.tags.tags_add')
    </div>
    <div class="col-8">
        @include('admin.products.tags.tags_list')
    </div>
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            var data_url = "{{route('tags')}}";
            var delete_url = "{{route('deleteTag')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "name"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#tags_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,"{{route('editTag')}}");

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    resetLanguage('#name');
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    renderDataTable($('#tags_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(response,this_form){
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#tags_table'),data_url,data_column);
                }
            }
        });
    </script>
@endsection

