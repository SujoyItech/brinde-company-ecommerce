<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Tags Add/Edit')}}</h4>
        <form class="tag-form" novalidate method="post" action="{{route('storeTag')}}" id="tag_form">
            <div class="form-group mb-3">
                <label for="name">{{__('Name')}} <span class="text-danger">*</span> </label>
                <input type="text" class="form-control" id="name" name="name"  placeholder="Filter" value="{{$tag->translations['name']['en'] ?? ''}}" required>
                <div class="valid-feedback">
                   {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label>{{__('Status')}}</label>
                <select class="form-control" name="status" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$tag->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$tag->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <input type="hidden" name="id" value="{{$tag->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(false,function (){})"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function (){
        resetValidation('tag-form');
        addLanguage('#name');
    });

</script>
