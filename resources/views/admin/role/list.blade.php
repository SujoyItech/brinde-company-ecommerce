<div class="card-box">
    <h4 class="header-title">{{__('Role List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the role list')}}
    </p>
    <div class="table-responsive">
        <table id="roleTable" class="table dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th>{{__('Title')}}</th>
                <th>{{__('Module')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
