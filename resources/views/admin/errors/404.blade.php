@extends('admin.layouts.app')
@section('content')
<div class="middle-box text-center animated fadeInDown py-xl-5 mb-xl-5">
    <h1>404</h1>
    <h2 class="font-bold">Page Not Found</h2>
    <div class="error-desc mt-5">
        Sorry, but the page you are looking for has not been found. Try checking the URL for error, then hit the refresh button on your browser or try found something else in our app.
    </div>
</div>
@endsection
