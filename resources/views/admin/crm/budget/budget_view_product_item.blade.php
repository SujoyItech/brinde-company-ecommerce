@if(isset($products[0]))
    @php $total_price = 0; $total_vat = 0; @endphp
    @foreach($products as $product)
        @php
            $total_price = $total_price + $product->total_price;
            $total_vat = $total_vat + $product->total_vat;
        @endphp
    @endforeach
@endif
@if(isset($products[0]))
    <label for="">
        <h3>{{__('Total budget : ')}} {{getBrindeMoney($total_price)}}
            + {{getBrindeMoney($total_vat)}} {{__(' VAT')}}</h3>
    </label>
    @foreach($products as $product)
        <div class="card-body mb-2 p-0" id="product-card-{{$product->id}}">
            <div class="card-header bg-soft-secondary text-dark p-1">
                <div class="card-widgets">
                    <button class="btn btn-success waves-effect waves-light btn-xs product-modal-edit"
                            data-id="{{$product->id}}"><i class="fa fa-edit mr-1"></i>{{__('Edit')}}</button>
                    <button class="btn btn-warning waves-effect waves-light btn-xs duplicate_item"
                            data-id="{{$product->id}}"><i class="fa fa-clone mr-1"></i>{{__('Duplicate')}} </button>
                    <button class="btn btn-danger waves-effect waves-light btn-xs delete_item"
                            data-id="{{$product->id}}"><i class="fa fa-trash mr-1"></i> {{__('Delete')}} </button>
                    {{--<a data-toggle="collapse" href="#cardCollpase{{$product->id}}" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>--}}
                </div>
                <h5 class="card-title my-1 pl-2 text-dark">@if(!empty($product->product_id)) {{$product->product->name}}
                    - {{$product->product_reference}} @else {{$product->product_name}} @endif</h5>
            </div>
                @php($combination = get_product_combination($product->product_id, $product->combination_id, $product->combination_type_id))
            <div id="cardCollpase{{$product->id}}" class="collapse show">
                <div class="row">
                    <div class="col-12 media">
                        <img class="d-flex align-self-start avatar-xl mt-1 mr-1" src="{{isset($combination) ? $combination['image'] : ''}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                        <div class="media-body m-1">
                            <p><b><i class="fa fa-calculator"></i> {{$product->quantity}}
                                    * {{getBrindeMoney($product->unit_price)}} = {{getBrindeMoney($product->total_price)}} (
                                    + {{getBrindeMoney($product->total_vat)}} of VAT at {{$product->vat}}%)</b></p>
                            <p><b>{{__('Quantity')}}:</b> {{$product->quantity}} &emsp; <b>{{__('Price')}}:</b>
                                {{$product->unit_price}} &emsp;
                                <b>{{__('Colors')}}:</b> {{get_combination_color($product->combination_id)}}
                            </p>
                            @if($product->additional_info != NULL)
                                <p><b>{{__('Additional Info')}}:</b> {{$product->additional_info}}</p>
                            @endif
                        </div>
                    </div>
                    @if($product->comments != NULL)
                        <div class="col-md-12">
                            <p><b>{{__('Comments')}}:</b> {{$product->comments}}</p>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    @endforeach
    <label for="">
        <h4>{{__('Total budget : ')}} {{getBrindeMoney($total_price)}}
            + {{getBrindeMoney($total_vat)}} {{__(' VAT')}}</h4>
    </label>
@endif
