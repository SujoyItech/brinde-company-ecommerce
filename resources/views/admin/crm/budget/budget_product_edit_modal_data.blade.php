@if(isset($product))
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{__('Edit ')}} {{$product->product_name}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <form action="{{route('crmUpdateBudgetProduct')}}" novalidate method="post"
              id="update_product_form_id_{{$product->id}}" data-modalid="product-modal-{{$product->id}}"
              class="update_product_form">
            <div class="modal-body">
                <input type="hidden" name="quotation_id" value="{{$product->quotation_id}}">
                <input type="hidden" name="edit_id" value="{{$product->id}}">
                @if(!empty($product->product_id))
                    @php $pricings = get_product_pricing($product->product_id) @endphp
                @endif
                @if(isset($pricings) && isset($pricings[0]))
                    <div class="button-list mb-2">
                        @foreach($pricings as $pricing)
                            <button type="button" class="btn btn-blue waves-effect waves-light btn-xs">
                            <span
                                class="btn-label">{!! pricing_compare($pricing->compare) !!}{{$pricing->quantity}}</span>{{$pricing->price}}
                            </button>
                        @endforeach
                    </div>
                @endif
                <div class="form-group mt-3">
                    <label for="">{{__('Product Color')}}</label>
                    <select name="combination_id" class="form-control">
                        <option value="">{{__('Select')}}</option>
                        @if (isset($colors))
                            @foreach($colors as $color)
                                <option value="{{$color->id}}" {{is_selected($color->id,$product->combination_id ?? '')}}>{{$color->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    <div class="valid-feedback">{{__('Looks good!')}}</div>
                </div>
                <div class="form-group">
                    <label for="">{{__('Item Quantity')}}</label>
                    <input name="quantity" value="{{$product->quantity}}" class="form-control" type="text"
                           required
                           placeholder="{{__('Quantity')}}">
                    <div class="valid-feedback">{{__('Looks good!')}}</div>
                </div>
                <div class="form-group">
                    <label for="">{{__('Unit Price')}}</label>
                    <input name="unit_price" value="{{$product->unit_price}}" class="form-control" type="text"
                           required placeholder="{{__('Unit value without vat')}}">
                    <div class="valid-feedback">{{__('Looks good!')}}</div>
                </div>
                <div class="form-group">
                    <label for="">{{__('Vat ')}} %</label>
                    <input name="vat" class="form-control" type="text" value="{{$product->vat}}" required
                           placeholder="% {{__('Vat rate')}}">
                    <div class="valid-feedback">{{__('Looks good!')}}</div>
                </div>
                <div class="form-group">
                    <label for="">{{__('Additional info ')}} </label>
                    <textarea name="additional_info" class="form-control" cols="30" rows="3"
                              placeholder="{{__("Information about color, type printing etc")}}">{{$product->additional_info}}</textarea>
                </div>
                <div class="form-group">
                    <label for="">{{__('Description')}} </label>
                    <textarea name="comments" class="form-control" cols="30" rows="3"
                              placeholder="{{__('Description about the item')}}">{{$product->comments}}</textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect"
                        data-dismiss="modal">{{__('Close')}}</button>
                <button type="submit"
                        class="btn btn-info waves-effect waves-light update_product">{{__('Update')}}</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
@else
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{__('Product not found')}}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
    </div>
@endif

