@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'budget'])
@section('title', isset($title) ? $title : '')
@section('style')
    <link href="{{adminAsset('libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">
                        <h4 class="header-title">{{__('Budget List')}}</h4>
                    </div>
                    <div class="col-md-3 text-right">
                        <button class="btn btn-sm btn-dark create-budget"><i class="fa fa-plus-square"></i> {{__('Create New')}}</button>
                    </div>
                </div>
                <hr>
                @include('admin.search_blade.quotation_search',['reference'=>'budget_reference'])
                <hr>
                <div class="table-responsive">
                    <table id="budgetTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Reference')}}</th>
                            <th>{{__('Client')}}</th>
                            <th>{{__('Commercial')}}</th>
                            <th>{{__('Store')}}</th>
                            <th>{{__('Client Details')}}</th>
                            <th>{{__('Products')}}</th>
                            <th>{{__('Date')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @include('admin.crm.budget.budget_create_modal')
@endsection

@section('script')
    <script src="{{adminAsset('libs/selectize/js/standalone/selectize.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('crmBudgetList')}}";
            var data_column = [
                {"data": "updated_at", visible: false},
                {"data": "budget_reference"},
                {"data": "customer_code"},
                {"data": "assigned_salesman"},
                {"data": "brand_id"},
                {"data": "email"},
                {"data": "products", orderable: false, searchable: false},
                {"data": "expired_at"},
                {"data": "status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#budgetTable'), data_url, data_column);
            searchDataTable($('#budgetTable'), data_url, data_column);
            clearSearch($('#budgetTable'), data_url, data_column);

            $(document).on('click','.create-budget',function (){
                $('#budget-create-modal').modal('show');
                submitOperation(submitResponse,'save_new_budget');
            });

            function submitResponse(response){
                console.log(response);
                if (response.success == true){
                    swalRedirect("{{route('crmBudgetDetails')}}"+'/'+response.data.data.budget_reference,response.message,'success')
                }else {
                    swalError(response.message);
                }
            }

        });
    </script>

@endsection
