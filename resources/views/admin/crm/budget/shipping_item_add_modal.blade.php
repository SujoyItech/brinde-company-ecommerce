<div id="shipping-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('ADD SHIPPING / ITEM')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{route('crmSaveBudgetProduct')}}" novalidate method="post" id="save_product_form_id" class="save_product_form_id px-3">
                <div class="modal-body">
                    <input type="hidden" name="quotation_id" value="{{$item->id}}">
                    <input type="hidden" name="is_shipped_item" value="{{IS_SHIPPED_ITEM}}">
                    <input type="hidden" name="combination_type_id" value="1">
                    <div class="form-group">
                        <label for="product_name">{{__('Shipping Item')}}</label>
                        <input class="form-control" name="product_name" type="text" id="product_name" required  value="" placeholder="{{__('The name of the item to add')}}">
                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                    </div>
                    <div class="form-group">
                        <label for="product_name">{{__('Product Color')}}</label>
                        <select name="combination_id" class="form-control">
                            <option value="">{{__('Select')}}</option>
                            @php($colors = \App\Models\Product\Combination::where(['combination_type_id'=>1])->get())
                            @if (isset($colors))
                                @foreach($colors as $color)
                                    <option value="{{$color->id}}">{{$color->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                    </div>
                    <div class="form-group">
                        <label for="quantity">{{__('Item Quantity')}}</label>
                        <input name="quantity" class="form-control" type="text" id="quantity" required placeholder="{{__('Quantity')}}">
                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                    </div>
                    <div class="form-group">
                        <label for="unit_price">{{__('Unit Price')}}</label>
                        <input name="unit_price" class="form-control" type="text" id="unit_price" required placeholder="{{__('Unit value without vat')}}">
                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                    </div>
                    <div class="form-group">
                        <label for="vat">{{__('Vat ')}} %</label>
                        <input name="vat" class="form-control" type="text" value="23" id="vat" required placeholder="% {{__('Vat rate')}}">
                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                    </div>
                    <div class="form-group">
                        <label for="additional_info">{{__('Additional info ')}} </label>
                        <textarea name="additional_info" id="additional_info" class="form-control" cols="30" rows="3" placeholder="{{__("Information about color, type printing etc")}}"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="comments">{{__('Description')}} </label>
                        <textarea name="comments" id="comments" class="form-control" cols="30" rows="3" placeholder="{{__('Description about the item')}}"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light submit_shipping">{{__('Save')}}</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
