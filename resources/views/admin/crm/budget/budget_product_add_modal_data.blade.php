@if(isset($product))
    <input type="hidden" name="product_id" value="{{$product->id}}">
    <input type="hidden" name="combination_type_id" value="1">
    <input type="hidden" name="is_shipped_item" value="{{INACTIVE}}">
    @if(!empty($product->id))
        @php $pricings = get_product_pricing($product->id) @endphp
    @endif
    @if(isset($pricings) && isset($pricings[0]))
        <div class="button-list">
            @foreach($pricings as $pricing)
                <button type="button" class="btn btn-blue waves-effect waves-light btn-xs">
                    <span class="btn-label">{!! pricing_compare($pricing->compare) !!}{{$pricing->quantity}}</span>{{$pricing->price}}
                </button>
            @endforeach
        </div>
    @endif

    <div class="form-group mt-3">
        <label for="">{{__('Product Name')}}</label>
        <input class="form-control" name="product_name" type="text" value="{{$product->name}}" placeholder="{{__('The name of the item to add')}}">
        <div class="valid-feedback">{{__('Looks good!')}}</div>
    </div>
    <div class="form-group mt-3">
        <label for="">{{__('Product Color')}}</label>
        <select name="combination_id" class="form-control">
            <option value="">{{__('Select')}}</option>
            @if (isset($colors))
                @foreach($colors as $color)
                    <option value="{{$color->id}}">{{$color->name}}</option>
                @endforeach
            @endif
        </select>
        <div class="valid-feedback">{{__('Looks good!')}}</div>
    </div>
    <div class="form-group">
        <label for="">{{__('Item Quantity')}}</label>
        <input name="quantity" class="form-control" type="text" placeholder="{{__('Quantity')}}">
        <div class="valid-feedback">{{__('Looks good!')}}</div>
    </div>
    <div class="form-group">
        <label for="">{{__('Unit Price')}}</label>
        <input name="unit_price" class="form-control" type="text"  placeholder="{{__('Unit value without vat')}}">
        <div class="valid-feedback">{{__('Looks good!')}}</div>
    </div>
    <div class="form-group">
        <label for="vat">{{__('Vat ')}} %</label>
        <input name="vat" class="form-control" type="text" value="23" placeholder="% {{__('Vat rate')}}">
        <div class="valid-feedback">{{__('Looks good!')}}</div>
    </div>
    <div class="form-group">
        <label for="">{{__('Additional info ')}} </label>
        <textarea name="additional_info" class="form-control" cols="30" rows="3" placeholder="{{__("Information about color, type printing etc")}}"></textarea>
    </div>
    <div class="form-group">
        <label for="">{{__('Description')}} </label>
        <textarea name="comments" class="form-control" cols="30" rows="3" placeholder="{{__('Description about the item')}}"></textarea>
    </div>
@else
@endif

