<div id="new-product-modal" class="new-product-modal modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{__('Add product to budget list')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form action="{{route('crmSaveBudgetProduct')}}" novalidate method="post" id="add_new_product_form_id_" class="add_new_product_form">
                    <div class="modal-body">
                        <input type="hidden" name="quotation_id" value="{{$item->id}}">
                        <div id="new-product-modal-data">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light save_new_product">{{__('Add to budget')}}</button>
                    </div>
                </form>
            </div>
    </div>
</div>
