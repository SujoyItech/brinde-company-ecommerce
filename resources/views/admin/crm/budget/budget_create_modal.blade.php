<div id="budget-create-modal" class="new-budget-modal modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{__('Create new budget')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form action="{{route('crmBudgetCreateSave')}}" novalidate method="post" id="add_new_budget_form_id" class="add_new_budget_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>{{__('Customer')}} <span class="text-danger">*</span></label>
                            <select id="customer_id" name="customer_id" placeholder="{{__('Type to select customer')}}" required>
                                <option value="">{{__('Select')}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('Brand')}} <span class="text-danger">*</span></label>
                            <select name="brand_id" class="form-control" required>
                                <option value="">{{__('Select')}}</option>
                                @php($permitted_brands = getPermittedBrandsData())
                                @if (isset($permitted_brands))
                                    @foreach($permitted_brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light save_new_budget">{{__('Create budget')}}</button>
                    </div>
                </form>
            </div>
    </div>
</div>

<script>

    $(()=>{
        resetValidation('add_new_budget_form');
        callCustomerSelectizeFunction();
    });
    function callCustomerSelectizeFunction() {
        let url = "{{route('getCustomerListBySalesMan')}}";
        let obj = {
            targetDom: '#customer_id',
            url: url,
            valueField: 'id',
            labelField: 'contact_name',
            searchField: ['contact_name','email','phone'],
            create: function(input) {
                createCustomerModalShow();
            }
        }
        selectizeFunction(function (item, escape) {
            let USER_IMAGE_URL = "{{asset(get_image_path('customers'))}}";
            let DEFAULT_IMAGE_URL = "{{adminAsset('images/no-image.png')}}";
            let html = '';
            html += '<div class="media bg-light p-2 border-bottom">';
            html += '<img src="' +USER_IMAGE_URL+'/'+escape(item.image) + '" class="avatar-sm mr-2 rounded-circle" onerror=\'this.src="' + DEFAULT_IMAGE_URL + '"\'>';
            html += '<div class="media-body">';
            html += '<h5 class="mt-0 mb-0 font-14">' + escape(item.contact_name) + '</h5>';
            html += '<p class="mt-1 mb-0 text-muted font-14"><span class="w-75">' + escape(item.email) + '</span></p>';
            html += '<p class="mt-1 mb-0 text-muted font-14"><span class="w-75">' + escape(item.phone) + '</span></p>';
            html += '</div>';
            html += '</div>';
            return html;
        }, obj);
    }
    function createCustomerModalShow(){
        location.href="{{route('crmCustomerAdd')}}";
        callCustomerSelectizeFunction();
    }

</script>
