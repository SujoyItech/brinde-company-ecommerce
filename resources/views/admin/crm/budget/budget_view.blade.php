@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'budget'])
@section('title', isset($title) ? $title : '')
@section('style')
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{__('Budget From ')}} <span id="budget_reference"> {{$item->budget_reference}} </span></h4>
                    </div>
                    <div class="col-md-6">
                        <h4 class="float-right">{{__('Current Status ')}} : <span id="budget_status" class="badge badge-success">
                                {{quotation_status($item->status)}} @if(!empty($item->approved_by)) ({{__('By')}} {{ $item->approved_by == STATUS_APPROVED_BY_CUSTOMER ? __('Customer') : __('Salesman')}}) @endif
                            </span></h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-5 col-xl-5 col-md-6">
                        <form class="update_budget-form" novalidate method="post" action="{{route('crmSaveBudget')}}" id="update_budget-form_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="button-list">
                                        <button type="submit" class="btn btn-success waves-effect waves-light update_budget"><i class="fa fa-save mr-1"></i>{{__('Save Record')}}</button>
                                        <a href="{{route('crmBudgetList')}}" class="btn btn-warning waves-effect waves-light"><i class="fa fa-arrow-circle-left mr-1"></i>
                                            {{__('Cancel')}}
                                        </a>
                                        @if($item->status == STATUS_BUDGET_APPROVED)
                                            <a href="javascript:void(0)" class="btn btn-info waves-effect waves-light make_order" data-id="{{$item->id}}">
                                                <i class="fa fa-shopping-basket mr-1"></i>{{__('Make Order')}}
                                            </a>
                                        @endif
                                        @if($item->status < STATUS_BUDGET_APPROVED && !empty($item->expired_at))
                                            <a href="javascript:void(0)" class="btn btn-info waves-effect waves-light budget_sent" data-id="{{$item->id}}">
                                                <i class="fa fa-envelope mr-1"></i>{{__('Send Mail to Customer')}}
                                            </a>
                                            <a href="javascript:void(0)" class="btn btn-success waves-effect waves-light approve_budget" data-id="{{$item->id}}">
                                                <i class="fa fa-check-circle mr-1"></i>{{__('Approve')}}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="quotation_id" value="{{$item->id}}">
                            <div class="row mt-4">
                                <div class="col-md-12">
                                    @php
                                        $printing_notice = __('In order to be able to approve a quote, it is necessary to enter information related to printing.
                                                You will not be able to approve the budget until you do.
                                                Click on the " Print types (required for approval only) " button to enter this information.');
                                    @endphp
                                    <b>{{__('Printing Notice ')}}  </b><br/>
                                    <p>{{__($printing_notice)}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="switchery-demo">
                                        <label>{{__('Enable Printing?')}}</label>&ensp;
                                        <input type="checkbox" @if($item->budget_printing_enable == ACTIVE) checked @endif name="printing_enable"  data-plugin="switchery" data-color="#2b3d51" id="enablePrinting"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row printingOption {{$item->budget_printing_enable == INACTIVE ? 'hidden' : ''}}">
                                <div class="col-md-12 mt-3">
                                    <label>{{__("Indicate the types of printing required ")}}</label>
                                </div>
                                @php
                                    $actions=[];
                                    if(isset($item->budget_printing_option)){
                                        $actions = array_filter(explode(',', $item->budget_printing_option));
                                    }
                                @endphp
                                <div class="col-md-12 pl-3">
                                    @isset($printing_options[0])
                                        @foreach($printing_options as $printing_option)
                                            <div class="checkbox form-check-inline">
                                                <input name="printing_options[]" type="checkbox" id="{{'check-'.$printing_option->id}}" value="{{$printing_option->id}}" @if(in_array($printing_option->id, $actions)) checked @endif>
                                                <label for="{{'check-'.$printing_option->id}}"> {{$printing_option->translations['name'][app()->getLocale()] ?? $printing_option->translations['name']['en']}} </label>
                                            </div>
                                        @endforeach
                                    @endif
                                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label>{{__('Print Comments')}}</label>
                                    <textarea name="budget_printing_comments" cols="10" rows="4" class="form-control">{{$item->budget_printing_comments}}</textarea>
                                    <div class="valid-feedback">{{__('Looks good!')}}</div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>{{__('Budget Date ')}}</label>
                                    <input type="date" disabled class="form-control" value="{{$item->created_at->toDateString()}}" >
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>{{__('Expiration Date ')}}</label>
                                    <input type="date" class="form-control" value="{{$item->expired_at ?? ''}}" name="expired_at">
                                    <div class="valid-feedback">{{__('Looks good!')}}</div>
                                </div>
                            </div>
                            <div class="row mt-3 ">
                                <div class="col-md-12">
                                    <div class="switchery-demo">
                                        <label>{{__('Mask References?')}}</label> &ensp;
                                        <input type="checkbox" @if($item->budget_masking_enable == ACTIVE) checked @endif name="masking_enable" data-plugin="switchery" data-color="#1bb99a" data-secondary-color="#ff5d48" />
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div class="switchery-demo">
                                        <label>{{__('Show Totals?')}}</label> &ensp;
                                        <input type="checkbox" @if($item->budget_show_total_enable == ACTIVE) checked @endif name="show_total_enable" data-plugin="switchery" data-color="#1bb99a" data-secondary-color="#ff5d48" />
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label>{{__('Models')}}</label>
                                    <input type="text" value="{{$item->budget_model}}" name="budget_model" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <label>{{__("Products")}}</label>
                                    <input type="text" class="form-control" value="" id="search" name="search">
                                    <div id="new-product-list"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <label>{{__("No shipping / item")}}</label>
                                    <input type="text" name="" id="shipping_product_name" class="form-control" value="">
                                    <span id="no-shipping-message" class="text-danger"></span><br>
                                    <a href="javascript:void(0)" id="openShippingModal" class="btn btn-outline-info">{{__('Add nonexistent postage / article')}}</a>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-md-12">
                                    <div class="button-list">
                                        <button type="submit" class="btn btn-success waves-effect waves-light update_budget"><i class="fa fa-save mr-1"></i>{{__('Save Record')}}</button>
                                        <a href="{{route('crmBudgetList')}}" class="btn btn-warning waves-effect waves-light"><i class="fa fa-arrow-circle-left mr-1"></i>
                                            {{__('Cancel')}}
                                        </a>
                                        @if($item->status == STATUS_BUDGET_APPROVED)
                                            <a href="javascript:void(0)" class="btn btn-info waves-effect waves-light make_order" data-id="{{$item->id}}">
                                                <i class="fa fa-shopping-basket mr-1"></i>{{__('Make Order')}}
                                            </a>
                                        @endif
                                        @if($item->status < STATUS_BUDGET_APPROVED && !empty($item->expired_at))
                                            <a href="javascript:void(0)" class="btn btn-info waves-effect waves-light budget_sent" data-id="{{$item->id}}">
                                                <i class="fa fa-envelope mr-1"></i>{{__('Send Mail to Customer')}}
                                            </a>
                                            <a href="javascript:void(0)" class="btn btn-success waves-effect waves-light approve_budget" data-id="{{$item->id}}">
                                                <i class="fa fa-check-circle mr-1"></i>{{__('Approve')}}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-7 col-xl-7 col-md-6">
                        <div class="text-right d-print-none">
                            <a href="{{route('crmPrintBudget',$item->budget_reference)}}" target="_blank" class="btn btn-success waves-effect waves-light"><i class="fa fa-print mr-1"></i> {{__('Print')}}</a>
                        </div>
                        <label>{{__('Client  :')}} {{$item->contact_name. ', '.$item->customer_code}}</label> <br>
                        <label>{{__('Store  :')}} {{$item->brand->name}}</label> <br>
                        @if(!empty($item->attached_file) && file_exists( public_path().'/'.get_image_path('quotation').$item->attached_file))
                            <a href="{{asset(get_image_path('quotation').$item->attached_file)}}" target="_blank">
                                <button class="btn btn-outline-secondary btn-xl"><i class="fe-paperclip"></i> {{__('View Attached File')}} </button>
                            </a>
                        @endif
                        <a href="{{route('crmCustomerEdit',[getCustomerReferenceCode($item->customer_code), $item->quote_reference])}}" class="btn btn-dark btn-xl">
                            <i class="fe-user"></i> {{__('Edit Customer')}}
                        </a>
                        <br>
                        <label class="mt-2">{{__('Client Comments')}}</label>
                        <p> {{$item->comments}} </p>
                        <br>
                        @if(!empty($item->budget_modification_comments))
                            <label>{{__('Budget Modification Comments')}}</label>
                            @foreach(array_reverse(json_decode($item->budget_modification_comments)) as $modify_comments)
                                <p><b>{{$modify_comments->date_time}}</b> <br> {!! $modify_comments->comments !!} </p>
                            @endforeach
                        @endif
                        {{--------------------------------}}
                        <div id="product_items">
                            @include('admin.crm.budget.budget_view_product_item')
                        </div>
                        @include('admin.crm.budget.budget_product_item_edit_modal')
                        {{--------------------------------}}
                        <div class="text-right d-print-none">
                            <a href="{{route('crmPrintBudget',$item->budget_reference)}}" target="_blank" class="btn btn-success waves-effect waves-light"><i class="fa fa-print mr-1"></i> {{__('Print')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.crm.budget.shipping_item_add_modal')
    @include('admin.crm.budget.budget_product_add_modal')
@endsection
@section('script')
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            //
        });
        /*----------------------------------------*/
        /**
         * update the budget
         */
        resetValidation('update_budget-form');
        submitOperation(function(response, this_form){
            if (response.success === true) {
                swalRedirect("{{Request::url()}}",response.message,'success');
            } else {
                swalError(response.message);
            }
        }, 'update_budget');

        /*------------------------------*/
        /**
         * Show product edit modal
         */
        $(document).on('click', '.product-modal-edit', function(e) {
            var edit_url = "{{route('crmBudgetProductEdit')}}";
            var data = {
              id : $(this).data('id')
            };
            makeAjaxPostText(data, edit_url).done (function (response) {
                remove_loader();
                $("#product-edit-modal-data").html(response);
            });

            $('#product-edit-modal').modal('show');

        });

        /*-------------------******---------------------*/

        /*----------------------------------------*/
        /**
         * Product Item Edit Modal Save
         */
        resetValidation('update_product_form');
        $(document).on('click', '.update_product', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#' + form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        swalSuccess(response.message);
                        this_form.removeClass('was-validated');
                        $('#product-edit-modal').modal('hide');
                        $("#product_items").html(response.data);


                    });
                }
            });
        });
        /*-----------------****-----------------------*/



        /*----------------------------------------*/
        /**
         * Show the shipping product modal
         */
        $(document).on('click', '#openShippingModal', (e) => {
            var name = $('#shipping_product_name').val();
            if(name == '') {
                $("#no-shipping-message").html("Please add item name first")
            } else {
                $("#no-shipping-message").html("")
                $("#product_name").val(name);
                $('#shipping-modal').modal('show');
            }

        });

        /**
         * Save the shipping modal product
         */
        resetValidation('save_product_form_id');
        submitOperation(function(response, this_form){
            if (response.success === true) {
                swalSuccess(response.message);
                this_form.removeClass('was-validated');
                $('#shipping-modal').modal('hide');
                $("#product_items").html(response.data);
            } else {
                swalError(response.message);
            }
        }, 'submit_shipping');
        /*----------------****------------------------*/



        /*----------------------------------------*/
        /**
         * enable disable printing option
         */
        $('#enablePrinting').change(function () {
            if (!this.checked)
                $(".printingOption").addClass('hidden');
            else
                $('.printingOption').removeClass('hidden');
        });

        /*---------------****-------------------------*/

        /*----------------------------------------*/
        /**
         * Product duplicate operation
         */

        const duplicate_url = "{{route('crmDuplicateBudgetProduct')}}";
        makeDuplicateOperation(function(response){
            if(response.success === false) {
                swalError(response.message);
            } else {
                swalSuccess(response.message);
                $("#product_items").html(response.data);
            }
        },'duplicate_item', duplicate_url);

        /*----------------------------------------*/
        /**
         * Send budget mail to customer
         */
        const email_url = "{{route('crmSendBudgetMail')}}";
        sendBudgetOperation(function(response){
            if(response.success === false) {
                swalError(response.message);
            } else {
                swalSuccess(response.message);
            }
        },'budget_sent',email_url);

        /*--------------------******--------------------*/

        /*----------------------------------------*/
        /**
         * Delete product
         */
        const delete_url = "{{route('crmDeleteBudgetProduct')}}";
        deleteOperation(function(response) {
            if(response.success === false) {
                swalError(response.message);
            } else {
                var id = response.data;
                $("#product-card-"+id).remove();
                $("#product-modal-"+id).remove();
                swalRedirect("{{Request::url()}}",response.message,'success');
            }
        },'delete_item',delete_url);

        /*--------------------****--------------------*/
        /*----------------------------------------*/
        /**
         * Product live search
         */

        $('#search').on('keyup',function() {
            var value = $(this).val();
            var quotation_id = "{{$item->id}}";
            if(value.length > 0){
                $.ajax({
                    type: 'post',
                    url: '{{ route('crmBudgetProductSearch') }}',
                    data: {
                        'search_item': value,
                        'quotation_id' : quotation_id
                    },
                    success: function (data) {
                        $("#new-product-list").html(data)
                    }
                });
            }else{
                $('#searched-product-list').empty();
            }

        });

        /*------------------------------*/
        /**
         * Show new product add modal
         */
        $(document).on('click', '.new-product-add-modal-show', function(e) {
            var edit_url = "{{route('crmBudgetProductEdit')}}";
            var data = {
                id : $(this).data('id'),
                new_product : 'new',
            };
            makeAjaxPostText(data, edit_url).done (function (response) {
                remove_loader();
                $("#new-product-modal-data").html(response);
            });
            $('#new-product-modal').modal('show');
        });
        /*----------------------------------------*/
        /**
         * Save new product
         */
        resetValidation('add_new_product_form');
        $(document).on('click','.save_new_product',function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#' + form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        swalSuccess(response.message);
                        this_form.removeClass('was-validated');
                        $('#new-product-modal').modal('hide');
                        $('#searched-product-list').empty();
                        $('#search').val('');
                        $("#product_items").html(response.data);
                    });
                }
            });
        });
        /*-------------------*********---------------------*/

        $(document).on('click','.approve_budget',function (){
            Ladda.bind(this);
            let load = $(this).ladda();
            let quotation_id = $(this).data('id');
            swalConfirm("{{__('Do you really want to approve this ?')}}").then(function (s) {
                if(s.value){
                    var url = "{{route('crmBudgetApprove')}}";
                    var data = {
                        id : quotation_id
                    };
                    makeAjaxPost(data, url, load).done(function (response) {
                        if (response.success == true){
                            swalRedirect("{{Request::url()}}",response.message,'success');
                        }else {
                            swalError(response.message);
                        }
                    });
                }else{
                    load.ladda('stop');
                }
            })

        });

        /*----------------------------------------*/
        /**
         * make it order
         * @type {string}
         */
        const make_order_url = "{{route('crmMakeBudgetOrder')}}";
        actionOperation(function(response) {
            if(response.success === false) {
                swalError(response.message);
            } else {
                swalRedirect("{{route('crmBudgetList')}}",response.message,'success');
            }
        },'make_order',make_order_url, 'Do you really want to make this budget to order ?');
        /*--------------------******--------------------*/


    </script>
@endsection
