<div class="list-group" id="searched-product-list">
    @if(isset($products[0]))
        @foreach($products as $product)
            <div class="list-group-item list-group-item-action p-0 border border-0">
                <div class="media border new-product-add-modal-show cursor-pointer zoom-sm" data-id="{{$product->id}}">
                    <img class="d-flex align-self-start avatar-md mr-1 zoom" src="{{check_storage_image_exists($product->media_url)}}" alt="Generic placeholder image">
                    <div class="media-body p-0">
                        <h5 class="mt-1">{{$product->reference}}</h5>
                        <p class="m-0">{{$product->name}}</p>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="list-group-item list-group-item-action p-0 border border-0">
            <h5 class="my-2 text-danger">{{__('No Product Found')}}</h5>
        </div>
    @endif
</div>
