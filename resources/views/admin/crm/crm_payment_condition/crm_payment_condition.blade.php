@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'payment_condition'])
@section('content')
    <div class="col-4 add_edit">
        @include('admin.crm.crm_payment_condition.crm_payment_condition_add')
    </div>
    <div class="col-8 list">
        @include('admin.crm.crm_payment_condition.crm_payment_condition_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('crmPaymentCondition')}}";
            var delete_url = "{{route('deleteCrmPaymentCondition')}}";
            var edit_url = "{{route('editCrmPaymentCondition')}}";
            var data_column = [
                {"data": "id", visible: false},
                {"data": "name"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#crm_payment_condition'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,edit_url);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    renderDataTable($('#crm_payment_condition'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#crm_payment_condition'),data_url,data_column);
                }
            }
        });
    </script>
@endsection
