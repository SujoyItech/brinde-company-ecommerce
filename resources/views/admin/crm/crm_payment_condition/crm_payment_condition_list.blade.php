<div class="card-box">
    <h4 class="header-title">{{__('Payment condition List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the payment condition list')}}
    </p>
    <div class="table-responsive">
        <table id="crm_payment_condition" class="table table-sm table-striped dt-responsive">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
