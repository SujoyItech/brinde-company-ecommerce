<div class="card-box">
    <h4 class="header-title">{{__('Printing option List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the printing option list')}}
    </p>
    <div class="table-responsive">
        <table id="crm_printing_option" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
