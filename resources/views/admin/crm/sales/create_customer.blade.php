@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'printing_option'])
@section('style')
    <link href="{{adminAsset('libs/mohithg-switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="card ajax-load">
            <div class="card-body">
                <h4 class="card-title">{{__('Customer Profile')}}</h4>
                <form class="brands-form" novalidate method="post" action="{{route('saveBrand')}}" id="brand_form">
                    <div class="row mb-2">
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group mb-2">
                                <label for="name">{{__('Name')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Name')}}" value="{{$brand->name ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="slug">{{__('Contact')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('brandSlugCheck')}}" name="slug" id="slug" value="{{$brand->slug ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="store_url">{{__('Email')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label class="col-form-label" for="icon">{{__('Phone')}}</label>
                                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}" required>
                            </div>
                            <div class="form-group mb-2">
                                <label>{{__('Payment Condition')}}</label>
                                <select class="form-control" name="status" required>
                                    <option value="">{{__('Select')}}</option>
                                </select>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label class="col-form-label" for="icon">{{__('TIN Number')}}</label>
                                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}" required>
                            </div>
                            <div class="form-group">
                                <h5 class="card-title">{{__('Status')}}</h5>
                                <input type="checkbox" data-plugin="switchery" name="status" data-color="#1bb99a" data-secondaryColor="#666666"/>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <h5 class="pt-2">{{__('Address')}}</h5>
                            <div class="form-group mb-2">
                                <label for="name">{{__('Delivery Address')}}</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Name')}}" value="{{$brand->name ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="slug">{{__('Delivery Postcode')}}</label>
                                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('brandSlugCheck')}}" name="slug" id="slug" value="{{$brand->slug ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="store_url">{{__('Delivery City')}}</label>
                                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label class="col-form-label" for="icon">{{__('Delivery Country')}}</label>
                                <select class="form-control" name="status" required>
                                    <option value="">{{__('Select')}}</option>
                                </select>
                            </div>
                            <button type="button" class="btn btn-outline-dark waves-effect waves-light float-right">{{__('Copy Address')}} <i class="fe-arrow-right"></i></button>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <h5 class="pt-2">{{__('Address')}}</h5>
                            <div class="form-group mb-2">
                                <label for="name">{{__('Delivery Address')}}</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Name')}}" value="{{$brand->name ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="slug">{{__('Delivery Postcode')}}</label>
                                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('brandSlugCheck')}}" name="slug" id="slug" value="{{$brand->slug ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="store_url">{{__('Delivery City')}}</label>
                                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label class="col-form-label" for="icon">{{__('Delivery Country')}}</label>
                                <select class="form-control" name="status" required>
                                    <option value="">{{__('Select')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
                    <button class="btn btn-outline-secondary waves-effect waves-light reset_from float-right" type="button" onclick="reset_form(true,function (){ $('input:checkbox').removeAttr('checked') })"><i class="fas fa-sync-alt"></i> {{__('Reset')}}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{adminAsset('libs/mohithg-switchery/switchery.min.js')}}"></script>
@endsection
