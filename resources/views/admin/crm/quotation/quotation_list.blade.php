@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'quotation'])
@section('title', $title ?? '')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">
                        <h4 class="header-title">{{__('Quotation List')}}</h4>
                    </div>
                </div>
                <hr>
                <div class="table-responsive">
                    <table id="quotationTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th>{{__('Date')}}</th>
                            <th>{{__('Reference')}}</th>
                            <th>{{__('Customer')}}</th>
                            <th>{{__('Store')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('CrmQuotationList',['brand_id'=>$brand_id ?? ''])}}";
            var data_column = [
                {"data": "updated_at"},
                {"data": "quote_reference"},
                {"data": "customer"},
                {"data": "brand_name"},
                {"data": "status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#quotationTable'), data_url, data_column);
            $(document).on('click','.search_product',function (){
                var form_id = $(this).closest('form').attr('id');
                var this_form = $('#' + form_id);
                $(this_form).on('submit', function (e) {
                    if (!e.isDefaultPrevented()) {
                        e.preventDefault();
                        var formData = $(this).serializeArray();
                        renderDataTableWithData($('#quotationTable'),data_url,data_column,formData);
                    }
                });
            })
        });
    </script>

@endsection
