@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'quotation'])
@section('title', isset($title) ? $title : '')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Quotation From ')}} {{$item->quote_reference}}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-xl-6 col-md-6">
                        <p><b>{{__('Budget Reference')}}: </b> <span id="budget_reference">{{$item->budget_reference ?? ''}}</span></p>
                        <p><b>{{__('Customer Code')}}: </b> <span id="customer_code">{{$item->customer_code ?? ''}}</span></p>
                        <p><b>{{__('Company Name')}}: </b> {{$item->company_name}}</p>
                        <p><b>{{__('Contact Name')}}: </b> {{$item->contact_name}}</p>
                        <p><b>{{__('Email')}}: </b> {{$item->email}}</p>
                        <p><b>{{__('Phone Number')}}: </b> {{$item->phone}}</p>
                        <p><b>{{__('Quotation Date')}}: </b> {{$item->created_at}}</p>
                        <p><b>{{__('Additional Information')}}: </b> {{$item->comments}}</p>
                        @if(!empty($item->attached_file) && file_exists( public_path().'/'.get_image_path('quotation').$item->attached_file))
                            <a href="{{asset(get_image_path('quotation').$item->attached_file)}}" target="_blank">
                                <button class="btn btn-outline-secondary btn-xl"><i class="fe-paperclip"></i> {{__('View Attached File')}} </button>
                            </a>
                        @endif
                        @if(!empty($item->customer_id))
                            <a href="{{route('crmCustomerEdit',[getCustomerReferenceCode($item->customer_code), $item->quote_reference])}}">
                                <button class="btn btn-dark btn-xl" data-style="zoom-in"><i class="fe-user"></i> {{__('Edit Customer')}}</button>
                            </a>
                        @else
                            <a href="{{route('crmCustomerAdd',[$item->quote_reference])}}">
                                <button class="btn btn-dark btn-xl" data-style="zoom-in"><i class="fe-user"></i> {{__('Create Customer')}}</button>
                            </a>
                        @endif
                        <hr/>

                        @if(!empty($item->customer_id))
                            <form class="assign_salesman-form" novalidate method="post" action="{{route('CrmAssignSalesman')}}" id="assign_salesman_form_id">
                                <input type="hidden" name="quotation_id" value="{{$item->id}}">
                                <select class="form-control form-control-lg" name="assigned_salesman" >
                                    <option value="">{{__('Select')}}</option>
                                    @if(isset($salesmans[0]))
                                        @foreach($salesmans as $salesman)
                                            <option value="{{$salesman->id}}"
                                                @if (!empty($item->assigned_salesman))
                                                    @if ($salesman->id == $item->assigned_salesman)
                                                        selected
                                                    @endif
                                                @elseif (isset($customer) && $customer->salesman_id == $salesman->id)
                                                    selected
                                                @elseif((old('assigned_salesman') != null) && (old('assigned_salesman') == $salesman->id)) @endif >{{ $salesman->name}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                                <br/>
                                <button class="btn btn-dark btn-xl submit_basic" type="submit"><i class="fe-arrow-right-circle"></i> {{__('Assign Salesman')}}</button>
                            </form>
                        @endif
                    </div>
                    <div class="col-lg-6 col-xl-6 col-md-6">
                        @if(isset($products[0]))
                            @foreach($products as $product)
                                <div class="card-body p-0 mb-2">
                                    <div class="card-header bg-secondary py-2 text-white">
                                        <div class="card-widgets">
                                            <a data-toggle="collapse" href="#cardCollpase{{$product->id}}" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                                        </div>
                                        <h5 class="card-title mb-0 text-white">{{$product->product->name}} - {{$product->product_reference}}</h5>
                                    </div>
                                    @php $combination = get_product_combination($product->product_id, $product->combination_id, $product->combination_type_id) @endphp
                                    <div id="cardCollpase{{$product->id}}" class="collapse show">
                                        <div class="row">
                                            <div class="col-2 p-2">
                                                <img class="img-fluid img-thumbnail" src="{{$combination['image']}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                                            </div>
                                            <div class="col-9 p-2">
                                                <p><b>{{__('Quantity')}}:</b> {{$product->quantity}} &ensp;<b>{{__('Price')}}:</b> {{$product->unit_price}} &ensp;
                                                @if(!empty($combination['combination']))
                                                    <b>{{$combination['combination_type']}}:</b> {{$combination['combination']}}
                                                @endif
                                                </p>
                                            </div>
                                            <div class="col-12">
                                                <p><b>{{__('Extra Information')}}:</b> {{$product->additional_info}}</p>
                                                <p><b>{{__('Description')}}:</b> {{$product->comments}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            submitOperation(submitResponse, 'submit_basic');

            function submitResponse(response, this_form){
                if (response.success == true) {
                    $('#budget_reference').html(response.data.budget_reference);
                    $('#customer_code').html(response.data.customer_code);
                    swalSuccess(response.message);
                    this_form.removeClass('was-validated')
                } else {
                    swalError(response.message);
                }
            }
        });
    </script>
@endsection
