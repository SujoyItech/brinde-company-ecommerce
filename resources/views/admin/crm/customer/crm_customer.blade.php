@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'customer'])
@section('title', isset($title) ? $title : '')
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        @include('admin.crm.customer.crm_customer_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('crmCustomerList')}}";
            var delete_url = "{{route('crmCustomerDelete')}}";
            deleteOperation(deleteResponse,'delete_item',delete_url);
            var data_column = [
                {"data": "id", visible: false},
                {"data": "customer_code"},
                {"data": "name"},
                {"data": "contact_name"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "country"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#crm_customer'),data_url,data_column);

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#crm_customer'),data_url,data_column);
                }
            }
        });
    </script>
@endsection
