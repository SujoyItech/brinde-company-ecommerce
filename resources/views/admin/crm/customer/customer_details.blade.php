@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'customer'])
@section('title', isset($title) ? $title : '')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">
                        <h4 class="header-title">{{__('Customer Order Histories')}}</h4>
                    </div>
                </div>
                <hr>
                @include('admin.search_blade.quotation_search',['reference'=>'quote_reference'])
                <hr>
                <div class="table-responsive">
                    <table id="budgetTable" class="table table-sm table-striped dt-responsive">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('Reference')}}</th>
                            <th>{{__('Client')}}</th>
                            <th>{{__('Commercial')}}</th>
                            <th>{{__('Store')}}</th>
                            <th>{{__('Client Details')}}</th>
                            <th>{{__('Products')}}</th>
                            <th>{{__('Date')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            var data_url = "{{route('customerDetails',['customer_code'=>$customer_code ?? ''])}}";
            var data_column = [
                {"data": "updated_at", visible: false},
                {"data": "quote_reference"},
                {"data": "customer_code"},
                {"data": "assigned_salesman"},
                {"data": "brand_id"},
                {"data": "email"},
                {"data": "products", orderable: false, searchable: false},
                {"data": "expired_at"},
                {"data": "status"},
                {"data": "action", orderable: false, searchable: false}
            ];
            renderDataTable($('#budgetTable'), data_url, data_column);
            searchDataTable($('#budgetTable'), data_url, data_column);
            clearSearch($('#budgetTable'), data_url, data_column);

        });
    </script>

@endsection
