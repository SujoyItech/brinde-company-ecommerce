<div class="card-box">
    <div class="row">
        <div class="col-md-9">
            <h4 class="header-title">{{__('Customer List')}}</h4>
        </div>
        <div class="col-md-3 text-right">
            <a href="{{route('crmCustomerAdd')}}">
                <button class="btn btn-sm btn-success"><i class="fa fa-plus-square"></i> {{__('Create New Customer')}}</button>
            </a>
        </div>
    </div>
    <hr>
    <div class="table-responsive">
        <table id="crm_customer" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Customer Code')}}</th>
                <th>{{__('Company Name')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Email')}}</th>
                <th>{{__('Phone')}}</th>
                <th>{{__('Country')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
