@extends('admin.layouts.app',['menu'=>'crm','sub_menu'=>'customer'])
@section('title', isset($title) ? $title : '')
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="card ajax-load">
            <div class="card-body">
                <h4 class="card-title">{{__('Customer Add/Edit')}}</h4>
                <form class="crm-customer-form" novalidate method="post" action="{{route('crmCustomerSave')}}" id="crm_customer_form_id">
                    <div class="row mb-2">
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div class="form-group mb-2">
                                <input type="hidden" name="quotation_id" value="{{$quotation->id ?? ''}}">
                                @if(  \Illuminate\Support\Facades\Auth::user()->role == ROLE_SALESMAN)
                                    <input type="hidden" name="assigned_salesman" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                                @else
                                    <label>{{__('Assigned Salesman')}}</label>
                                    <select class="form-control" name="assigned_salesman" required>
                                        <option value="">{{__('Select')}}</option>
                                        @if(isset($salesmans[0]))
                                            @foreach($salesmans as $salesman)
                                                <option value="{{$salesman->id}}"
                                                        @if (isset($quotation) && !empty($quotation->assigned_salesman))
                                                        @if ($salesman->id == $quotation->assigned_salesman)
                                                        selected
                                                        @endif
                                                        @elseif (isset($item) && $item->salesman_id == $salesman->id)
                                                        selected
                                                @elseif((old('assigned_salesman') != null) && (old('assigned_salesman') == $salesman->id)) @endif >{{ $salesman->name}}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if(isset($suggest_salesman))
                                        <div class="mt-1">
                                            <span>{{__('Suggested: ')}} {{$suggest_salesman}}</span>
                                        </div>
                                    @endif
                                    <div class="valid-feedback">
                                        {{__('Looks good!')}}
                                    </div>
                                @endif

                            </div>
                            <div class="form-group mb-3">
                                <label for="name">{{__('Company Name')}}</label>
                                <input type="text" class="form-control" id="name" name="name"  placeholder="{{__('Company name')}}"
                                       value="@if(isset($item)){{$item->name}} @elseif(isset($quotation)) {{$quotation->company_name}}  @endif" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="contact_name">{{__('Contact Name')}}</label>
                                <input type="text" class="form-control" id="contact_name" name="contact_name"  placeholder="{{__('Contact name')}}"
                                       value="@if(isset($item)) {{$item->contact_name}} @elseif(isset($quotation)) {{$quotation->contact_name}} @endif" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="email">{{__('Contact Email')}}</label>
                                <input type="email" class="form-control" id="email" name="email"  placeholder="{{__('Contact email')}}"
                                       value="@if(isset($item)) {{$item->email}} @elseif(isset($quotation)) {{$quotation->email}} @endif" required>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="phone">{{__('Contact Phone')}}</label>
                                <input type="text" class="form-control" id="phone" name="phone"  placeholder="{{__('Contact phone')}}"
                                       value="@if(isset($item)) {{$item->phone}} @elseif(isset($quotation)) {{$quotation->phone}}  @endif"  >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label>{{__('Payment Conditions')}}</label>
                                <select class="form-control" name="crm_payment_condition_id" required>
                                    <option value="">{{__('Select')}}</option>
                                    @if(isset($payment_conditions[0]))
                                        @foreach($payment_conditions as $payment)
                                            <option value="{{$payment->id}}"  @if(isset($item) && ($item->crm_payment_condition_id == $payment->id)) selected
                                            @elseif((old('crm_payment_condition_id') != null) && (old('crm_payment_condition_id') == $payment->id)) @endif >{{ $payment->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="tin_number">{{__('VAT number')}} </label>
                                <input type="text" class="form-control" id="tin_number" name="tin_number"  placeholder="{{__('Tax identification number')}}" value="{{$item->tin_number ?? ''}}">
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label>{{__('Status')}}</label>
                                <select class="form-control" name="status" required>
                                    @foreach(activation_status() as $key => $value)
                                        <option value="{{$key}}"  @if(isset($item) && ($item->status == $key)) selected
                                        @elseif((old('status') != null) && (old('status') == $key)) @endif >{{$value}}</option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <h5 class="pt-2">{{__('Invoice Address')}}</h5>
                            <div class="form-group mb-3">
                                <label for="tax_address">{{__('Tax Address')}}</label>
                                <input type="text" class="form-control" id="tax_address" name="tax_address"  placeholder="{{__('Tax address')}}" value="{{$item->tax_address ?? ''}}" >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="fiscal_postcode">{{__('Fiscal Postal Code')}}</label>
                                <input type="text" class="form-control" id="fiscal_postcode" name="fiscal_postcode"  placeholder="{{__('Tax postal code')}}" value="{{$item->fiscal_postcode ?? ''}}" >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="tax_location">{{__('Tax Location')}}</label>
                                <input type="text" class="form-control" id="tax_location" name="tax_location"  placeholder="{{__('Tax location')}}" value="{{$item->tax_location ?? ''}}" >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label>{{__('Tax Country')}}</label>
                                <select class="form-control" name="tax_country" id="tax_country" required>
                                    <option value="">{{__('Select')}}</option>
                                    @foreach(countries() as $key => $value)
                                        <option value="{{$value}}" @if(isset($item) && ($item->tax_country == $value)) selected @elseif((old('tax_country') != null) && (old('tax_country') == $value)) selected @endif>{{$value}}</option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <button type="button" id="copy_address" class="btn btn-outline-dark waves-effect waves-light float-right">{{__('Copy Address Data To Tax Data')}} <i class="fe-arrow-right"></i></button>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <h5 class="pt-2">{{__('Delivery Address')}}</h5>
                            <div class="form-group mb-3">
                                <label for="address">{{__('Address')}}</label>
                                <input type="text" class="form-control" id="address" name="address"  placeholder="{{__('Address')}}" value="{{$item->address ?? ''}}" >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="postcode">{{__('Post Code')}}</label>
                                <input type="text" class="form-control" id="postcode" name="postcode"  placeholder="{{__('Postal code')}}" value="{{$item->postcode ?? ''}}" >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="locality">{{__('Location')}}</label>
                                <input type="text" class="form-control" id="locality" name="locality"  placeholder="{{__('Locality')}}" value="{{$item->locality ?? ''}}" >
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label>{{__('Country')}}</label>
                                <select class="form-control" name="country" id="country" required>
                                    <option value="">{{__('Select')}}</option>
                                    @foreach(countries() as $key => $value)
                                        <option value="{{$value}}"  @if(isset($item) && ($item->country == $value)) selected @elseif((old('country') != null) && (old('country') == $value)) selected @endif >{{$value}}</option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback">
                                    {{__('Looks good!')}}
                                </div>
                            </div>

                        </div>

                    </div>

                    <input type="hidden" name="id" value="{{$item->id ?? ''}}">
                    <button class="btn btn-dark waves-effect waves-light submit_basic" data-style="zoom-in" type="submit"><i class="fa fa-save"></i> {{isset($item) ? __('Update') : __('Save')}}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            resetValidation('crm-customer-form')
            submitOperation(submitResponse, 'submit_basic');

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    window.location.href = '{{route('crmCustomerList')}}';
                } else {
                    swalError(response.message);
                }
            }
        });
        $(document).on('click', '#copy_address', function() {
            $('#address').val($('#tax_address').val());
            $('#postcode').val($('#fiscal_postcode').val());
            $('#locality').val($('#tax_location').val());
            $('#country').val($('#tax_country').val());
        });

    </script>
@endsection


