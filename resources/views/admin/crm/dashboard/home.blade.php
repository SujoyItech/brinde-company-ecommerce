@extends('admin.layouts.app',['menu'=>'home','sub_menu'=>'home'])
@section('content')
    @if (isset($quotations) && !empty($quotations))
        <div class="col-md-12">
            <h4>{{__('Brand-wise New Quotation')}}</h4>
        </div>
        <div class="col-12 p-0 d-flex">
            @foreach($quotations as $quotation)
                <div class="col">
                    <a href="{{route('CrmQuotationList',['brand_id'=>$quotation->id])}}">
                        <div class="widget-rounded-circle card-box">
                            <div class="row">
                                <div class="col-4">
                                    <div class="avatar-lg rounded-circle bg-soft-success">
                                        <img class="rounded-circle font-22 avatar-title text-success" src="{{check_image_exists(get_image_path('brand'),$quotation->icon)}}">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="text-right">
                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$quotation->new_quotation}}</span></h3>
                                        <p class="text-muted mb-1 text-truncate">{{$quotation->name}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    @endif

    @if (isset($quotations) && !empty($quotations))
        <div class="col-md-12">
            <h4>{{__('CRM Information')}}</h4>
        </div>
        <div class="col-12 p-0 d-flex">
            @php($assigned_quotation = 0)
            @php($budget_process = 0)
            @foreach($quotations as $quotation)
                @php($assigned_quotation += $quotation->quotation_assigned)
                @php($budget_process += $quotation->budget_process)
            @endforeach
            <div class="col">
                <div class="widget-rounded-circle card-box bg-warning">
                    <div class="row">
                        <div class="col-4">
                            <div class="avatar-lg rounded-circle bg-light">
                                <i class="fe-tag font-28 avatar-title text-dark"></i>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$assigned_quotation}}</span></h3>
                                <p class="mb-1 text-dark">{{__('Salesman Assigned')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="widget-rounded-circle card-box bg-secondary">
                    <div class="row">
                        <div class="col-4">
                            <div class="avatar-lg rounded-circle bg-light">
                                <i class="fe-refresh-cw font-28 avatar-title text-dark"></i>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="text-right">
                                <h3 class="text-light mt-1"><span data-plugin="counterup">{{$budget_process}}</span></h3>
                                <p class="mb-1 text-light">{{__('Budget Processing')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="widget-rounded-circle card-box bg-pink">
                    <div class="row">
                        <div class="col-4">
                            <div class="avatar-lg rounded-circle bg-light">
                                <i class="fe-users font-28 avatar-title text-dark"></i>
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="text-right">
                                <h3 class="text-light mt-1"><span data-plugin="counterup">{{\App\Models\CRM\CrmCustomer::where('status',STATUS_ACTIVE)->count()}}</span></h3>
                                <p class="mb-1 text-light">{{__('Total customer')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script src="{{adminAsset('libs/jquery.counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{adminAsset('libs/waypoints/lib/jquery.waypoints.min.js')}}"></script>
@endsection

