@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'command_setting'])
@section('content')
    <div class="col-md-12">
        <div class="card-box">
            <ul class="nav nav-pills navtab-bg nav-justified">
                <li class="nav-item">
                    <a href="#basic" data-toggle="tab" aria-expanded="true" class="nav-link active">{{__('Basic Commands')}}</a>
                </li>
                <li class="nav-item">
                    <a href="#application" data-toggle="tab" aria-expanded="false" class="nav-link">{{__('Application Command')}}</a>
                </li>
                <li class="nav-item">
                    <a href="#languages" data-toggle="tab" aria-expanded="false" class="nav-link">{{__('Language Settings Command')}}</a>
                </li>
                <li class="nav-item">
                    <a href="#others" data-toggle="tab" aria-expanded="false" class="nav-link">{{__('Others')}}</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane show active" id="basic">
                    @include('admin.settings.commands.basic_command')
                </div>

                <div class="tab-pane" id="application">
                    @include('admin.settings.commands.application_command')
                </div>

                <div class="tab-pane" id="languages">
                    @include('admin.settings.commands.language_command')
                </div>

                <div class="tab-pane" id="others">
                    @include('admin.settings.commands.other_command')
                </div>
            </div>
        </div>
    </div
@endsection
@section('script')

@endsection
