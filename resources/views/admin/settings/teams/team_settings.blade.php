@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'teamSettings'])

@section('content')
    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12">
        @include('admin.settings.teams.team_lists')
    </div>
    @include('admin.settings.teams.team_add_edit')
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            var data_url = "{{route('teamSettings')}}";
            var delete_url = "{{route('deleteTeamItem')}}";
            var data_column =  [
                {"data": "id", visible: false},
                {"data": "name"},
                {"data": "designation"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#teams_table'),data_url,data_column);

            $(document).on('click','#create_team',function (){
                clearTeams();
                $('#team-modal').modal('show');
            });

            $(document).on('click','.edit_item',function (){
                let data = {
                    id : $(this).data('id')
                }
                makeAjaxPostText(data,"{{route('getTeamItemById')}}",null).done(function (response){
                    $('#name').val(response.name);
                    $('#designation').val(response.designation)
                    $('#email').val(response.email)
                    $('#phone').val(response.phone)
                    $('.status').val(response.status);
                    $('#id').val(response.id);
                    $('#team-modal').modal('show');
                });
            });

            $(document).on('click','.close_modal',function (){
                clearTeams();
                $(this).closest('.modal').modal('hide');
            })

            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    this_form.removeClass('was-validated');
                    clearTeams();
                    $('.modal').modal('hide');
                    swalSuccess(response.message);
                    renderDataTable($('#teams_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#teams_table'),data_url,data_column);
                }
            }

            function clearTeams(){
                $('#name').val('');
                $('#designation').val('')
                $('#email').val('')
                $('#phone').val('')
                $('.status').val('');
                $('#id').val('');
            }
        });
    </script>
@endsection
