<div id="team-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">{{__('Team Add/Edit')}}</h4>
                <button type="button" class="close close_modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{route('teamSettingSave')}}"  method="POST" id="submit_teams_id" class="submit_team" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">{{__('Name')}} <span class="text-danger">*</span></label>
                                <input type="text" name="name"  id="name" placeholder="{{__('Name')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="designation">{{__('Designation')}} </label>
                                <input type="text" name="designation"  id="designation" placeholder="{{__('Designation')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                <input type="text" name="email"  id="email" placeholder="{{__('Email')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                <input type="text" name="phone"  id="phone" placeholder="{{__('Phone')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="status">{{__('Status')}}</label>
                                <select class="form-control status" name="status" required >
                                    <option value="">{{__('Select')}}</option>
                                    <option value="{{STATUS_ACTIVE}}">{{__('Active')}}</option>
                                    <option value="{{INACTIVE}}">{{__('Inactive')}}</option>
                                </select>
                            </div>
                            <input type="hidden" name="id" id="id" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light close_modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-dark submit_basic"> <i class="fa fa-save"></i> {{__('Save')}}</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



