<div class="card-box">
    <div class="row">
        <div class="col-md-6">
            <h4 class="header-title">{{__('Team List')}}</h4>
            <p class="sub-header">
                {{__('Here goes the team list')}}
            </p>
        </div>
        <div class="col-md-6 text-right">
            <button class="btn btn-sm btn-dark" id="create_team"><i class="fa fa-plus"></i> {{__('Create new')}}</button>
        </div>
    </div>

    <div class="table-responsive">
        <table id="teams_table" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Name')}}</th>
                <th>{{__('Designation')}}</th>
                <th>{{__('Email')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
