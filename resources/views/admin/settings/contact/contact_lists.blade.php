<div class="card-box">
    <div class="row">
        <div class="col-md-6">
            <h4 class="header-title">{{__('Contact List')}}</h4>
            <p class="sub-header">
                {{__('Here goes the contact list')}}
            </p>
        </div>
        <div class="col-md-6 text-right">
            <button class="btn btn-sm btn-dark" id="create_contact"><i class="fa fa-plus"></i> {{__('Create new')}}</button>
        </div>
    </div>

    <div class="table-responsive">
        <table id="contacts_table" class="table table-sm dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th></th>
                <th>{{__('Type')}}</th>
                <th>{{__('Title')}}</th>
                <th>{{__('Address')}}</th>
                <th>{{__('Business Days')}}</th>
                <th>{{__('Contact')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
