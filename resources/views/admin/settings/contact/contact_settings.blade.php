@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'contactSettings'])

@section('content')
    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12">
        @include('admin.settings.contact.contact_lists')
    </div>
    @include('admin.settings.contact.contact_add_edit')
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            resetValidation('submit_contact_class');
            var data_url = "{{route('contactSettings')}}";
            var delete_url = "{{route('deleteContactItem')}}";
            var data_column =  [
                {"data": "id", visible: false},
                {"data": "contact_type"},
                {"data": "title"},
                {"data": "address"},
                {"data": "business_days"},
                {"data": "contact_details"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#contacts_table'),data_url,data_column);

            $(document).on('click','#create_contact',function (){
                clearContact();
                $('#contact-modal').modal('show');
            });

            $(document).on('click','.edit_item',function (){
                let data = {
                    id : $(this).data('id')
                }
                makeAjaxPostText(data,"{{route('getContactItemById')}}",null).done(function (response){
                    $('.contact_type').val(response.contact_type);
                    $('#title').val(response.title)
                    $('#address').val(response.address)
                    $('#business_days').val(response.business_days)
                    $('#email').val(response.email)
                    $('#phone').val(response.phone)
                    $('#gps').val(response.gps)
                    $('#fax').val(response.fax)
                    $('.status').val(response.status);
                    $('#id').val(response.id);
                    $('#contact-modal').modal('show');
                });
            });

            $(document).on('click','.close_modal',function (){
                clearContact();
                $(this).closest('.modal').modal('hide');
            })

            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    this_form.removeClass('was-validated');
                    clearContact();
                    $('.modal').modal('hide');
                    swalSuccess(response.message);
                    renderDataTable($('#contacts_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#contacts_table'),data_url,data_column);
                }
            }

            function clearContact(){
                $('.contact_type').val('');
                $('#title').val('')
                $('#address').val('')
                $('#business_days').val('')
                $('#email').val('')
                $('#phone').val('')
                $('#gps').val('')
                $('#fax').val('')
                $('.status').val('');
                $('#id').val('');
            }
        });
    </script>
@endsection
