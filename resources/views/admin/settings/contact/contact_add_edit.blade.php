<div id="contact-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">{{__('Contact Add/Edit')}}</h4>
                <button type="button" class="close close_modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{route('contactSettingSave')}}"  method="POST" id="submit_contacts_id" class="submit_contact_class" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="contact_type">{{__('Type')}}</label>
                                <select class="form-control contact_type" name="contact_type" required >
                                    <option value="">{{__('Select')}}</option>
                                    <option value="{{MAIN_CONTACT}}">{{__('Main')}}</option>
                                    <option value="{{SUB_CONTACT}}">{{__('Sub')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">{{__('Title')}} <span class="text-danger">*</span></label>
                                <input type="text" name="title"  id="title" placeholder="{{__('General Data Porto')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="address">{{__('Address')}} </label>
                                <input type="text" name="address"  id="address" placeholder="{{__('Zona Industrial da Poupa 2, Rua B n.º 92 4780-321 - Santo Tirso, Porto, Portugal')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="business_days">{{__('Business Days')}}</label>
                                <input type="text" name="business_days"  id="business_days" placeholder="{{__('9:00 - 12:30 / 13:30 - 18:00')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                <input type="text" name="email"  id="email" placeholder="{{__('info@brinde-companhia.pt')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                <input type="text" name="phone"  id="phone" placeholder="{{__('(+351) 252 860 330')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="gps">{{__('GPS')}}</label>
                                <input type="text" name="gps"  id="gps" placeholder="{{__('Lat: 41.33699 Long: -8.49561')}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="fax">{{__('Fax')}}</label>
                                <input type="text" name="fax"  id="fax" placeholder="{{__('(+351) 252 860 330')}}" class="form-control" >
                            </div>

                            <div class="form-group">
                                <label for="status">{{__('Status')}}</label>
                                <select class="form-control status" name="status" required >
                                    <option value="">{{__('Select')}}</option>
                                    <option value="{{STATUS_ACTIVE}}">{{__('Active')}}</option>
                                    <option value="{{INACTIVE}}">{{__('Inactive')}}</option>
                                </select>
                            </div>
                            <input type="hidden" name="id" id="id" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light close_modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-dark submit_basic"> <i class="fa fa-save"></i> {{__('Save')}}</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



