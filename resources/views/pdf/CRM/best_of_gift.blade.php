<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{$title ?? ''}}</title>
    <style>
        {{--@font-face {--}}
        {{--    font-family: NeoTech;--}}
        {{--    src: url("{{ storage_path('fonts/neotech.otf') }}");--}}
        {{--    font-weight: normal;--}}
        {{--}--}}
        {{--@font-face {--}}
        {{--    font-family: NeoTech;--}}
        {{--    src: url("{{ storage_path('fonts/neotech.otf') }}");--}}
        {{--    font-weight: bold;--}}
        {{--}--}}

        .header{
            font-size: x-small;
        }
        .middle {

        }
        .middle h2{
            text-align: center;
            margin-bottom: 5px;
            margin-top: 0;
            padding-top: 3px;
            padding-bottom: 3px;
            line-height: 30px;
        }
        .product-section table thead tr th{
            background: #000000;
            color: #ffffff;
            text-transform: uppercase;
            font-weight: 400;
            white-space: nowrap;
            padding: 3px 10px;
            font-size: 11px;
        }
        .product-section table tbody tr td{
            text-align: center;
            font-size: small;
        }
        .special-character-font{
            font-family: Helvetica sans-serif;
            font-size: small;
        }

    </style>
</head>
<body>
    <div>
        <div class="header">
            <table style="width: 100%;text-align: left;">
                <tr>
                    <th style="width: 20%;">
                        <div class="logo" style="margin-bottom: 20px;">
                            <img src="{{isset($item->brand->icon) && file_exists(public_path(get_image_path('brand').$item->brand->icon)) ? get_image_path('brand').$item->brand->icon : adminAsset('images/no-image.png')}}" style="max-width: 180px;">
                        </div>
                    </th>
                    <th style="width: 40%;">
                    <span>
                        www.brinde-company.pt<br>
                        Email<span class="special-character-font">:</span>info@brinde-company.pt<br>
                        Nif<span class="special-character-font">:</span>504803234<br>
                        Tel<span class="special-character-font">:</span>+352346645444<br>
                        Fax<span class="special-character-font">:</span>+351346664<br>
                    </span>
                    </th>
                    <th style="width: 40%;">
                        <span style="font-family: NeoTech bold;">Porto:</span> Zona Industrial da Poupa 2, Rua B n.º 92 4780-321 - Santo Tirso Portugal <br>
                        <span style="font-family: NeoTech bold;"> Lisbon:</span>  Centro de Escritórios Panoramic Avenida do Atlântico, 16 - Escritório 4.01 (Piso 4 escritório 1)
                        1990-019 Parque das Nações, Lisboa, Portugal
                    </th>
                </tr>
            </table>
        </div>
        <div class="middle">
            <hr>
            <h2 style="font-weight: bold">{{ __('ORCAMENTO DE VENDA') }}</h2>
            <hr>
            <table style="width: 100%; font-size: 14px;">
                <tr>
                    <th style="width: 33%; padding-top: 10px; background: #6d6565; color: #ffffff">
                        <b>{{__('Budget Reference')}}</b><br>
                        <p style="margin-bottom: 10px; margin-top: 5px; padding: 0; line-height: 20px">{{$item->budget_reference ?? ''}}</p>
                    </th>
                    <th style="width: 33%; padding-top: 10px; background: #000000; color: #ffffff">
                        <b>{{__('Date')}}</b>
                        <br>
                        <p style="margin-bottom: 10px; margin-top: 5px; padding: 0; line-height: 20px">{{date('d M Y', strtotime($item->created_at))}}</p>
                    </th>
                    <th style="width: 33%;  padding-top: 10px; background: #000000; color: #ffffff">
                        <b>{{__('Comercial')}}</b>
                        <br>
                        <p style="margin-bottom: 10px; margin-top: 5px; padding: 0; line-height: 20px">{{$item->salesman_name ?? ''}}</p>
                    </th>
                </tr>
            </table>
        </div>
        <div class="middle">
            <table style="width: 100%; font-size: 14px;">
                <tr>
                    <th style="width: 50%; padding: 10px;  text-align: left;border: 1px solid;" >
                        <p style="margin-bottom: 10px; margin-top: 5px; padding: 0; line-height: 20px">
                            <strong>Cliente:</strong> {{$item->company_name}} (NIF: {{$item->tin_number}}) {{$item->tax_address}}  {{$item->fiscal_postcode}}  {{ !empty($item->tax_location) ? '- '.$item->tax_location : ''}} {{ !empty($item->tax_country) ? '- '.$item->tax_country : ''}}
                        </p>
                    </th>
                </tr>
            </table>
        </div>
        @if(isset($item->brand) && $item->brand->is_reseller == TRUE)
            <div class="middle">
                <table style="width: 100%; font-size: 14px; margin-top: 30px; margin-bottom: 30px; background-color: #000000; color: #FFFFFF">
                    <tr>
                        <th style="padding: 10px;  text-align: center;" >
                            <p>Para aprovar ou rejeitar o orçamento, por favor clique em "APROVAR/REJEITAR ORÇAMENTO".</p>
                            <h4 class="bottom-line" style="margin-top:10px;"><a href="{{route('clientBudgetDetails',encrypt($item->budget_reference))}}" style="color: #10ba62">APROVAR/REJEITAR ORÇAMENTO</a></h4>
                            <span>
                            Nota:<br>
                            Se o orçamento tiver mais do que uma linha, poderá aprovar apenas as partes que lhe interessam;<br>
                            - Se for necessário, depois de aprovar o orçamento siga as instruções para nos enviar ficheiros para a personalização dos seus artigos; - Após aprovar o orçamento, será criada uma maquete que lhe será enviada para aprovação;<br>
                            - Nada será enviado para produção sem a sua aprovação;<br>
                            - Portes grátis para encomendas iguais ou superiores a 100,00 € (Portugal continental);
                        </span>
                        </th>
                    </tr>
                </table>
            </div>
        @endif
        <div class="product-section">
            <table style="width: 100%; min-width: 100%;">
                <thead>
                <tr>
                    <th  style="padding: 10px !important;">{{__('ARTIGO')}}</th>
                    <th  style="padding: 10px !important;">{{__('DESCRIÇÃO')}}</th>
                    <th  style="padding: 10px !important;">{{ __('INF. EXTRA') }}</th>
                    <th  style="padding: 10px !important;">{{ __('QTD') }}</th>
                    <th  style="padding: 10px !important;">{{ __('PREÇO UNI.') }}</th>
                    <th  style="padding: 10px !important;">{{ __('IVA') }}</th>
                    <th  style="padding: 10px !important;">{{__('VALOR LINHA')}}</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($products[0]))
                    @php $totalVat = 0; $totalPrice = 0 ; @endphp
                    @foreach($products as $product)
                        @php
                            $totalPrice = $totalPrice + $product->total_price;
                            $totalVat = $totalVat + $product->total_vat;
                        @endphp
                        <tr>
                            <td style="padding: 10px !important;">
                                @if (!empty($product->product_id ))
                                    @php $combination = get_product_featured_media($product->product_id) @endphp
                                @endif
                                <img style="width: 70px;"  src="{{isset($combination) ? $combination['media_url'] : adminAsset('images/no-image.png')}}" ><br>
                                <span>{{ $item->budget_masking_enable == ACTIVE ? $product->product_mask_reference : $product->product_reference }}</span>
                            </td>
                            <td style="padding: 10px !important;">
                                {{ $product->comments}}
                            </td>
                            <td style="padding: 10px !important;">
                                {{ $product->additional_info}}
                            </td>
                            <td style="padding: 10px !important;">
                                {{ $product->quantity}}
                            </td>
                            <td style="padding: 10px !important;">
                                {{ getBrindeMoneyWithoutSign($product->unit_price)}} <span class="special-character-font">€</span>
                            </td>
                            <td style="padding: 10px !important;">
                                {{$product->vat}}<span class="special-character-font"> %</span> <br/>
                                @php($total_vat = ($product->vat*$product->unit_price)/100 )
                                ({{getBrindeMoneyWithoutSign($total_vat)}} <span class="special-character-font">€</span>)
                            </td>
                            <td style="padding: 10px !important;text-align: center">
                                {{__('Sem IVA:')}}<br/>
                                {{getBrindeMoneyWithoutSign($product->total_price)}} <span class="special-character-font">€</span> <br>
                                {{__('Com IVA:')}}<br/>
                                {{getBrindeMoneyWithoutSign($product->total_vat)}} <span class="special-character-font">€</span>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>{{__('No item found')}}</td>
                    </tr>
                @endif

                </tbody>
            </table>
            <div style="text-align: right">
                @if($item->budget_show_total_enable == ACTIVE)
                    <h3>{{__('Total Budget: ')}}  {{getBrindeMoneyWithoutSign($totalVat + $totalPrice)}} <span class="special-character-font">€</span></h3>
                @endif
            </div>
        </div>
    </div>
    </body>
</html>




