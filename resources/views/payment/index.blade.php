<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>{{env('APP_NAME')}}| 404</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{adminAsset('images/favicon.ico')}}">

    <!-- App css -->
    <link href="{{adminAsset('css/bootstrap-saas.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet"/>
    {{--    <link href="{{adminAsset('css/bootstrap-saas-dark.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet"/>--}}

    {{--    <link href="{{adminAsset('css/app-saas.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />--}}
    {{--    <link href="{{adminAsset('css/app-saas-dark.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet"/>--}}

</head>
<body class="loading authentication-bg authentication-bg-pattern">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                @include('payment.reference')
                <!-- end card -->

{{--                <div class="row mt-3">--}}
{{--                    <div class="col-12 text-center">--}}
{{--                        <p class="text-white-50">{{__('Return to ')}}<a href="{{env('APP_URL')}}"--}}
{{--                                                                        class="text-white ml-1"><b>{{__('Home')}}</b></a>--}}
{{--                        </p>--}}
{{--                    </div> <!-- end col -->--}}
{{--                </div>--}}
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

<footer class="footer footer-alt">

</footer>

</body>
</html>
