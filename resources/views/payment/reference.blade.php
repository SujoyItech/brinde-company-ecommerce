<div class="card">
    <div class="card-body p-4">
        <table>
            <tbody>
            <tr>
                <td style="width: 100.0000%;">
                    <p
                        style="margin-bottom: 10pt; margin-left: 17px; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif; text-align: center;">
                        <strong><span
                                style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;">
                                                <img
                                                    src="{{asset('payment/images/multibanco.png')}}"
                                                    style="width: auto;" class="fr-fic fr-fil fr-dii lightbox-image"></span></strong><strong><span
                                style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;"><font
                                    style="vertical-align: inherit;"><font
                                        style="vertical-align: inherit;">{{__('Payment by ATM or Homebanking')}}</font></font></span></strong>
                    </p>
                    @if($payRecord->notice)
                        <p style="margin-bottom: 10pt; margin-left: 0cm; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 21.3pt;">
                            <strong><span style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;"><font
                                        style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                        {{__('Fractional payment for exceeding the limit value for payments in the Multibanco system')}}
                                                    </font></font></span></strong>
                        </p>
                    @endif
                    <p style="margin-bottom: 10pt; margin-left: 0cm; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 21.3pt;">
                                        <span
                                            style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;"><font
                                                style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{__('Entity')}}: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </font></font><strong><font
                                                    style="vertical-align: inherit;"><font
                                                        style="vertical-align: inherit;">{{$payRecord->entity}}</font></font></strong></span>
                    </p>
                    <p style="margin-bottom: 10pt; margin-left: 0cm; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 21.3pt;">
                                        <span
                                            style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;"><font
                                                style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{__('Reference')}}: &nbsp;  </font></font><strong><font
                                                    style="vertical-align: inherit;"><font
                                                        style="vertical-align: inherit;">{{$payRecord->showable_reference}}</font></font></strong></span>
                    </p>
                    <p style="margin-bottom: 10pt; margin-left: 0cm; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 21.3pt;">
                                        <span
                                            style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;"><font
                                                style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{__('Value')}}: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font></font><strong><font
                                                    style="vertical-align: inherit;"><span
                                                        style="vertical-align: inherit;">{{__('€')}} {{number_format($payRecord->amount, 2,',','.')}}</span></font></strong></span>
                    </p>

                    <p style="margin-bottom: 10pt; margin-left: 0cm; line-height: 115%; font-size: 15px; font-family: Calibri, sans-serif; text-indent: 21.3pt;">
                        <strong><span style="font-size:12px;line-height:115%;font-family:&quot;Fira Sans Book&quot;,sans-serif;"><font
                                    style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                                    {{__('The receipt issued by the ATM is proof of payment. Keep it.')}}
                                                </font></font></span></strong>
                    </p>

                </td>
            </tr>
            </tbody>
        </table>

    </div> <!-- end card-body -->
</div>
