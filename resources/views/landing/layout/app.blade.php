<!DOCTYPE html>
<html lang="en">
<head>
    @php($settings = __options(['site_settings', 'logo_settings', 'social_settings']))
    <meta charset="utf-8" />
    <title>{{__('Landing')}} | {{$settings->app_title ?? env('APP_NAME')}}::@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{isset($settings->favicon_logo) && !empty($settings->favicon_logo) ? asset(get_image_path('settings').'/'.$settings->favicon_logo) :  adminAsset('images/favicon.ico')}}">
    <script src="{{adminAsset('vendors/jQuery.min.js')}}"></script>
    <link href="{{adminAsset('css/bootstrap-saas.css')}}" rel="stylesheet" type="text/css"/>

    <!-- icons -->
    <link href="{{adminAsset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- icons -->

</head>

<body class="authentication-bg">

<div class="mt-5 mb-5">
    @yield('content')
    <!-- end container -->
</div>
<!-- end page -->

<footer class="footer footer-alt text-white-50">
</footer>


<script src="{{adminAsset('vendors/jQuery.min.js')}}"></script>
<script src="{{adminAsset('vendors/bootstrap.bundle.min.js')}}"></script>

@yield('script')
<script src="{{adminAsset('js/app.js')}}"></script>

</body>
</html>
