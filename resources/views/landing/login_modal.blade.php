<div id="login-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center mt-2 mb-4">
                    <a href="{{url('/')}}" class="text-success">
                        <span><img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-sm.png') }}" alt="" height="24"></span>
                    </a>
                </div>

                <form class="px-3" novalidate action="{{ route('postLogin') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="emailaddress">{{__('Email address')}}</label>
                        <input class="form-control" type="email" id="emailaddress" required="" placeholder="Enter your email" name="email">
                        @if($errors->first('email'))
                            <span class="text-danger">{{$errors->first('email')}}</span> @endif
                    </div>

                    <div class="form-group">
                        <label for="password">{{__('Password')}}</label>
                        <div class="input-group input-group-merge">
                            <input type="password" id="password" class="form-control" placeholder="Enter your password" name="password">
                            <div class="input-group-append" data-password="false">
                                <div class="input-group-text">
                                    <span class="password-eye font-12"></span>
                                </div>
                            </div>
                        </div>
                        @if($errors->first('password'))
                            <span class="text-danger">{{$errors->first('password')}}</span> @endif
                    </div>
                    <div class="form-group text-center">
                        <button data-style="zoom-in" class="btn btn-rounded btn-primary" type="submit">{{__('Login In')}}</button>
                    </div>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
