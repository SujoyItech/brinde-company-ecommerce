@extends('customer.layouts.customer_app')
@section('title', isset($title) ? $title : 'Budget Details')
@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-xl-8 col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <div class="card-box">
                <div class="clearfix">
                    <div class="float-left">
                        <div class="auth-logo">
                            <div class="logo logo-dark">
                                <span class="logo-lg">
                                    <img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-dark.png') }}" alt="" height="35">
                                </span>
                            </div>
                            <div class="logo logo-light">
                                <span class="logo-lg">
                                    <img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-light.png') }}" alt="" height="35">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="float-right">
                        <h4 class="m-0 d-print-none">{{$item->budget_reference}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="mt-3">
                            <p><b>{{__('Hello')}}, {{$item->contact_name}}</b></p>
                            <p class="text-muted">{{__('Thanks a lot because you keep purchasing our products. Our company
                                promises to provide high quality products for you as well as outstanding
                                customer service for every transaction.')}} </p>
                            <p><i class="fa fa-building mr-1"></i>{{$item->company_name}}</p>
                            <p><i class="fa fa-phone mr-1"></i>{{$item->phone}} &emsp; <i class="fa fa-envelope mr-1"></i>{{$item->email}}</p>
                        </div>
                    </div><!-- end col -->
                    <div class="col-md-5">
                        <div class="mt-3 float-right">
                            <p class="mb-2"><strong>{{__('Budget Date ')}} : </strong>&emsp;<span class="float-right">{{date('d M Y', strtotime($item->created_at))}}</span></p>
                            <p class="mb-2"><strong>{{__('Expire Date ')}}: </strong>&emsp;<span class="float-right">{{ !empty($item->expired_at) ? date('d M Y', strtotime($item->expired_at)) : '' }}</span></p>
                            <p class="mb-3"><strong>{{__('Prepared By ')}}: </strong>&emsp;<span class="float-right">{{isset($item->salesman->name) ? $item->salesman->name : ''}}</span></p>
                            <h4>
                                <span class="p-1 badge badge-success">{{ quotation_status($item->status) }}</span>
                                <span class="p-1 badge badge-danger">{{ payment_status($item->payment_status) }}</span>
                            </h4>
                        </div>
                    </div><!-- end col -->
                </div>

                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 10%"></th>
                                        <th style="">{{__('Item')}}</th>
                                        <th style="width: 10%">{{ __('Quantity') }}</th>
                                        <th style="width: 10%">{{ __('Unit Price') }}</th>
                                        <th style="width: 5%">{{ __('Vat') }}</th>
                                        <th style="width: 15%" class="text-right">{{__('Total')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($products[0]))
                                    @php $totalVat = 0; $totalPrice = 0 ; @endphp
                                    @foreach($products as $product)
                                        @php
                                            $totalPrice = $totalPrice + $product->total_price;
                                            $totalVat = $totalVat + $product->total_vat;
                                        @endphp
                                        <tr>
                                            <td>
                                                @if (!empty($product->product_id ))
                                                    @php $combination = get_product_combination($product->product_id, $product->combination_id, $product->combination_type_id) @endphp
                                                @endif
                                                <img class="avatar-md" src="{{isset($combination) ? $combination['image'] : ''}}" onerror='this.src="{{adminAsset('images/no-image.png')}}"'>
                                            </td>
                                            <td>
                                                <p>{{ $product->product_name}} {{$combination['combination'] ? '('.$combination['combination'].')' : '' }}</p>
                                                <b>{{ $item->budget_masking_enable == ACTIVE ? $product->product_mask_reference : $product->product_reference }}</b>
                                                <p>{{ $product->comments}}</p>
                                            </td>
                                            <td> {{$product->quantity}} </td>
                                            <td> {{getBrindeMoney($product->unit_price)}} </td>
                                            <td>
                                                <b>{{$product->vat}}%</b> <br/>
                                                ({{getBrindeMoney(($product->vat*$product->unit_price)/100)}})
                                            </td>
                                            <td class="text-right">
                                                <b>{{__('Price:')}}</b> {{getBrindeMoney($product->total_price)}}<br>
                                                <b>{{__('Vat')}}</b> {{getBrindeMoney($product->total_vat)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>{{__('No item found')}}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div> <!-- end table-responsive -->
                    </div> <!-- end col -->
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="clearfix pt-1">
                            <h6 class="text-muted">{{__('Notes:')}}</h6>
                            <small class="text-muted">
                                {{__('All accounts are to be paid within 7 days from receipt of
                                invoice. To be paid by cheque or credit card or direct payment
                                online. If account is not paid within 7 days the credits details
                                supplied as confirmation of work undertaken will be charged the
                                agreed quoted fee noted above.')}}
                            </small>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="float-right">
                            @if($item->budget_show_total_enable == ACTIVE)
                                <h4>{{__('Total Price: ')}}&emsp;<span class="float-right">{{getBrindeMoney($totalPrice)}}</span></h4>
                                <h4>{{__('Total VAT: ')}}&emsp;<span class="float-right">{{getBrindeMoney($totalVat)}}</span></h4>
                                <h4>{{__('Total Budget: ')}}&emsp;<span class="float-right">{{getBrindeMoney($totalVat + $totalPrice)}}</span></h4>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

                <div class="mt-4 mb-1">
                    <div class="text-right d-print-none">
                        <a href="javascript:window.print()" class="btn btn-info waves-effect waves-light"><i class="fa fa-print mr-1"></i> {{__('Print')}}</a>
                        @if($item->expired_at > date('Y-m-d'))
                            @if($item->status < STATUS_BUDGET_APPROVED)
                                &emsp;<button data-toggle="modal" data-target="#modify-request-modal" class="btn btn-warning waves-effect waves-light modify_button"><i class="fa fa-edit mr-1"></i> {{__('Modify Request')}}</button>
                                &emsp;<button class="btn btn-success waves-effect waves-light approve_budget" data-id="{{$item->id}}"><i class="fa fa-thumbs-up"></i> {{__('Approve')}}</button>
                            @endif
                            @if($item->status > STATUS_BUDGET_APPROVED && ($item->payment_status != PAYMENT_DONE) && !empty($item->mb_reference))
                                <h5>{{__('Please make payment using this')}}</h5>
                                {{$item->mb_reference}}
                            @endif
                        @else
                            <span class="p-2 badge badge-danger float-left font-16">{{__('Budget Expired!')}}</span>
                        @endif
                    </div>
                </div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->
    </div>
    @include('customer.budget.budget_modification_modal')
@endsection
@section('script')
    <script>
        resetValidation('send_modification-form');
        submitOperation(updateBudgetResponse, 'send_modification');

        function updateBudgetResponse(response, this_form){
            if (response.success == true) {
                $("#modify-request-modal").modal('hide');
                swalSuccess(response.message);
                this_form.removeClass('was-validated');
            } else {
                swalError(response.message);
            }
        }


        var approve_url = "{{route('approveBudget')}}";
        makeApproveOperation(approveResponse,'approve_budget',approve_url);

        function approveResponse(response){
            if(response.success == false) {
                swalError(response.message);
            } else {
                swalSuccess(response.message);
                console.log(response.data['need_payment']);
                $(".approve_budget").remove();
                $(".modify_button").remove();
                if(response.data['need_payment'] == 1) {
                    window.location.href = '{{route('paymentPage', encrypt($item->id))}}';
                }
            }
        }
    </script>
@endsection
