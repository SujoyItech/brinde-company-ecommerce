<div id="modify-request-modal" class="product-modal modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('Send Budget Modify Request')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <form action="{{route('sendBudgetModifyRequest')}}" novalidate method="post" id="send_modification-form-id" class="send_modification-form">
                    <input type="hidden" name="quotation_id" value="{{$item->id}}">
                    <div class="form-group">
                        <label for="">{{__('Modifications')}} </label>
                        <textarea name="budget_modification_comments" required class="form-control" cols="30" rows="3" placeholder="{{__('Budget modification comments')}}"></textarea>
                        <div class="valid-feedback">{{__('Looks good!')}}</div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="button" class="btn btn-secondary waves-effect mx-2" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;{{__('Close')}}</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light send_modification mx-2"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;{{__('Send')}}</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
