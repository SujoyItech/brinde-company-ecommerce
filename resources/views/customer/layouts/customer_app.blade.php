<!DOCTYPE html>
<html lang="en">
@php($settings = __options(['site_settings', 'logo_settings', 'social_settings']))

<head>
    <meta charset="utf-8"/>
    <title>
        {{$settings->app_title ?? env('APP_NAME')}}
        @if (trim($__env->yieldContent('title')))
            :: @yield('title')
        @endif
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description"/>
    <meta content="ItechCoders" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="laraframe" content="{{ csrf_token() }}"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{isset($settings->favicon_logo) && !empty($settings->favicon_logo) ? asset(get_image_path('settings').'/'.$settings->favicon_logo) :  adminAsset('images/favicon.png')}}">
    @include('admin.includes.header_asset')
</head>
<body>
    <div class="py-5 px-5">
        @yield('content')
    </div>
@include('admin.includes.footer_asset')

</body>
</html>
