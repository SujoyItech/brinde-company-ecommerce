@extends('auth.layout.app',['title'=>'Reset password'])
@section('content')
    @php($settings = __options(['logo_settings']))
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
            <div class="card bg-pattern">
                <div class="card-body p-4">

                    <div class="text-center w-75 m-auto">
                        <div class="auth-logo">
                            <a href="" class="logo logo-dark text-center">
                                <span class="logo-lg">
                                    <img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-sm.png') }}" alt="" height="22">
                                </span>
                            </a>

                            <a href="" class="logo logo-light text-center">
                                <span class="logo-lg">
                                    <img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-sm.png') }}" alt="" height="22">
                                </span>
                            </a>
                        </div>
                        <p class="text-muted mb-4 mt-3">{{__('Change your password from here.')}}</p>
                    </div>

                    <form method="post" action="{{ route('password.update') }}">
                        @csrf
                        <div class="form-group mb-3">
                            <label for="">{{__('Email')}}</label>
                            <input type="email" name="email" class="form-control" value="{{old('email', $request->email)}}" required>
                            @if($errors->first('email'))<span class="text-danger">{{$errors->first('email')}}</span> @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="">{{__('Password')}}</label>
                            <input type="password" name="password" class="form-control" placeholder="Minimum 6 character">
                            @if($errors->first('password'))<span class="text-danger">{{$errors->first('password')}}</span> @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="">{{__('Confirm Password')}}</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Minimum 6 character">
                            @if($errors->first('password_confirmation'))<span class="text-danger">{{$errors->first('password_confirmation')}}</span> @endif
                        </div>
                        <div class="form-group mb-0 text-center">
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">
                            <button data-style="zoom-in" class="btn btn-primary btn-block" type="submit"> {{__('Reset Password ')}}</button>
                        </div>
                    </form>
                </div> <!-- end card-body -->
            </div>
            <!-- end card -->

            <div class="row mt-3">
                <div class="col-12 text-center">
                    <p> <a href="{{route('login')}}" class="text-white-50 ml-1">{{__('Sign In?')}}</a></p>
                </div>
            </div>

        </div>
    </div>
@endsection
