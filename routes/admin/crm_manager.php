<?php
use Illuminate\Support\Facades\Route;

Route::get('crm-manager-home', 'Web\Admin\CRM\DashboardController@index')->name('crmManagerHome');

/*====================================PaymentCondition==========================================*/
Route::get('crm-payment-condition','Web\Admin\CRM\CrmPaymentConditionController@index')->name('crmPaymentCondition');
Route::post('crm-payment-condition-edit','Web\Admin\CRM\CrmPaymentConditionController@edit')->name('editCrmPaymentCondition');
Route::post('crm-payment-condition-save','Web\Admin\CRM\CrmPaymentConditionController@store')->name('storeCrmPaymentCondition');
Route::post('crm-payment-condition-delete','Web\Admin\CRM\CrmPaymentConditionController@delete')->name('deleteCrmPaymentCondition');
/*====================================PaymentCondition==========================================*/

/*====================================PrintingOption==========================================*/
Route::get('crm-printing-option','Web\Admin\CRM\CrmPrintingOptionController@index')->name('crmPrintingOption');
Route::post('crm-printing-option-edit','Web\Admin\CRM\CrmPrintingOptionController@edit')->name('editCrmPrintingOption');
Route::post('crm-printing-option-save','Web\Admin\CRM\CrmPrintingOptionController@store')->name('storeCrmPrintingOption');
Route::post('crm-printing-option-delete','Web\Admin\CRM\CrmPrintingOptionController@delete')->name('deleteCrmPrintingOption');
/*====================================PrintingOption==========================================*/

/*====================================QuotationManagement==========================================*/
Route::get('crm-customer-list','Web\Admin\CRM\CustomerController@customerList')->name('crmCustomerList');
Route::any('crm-customer-details/{customer_code}','Web\Admin\CRM\CustomerController@customerDetails')->name('customerDetails');
Route::get('get-customer-list-by-salesman','Web\Admin\CRM\CustomerController@getCustomerListBySalesMan')->name('getCustomerListBySalesMan');
Route::get('crm-customer-add/{quotation_id?}','Web\Admin\CRM\CustomerController@customerAdd')->name('crmCustomerAdd');
Route::get('crm-customer-edit/{id}/{quotation_id?}','Web\Admin\CRM\CustomerController@customerEdit')->name('crmCustomerEdit');
Route::post('crm-customer-save','Web\Admin\CRM\CustomerController@customerSave')->name('crmCustomerSave');
Route::post('crm-customer-delete','Web\Admin\CRM\CustomerController@customerDelete')->name('crmCustomerDelete');


Route::get('crm-quotation-list/{brand_id?}','Web\Admin\CRM\CrmQuotationController@index')->name('CrmQuotationList');
Route::post('crm-assign-salesman','Web\Admin\CRM\CrmQuotationController@assignSalesman')->name('CrmAssignSalesman');
Route::get('crm-quote-view/{code?}', 'Web\Admin\CRM\CrmQuotationController@quotationDetails')->name('crmQuotationView');
/*====================================QuotationManagement==========================================*/

/*====================================BudgetManagement==========================================*/
Route::any('crm-budget-list','Web\Admin\CRM\CrmBudgetController@index')->name('crmBudgetList');
Route::get('crm-budget-view/{code?}', 'Web\Admin\CRM\CrmBudgetController@budgetDetails')->name('crmBudgetDetails');
Route::post('crm-budget-save', 'Web\Admin\CRM\CrmBudgetController@saveBudget')->name('crmSaveBudget');
Route::post('crm-budget-create-save', 'Web\Admin\CRM\CrmBudgetController@crmBudgetCreateSave')->name('crmBudgetCreateSave');
Route::post('crm-budget-product-save', 'Web\Admin\CRM\CrmBudgetController@saveBudgetProduct')->name('crmSaveBudgetProduct');
Route::post('crm-budget-product-update', 'Web\Admin\CRM\CrmBudgetController@updateBudgetProduct')->name('crmUpdateBudgetProduct');
Route::post('crm-budget-product-duplicate', 'Web\Admin\CRM\CrmBudgetController@duplicateBudgetProduct')->name('crmDuplicateBudgetProduct');
Route::post('crm-budget-product-delete', 'Web\Admin\CRM\CrmBudgetController@deleteBudgetProduct')->name('crmDeleteBudgetProduct');
Route::post('send-budget-mail', 'Web\Admin\CRM\CrmBudgetController@sendBudgetMail')->name('crmSendBudgetMail');
Route::post('product-search-for-budget', 'Web\Admin\CRM\CrmBudgetController@budgetProductSearch')->name('crmBudgetProductSearch');
Route::post('budget-product-edit', 'Web\Admin\CRM\CrmBudgetController@budgetProductEdit')->name('crmBudgetProductEdit');
Route::get('print-budget-details/{code?}', 'Web\Admin\CRM\CrmBudgetController@printBudget')->name('crmPrintBudget');
Route::post('crm-budget-approve', 'Web\Admin\CRM\CrmBudgetController@crmBudgetApprove')->name('crmBudgetApprove');
Route::post('crm-budget-make-order', 'Web\Admin\CRM\CrmBudgetController@makeBudgetOrder')->name('crmMakeBudgetOrder');

Route::any('crm-all-lists', 'Web\Admin\CRM\DashboardController@crmAllList')->name('crmAllList');
Route::any('crm-all-list-details/{code}', 'Web\Admin\CRM\DashboardController@crmAllListDetails')->name('crmAllListDetails');

/*====================================BudgetManagement==========================================*/




