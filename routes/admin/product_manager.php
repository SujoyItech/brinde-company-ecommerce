<?php
use Illuminate\Support\Facades\Route;

Route::get('product-manager-home', 'Web\Admin\Product\DashboardController@index')->name('productManagerHome');

Route::get('supplier','Web\Admin\Product\SupplierController@index')->name('supplier');
Route::post('supplier-edit','Web\Admin\Product\SupplierController@edit')->name('editSupplier');
Route::post('supplier-save','Web\Admin\Product\SupplierController@store')->name('saveSupplier');
Route::post('supplier-delete','Web\Admin\Product\SupplierController@delete')->name('deleteSupplier');

Route::get('delivery-type','Web\Admin\Product\DeliveryTypeController@index')->name('deliveryType');
Route::post('delivery-type-edit','Web\Admin\Product\DeliveryTypeController@edit')->name('editDeliveryType');
Route::post('delivery-type-save','Web\Admin\Product\DeliveryTypeController@store')->name('storeDeliveryType');
Route::post('delivery-type-delete','Web\Admin\Product\DeliveryTypeController@delete')->name('deleteDeliveryType');

Route::get('tag','Web\Admin\Product\TagController@index')->name('tags');
Route::post('tag-edit','Web\Admin\Product\TagController@edit')->name('editTag');
Route::post('tag-save','Web\Admin\Product\TagController@store')->name('storeTag');
Route::post('tag-delete','Web\Admin\Product\TagController@delete')->name('deleteTag');

Route::get('brand','Web\Admin\Product\BrandController@index')->name('brands');
Route::post('brand-edit','Web\Admin\Product\BrandController@edit')->name('editBrand');
Route::post('brand-save','Web\Admin\Product\BrandController@store')->name('saveBrand');
Route::post('brand-slug-check','Web\Admin\Product\BrandController@brandSlugCheck')->name('brandSlugCheck');
Route::post('brand-delete','Web\Admin\Product\BrandController@delete')->name('deleteBrand');

Route::get('category/{id?}','Web\Admin\Product\CategoryController@index')->name('categories');
Route::post('category-edit','Web\Admin\Product\CategoryController@edit')->name('editCategory');
Route::post('category-show','Web\Admin\Product\CategoryController@show')->name('showCategory');
Route::post('category-save','Web\Admin\Product\CategoryController@store')->name('saveCategory');
Route::post('category-delete','Web\Admin\Product\CategoryController@delete')->name('deleteCategory');
Route::post('category-order-update','Web\Admin\Product\CategoryController@categoryOrderUpdate')->name('categoryOrderUpdate');
Route::post('category-order-save','Web\Admin\Product\CategoryController@categoryOrderSave')->name('categoryOrderSave');
Route::post('category-slug-check','Web\Admin\Product\CategoryController@categorySlugCheck')->name('categorySlugCheck');

Route::get('combination-type','Web\Admin\Product\CombinationTypeController@index')->name('combinationType');
Route::post('combination-type-edit','Web\Admin\Product\CombinationTypeController@edit')->name('editCombinationType');
Route::post('combination-type-save','Web\Admin\Product\CombinationTypeController@store')->name('storeCombinationType');
Route::post('combination-type-delete','Web\Admin\Product\CombinationTypeController@delete')->name('deleteCombinationType');

Route::get('combination','Web\Admin\Product\CombinationController@index')->name('combination');
Route::post('combination-edit','Web\Admin\Product\CombinationController@edit')->name('editCombination');
Route::post('combination-save','Web\Admin\Product\CombinationController@store')->name('storeCombination');
Route::post('combination-delete','Web\Admin\Product\CombinationController@delete')->name('deleteCombination');

Route::any('product','Web\Admin\Product\ProductController@index')->name('product');
Route::get('product-edit/{reference}','Web\Admin\Product\ProductController@edit')->name('editProduct');
Route::post('product-init','Web\Admin\Product\ProductController@initProduct')->name('initProduct');
Route::post('product-save','Web\Admin\Product\ProductController@saveProduct')->name('saveProduct');
Route::post('product-delete','Web\Admin\Product\ProductController@delete')->name('deleteProduct');
Route::post('product-slug-check','Web\Admin\Product\ProductController@productSlugCheck')->name('productSlugCheck');
Route::post('product-reference-check','Web\Admin\Product\ProductController@productReferenceCheck')->name('productReferenceCheck');
Route::post('product-status-change','Web\Admin\Product\ProductController@productStatusChange')->name('productStatusChange');
Route::post('product-categories-tags-save','Web\Admin\Product\ProductController@saveProductCategoriesAndTags')->name('saveProductCategoriesAndTags');
Route::post('product-supplier','Web\Admin\Product\ProductController@getProductSupplier')->name('getProductSupplier');
Route::post('product-pricing-save','Web\Admin\Product\ProductController@saveProductPricing')->name('saveProductPricing');
Route::post('product-combination-save','Web\Admin\Product\ProductController@saveProductCombinations')->name('saveProductCombinations');
Route::post('product-media-save','Web\Admin\Product\ProductController@saveProductMedia')->name('saveProductMedia');
Route::post('product-featured-media-save','Web\Admin\Product\ProductController@saveProductFeaturedMedia')->name('saveProductFeaturedMedia');
Route::post('delete-product-combination','Web\Admin\Product\ProductController@deleteProductCombination')->name('deleteProductCombination');
Route::post('product-duplicate','Web\Admin\Product\ProductController@duplicateProduct')->name('duplicateProduct');

Route::get('product-upload','Web\Admin\Product\ProductController@productUpload')->name('productUpload');
Route::post('delete-product-excel','Web\Admin\Product\ProductController@deleteProductExcel')->name('deleteProductExcel');
Route::get('product-export','Web\Admin\Product\ProductController@productExport')->name('productExport');
Route::post('product-import','Web\Admin\Product\ProductController@productImport')->name('productImport');
Route::post('product-upload-process','Web\Admin\Product\ProductController@productUploadProcess')->name('productUploadProcess');
Route::get('product-upload-failed/{payload?}','Web\Admin\Product\ProductController@productUploadFailedList')->name('productUploadFailedList');
Route::post('delete-upload-failed-product','Web\Admin\Product\ProductController@deleteUploadFailProduct')->name('deleteUploadFailProduct');




