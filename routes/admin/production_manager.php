<?php
use Illuminate\Support\Facades\Route;

Route::get('production-manager-home', 'Web\Admin\Production\DashboardController@index')->name('productionManagerHome');

/*====================================PurchaseManagement==========================================*/
Route::any('production-order-list','Web\Admin\Production\PurchaseController@index')->name('productionOrderList');
Route::get('production-order-details/{code?}','Web\Admin\Production\PurchaseController@productionOrderDetails')->name('productionOrderDetails');
Route::post('production-order-product-purchase','Web\Admin\Production\PurchaseController@productionOrderProductPurchase')->name('productionOrderProductPurchase');
Route::post('production-order-save','Web\Admin\Production\PurchaseController@productionOrderSave')->name('productionOrderSave');
Route::post('production-complete-save','Web\Admin\Production\PurchaseController@productionCompleteSave')->name('productionCompleteSave');

/*====================================PurchaseManagement==========================================*/

/*====================================LogisticManagement==========================================*/
Route::any('production-order-purchase-list','Web\Admin\Production\LogisticController@index')->name('productionPurchaseList');
Route::get('production-order-purchase-details/{code?}','Web\Admin\Production\LogisticController@productionPurchaseDetails')->name('productionPurchaseDetails');
Route::post('save-arrival-quantity','Web\Admin\Production\LogisticController@saveArrivalQuantity')->name('saveArrivalQuantity');
Route::get('send-to-production/{code?}','Web\Admin\Production\LogisticController@sendToProduction')->name('sendToProduction');
Route::post('send-to-shipment','Web\Admin\Production\LogisticController@sendToShipment')->name('sendToShipment');

/*====================================LogisticManagement==========================================*/

/*====================================ProductionManagement==========================================*/
Route::any('production-list','Web\Admin\Production\ProductionController@productionList')->name('productionList');
Route::get('production-details/{code?}','Web\Admin\Production\ProductionController@productionDetails')->name('productionDetails');
Route::any('production-completed-list','Web\Admin\Production\ProductionController@productionCompletedList')->name('productionCompletedList');
Route::get('production-completed-details/{code}','Web\Admin\Production\ProductionController@productionCompleteDetails')->name('productionCompleteDetails');
/*====================================ProductionManagement==========================================*/

/*====================================ShipmentManagement==========================================*/
Route::any('production-shipment-list','Web\Admin\Production\ShipmentController@index')->name('productionShipmentList');
Route::any('production-shipment-details/{code?}','Web\Admin\Production\ShipmentController@productionShipmentDetails')->name('productionShipmentDetails');
Route::post('production-shipment-complete','Web\Admin\Production\ShipmentController@productionShipmentComplete')->name('productionShipmentComplete');


/*====================================ShipmentManagement==========================================*/
Route::any('production-expire-today','Web\Admin\Production\DashboardController@productionExpireToday')->name('productionExpireToday');
Route::any('production-expire-tomorrow','Web\Admin\Production\DashboardController@productionExpireTomorrow')->name('productionExpireTomorrow');
Route::any('production-expired','Web\Admin\Production\DashboardController@productionExpired')->name('productionExpired');


