<?php
use Illuminate\Support\Facades\Route;

Route::get('super-admin-home', 'Web\Admin\SuperAdmin\DashboardController@index')->name('superAdminHome');
Route::get('download-pdf', 'Web\Pdf\PdfController@downloadPdf')->name('downloadPdf');
Route::get('view-pdf', 'Web\Pdf\PdfController@viewPdf')->name('viewPdf');




