<?php
Route::get('admin-home', 'Web\Admin\DashboardController@index')->name('adminHome');

/*==============================================Settings===============================================*/

Route::get('commands-settings','Web\Admin\Settings\SettingsController@commandSettings')->name('commandSettings');
Route::post('runCommand','Web\Admin\Settings\SettingsController@runCommand')->name('runCommand');

Route::post('create-command','Web\Admin\Settings\SettingsController@createCommand')->name('createCommand');




/*==============================================Settings===============================================*/

/*==============================================Roles && Permissions=======================================*/
Route::get('roles', 'Web\Admin\Role\RolePermissionController@index')->name('roles');
Route::post('role-save', 'Web\Admin\Role\RolePermissionController@store')->name('saveRole');
Route::post('role-edit', 'Web\Admin\Role\RolePermissionController@edit')->name('editRole');
Route::post('role-delete', 'Web\Admin\Role\RolePermissionController@delete')->name('deleteRole');
Route::get('update-routing-list','Web\Admin\Role\RouteController@updateRouteList')->name('updateRouteList');
Route::post('get-routes-by-type','Web\Admin\Role\RouteController@getRouteByType')->name('getRouteByType');
/*==============================================Roles && Permissions=======================================*/


/*==============================================Users===============================================*/
Route::get('users', 'Web\Admin\User\UserController@index')->name('users');
Route::post('user-save', 'Web\Admin\User\UserController@store')->name('saveUser');
Route::post('user-edit', 'Web\Admin\User\UserController@edit')->name('editUser');
Route::post('user-delete', 'Web\Admin\User\UserController@delete')->name('deleteUser');
Route::post('user-slug-check','Web\Admin\User\UserController@userSlugCheck')->name('userSlugCheck');
/*==============================================Users===============================================*/

/*==============================================User Brands=======================================*/
Route::get('user-brands', 'Web\Admin\User\UserController@userBrands')->name('userBrands');
Route::post('user-brand-check', 'Web\Admin\User\UserController@userBrandCheck')->name('userBrandCheck');

Route::get('subscriber-list', 'Web\Admin\BrindeController@subscriberList')->name('subscriberList');
Route::post('delete-subscriber', 'Web\Admin\BrindeController@deleteSubscriber')->name('deleteSubscriber');
Route::post('send-message-to-subscriber', 'Web\Admin\BrindeController@sendMessageToAllSubscribers')->name('sendMessageToAllSubscribers');
Route::get('messages', 'Web\Admin\BrindeController@messages')->name('messages');
Route::post('delete-messages', 'Web\Admin\BrindeController@deleteMessage')->name('deleteMessage');
Route::post('reply-messages-to-user', 'Web\Admin\BrindeController@replyMessageToUser')->name('replyMessageToUser');
/*==============================================User Brands=======================================*/

// Logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('adminLogs');

