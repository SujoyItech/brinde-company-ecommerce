<?php
use Illuminate\Support\Facades\Route;
Route::get('website-pages/{brand_id?}', 'Web\Admin\Website\WebsitePageController@index')->name('websitePages');
Route::post('website-pages-edit','Web\Admin\Website\WebsitePageController@edit')->name('editWebsitePage');
Route::post('website-pages-save','Web\Admin\Website\WebsitePageController@store')->name('saveWebsitePage');
Route::post('website-pages-delete','Web\Admin\Website\WebsitePageController@delete')->name('deleteWebsitePage');
Route::post('website-pages-status-change','Web\Admin\Website\WebsitePageController@websitePageStatusChange')->name('websitePageStatusChange');
Route::get('website-pages-details/{id}','Web\Admin\Website\WebsitePageController@websitePageDetails')->name('websitePageDetails');

Route::post('website-pages-layer-edit','Web\Admin\Website\WebsitePageLayerController@editWebsitePageLayer')->name('editWebsitePageLayer');
Route::post('website-pages-layer-save','Web\Admin\Website\WebsitePageLayerController@saveWebsitePageLayer')->name('saveWebsitePageLayer');

Route::post('website-pages-layer-order-update','Web\Admin\Website\WebsitePageLayerController@websitePageLayerOrderUpdate')->name('websitePageLayerOrderUpdate');
Route::post('website-pages-layer-status-change','Web\Admin\Website\WebsitePageLayerController@websitePageLayerStatusChange')->name('websitePageLayerStatusChange');
Route::post('website-pages-layer-delete','Web\Admin\Website\WebsitePageLayerController@websitePageLayerDelete')->name('websitePageLayerDelete');


Route::post('show-layer-content-modal','Web\Admin\Website\WebsitePageLayerController@showLayerContentModal')->name('showLayerContentModal');
Route::post('save-layer-content','Web\Admin\Website\WebsitePageLayerController@saveLayerContent')->name('saveLayerContent');
Route::post('save-banner-layer-content','Web\Admin\Website\WebsitePageLayerController@saveBannerLayerContent')->name('saveBannerLayerContent');
Route::post('layer-content-delete','Web\Admin\Website\WebsitePageLayerController@layerContentDelete')->name('layerContentDelete');

Route::post('get-category-slug-by-id','Web\Admin\Website\WebsitePageLayerController@getCategorySlugById')->name('getCategorySlugById');

Route::get('render-product-by-category/{category_id}','Web\Admin\Website\WebsitePageLayerController@renderProductByCategory')->name('renderProductByCategory');


Route::get('website-categories','Web\Admin\Website\WebsiteCategoriesMenuController@websiteCategories')->name('websiteCategories');
Route::post('show-category-image-add-modal','Web\Admin\Website\WebsiteCategoriesMenuController@showCategoryImageAddModal')->name('showCategoryImageAddModal');
Route::post('website-category-image-save','Web\Admin\Website\WebsiteCategoriesMenuController@saveWebsiteCategoryImage')->name('saveWebsiteCategoryImage');
Route::post('website-category-image-delete','Web\Admin\Website\WebsiteCategoriesMenuController@categoryImageDelete')->name('categoryImageDelete');

/*==============================================Settings======================================*/

Route::get('site-settings','Web\Admin\Settings\SettingsController@siteSetting')->name('siteSetting');
Route::get('logo-settings','Web\Admin\Settings\SettingsController@logoSetting')->name('logoSetting');
Route::get('social-settings','Web\Admin\Settings\SettingsController@socialSetting')->name('socialSetting');
Route::get('application-settings','Web\Admin\Settings\SettingsController@applicationSetting')->name('applicationSetting');
Route::post('admin-settings-save','Web\Admin\Settings\SettingsController@adminSettingsSave')->name('adminSettingsSave');

Route::get('terms-condition-settings','Web\Admin\Settings\SettingsController@termsConditionSettings')->name('termsConditionSettings');
Route::get('privacy-policy-settings','Web\Admin\Settings\SettingsController@privacyPolicySettings')->name('privacyPolicySettings');
Route::get('about-us-settings','Web\Admin\Settings\SettingsController@aboutUsSettings')->name('aboutUsSettings');
Route::get('why-choose-us-settings','Web\Admin\Settings\SettingsController@whyChooseUsSettings')->name('whyChooseUsSettings');
Route::get('team-settings','Web\Admin\Settings\SettingsController@teamSettings')->name('teamSettings');

Route::get('faqs-settings','Web\Admin\Settings\SettingsController@faqsSettings')->name('faqsSettings');
Route::post('faqs-setting-save','Web\Admin\Settings\SettingsController@faqsSettingSave')->name('faqsSettingSave');
Route::post('faqs-item-delete','Web\Admin\Settings\SettingsController@deleteFaqsItem')->name('deleteFaqsItem');
Route::post('faqs-item-by-id','Web\Admin\Settings\SettingsController@getFaqItemById')->name('getFaqItemById');

Route::get('team-settings','Web\Admin\Settings\SettingsController@teamSettings')->name('teamSettings');
Route::post('team-setting-save','Web\Admin\Settings\SettingsController@teamSettingSave')->name('teamSettingSave');
Route::post('team-item-delete','Web\Admin\Settings\SettingsController@deleteTeamItem')->name('deleteTeamItem');
Route::post('team-item-by-id','Web\Admin\Settings\SettingsController@getTeamItemById')->name('getTeamItemById');

Route::get('contact-settings','Web\Admin\Settings\SettingsController@contactSettings')->name('contactSettings');
Route::post('contact-setting-save','Web\Admin\Settings\SettingsController@contactSettingSave')->name('contactSettingSave');
Route::post('contact-item-delete','Web\Admin\Settings\SettingsController@deleteContactItem')->name('deleteContactItem');
Route::post('contact-item-by-id','Web\Admin\Settings\SettingsController@getContactItemById')->name('getContactItemById');

/*==============================================Settings======================================*/

