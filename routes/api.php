<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login','Api\Auth\AuthController@login');
Route::post('register','Api\Auth\AuthController@register');
Route::post('send-forget-password-mail', 'Api\Auth\AuthController@sendForgetPasswordMail')->name('sendForgetPasswordMail');
Route::post('reset-password', 'Api\Auth\AuthController@resetPassword')->name('resetPassword');

Route::post('customer-verify-email','Api\Auth\AuthController@customerVerifyEmail')->name('customerVerifyEmail');


Route::group(['middleware'=>['auth:api','customer_status']],function (){
    Route::post('submit-quotation-auth-request','Api\CRM\QuotationController@quotationAuthRequest');
    Route::get('get-customer','Api\CRM\CrmCustomerController@getCustomer');
    Route::post('update-customer-profile','Api\CRM\CrmCustomerController@updateCustomerProfile');
    Route::post('update-customer-password','Api\CRM\CrmCustomerController@updateCustomerPassword');
});

Route::get('country-list','Api\BrindeController@countryList');
Route::get('categories/{brand_slug}/{parent_slug?}','Api\Product\ProductController@categoryList');
Route::get('product/{slug}','Api\Product\ProductController@product');
Route::any('product-list/{brand_slug?}','Api\Product\ProductController@productList');
Route::get('get-color-list/{category_id?}','Api\Product\ProductController@getColorList');
Route::get('get-delivery-types','Api\Product\ProductController@getDeliveryTypes');

Route::get('search-product/{brand}/{keywords}/{order?}','Api\Product\ProductController@searchProduct')->name('searchProduct');

Route::get('webpage-list/{brand_slug}','Api\Website\WebsiteController@webpageList')->name('webpageList');
Route::get('webpage-content/{page_slug}','Api\Website\WebsiteController@webpageContent')->name('webpageContent');
Route::get('homepage-content/{brand_slug}','Api\Website\WebsiteController@homePageContent')->name('homePageContent');

Route::post('subscribe','Api\BrindeController@subscribe')->name('subscribe');
Route::post('un-subscribe','Api\BrindeController@unSubscribe')->name('unSubscribe');
Route::post('send-message','Api\BrindeController@sendMessage')->name('sendMessage');

Route::get('web-content-extra','Api\BrindeController@webExtraContent')->name('webExtraContent');
Route::get('faqs','Api\BrindeController@faqsApi')->name('faqsApi');
Route::get('teams','Api\BrindeController@teamsApi')->name('teamsApi');
Route::get('contacts','Api\BrindeController@contactsApi')->name('contactsApi');
Route::get('social-links','Api\BrindeController@socialLinkApis')->name('socialLinkApis');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('submit-quotation-request','Api\CRM\QuotationController@quotationRequest');
