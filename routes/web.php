<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', 'Web\Landing\LandingController@index')->name('landing');
Route::get('/', 'Web\Auth\AuthController@login')->name('landing');
Route::get('budget-details-{id?}', 'Web\Admin\CRM\CustomerBudget\CustomerBudgetController@clientBudgetDetails')->name('clientBudgetDetails');
Route::post('send-budget-modify-request', 'Web\Admin\CRM\CustomerBudget\CustomerBudgetController@sendBudgetModifyRequest')->name('sendBudgetModifyRequest');
Route::post('approve-budget', 'Web\Admin\CRM\CustomerBudget\CustomerBudgetController@approveBudget')->name('approveBudget');

Route::get('/payment/{quotation_id}', 'Web\Payment\PaymentController@index')->name('paymentPage');
//http://www.yoursite.com/ifthenpay-callback?chave=[CHAVE_ANTI_PHISHING]&entidade=[ENTIDADE]&referencia=[REFERENCIA]&valor=[VALOR]&datahorapag=[DATA_HORA_PAGAMENTO]&terminal=[TERMINAL]
Route::get('/ifthenpay-callback', 'Web\Payment\PaymentController@ifthenpayCallback')->name('ifthenpayCallback');
