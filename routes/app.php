<?php

use Illuminate\Support\Facades\Route;
Route::get('lang/{code}','HomeController@setLang')->name('setLang');

Route::group(['middleware'=>['language']],function (){
    Route::get('permission-denied', 'Web\Admin\DashboardController@permissionDenied')->name('permissionDenied');
    Route::get('profile', 'Web\Admin\Profile\ProfileController@index')->name('profile');
    Route::post('update-profile', 'Web\Admin\Profile\ProfileController@updateProfile')->name('updateProfile');
    Route::post('update-password', 'Web\Admin\Profile\ProfileController@updatePassword')->name('updatePassword');
    Route::post('update-user-avatar', 'Web\Admin\Profile\ProfileController@userAvatarUpdate')->name('userAvatarUpdate');
});

Route::get('order', 'Web\Admin\Settings\SettingsController@order')->name('order');
