<?php

namespace App\Http\Controllers\Web\Admin\Production;


use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Production\OrderSaveRequest;
use App\Http\Services\Production\PurchaseService;
use App\Models\Product\Brand;
use App\Models\Product\Supplier;
use App\Models\User;
use Illuminate\Http\Request;


class PurchaseController extends Controller {

    private $service;

    public function __construct(PurchaseService $service) {
        $this->service = $service;
    }


    /**
     * order list
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse|mixed
     *
     */

    public function index(Request $request)
    {
        $data['title'] = __('Order List');
        if ($request->has('search_data')){
            $lists = $this->service->getOrderData($request->search_data);
        }else{
            $lists = $this->service->getOrderData();
        }
        $data['brands'] = Brand::where('status',STATUS_ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();

        if ($request->ajax()) {
            return $this->showTableData($lists);
        }

        return view('admin.production.purchase.order_list', $data);
    }

    /**
     * show order table data
     * @param $lists
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Exception
     *
     */

    private function showTableData($lists)
    {
        return datatables($lists)->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('payment_status', function ($item) {
            return payment_status($item->payment_status);
        })->editColumn('expired_at', function ($item) {
            $html = '<p> <b><i class="fa fa-clock"> </i>'.__(' Created at : ').'</b>'.$item->created_at.'</p>';
            $html .= '<p><b><i class="fa fa-user-clock"> </i>'.__(' Expire at : ').'</b>'.$item->expired_at.'</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();

            $html = '';
            if(isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p><b><i class="fa fa-shopping-bag"> </i> ' . $product->product->name.' ('.$product->quantity.')' ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p><b><i class="fa fa-shopping-bag"> </i> ' . $product->product_name.' ('.$product->quantity.')' . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : '';
        })->editColumn('budget_printing_enable', function ($item) {
            return $item->budget_printing_enable == TRUE ? '<span class="badge badge-success">'.__('Yes').'</span>' : '<span class="badge badge-danger">'.__('No').'</span>';
        })->editColumn('in_staging', function ($item) {
            return quotation_stage($item->in_staging);
        })->editColumn('action',function ($item) {
            $html = '<a href="'.route('productionOrderDetails', $item->order_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Details').'</a>';

            return $html;
        })->rawColumns(['budget_printing_enable','status', 'action', 'expired_at', 'products'])
            ->make(TRUE);
    }

    /**
     * order details
     * @param $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     *
     */
    public function productionOrderDetails($code='')
    {
        $data['title'] = __('Order Details');
        $data['item'] = $this->service->firstWhere(['order_reference' => $code]);

        if (isset($data['item'])) {
            $data['products'] = $data['item']->quotation_products()->get();

            return view('admin.production.purchase.order_view', $data);
        }
        return redirect()->back()->with('dismiss',__('Data not found'));
    }

    /**
     * purchase product
     * @param Request $request
     * @return mixed
     *
     */
    public function productionOrderProductPurchase(Request $request)
    {
        return $this->service->purchaseProduct($request);
    }

    /**
     * @param OrderSaveRequest $request
     * @return mixed
     *
     */
    public function productionOrderSave(OrderSaveRequest $request)
    {
        return $this->service->updateProductionOrder($request);
    }

    public function productionCompleteSave(OrderSaveRequest $request)
    {
        return $this->service->updateProductionComplete($request);
    }
}
