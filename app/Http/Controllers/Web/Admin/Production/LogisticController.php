<?php

namespace App\Http\Controllers\Web\Admin\Production;


use App\Http\Controllers\Controller;
use App\Http\Services\Production\LogisticService;
use App\Models\Product\Brand;
use App\Models\User;
use Illuminate\Http\Request;


class LogisticController extends Controller {

    private $service;

    public function __construct(LogisticService $service) {
        $this->service = $service;
    }


    public function index(Request $request)
    {
        $data['title'] = __('Purchase List');
        if ($request->has('search_data')){
            $lists = $this->service->getOrderData($request->search_data);
        }else{
            $lists = $this->service->getOrderData();
        }
        $data['brands'] = Brand::where('status',STATUS_ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();

        if ($request->ajax()) {
            return $this->showTableData($lists);
        }

        return view('admin.production.logistic.purchase_list', $data);
    }

    private function showTableData($lists)
    {
        return datatables($lists)->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('payment_status', function ($item) {
            return payment_status($item->payment_status);
        })->editColumn('expired_at', function ($item) {
            $html = '<p class="m-0"> <b><i class="fa fa-clock"> </i>'.__(' Created at : ').'</b>'.$item->created_at.'</p>';
            $html .= '<p class="m-0"><b><i class="fa fa-user-clock"> </i>'.__(' Expire at : ').'</b>'.$item->expired_at.'</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();

            $html = '';
            if(isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p class="m-0"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product->name.' ('.$product->quantity.')' ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p class="m-0"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product_name.' ('.$product->quantity.')' . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : '';
        })->editColumn('budget_printing_enable', function ($item) {
            return $item->budget_printing_enable == TRUE ? '<span class="badge badge-success">'.__('Yes').'</span>' : '<span class="badge badge-danger">'.__('No').'</span>';
        })->editColumn('in_staging', function ($item) {
            return quotation_stage($item->in_staging);
        }) ->editColumn('action',function ($item) {
            $html = '<a href="'.route('productionPurchaseDetails', $item->purchase_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Details').'</a>';

            return $html;
        })->rawColumns(['budget_printing_enable','status', 'action', 'expired_at', 'products'])
            ->make(TRUE);
    }

    public function productionPurchaseDetails($code='') {
        $data['title'] = __('Purchase Details');
        $data['item'] = $this->service->getSingleRowData(['purchase_reference' => $code, 'status'=>['>=',STATUS_ORDER_PURCHASED_DONE]]);
        if (isset($data['item']) && $data['item']->status <= STATUS_ORDER_ARRIVED_DONE) {
            $data['products'] = $data['item']->quotation_products()->get();
            return view('admin.production.logistic.purchase_details', $data);
        }
        return redirect()->back()->with('dismiss',__('Data not found'));
    }

    public function saveArrivalQuantity(Request $request) {
        return $this->service->saveArrivalQuantity($request);
    }

    public function sendToProduction(Request $request) {
        return $this->service->sendToProduction($request->code);
    }
    public function sendToShipment(Request $request) {
        return $this->service->sendToShipment($request->id);
    }

}
