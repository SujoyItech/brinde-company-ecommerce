<?php

namespace App\Http\Controllers\Web\Admin\Production;

use App\Http\Controllers\Controller;
use App\Http\Services\CRM\QuotationService;
use App\Http\Services\DashboardService;
use App\Http\Services\Production\ProductionService;
use App\Models\Product\Brand;
use App\Models\User;
use Illuminate\Http\Request;


class DashboardController extends Controller {

    private $production_service;

    public function __construct(ProductionService $production_service){
        $this->production_service = $production_service;
    }

    public function index(Request $request){
        $data['production'] = $this->production_service->getDashboardData();
        return view('admin.production.dashboard.home',$data);
    }

    public function productionExpireToday(Request $request){
        if ($request->has('search_data')){
            $lists = $this->production_service->productionExpireToday($request->search_data);
        }else{
            $lists = $this->production_service->productionExpireToday();
        }
        if ($request->ajax()) {
           return $this->showTableData($lists);
        }
        $data['sub_menu'] = 'productionExpireToday';
        $data['brands'] = Brand::where('status',ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();
        return view('admin.production.lists.expire_today_list',$data);
    }

    public function productionExpireTomorrow(Request $request){
        if ($request->has('search_data')){
            $lists = $this->production_service->productionExpireTomorrow($request->search_data);
        }else{
            $lists = $this->production_service->productionExpireTomorrow();
        }
        if ($request->ajax()) {
            return $this->showTableData($lists);
        }
        $data['sub_menu'] = 'productionExpireTomorrow';
        $data['brands'] = Brand::where('status',ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();
        return view('admin.production.lists.expire_tomorrow_list',$data);

    }

    public function productionExpired(Request $request){

        if ($request->has('search_data')){
            $lists = $this->production_service->productionExpired($request->search_data);
        }else{
            $lists = $this->production_service->productionExpired();
        }
        if ($request->ajax()) {
           return $this->showTableData($lists);
        }
        $data['sub_menu'] = 'productionExpired';
        $data['brands'] = Brand::where('status',ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();
        return view('admin.production.lists.expired_list',$data);

    }

    private function showTableData($lists)
    {
        return datatables($lists)->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('payment_status', function ($item) {
            return payment_status($item->payment_status);
        })->editColumn('expired_at', function ($item) {
            $html = '<p class="m-0"> <b><i class="fa fa-clock"> </i>'.__(' Created at : ').'</b>'.$item->created_at.'</p>';
            $html .= '<p class="m-0"><b><i class="fa fa-user-clock"> </i>'.__(' Expire at : ').'</b>'.$item->expired_at.'</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();
            $html = '';
            if(isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p class="m-0"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product->name.' ('.$product->quantity.')' ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p class="m-0"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product_name.' ('.$product->quantity.')' . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : '';
        })->editColumn('in_staging', function ($item) {
            return quotation_stage($item->in_staging);
        }) ->editColumn('action',function ($item) {
            $html = '<a href="'.route('productionOrderDetails', ['code' =>$item->order_reference]).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Details').'</a>';

            return $html;
        })->rawColumns(['status', 'action', 'expired_at', 'products'])
                                 ->make(TRUE);
    }

}
