<?php

namespace App\Http\Controllers\Web\Admin\Production;


use App\Http\Controllers\Controller;
use App\Http\Services\Production\ProductionService;
use App\Models\Product\Brand;
use App\Models\User;
use Illuminate\Http\Request;


class ProductionController extends Controller {

    private $service;

    public function __construct(ProductionService $service) {
        $this->service = $service;
    }


    public function productionList(Request $request) {
        $data['title'] = __('In Production');
        $data['sub_menu'] = 'productionList';
        if ($request->has('search_data')) {
            $lists = $this->service->getOrderData($request->search_data);
        } else {
            $lists = $this->service->getOrderData();
        }
        $data['brands'] = Brand::where('status',STATUS_ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();
        if ($request->ajax()) {
            return $this->showTableData($lists);
        }
        return view('admin.production.production.production_list', $data);
    }

    public function productionCompletedList(Request $request) {
        $data['title'] = __('Production Completed');
        $data['sub_menu'] = 'productionCompletedList';
        if ($request->has('search_data')) {
            $lists = $this->service->getProductionCompleteData($request->search_data);
        } else {
            $lists = $this->service->getProductionCompleteData();
        }
        $data['brands'] = Brand::where('status',STATUS_ACTIVE)->get();
        $data['salesmans'] = User::where(['role'=>ROLE_SALESMAN,'status'=>STATUS_ACTIVE])->get();
        if ($request->ajax()) {
            return $this->showTableData($lists);
        }

        return view('admin.production.production.production_completed_list', $data);
    }

    private function showTableData($lists ,$type = '') {
        return datatables($lists)->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('payment_status', function ($item) {
            return payment_status($item->payment_status);
        })->editColumn('expired_at', function ($item) {
            $html = '<p> <b><i class="fa fa-clock"> </i>' . __(' Created at : ') . '</b>' . $item->created_at . '</p>';
            $html .= '<p><b><i class="fa fa-user-clock"> </i>' . __(' Expire at : ') . '</b>' . $item->expired_at . '</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();

            $html = '';
            if (isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p><b><i class="fa fa-shopping-bag"> </i> ' . $product->product->name . ' (' . $product->quantity . ')' ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p><b><i class="fa fa-shopping-bag"> </i> ' . $product->product_name . ' (' . $product->quantity . ')' . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : '';
        })->editColumn('budget_printing_enable', function ($item) {
            return $item->budget_printing_enable == TRUE ? '<span class="badge badge-success">'.__('Yes').'</span>' : '<span class="badge badge-danger">'.__('No').'</span>';
        })->editColumn('in_staging', function ($item) {
            return quotation_stage($item->in_staging);
        })->editColumn('action', function ($item) {
            if ($item->status == STATUS_PRODUCTION_COMPLETED){
                $html = '<a href="' . route('productionCompleteDetails', $item->production_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
            }else{
                $html = '<a href="' . route('productionDetails', $item->production_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
            }
            return $html;
        })->rawColumns(['status', 'budget_printing_enable','action', 'expired_at', 'products'])
                                 ->make(TRUE);
    }

    public function productionDetails($code) {
        $data['title'] = __('Production Details');
        $data['sub_menu'] = 'productionList';
        $data['item'] = $this->service->getSingleRowData(['production_reference' => $code, 'in_staging'=>QUOTATION_STAGE_IN_PRODUCTION]);
        if (isset($data['item'])) {
            $data['products'] = $data['item']->quotation_products()->get();
            return view('admin.production.production.production_details', $data);
        }
        return redirect()->back()->with('dismiss',__('Data not found'));
    }

    public function productionCompleteDetails($code) {
        $data['title'] = __('Production Details');
        $data['sub_menu'] = 'productionCompletedList';
        $data['item'] = $this->service->getSingleRowData(['production_reference' => $code, 'in_staging'=>QUOTATION_STAGE_IN_PRODUCTION]);
        if (isset($data['item'])) {
            $data['products'] = $data['item']->quotation_products()->get();
            return view('admin.production.production.production_completed_details', $data);
        }
        return redirect()->back()->with('dismiss',__('Data not found'));
    }

}
