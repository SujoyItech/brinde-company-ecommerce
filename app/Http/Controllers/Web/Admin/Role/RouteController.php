<?php

namespace App\Http\Controllers\Web\Admin\Role;

use App\Http\Controllers\Controller;
use App\Http\Services\RoleManagementServices\RouteService;
use App\Models\Role\RoleRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RouteController extends Controller
{
    private $route_service;

    public function __construct(RouteService $route_service){
        $this->route_service = $route_service;
    }
    public function updateRouteList(){
        return $this->route_service->updateRouteList();
    }
    public function getRouteByType(Request $request){
        $data['routes'] = RoleRoute::where('module_id',$request->module_id)->get();
        $data['role_action'] = $request->role_action ?? [];
        return view('admin.role.role_list_by_type',$data);
    }
}
