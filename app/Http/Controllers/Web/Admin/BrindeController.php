<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\BrindeService;
use App\Models\Message;
use App\Models\Subscriber;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BrindeController extends Controller
{
    private $service;
    public function __construct(BrindeService $service){
        $this->service=$service;
    }
    public function subscriberList(Request $request){
        if ($request->ajax()){
            $list = Subscriber::all();
            return datatables($list)
               ->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html ='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-style="zoom-in" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }

        return view('admin.messages.subscribers');
    }

    public function deleteSubscriber(Request $request){
        return $this->service->deleteSubscriber($request->id);
    }

    public function sendMessageToAllSubscribers(Request $request){
        return $this->service->sendMailToAllSubscriber($request->all());
    }

    public function messages(Request $request){
        if ($request->ajax()){
            $list = Message::all();
            return datatables($list)
                ->editColumn('created_at',function ($item){
                    return Carbon::parse($item->created_at)->format('Y-m-d H:i:s');
                })->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Replied').'</span>':'<span class="badge badge-primary">'.__('New').'</span>';
                }) ->editColumn('action',function ($item){
                    $html ='<a href="javascript:void(0)" class="text-primary p-1 reply_message" data-style="zoom-in" data-id="'.$item->id.'"><i class="fa fa-reply"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-style="zoom-in" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }

        return view('admin.messages.messages');
    }

    public function deleteMessage(Request $request){
        return $this->service->deleteMessage($request->id);
    }

    public function replyMessageToUser(Request $request){
        return $this->service->replyMessageToUser($request->all());
    }
}
