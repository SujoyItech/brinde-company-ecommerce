<?php

namespace App\Http\Controllers\Web\Admin\CRM;


use App\Http\Controllers\Controller;
use App\Http\Services\CRM\QuotationService;
use App\Http\Services\DashboardService;
use App\Http\Services\Product\BrandService;
use App\Http\Services\User\UserService;
use App\Models\CRM\CrmQuotation;
use App\Models\Product\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller {

    private $quotation_service,$user_service,$brand_service;

    public function __construct(QuotationService $quotation_service,UserService $user_service,BrandService $brand_service){
        $this->quotation_service = $quotation_service;
        $this->user_service = $user_service;
        $this->brand_service = $brand_service;
    }

    public function index(){
        $user_brand = $this->getBrands();
        $data['quotations'] = $this->quotation_service->getDashboardData($user_brand);
        return view('admin.crm.dashboard.home',$data);
    }

    private function getBrands(){
        if(Auth::user()->module_id == MODULE_SUPER_ADMIN || Auth::user()->module_id == MODULE_USER_ADMIN){
            $brands = Brand::where('status',STATUS_ACTIVE)->pluck('id')->toArray();
        }else{
            $brands = Brand::where('status',STATUS_ACTIVE)->whereIn('id',getPermittedBrands())->pluck('id')->toArray();
        }
        return $brands;
    }

    public function crmAllList(Request $request){
        $data['title'] = __('All List');
        if ($request->has('search_data')) {
            $lists = $this->quotation_service->getAllQuotationList($request->search_data);
        } else {
            $lists = $this->quotation_service->getAllQuotationList();
        }
        $data['salesmans'] = $this->user_service->getSalesmanList()->get();
        $data['brands'] = $this->brand_service->getData(['status' => ACTIVE], [], ['created_at' => 'desc']);

        if ($request->ajax()) {
            return $this->showTableData($lists);
        }
        return view('admin.crm.lists.all_lists', $data);
    }

    private function showTableData($lists) {
        return datatables($lists)->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('email', function ($item) {
            $html = '<p class="mb-1"><b><i class="fa fa-address-card"> </i>' . __(' Name : ') . '</b>' . $item->contact_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-building"> </i>' . __(' Company : ') . '</b>' . $item->company_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-envelope"> </i>' . __(' Email : ') . '</b>' . $item->email . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-phone"> </i>' . __(' Phone : ') . '</b>' . $item->phone . '</p>';

            return $html;
        })->editColumn('expired_at', function ($item) {
            $html = '<p class="mb-1"><b><i class="fa fa-clock"> </i>' . __(' Created at : ') . '</b>' . $item->created_at . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-user-clock"> </i>' . __(' Expire at : ') . '</b>' . $item->expired_at . '</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();

            $html = '';
            if (isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p class="mb-1"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product->name ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p class="mb-1"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product_name . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : '';
        })->editColumn('action', function ($item) {
            $html = '<a href="' . route('crmAllListDetails', $item->quote_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
            return $html;
        })->rawColumns(['status', 'action', 'email', 'expired_at', 'products'])
                                 ->make(TRUE);
    }


    public function crmAllListDetails($code){
        $data['title'] = __('Crm all Details');
        $data['sub_menu'] = 'crmAllList';
        $data['item'] = $this->quotation_service->getSingleRowData(['quote_reference' => $code]);
        if (isset($data['item'])) {
            $data['products'] = $data['item']->quotation_products()->get();
            return view('admin.crm.lists.crm_all_list_details', $data);
        }
        return redirect()->back()->with('dismiss',__('Data not found'));
    }

}
