<?php

namespace App\Http\Controllers\Web\Admin\CRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\CRM\CrmCustomerRequest;
use App\Http\Requests\Web\Admin\CRM\CrmSalesmanAssignRequest;
use App\Http\Services\CRM\CrmCustomerService;
use App\Http\Services\CRM\CrmPaymentConditionService;
use App\Http\Services\CRM\QuotationService;
use App\Http\Services\User\UserService;
use App\Models\CRM\CrmCustomer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CrmQuotationController extends Controller {
    private $quotationService, $customerService, $paymentConditionService, $userService;

    public function __construct(QuotationService $service,
                                CrmCustomerService $customerService,
                                CrmPaymentConditionService $paymentConditionService,
                                UserService $userService) {
        $this->quotationService = $service;
        $this->customerService = $customerService;
        $this->paymentConditionService = $paymentConditionService;
        $this->userService = $userService;
    }


    public function index($brand_id = '',Request $request)
    {
        $data['title'] = __('Quotation List');
        $data['brand_id'] = $brand_id;
        if ($request->ajax()) {
            $lists = $this->quotationService->getQuotationData();
            if (!empty($brand_id)) {
                $lists = $lists->where(['brand_id'=>$brand_id]);
            }
            return $this->showQuotationTable($lists);
        }
        return view('admin.crm.quotation.quotation_list', $data);
    }


    private function showQuotationTable($lists)
    {
        $customer_email = CrmCustomer::pluck('email')->toArray();
        return datatables($lists)->editColumn('brand_name', function ($item) {
            return $item->brand_name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('updated_at', function ($item) {
            return Carbon::parse($item->updated_at)->format('d-m-Y h:i:s');
        })->addColumn('customer', function ($item) {
            $html = '';
            $html .= '<p class="mb-1"><b><i class="fa fa-address-card"> </i>' . __(' Name : ') . '</b>' . $item->contact_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-building"> </i>' . __(' Company : ') . '</b>' . $item->company_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-envelope"> </i>' . __(' Email : ') . '</b>' . $item->email . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-phone"> </i>' . __(' Phone : ') . '</b>' . $item->phone . '</p>';
            return $html;
        }) ->editColumn('action',function ($item) use ($customer_email) {
            $html = '';
            if (isset($item->id)){
                $html .= '<a href="'.route('crmQuotationView', $item->quote_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'. $item->id .'">'.__('Details').'</a>';
                if(!empty($item->customer_id) || in_array($item->email,$customer_email)) {
                    if (in_array($item->email,$customer_email)) {
                        if(empty($item->customer_id)) {
                            $update = $this->updateQuotationByEmail($item->id, $item->email);
                        }
                    }
                    if(isset($update)) {
                        $html .= '<a href="'.route('crmCustomerEdit', [getCustomerReferenceCode($update), $item->quote_reference]).'" class="btn btn-xs btn-outline-warning waves-effect waves-light m-1 edit_item" data-id="'. $item->id .'">'.__('Check Customer').'</a>';
                    } else {
                        $html .= '<a href="'.route('crmCustomerEdit', [getCustomerReferenceCode($item->customer_code), $item->quote_reference]).'" class="btn btn-xs btn-outline-warning waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Check Customer').'</a>';
                    }
                } else {
                    $html .= '<a href="' . route('crmCustomerAdd', $item->quote_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Create Customer') . '</a>';
                }
            }
            return $html;

        })->rawColumns(['status','customer','action'])->make(TRUE);
    }


    public function assignSalesman(CrmSalesmanAssignRequest $request)
    {
        return $this->customerService->assignSalesman($request);
    }



    public function updateQuotationByEmail($quotation_id, $email)
    {
        $customer = $this->customerService->firstWhere(['email' => $email]);
        $updateData = [
            'customer_id' => $customer->id,
            'customer_code' => $customer->customer_code,
        ];
        $update = $this->quotationService->update($quotation_id,$updateData);
        if($update) {
            $quotation = $this->quotationService->firstWhere(['id' => $quotation_id]);
            return $quotation->customer_code;
        }
        return false;
    }

    public function quotationDetails($code)
    {
        $data['title'] = __('Quotation Details');
        $quotation = $this->quotationService->firstWhere(['quote_reference' => $code]);
        if (isset($quotation)) {
            $data['item'] = $quotation;
            $data['customer'] = $this->customerService->firstWhere(['id' => $quotation->customer_id]);
            if (isset($data['item'])) {
                $data['products'] = $data['item']->quotation_products()->get();
                $data['salesmans'] = $this->userService->getSalesmanList()->get();
                return view('admin.crm.quotation.quotation_view', $data);
            }
        }

        return redirect()->back()->with('dismiss',__('Data not found'));
    }
}
