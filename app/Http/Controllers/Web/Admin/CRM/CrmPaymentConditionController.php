<?php

namespace App\Http\Controllers\Web\Admin\CRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\CRM\CrmPaymentConditionRequest;
use App\Http\Services\CRM\CrmPaymentConditionService;
use Illuminate\Http\Request;

class CrmPaymentConditionController extends Controller
{
    private $crmPaymentConditionService;

    public function __construct(CrmPaymentConditionService $service){
        $this->crmPaymentConditionService = $service;
    }

    public function index(Request $request,$id = NULL){
        $payment_condition_lists = $this->crmPaymentConditionService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($payment_condition_lists)
                ->editColumn('name',function ($item){
                    if (isset($item->translations['name'][app()->getLocale()])){
                        return $item->translations['name'][app()->getLocale()];
                    }else{
                        return  $item->translations['name']['en'];
                    }
                })
                 ->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }

        return view('admin.crm.crm_payment_condition.crm_payment_condition');
    }
    public function edit(Request $request){
        return view('admin.crm.crm_payment_condition.crm_payment_condition_add',$this->crmPaymentConditionService->getPaymentConditionData($request->id));
    }

    public function store(CrmPaymentConditionRequest $request){
        if(!empty($request->id)){
            return $this->crmPaymentConditionService->update($request->id,$request->except('id'));
        }else{
            return $this->crmPaymentConditionService->create($request->except('id'));
        }

    }

    public function delete(Request $request){
        return $this->crmPaymentConditionService->delete($request->id);
    }
}
