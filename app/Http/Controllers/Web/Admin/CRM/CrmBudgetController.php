<?php

namespace App\Http\Controllers\Web\Admin\CRM;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\Pdf\PdfService;
use App\Http\Requests\Web\Admin\CRM\CrmBudgetProductRequest;
use App\Http\Requests\Web\Admin\CRM\CrmBudgetSaveRequest;
use App\Http\Services\CRM\BudgetService;
use App\Http\Services\CRM\PurchaseService;
use App\Http\Services\CRM\CrmCustomerService;
use App\Http\Services\CRM\CrmPaymentConditionService;
use App\Http\Services\CRM\CrmPrintingOptionService;
use App\Http\Services\CRM\QuotationService;
use App\Http\Services\Product\BrandService;
use App\Http\Services\User\UserService;
use App\Models\CRM\CrmPrintingOption;
use App\Models\CRM\CrmQuotation;
use App\Models\Product\Brand;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Log;
use Monolog\Handler\IFTTTHandler;
use function Symfony\Component\String\b;

class CrmBudgetController extends Controller {
    private $quotationService, $customerService, $paymentConditionService, $userService, $budgetService, $brandService, $crmPrintingOptionService, $pdfService;

    public function __construct(QuotationService $service,
                                CrmCustomerService $customerService,
                                CrmPaymentConditionService $paymentConditionService,
                                UserService $userService,
                                BudgetService $budgetService,
                                BrandService $brandService,
                                CrmPrintingOptionService $crmPrintingOptionService, PdfService $pdfService) {
        $this->quotationService = $service;
        $this->customerService = $customerService;
        $this->paymentConditionService = $paymentConditionService;
        $this->userService = $userService;
        $this->budgetService = $budgetService;
        $this->brandService = $brandService;
        $this->crmPrintingOptionService = $crmPrintingOptionService;
        $this->pdfService = $pdfService;
    }

    /*
    *
    * Budget List
    * Show the list of specified resource.
    * @return \Illuminate\Http\Response
    *
    */

    public function index(Request $request) {
        $data['title'] = __('Budget List');
        if ($request->has('search_data')) {
            $lists = $this->budgetService->getBudgetData($request->search_data);
        } else {
            $lists = $this->budgetService->getBudgetData();
        }
        $data['salesmans'] = $this->userService->getSalesmanList()->get();
        $data['brands'] = getPermittedBrandsData(Auth::user()->id);

        if ($request->ajax()) {
            return $this->showTableData($lists);
        }
        return view('admin.crm.budget.budget_list', $data);
    }

    private function showTableData($lists) {
        return datatables($lists)->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            return quotation_status($item->status);
        })->editColumn('email', function ($item) {
            $html = '<p class="mb-1"><b><i class="fa fa-address-card"> </i>' . __(' Name : ') . '</b>' . $item->contact_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-building"> </i>' . __(' Company : ') . '</b>' . $item->company_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-envelope"> </i>' . __(' Email : ') . '</b>' . $item->email . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-phone"> </i>' . __(' Phone : ') . '</b>' . $item->phone . '</p>';

            return $html;
        })->editColumn('expired_at', function ($item) {
            $html = '<p class="mb-1"><b><i class="fa fa-clock"> </i>' . __(' Created at : ') . '</b>' . $item->created_at . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-user-clock"> </i>' . __(' Expire at : ') . '</b>' . $item->expired_at . '</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();

            $html = '';
            if (isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p class="mb-1"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product->name ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p class="mb-1"><b><i class="fa fa-shopping-bag"> </i> ' . $product->product_name . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : '';
        })->editColumn('action', function ($item) {
            $html = '<a href="' . route('crmBudgetDetails', $item->budget_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
            $html .= '<a target="_blank" href="' . route('crmPrintBudget', $item->budget_reference) . '" class="btn btn-xs btn-outline-success waves-effect waves-light m-1 ">' . __('Print') . '</a>';

            return $html;
        })->rawColumns(['status', 'action', 'email', 'expired_at', 'products'])
                                 ->make(TRUE);
    }

    public function budgetDetails($code = '') {
        $data['title'] = __('Budget Details');
        $data['item'] = $this->quotationService->firstWhere(['budget_reference' => $code]);

        if (isset($data['item'])) {
            $data['products'] = $data['item']->quotation_products()->get();
            $data['printing_options'] = $this->crmPrintingOptionService->getData(['status' => ACTIVE], [], ['created_at' => 'desc']);

            return view('admin.crm.budget.budget_view', $data);
        }
        return redirect()->back()->with('dismiss', __('Data not found'));
    }

    public function saveBudgetProduct(CrmBudgetProductRequest $request) {
        return $this->budgetService->createBudgetProduct($request);
    }

    public function updateBudgetProduct(CrmBudgetProductRequest $request) {
        return $this->budgetService->updateBudgetProduct($request);
    }

    public function duplicateBudgetProduct(Request $request) {
        return $this->budgetService->duplicateBudgetProduct($request);
    }

    public function sendBudgetMail(Request $request) {
        return $this->budgetService->sendBudgetMail($request);
    }

    public function saveBudget(CrmBudgetSaveRequest $request) {
        return $this->budgetService->saveBudget($request);
    }

    public function crmBudgetCreateSave(Request $request) {
        return $this->budgetService->crmBudgetCreateSave($request->all());
    }

    public function deleteBudgetProduct(Request $request) {
        return $this->budgetService->deleteBudgetProduct($request);
    }

    public function budgetProductSearch(Request $request) {
        return $this->budgetService->searchProduct($request);
    }

    public function budgetProductEdit(Request $request) {
        return $this->budgetService->budgetProductEdit($request);
    }

    public function makeBudgetOrder(Request $request) {
        return $this->budgetService->makeBudgetOrder($request);
    }

    public function printBudget($code) {
        $data['title'] = __('Budget Details');
        $data['item'] = $this->quotationService->firstWhere(['budget_reference' => $code]);
        $quotation = CrmQuotation::select('crm_quotations.*', 'users.name as salesman_name',
            'crm_customers.contact_name as contact_name', 'crm_customers.name as company_name',
            'crm_customers.tin_number', 'crm_customers.tax_address','crm_customers.fiscal_postcode',
            'crm_customers.tax_location','crm_customers.tax_country')
                    ->leftJoin('users', 'crm_quotations.assigned_salesman', 'users.id')
                    ->leftJoin('crm_customers', 'crm_quotations.customer_id', 'crm_customers.id')
                    ->where('budget_reference', $code)
                    ->first();
        if (isset($quotation)) {
            $data['item'] = $quotation;
            $data['products'] = $quotation->quotation_products()->get();
        }
        if (isset($quotation)){
            if ($quotation->brand->id == BRAND_BRINDE_COMPANHIA){
                 $pdf = $this->pdfService->create_pdf('CRM.brinde_compania', $data);
            }elseif ($quotation->brand->id == BRAND_TOP_BRINDE){
                $pdf = $this->pdfService->create_pdf('CRM.top_brinde', $data);
            }elseif ($quotation->brand->id == BRAND_BRINDE_BRINDES){
                $pdf = $this->pdfService->create_pdf('CRM.brinde_brindes', $data);
            }elseif ($quotation->brand->id == BRAND_BEST_OF_GIFT){
                $pdf = $this->pdfService->create_pdf('CRM.best_of_gift', $data);
            }else{
                $pdf = $this->pdfService->create_pdf('CRM.budget', $data);
            }
            return $pdf->stream();
//          return view('pdf.CRM.budget',$data);
//            return $pdf->download('invoice.pdf');
        }
        return redirect()->back()->with('dismiss', __('Data not found'));
    }

    public function crmBudgetApprove(Request $request) {
        return $this->budgetService->crmBudgetApprove($request);
    }

}
