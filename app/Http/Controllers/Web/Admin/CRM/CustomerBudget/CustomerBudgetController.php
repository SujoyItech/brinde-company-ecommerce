<?php

namespace App\Http\Controllers\Web\Admin\CRM\CustomerBudget;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\CRM\CrmBudgetProductRequest;
use App\Http\Requests\Web\Customer\BudgetModificationRequest;
use App\Http\Services\CRM\BudgetService;
use App\Http\Services\CRM\PurchaseService;
use App\Http\Services\CRM\CrmCustomerService;
use App\Http\Services\CRM\CrmPaymentConditionService;
use App\Http\Services\CRM\CrmPrintingOptionService;
use App\Http\Services\CRM\QuotationService;
use App\Http\Services\Product\BrandService;
use App\Http\Services\User\UserService;
use App\Models\CRM\CrmPrintingOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function Symfony\Component\String\b;

class CustomerBudgetController extends Controller
{
    private $quotationService, $customerService, $paymentConditionService, $userService, $budgetService, $brandService, $crmPrintingOptionService;

    public function __construct(QuotationService $service,
                                CrmCustomerService $customerService,
                                CrmPaymentConditionService $paymentConditionService,
                                UserService $userService,
                                BudgetService $budgetService,
                                BrandService $brandService,
                                CrmPrintingOptionService $crmPrintingOptionService) {
        $this->quotationService = $service;
        $this->customerService = $customerService;
        $this->paymentConditionService = $paymentConditionService;
        $this->userService = $userService;
        $this->budgetService = $budgetService;
        $this->brandService = $brandService;
        $this->crmPrintingOptionService = $crmPrintingOptionService;
    }


    public function clientBudgetDetails($code)
    {
        $data['title'] = __('Your Budget Details');

        $data['item'] = $this->budgetService->getCreatedBudget($code);

        if (isset($data['item'])) {
            $data['products'] = $data['item']->quotation_products()->get();

            return view('customer.budget.budget_details', $data);
        }
        return __('Data not found');
    }

    public function sendBudgetModifyRequest(BudgetModificationRequest $request)
    {
        return $this->budgetService->sendBudgetModifyRequest($request);
    }

    public function approveBudget(Request $request)
    {
        return $this->budgetService->approveBudget($request);
    }

}
