<?php

namespace App\Http\Controllers\Web\Admin\CRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\CRM\CrmPrintingOptionRequest;
use App\Http\Services\CRM\CrmPrintingOptionService;
use App\Models\CRM\CrmPrintingOption;
use Illuminate\Http\Request;

class CrmPrintingOptionController extends Controller
{
    private $crmPrintingOptionService;

    public function __construct(CrmPrintingOptionService $service){
        $this->crmPrintingOptionService = $service;
    }

    public function index(Request $request,$id = NULL){
        $printing_option_lists = $this->crmPrintingOptionService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($printing_option_lists)
                ->editColumn('name',function ($item){
                    if (isset($item->translations['name'][app()->getLocale()])){
                        return $item->translations['name'][app()->getLocale()];
                    }else{
                        return  $item->translations['name']['en'];
                    }
                })
                ->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }

        return view('admin.crm.printing_option.printing_option');
    }
    public function edit(Request $request){
        return view('admin.crm.printing_option.printing_option_add',$this->crmPrintingOptionService->getPrintingOptionData($request->id));
    }

    public function store(CrmPrintingOptionRequest $request){
        if(!empty($request->id)){
            return $this->crmPrintingOptionService->update($request->id,$request->except('id'));
        }else{
            return $this->crmPrintingOptionService->create($request->except('id'));
        }

    }

    public function delete(Request $request){
        return $this->crmPrintingOptionService->delete($request->id);
    }
}
