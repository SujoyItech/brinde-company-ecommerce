<?php

namespace App\Http\Controllers\Web\Admin\CRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\CRM\CrmCustomerRequest;
use App\Http\Services\CRM\CrmCustomerService;
use App\Http\Services\CRM\CrmPaymentConditionService;
use App\Http\Services\CRM\QuotationService;
use App\Http\Services\User\UserService;
use App\Models\CRM\CrmCustomer;
use App\Models\CRM\CrmQuotation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class  CustomerController extends Controller
{
    private $quotationService, $customerService, $paymentConditionService, $userService;

    public function __construct(QuotationService $service,
                                CrmCustomerService $customerService,
                                CrmPaymentConditionService $paymentConditionService,
                                UserService $userService) {
        $this->quotationService = $service;
        $this->customerService = $customerService;
        $this->paymentConditionService = $paymentConditionService;
        $this->userService = $userService;
    }
    public function customerList(Request $request, $id = NULL)
    {
        $data['title'] = __('Customers');
        if (Auth::user()->role == ROLE_SALESMAN) {
            $lists = $this->customerService->getData(['salesman_id'=>Auth::user()->id], [], ['created_at' => 'desc']);
        }else{
            $lists = $this->customerService->getData([], [], ['created_at' => 'desc']);
        }
        if ($request->ajax()) {
            return $this->showCustomerTable($lists);
        }
        return view('admin.crm.customer.crm_customer', $data);
    }

    public function getCustomerListBySalesMan(){
        if (Auth::user()->role == ROLE_SALESMAN || Auth::user()->role == ROLE_SALES_MANAGER) {
            return $this->customerService->getData(['salesman_id'=>Auth::user()->id,'status'=>STATUS_ACTIVE], [], ['created_at' => 'desc']);
        }else{
            return $this->customerService->getData(['status'=>STATUS_ACTIVE], [], ['created_at' => 'desc']);
        }
    }

    private function showCustomerTable($lists)
    {
        return datatables($lists)
            ->editColumn('customer_code', function ($item) {
                return '<a href="'.route('customerDetails',['customer_code'=>$item->customer_code]).'">'.$item->customer_code.'</a>';
            }) ->editColumn('country', function ($item) {
                return !empty($item->country) ? $item->country : 'N/A';
            })
            ->editColumn('status', function ($item) {
                return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">' . __('Active') . '</span>' : '<span class="badge badge-warning">' . __('Inactive') . '</span>';
            })->editColumn('action', function ($item) {
                $edit_url = route('crmCustomerEdit', $item->customer_code);
                $html = '<a href="' . $edit_url . '" class="text-info p-1 edit_item"><i class="fa fa-edit"></i></a>';
                $html .= '<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="' . $item->id . '"><i class="fa fa-trash"></i></a>';
                return $html;
            })->rawColumns(['customer_code','status', 'action'])
            ->make(TRUE);
    }

    public function customerAdd($quotation_id = NULL)
    {
        $data['title'] = __('Add New Customer');
        $data['payment_conditions'] = $this->paymentConditionService->getData(['status' => ACTIVE], [], ['created_at' => 'desc']);
        if (isset($quotation_id)) {
            $data['quotation'] = $this->quotationService->firstWhere(['quote_reference' => $quotation_id]);
            $email = $data['quotation']->email;
            $email_domain = explode('@', $email)[1];
            $exclude_email_domain = ['gmail.com', 'hotmail.com', 'outlook.com'];
            if (!in_array($email_domain, $exclude_email_domain)){
                $customer = CrmCustomer::join('users',function ($join){
                    $join->on('crm_customers.salesman_id','=','users.id')->where('users.status',STATUS_ACTIVE);
                })->where(DB::raw("locate('{$email_domain}',crm_customers.email)"),'>',0)->where('crm_customers.salesman_id','<>',NULL)->orderBy('crm_customers.id','desc')->first();
                if (isset($customer)){
                    $data['suggest_salesman'] = User::where('id',$customer->salesman_id)->first()->name ?? '';
                }
            }
        }
        $data['salesmans'] = $this->userService->getSalesmanList()->get();
        return view('admin.crm.customer.crm_customer_add', $data);
    }

    public function customerEdit($code ,$quotation_id = null)
    {
        $data['title'] = __('Update Customer');
        $data['item'] = $this->customerService->getSingleData($code);
        $data['payment_conditions'] = $this->paymentConditionService->getData(['status' => ACTIVE], [], ['created_at' => 'desc']);
        if (isset($quotation_id)) {
            $data['quotation'] = $this->quotationService->firstWhere(['quote_reference' => $quotation_id]);
        }
        $data['salesmans'] = $this->userService->getSalesmanList()->get();
        return view('admin.crm.customer.crm_customer_add', $data);
    }

    public function customerSave(CrmCustomerRequest $request)
    {
        if (!empty($request->id)) {
            return $this->customerService->updateCustomer($request->id, $request);
        } else {
            return $this->customerService->create($request);
        }
    }

    public function customerDetails(Request $request,$customer_code){
        if ($request->ajax()){
            $search_data = [];
            if ($request->has('search_data')) {
                $search_data =$request->search_data;
            }
            $customer = CrmCustomer::where('customer_code',$customer_code)->first();
            $lists = $this->customerService->customerOrderDetails($customer->id,$search_data);
            return $this->showTableData($lists);
        }
        $data['customer_code'] = $customer_code;
        return  view('admin.crm.customer.customer_details',$data);
    }

    private function showTableData($lists) {
        return datatables($lists)
        ->editColumn('brand_id', function ($item) {
            return $item->brand->name;
        })->editColumn('status', function ($item) {
            $html = '<span><strong>'.__('Stage: ').'</strong>'.quotation_stage($item->in_staging).'</span><br>';
            $html .= '<span><strong>'.__('Status: ').'</strong></span>'.$this->prepareDataTableStatus($item);
            return $html;
        })->editColumn('email', function ($item) {
            $html = '<p class="mb-1"><b><i class="fa fa-address-card"> </i>' . __(' Name : ') . '</b>' . $item->contact_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-building"> </i>' . __(' Company : ') . '</b>' . $item->company_name . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-envelope"> </i>' . __(' Email : ') . '</b>' . $item->email . '</p>';
            $html .= '<p class="mb-1"><b><i class="fa fa-phone"> </i>' . __(' Phone : ') . '</b>' . $item->phone . '</p>';

            return $html;
        })->editColumn('expired_at', function ($item) {
            $html = '<p class="mb-1"><b>' . __(' Created at : ') . '</b>' . $item->created_at . '</p>';
            $html .= '<p class="mb-1"><b>' . __(' Expire at : ') . '</b>' . $item->expired_at . '</p>';

            return $html;
        })->editColumn('products', function ($item) {
            $products = $item->quotation_products()->get();

            $html = '';
            if (isset($products[0])) {
                foreach ($products as $product) {
                    if (!empty($product->product_id)) {
                        $html .= ' <p class="mb-1"><b>' . $product->product->name ?? '' . '</b></p>';
                    } else {
                        $html .= ' <p class="mb-1"><b>' . $product->product_name . '</b></p>';
                    }
                }
            }

            return $html;
        })->editColumn('assigned_salesman', function ($item) {
            return ($item->assigned_salesman) ? $item->salesman->name : 'N/A';
        })->editColumn('action', function ($item) {
            return $this->prepareDataTableAction($item);
        })->rawColumns(['status', 'action', 'email', 'expired_at', 'products'])
          ->make(TRUE);
    }


    private function prepareDataTableStatus($item){
        if ($item->status == STATUS_QUOTATION_NEW ){
            return '<span class="badge badge-primary">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_QUOTATION_SALESMAN_ASSIGNED){
            return '<span class="badge badge-pink">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_QUOTATION_BUDGET_CREATED){
            return '<span class="badge badge-info">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_BUDGET_SENT){
            return '<span class="badge badge-blue">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_BUDGET_SENT){
            return '<span class="badge badge-blue">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_BUDGET_MODIFY_REQUEST){
            return '<span class="badge badge-warning">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_BUDGET_APPROVED){
            return '<span class="badge badge-success">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_BUDGET_ORDERED){
            return '<span class="badge badge-primary">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_PARTIALLY_PURCHASED){
            return '<span class="badge badge-pink">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_PURCHASED_DONE){
            return '<span class="badge badge-success">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_PARTIALLY_ARRIVED){
            return '<span class="badge badge-pink">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_ARRIVED_DONE){
            return '<span class="badge badge-success">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_SENT_TO_PRODUCTION){
            return '<span class="badge badge-primary">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_IN_PRODUCTION){
            return '<span class="badge badge-warning">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_ORDER_IN_PRODUCTION || $item->status == STATUS_PRODUCTION_NOT_COMPLETED){
            return '<span class="badge badge-warning">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_PRODUCTION_PARTIALLY_COMPLETED){
            return '<span class="badge badge-pink">'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_PRODUCTION_COMPLETED){
            return '<span class="badge badge-success>'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_IN_SHIPMENT){
            return '<span class="badge badge-warning>'.quotation_status($item->status).'</span>';
        }elseif ($item->status == STATUS_SHIPPED_COMPLETED){
            return '<span class="badge badge-success>'.quotation_status($item->status).'</span>';
        }else{
            return '<span class="badge badge-primary>'.quotation_status($item->status).'</span>';
        }
    }
    private function prepareDataTableAction($item){
        if ($item->status >= STATUS_QUOTATION_NEW && $item->status <= STATUS_QUOTATION_SALESMAN_ASSIGNED &&  $item->assigned_salesman == NULL){
            return $this->prepareQuotationAction($item);
        }elseif ($item->status >= STATUS_QUOTATION_SALESMAN_ASSIGNED && $item->status < STATUS_BUDGET_ORDERED && $item->assigned_salesman != NULL){
            return $this->prepareBudgetAction($item);
        }elseif ($item->in_staging == QUOTATION_STAGE_IN_PURCHASE && $item->status >= STATUS_BUDGET_ORDERED){
            return $this->prepareOrderAction($item);
        }elseif ($item->in_staging == QUOTATION_STAGE_IN_ARRIVAL && $item->status >= STATUS_ORDER_PURCHASED_DONE && $item->status <= STATUS_ORDER_ARRIVED_DONE){
            return $this->prepareLogisticAction($item);
        }elseif ($item->in_staging == QUOTATION_STAGE_IN_PRODUCTION && $item->status >= STATUS_ORDER_SENT_TO_PRODUCTION && $item->status <= STATUS_PRODUCTION_COMPLETED){
            return $this->prepareProductionAction($item);
        }elseif ($item->in_staging == QUOTATION_STAGE_IN_SHIPMENT && $item->status >= STATUS_PRODUCTION_COMPLETED){
            return $this->prepareShipmentAction($item);
        }
    }

    private function prepareQuotationAction($item){
        $html = '';
        if (isset($item->id)){
            $customer_email = CrmCustomer::pluck('email')->toArray();
            $html .= '<a href="'.route('crmQuotationView', $item->quote_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'. $item->id .'">'.__('Details').'</a>';
            if(!empty($item->customer_id) || in_array($item->email,$customer_email)) {
                if (in_array($item->email,$customer_email)) {
                    if(empty($item->customer_id)) {
                        $update = $this->updateQuotationByEmail($item->id, $item->email);
                    }
                }
                if(isset($update)) {
                    $html .= '<a href="'.route('crmCustomerEdit', [getCustomerReferenceCode($update), $item->quote_reference]).'" class="btn btn-xs btn-outline-warning waves-effect waves-light m-1 edit_item" data-id="'. $item->id .'">'.__('Check Customer').'</a>';
                } else {
                    $html .= '<a href="'.route('crmCustomerEdit', [getCustomerReferenceCode($item->customer_code), $item->quote_reference]).'" class="btn btn-xs btn-outline-warning waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Check Customer').'</a>';
                }
            } else {
                $html .= '<a href="' . route('crmCustomerAdd', $item->quote_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Create Customer') . '</a>';
            }
        }
        return $html;
    }


    private function prepareBudgetAction($item){
        $html = '<a href="' . route('crmBudgetDetails', $item->budget_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
        $html .= '<a target="_blank" href="' . route('crmPrintBudget', $item->budget_reference) . '" class="btn btn-xs btn-outline-success waves-effect waves-light m-1 ">' . __('Print') . '</a>';
        return $html;
    }

    private function prepareOrderAction($item){
        $html = '<a href="'.route('productionOrderDetails', $item->order_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Details').'</a>';
        return $html;
    }

    private function prepareLogisticAction($item){
        $html = '<a href="'.route('productionPurchaseDetails', $item->purchase_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Details').'</a>';
        return $html;
    }
    private function prepareProductionAction($item){
        if ($item->status == STATUS_PRODUCTION_COMPLETED){
            $html = '<a href="' . route('productionCompleteDetails', $item->production_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
        }else{
            $html = '<a href="' . route('productionDetails', $item->production_reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Details') . '</a>';
        }
        return $html;
    }
    private function prepareShipmentAction($item){
        $html = '<a href="'.route('productionShipmentDetails', $item->order_reference).'" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="'.$item->id.'">'.__('Details').'</a>';
        return $html;
    }

    public function updateQuotationByEmail($quotation_id, $email)
    {
        $customer = $this->customerService->firstWhere(['email' => $email]);
        $updateData = [
            'customer_id' => $customer->id,
            'customer_code' => $customer->customer_code,
        ];
        $update = $this->quotationService->update($quotation_id,$updateData);
        if($update) {
            $quotation = $this->quotationService->firstWhere(['id' => $quotation_id]);
            return $quotation->customer_code;
        }
        return false;
    }

    public function customerDelete(Request $request)
    {
        return $this->customerService->delete($request->id);
    }
}
