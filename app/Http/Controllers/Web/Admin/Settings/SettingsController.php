<?php

namespace App\Http\Controllers\Web\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Services\SettingService;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Setting;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SettingsController extends Controller
{
    private $settingService;

    public function __construct(SettingService $service){
        $this->settingService = $service;
    }
    public function siteSetting(){
        $data['settings'] = __options(['site_settings']);
        return view('admin.settings.site_setting',$data);
    }

    public function logoSetting(){
        $data['settings'] = __options(['logo_settings']);
        return view('admin.settings.logo_setting',$data);
    }

    public function socialSetting(){
        $data['settings'] = __options(['social_settings']);
        return view('admin.settings.social_setting',$data);
    }

    public function applicationSetting(){
        $data['settings'] = __options(['application_settings']);
        return view('admin.settings.application_settings',$data);
    }

    public function adminSettingsSave(Request $request){
        return $this->settingService->adminSettingsSave($request->all());
    }

    public function commandSettings(){
        return view('admin.settings.command_settings');
    }

    public function runCommand(Request $request){
       return $this->settingService->runCommand($request->all());
    }

    public function createCommand(Request $request){
        return $this->settingService->createCommand($request->all());
    }

    public function termsConditionSettings(){
        $data['settings'] = __options(['web_content_settings']);
        return view('admin.settings.web_content.terms_condition_settings',$data);
    }
    public function privacyPolicySettings(){
        $data['settings'] = __options(['web_content_settings']);
        return view('admin.settings.web_content.privacy_policy_settings',$data);
    }

    public function aboutUsSettings(){
        $data['settings'] = __options(['web_content_settings']);
        return view('admin.settings.web_content.about_us_settings',$data);
    }
    public function whyChooseUsSettings(){
        $data['settings'] = __options(['web_content_settings']);
        return view('admin.settings.web_content.why_choose_us_settings',$data);
    }

    public function faqsSettings(Request $request){
        if ($request->ajax()) {
            $data_list = Faq::all();
            return datatables($data_list)
                ->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-style="zoom-in" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }
        return view('admin.settings.faqs.faqs_settings');
    }

    public function getFaqItemById(Request $request){
        return Faq::where('id',$request->id)->first();
    }

    public function faqsSettingSave(Request $request){
        if (!empty($request->id)) {
            return $this->settingService->updateFaqs($request->id,$request->except('id'));
        }else{
            return $this->settingService->createFaqs($request->except('id'));
        }
    }

    public function deleteFaqsItem(Request $request){
        return $this->settingService->deleteFaqs($request->id);
    }

    public function teamSettings(Request $request){
        if ($request->ajax()) {
            $data_list = Team::all();
            return datatables($data_list)
                ->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-style="zoom-in" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }
        return view('admin.settings.teams.team_settings');
    }

    public function getTeamItemById(Request $request){
        return Team::where('id',$request->id)->first();
    }

    public function teamSettingSave(Request $request){
        if (!empty($request->id)) {
            return $this->settingService->updateTeams($request->id,$request->except('id'));
        }else{
            return $this->settingService->createTeams($request->except('id'));
        }
    }

    public function deleteTeamItem(Request $request){
        return $this->settingService->deleteTeams($request->id);
    }

    public function contactSettings(Request $request){
        if ($request->ajax()) {
            $data_list = Contact::all();
            return datatables($data_list)
                ->editColumn('contact_type',function ($item){
                    return $item->contact_type == MAIN_CONTACT ? __('Main') :__('Sub');
                })->editColumn('contact_details',function ($item){
                    $html = '';
                    if (!empty($item->email)) {
                        $html .= '<strong>'.__('Email: ').'</strong>'.$item->email.'<br>';
                    }
                    if (!empty($item->email)) {
                        $html .= '<strong>'.__('Phone: ').'</strong>'.$item->phone.'<br>';
                    }
                    if (!empty($item->email)) {
                        $html .= '<strong>'.__('GPS: ').'</strong>'.$item->gps.'<br>';
                    }
                    if (!empty($item->email)) {
                        $html .= '<strong>'.__('Fax: ').'</strong>'.$item->fax.'<br>';
                    }
                    return $html;
                })->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-style="zoom-in" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['contact_details','status','action'])
                ->make(TRUE);
        }
        return view('admin.settings.contact.contact_settings');
    }

    public function getContactItemById(Request $request){
        return Contact::where('id',$request->id)->first();
    }

    public function contactSettingSave(Request $request){
        if (!empty($request->id)) {
            return $this->settingService->updateContact($request->id,$request->except('id'));
        }else{
            return $this->settingService->createContact($request->except('id'));
        }
    }

   public function deleteContactItem(Request $request){
        return $this->settingService->deleteContact($request->id);
    }

    public function order(Request $request){
        return $this->settingService->order($request->all());
    }

}
