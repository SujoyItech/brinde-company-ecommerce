<?php

namespace App\Http\Controllers\Web\Admin\Website;

use App\Http\Controllers\Controller;
use App\Http\Services\Product\BrandService;
use App\Http\Services\Product\CategoryService;
use App\Http\Services\Website\WebsiteCategoriesMenuService;
use App\Models\Product\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebsiteCategoriesMenuController extends Controller
{
    private $category_service,$brand_service,$categories_menu_service;
    public function __construct(CategoryService $category_service,BrandService $brand_service,WebsiteCategoriesMenuService $categories_menu_service){
        $this->category_service = $category_service;
        $this->brand_service = $brand_service;
        $this->categories_menu_service = $categories_menu_service;
    }
    public function websiteCategories(Request $request){
        $data['brands'] = $this->getBrands();
        if ($request->ajax()) {
            $category_data['categories'] = $this->category_service->getBrandCategories($request->brand_id,0);
            return view('admin.website_manager.categories_menu.category_list',$category_data);
        }
        return view('admin.website_manager.categories_menu.categories_menu',$data);
    }

    private function getBrands(){
        if(Auth::user()->module_id == MODULE_SUPER_ADMIN || Auth::user()->module_id == MODULE_USER_ADMIN){
            $brands = Brand::where('status',STATUS_ACTIVE)->get();
        }else{
            $brands = Brand::where('status',STATUS_ACTIVE)->whereIn('id',getPermittedBrands())->get();
        }
        return $brands;
    }

    public function showCategoryImageAddModal(Request $request){
        return $this->categories_menu_service->getCategoryImageAddModalData($request->all());
    }

    public function saveWebsiteCategoryImage(Request $request){
        if (!empty($request->id)) {
            return $this->categories_menu_service->update($request->id,$request->all());
        }else{
            return $this->categories_menu_service->create($request->all());
        }
    }

    public function categoryImageDelete(Request $request){
        return $this->categories_menu_service->delete($request->id);
    }
}
