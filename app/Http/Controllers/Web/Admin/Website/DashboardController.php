<?php

namespace App\Http\Controllers\Web\Admin\Website;


use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use Illuminate\Http\Request;


class DashboardController extends Controller {

    public function index(){
        return view('admin.website_manager.dashboard.home');
    }

}
