<?php

namespace App\Http\Controllers\Web\Admin\Website;

use App\Http\Controllers\Controller;
use App\Http\Repositories\BaseRepository;
use App\Http\Repositories\Product\BrandRepository;
use App\Http\Requests\Web\Admin\Website\PageRequest;
use App\Http\Services\Product\BrandService;
use App\Http\Services\Product\ProductService;
use App\Http\Services\Website\WebsitePageLayerDetailsService;
use App\Http\Services\Website\WebsitePageLayerService;
use App\Http\Services\Website\WebsitePageService;
use App\Models\Product\Brand;
use App\Models\UserBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebsitePageController extends Controller
{
    private $pageService,$brandService,$page_layer_service,$layer_details_service,$product_service;


    public function __construct(
        WebsitePageService $service,
        BrandService $brand_service,
        WebsitePageLayerService $page_layer_service,
        WebsitePageLayerDetailsService $layer_details_service,
        ProductService $product_service
    ){
        $this->pageService = $service;
        $this->brandService = $brand_service;
        $this->page_layer_service = $page_layer_service;
        $this->layer_details_service = $layer_details_service;
        $this->product_service = $product_service;
    }

    public function index(Request $request,$brand_id = ''){

        if ($request->ajax()){
            $lists = $this->pageService->getPageQuery(getPermittedBrands(),$brand_id);
            return datatables($lists)
                ->addColumn('move', function ($item) {
                    return '<img class="text-center" style="cursor:move" width="20px" src="' . adminAsset('images/move.svg') . '" alt="">';
                })
                ->editColumn('page_type',function ($item){
                    $html = '';
                    if (isset($item->brand_icon) && file_exists(public_path(get_image_path('brand').'/'.$item->brand_icon))){
                        $html .= '<img src="'.asset(get_image_path('brand')).'/'.$item->brand_icon.'" class="img-rounded" width="50"><br>';
                    }
                    $html .= '<h5 class="mt-2">'.ucfirst($item->page_type).'</h5>';
                    $html .= '<h5 class="mt-2">'.ucfirst($item->brand_name).'</h5>';
                    return $html;
                })->editColumn('page_name',function ($item){
                    $html = '';
                    $html .= '<strong>'.__('Name: ').'</strong>'.$item->page_name.'<br>';
                    $html .= '<strong>'.__('Title: ').'</strong>'.$item->title.'<br>';
                    $html .= '<strong>'.__('Sub Title: ').'</strong>'.$item->subtitle;
                    return $html;
                }) ->editColumn('status',function ($item){
                    $checked = $item->status == STATUS_ACTIVE ? 'checked' : '';
                    $html = '<input type="checkbox" '.$checked.' data-id="'.$item->id.'" data-plugin="switchery" data-size="small"
                     data-status="'.$item->status.'" data-brand-id="'.$item->brand_id.'" name="status" data-color="#1bb99a" class="status_change"/>';
                    return $html;
                })->editColumn('action',function ($item){
                    $html = '';
                    $html .= '<a href="'.route('websitePageDetails',['id'=>$item->id]).'" class="text-info p-1  font-18"
                         data-plugin="tippy"
                         data-tippy-followCursor="true"
                         data-tippy-arrow="true"
                         data-tippy-placement=""
                         data-tippy-animation="fade"
                         data-tippy-theme="gradient"
                         title="'.__('Details').'"><i class="fe-globe"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 edit_item font-18" data-style="zoom-in" data-id="'.$item->id.'"><i class="fe-edit"></i></a>';
                    return $html;
                })->rawColumns(['move','page_type','page_name','status','action'])
                  ->make(TRUE);
        }

       $data['brands'] = $this->getBrands();
        return view('admin.website_manager.pages.pages',$data);
    }
    public function edit(Request $request){
        $data = $this->pageService->getPageData($request->id);
        $data['brands'] = $this->getBrands();
        return view('admin.website_manager.pages.pages_add',$data);
    }

    private function getBrands(){
        if(Auth::user()->module_id == MODULE_SUPER_ADMIN || Auth::user()->module_id == MODULE_USER_ADMIN){
            $brands = Brand::where('status',STATUS_ACTIVE)->get();
        }else{
            $brands = Brand::where('status',STATUS_ACTIVE)->whereIn('id',getPermittedBrands())->get();
        }
        return $brands;
    }

    public function store(PageRequest $request){
        if(!empty($request->id)){
            return $this->pageService->update($request->id,$request->except('id'));
        }else{
            return $this->pageService->create($request->except('id'));
        }
    }

    public function delete(Request $request){
        return $this->pageService->delete($request->id);
    }

    public function websitePageStatusChange(Request $request){
        return $this->pageService->websitePageStatusChange($request->all());
    }

    public function websitePageDetails(Request $request,$id){
        $data = $this->pageService->getPageData($id);
        $data['website_pages_id'] = $id;
        $layers = $this->page_layer_service->getData(['website_pages_id'=>$id],[],['order'=>'asc']);
        if ($request->ajax()) {
            $data['layers'] = $this->getLayerContentData($layers);
            return view('admin.website_manager.layers.layer_list_data',$data);
        }
        return view('admin.website_manager.pages.page_details',$data);
    }

    private function getLayerContentData($layers){
        $layer_content = [];
        if (isset($layers)){
            foreach ($layers as  $key=>$value){
                $layer_content[$key] = $value;
                if ($value->layer_type == 'product'){
                    $layer_details = $this->layer_details_service->getLayerContentSingleData(['website_pages_layers_id'=>$value->id]);
                    $products_array = isset($layer_details) && !empty($layer_details->category_product_slugs) ? explode(',',$layer_details->category_product_slugs) : [];
                    $layer_content[$key]['layers_content_counts'] = empty($products_array) && isset($layer_details) ? 1 :count($products_array);
                }else{
                    $layer_details = $this->layer_details_service->getLayerContentData(['website_pages_layers_id'=>$value->id]);
                    $layer_content[$key]['layers_content_counts'] = $layer_details->count();
                    if (!empty($layer_details->count())){
                        $layer_content[$key]['segment'] = intval(12/$layer_details->count());
                    }
                }
                if($value->layer_type == 'banner'){
                    $layer_content[$key]['layers_content'] = $this->prepareBannerLayoutContentData($layer_details,$value->layout_number);
                }else{
                    $layer_content[$key]['layers_content'] = $layer_details;
                    if ($value->layer_type == 'product') {
                        if (!empty($layer_details->category_product_slugs)) {
                            $layer_content[$key]['layers_content']['product_lists'] = $this->product_service->getProductListByWhereIn([],explode(',',$layer_details->category_product_slugs));
                        }
                    }
                }

            }
        }
        return $layer_content;
    }

    private function prepareBannerLayoutContentData($layer_details,$layer_number){
        $data = [];
        foreach ($layer_details as $key=>$value){
            $index = 'b_'.$layer_number.'_'.$value->order;
            $data[$index] = $value;
        }
        return $data;
    }
}
