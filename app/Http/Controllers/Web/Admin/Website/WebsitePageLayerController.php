<?php

namespace App\Http\Controllers\Web\Admin\Website;

use App\Http\Controllers\Controller;
use App\Http\Services\Product\CategoryService;
use App\Http\Services\Product\ProductService;
use App\Http\Services\Website\WebsitePageLayerDetailsService;
use App\Http\Services\Website\WebsitePageLayerService;
use App\Models\Product\Product;
use Illuminate\Http\Request;

class WebsitePageLayerController extends Controller
{
    private $page_layer_service,$page_layer_details_service,$category_service,$product_service;
    public function __construct(
        WebsitePageLayerService $page_layer_service,
        WebsitePageLayerDetailsService $page_layer_details_service,
        CategoryService $category_service,
        ProductService $product_service
    ){
        $this->page_layer_service = $page_layer_service;
        $this->page_layer_details_service = $page_layer_details_service;
        $this->category_service = $category_service;
        $this->product_service = $product_service;
    }
/*==========================================================Page layer ======================================================*/

    public function editWebsitePageLayer(Request $request){
        return $this->page_layer_service->edit($request->all());
    }

    public function saveWebsitePageLayer(Request $request){
        if (!empty($request->id)) {
            return $this->page_layer_service->update($request->id,$request->except('id'));
        }else{
            return $this->page_layer_service->create($request->except('id'));
        }

    }

    public function websitePageLayerOrderUpdate(Request $request){
        return $this->page_layer_service->updateOrder($request->all());
    }

    public function websitePageLayerStatusChange(Request $request){
        return $this->page_layer_service->websitePageLayerStatusChange($request->all());
    }

    public function websitePageLayerDelete(Request $request){
        return $this->page_layer_service->websitePageLayerDelete($request->all());
    }
/*==========================================================Page layer ======================================================*/



/*==========================================================Page layer content ======================================================*/

    public function showLayerContentModal(Request $request){
        return $this->page_layer_details_service->showLayerContentModal($request->all());
    }

    public function saveLayerContent(Request $request){
        if(!empty($request->id)){
            return $this->page_layer_details_service->updateLayerContent($request->id,$request->except(['id','type']),$request->type);
        }else{
            return $this->page_layer_details_service->createLayerContent($request->except(['id','type']),$request->type);
        }
    }

    public function layerContentDelete(Request $request){
        return $this->page_layer_details_service->layerContentDelete($request->all());
    }

    public function getCategorySlugById(Request $request){
        return $this->page_layer_details_service->getCategorySlugById($request->id);
    }

    public function renderProductByCategory($category_id,Request $request){
        return $this->product_service->getProductSearchByCategory($category_id,$request->search);
    }

/*==========================================================Page layer content ======================================================*/

}
