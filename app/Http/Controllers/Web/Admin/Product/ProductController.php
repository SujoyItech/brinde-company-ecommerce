<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Exports\BrindeExport;
use App\Exports\ProductExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\ProductImportRequest;
use App\Http\Requests\Web\Admin\Product\ProductInitRequest;
use App\Http\Requests\Web\Admin\Product\ProductMediaRequest;
use App\Http\Requests\Web\Admin\Product\ProductSaveRequest;
use App\Http\Services\Product\BrandService;
use App\Http\Services\Product\ProductImportService;
use App\Http\Services\Product\ProductService;
use App\Http\Services\Product\SupplierService;
use App\Imports\BrindeImport;
use App\Imports\ProductImport;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Product\ProductBrand;
use App\Models\Product\ProductUpload;
use App\Models\Product\ProductUploadProcess;
use App\Models\Product\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    private $productService, $supplierService, $brand_service;

    public function __construct(ProductService $service, SupplierService $supplierService, BrandService $brand_service)
    {
        $this->productService = $service;
        $this->supplierService = $supplierService;
        $this->brand_service = $brand_service;
    }

    public function index(Request $request)
    {
        $data['suppliers'] = $this->supplierService->getData(['status' => ACTIVE]);
        $data['categories'] = Category::where('status', STATUS_ACTIVE)->get();
        if ($request->ajax()) {
            if ($request->has('search_data')) {
                $product_list = $this->productService->getProductData($request, $request->search_data);
            } else {
                $product_list = $this->productService->getProductData($request);
            }
            return $this->productService->showProductTable($product_list);
        }
        return view('admin.products.products.product_list', $data);
    }

    public function initProduct(ProductInitRequest $request): \Illuminate\Http\JsonResponse
    {
        return $this->productService->initProduct($request);
    }

    public function saveProduct(ProductSaveRequest $request)
    {
        return $this->productService->saveProduct($request);
    }

    public function productSlugCheck(Request $request)
    {
        return $this->productService->checkSlug($request);
    }

    public function productReferenceCheck(Request $request)
    {
        return $this->productService->checkReference($request);
    }

    public function edit($reference, Request $request)
    {
        $data = $this->productService->getProductEditPageData($request);
        $data['brands'] = $this->brand_service->getData(['status' => ACTIVE]);
        $product = $this->productService->firstWhere(['reference' => $reference]);
        $data['product_brands'] = ProductBrand::where('product_id', $product->id)->pluck('brand_id')->toArray();
        return view('admin.products.products.add_product', $data);
    }

    public function saveProductCategoriesAndTags(Request $request)
    {
        return $this->productService->saveProductCategoriesAndTags($request);
    }

    public function productStatusChange(Request $request)
    {
        return $this->productService->statusChange($request->all());
    }

    public function getProductSupplier(Request $request)
    {
        $supplier = Product::select('suppliers.*')
            ->join('suppliers', 'products.supplier_id', 'suppliers.id')
            ->where(['products.id' => $request->id])->first();
        if (isset($supplier)) {
            return ['success' => TRUE, 'supplier' => $supplier];
        } else {
            return ['success' => FALSE];
        }
    }

    public function saveProductPricing(Request $request)
    {
        return $this->productService->saveProductPricing($request);
    }

    public function saveProductCombinations(Request $request)
    {
        return $this->productService->saveProductCombinations($request);
    }

    public function saveProductMedia(ProductMediaRequest $request)
    {
        return $this->productService->saveProductMedia($request);
    }

    public function saveProductFeaturedMedia(Request $request)
    {
        return $this->productService->saveProductFeaturedMedia($request);
    }

    public function deleteProductCombination(Request $request)
    {
        return $this->productService->deleteProductCombination($request->id);
    }

    public function duplicateProduct(ProductInitRequest $request)
    {
        return $this->productService->duplicateProduct($request);
    }

    public function productUpload(Request $request)
    {
        $data['suppliers'] = Supplier::where('status', STATUS_ACTIVE)->get();
        if ($request->ajax()) {
            $product_uploads = ProductUpload::select('product_uploads.*', 'suppliers.name as supplier_name', 'users.name as uploader_name')
                ->leftJoin('suppliers', 'product_uploads.supplier_id', 'suppliers.id')
                ->leftJoin('users', 'product_uploads.uploader_id', 'users.id')
                ->get();
            return datatables($product_uploads)
                ->editColumn('file', function ($item) {
                    $file = explode('/', $item->file);
                    return  end($file);
                })->editColumn('upload_time', function ($item) {
                    return Carbon::parse($item->upload_time)->format('Y-m-d h:is');
                })->editColumn('uploaded_product', function ($item) {
                    return '<a href="' . route('productUploadFailedList', ['payload' => $item->id]) . '">' . $item->uploaded_product . '/' . $item->total_product . '</a>';
                })->editColumn('status', function ($item) {
                    $html = '';
                    if ($item->status == STATUS_PRODUCT_PROCESSING_NEW) {
                        $html = '<span class="badge badge-secondary">' . __('New Process') . '</span>';
                    } elseif ($item->status == STATUS_PRODUCT_PROCESSING_START) {
                        $html = '<span class="badge badge-primary">' . __('Ready For Processing') . '</span>';
                    } elseif ($item->status == STATUS_PRODUCT_PROCESSING_PROCESSING) {
                        $html = '<span class="badge badge-warning">' . __('Processing Running') . '</span>';
                    } elseif ($item->status == STATUS_PRODUCT_PROCESSING_COMPLETED) {
                        $html = '<span class="badge badge-success">' . __('Processing Completed') . '</span>';
                    }
                    return $html;
                })->editColumn('action', function ($item) {
                    $html = '';
                    if ($item->status == STATUS_PRODUCT_PROCESSING_NEW) {
                        $html = '<button class="btn btn-dark btn-xs delete_item" data-id="' . $item->id . '"><i class="fa fa-trash"></i> ' . __('Delete') . '</button>';
                    } elseif ($item->status == STATUS_PRODUCT_PROCESSING_START) {
                        $html = '<button class="btn btn-dark btn-xs product_upload_process mr-1 mb-1" data-id="' . $item->id . '"><i class="fa fa-upload"></i> ' . __('Start Upload Process') . '</button>';
                        $html .= '<button class="btn btn-danger btn-xs delete_item" data-id="' . $item->id . '"><i class="fa fa-trash"></i> ' . __('Delete') . '</button>';
                    } elseif ($item->status == STATUS_PRODUCT_PROCESSING_PROCESSING) {
                        $html = '<span class="badge badge-warning">' . __('Processing Running') . '</span>';
                    } elseif ($item->status == STATUS_PRODUCT_PROCESSING_COMPLETED) {
                        $html = '<span class="badge badge-success">' . __('Processing Completed') . '</span>';
                    }
                    return $html;
                })->rawColumns(['icon', 'status', 'uploaded_product', 'action'])
                ->make(TRUE);
        }
        return view('admin.products.products.product_upload.product_upload', $data);
    }

    public function deleteProductExcel(Request $request)
    {
        try {
            DB::beginTransaction();
            $uploaded_product = ProductUpload::where('id', $request->id)->first();
            ProductUpload::where('id', $request->id)->delete();
            ProductUploadProcess::where('batch_payload', $uploaded_product->id)->delete();
            if (!empty($uploaded_product->file)) {
                unlink($uploaded_product->file);
            }
            DB::commit();
            return jsonResponse(TRUE)->message(__('Product excel file deleted successfully.'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return jsonResponse(FALSE)->default();
        }
    }

    public function productExport(Excel $excel)
    {
        return (new BrindeExport())->download('products.xlsx');
    }

    public function productImport(ProductImportRequest $request)
    {
        return ProductImportService::productUpload($request->all());
    }

    public function productUploadProcess(Request $request)
    {
        return ProductImportService::productUploadProcess($request->id);
    }

    public function productUploadFailedList(Request $request, $payload = NULL)
    {
        if ($request->ajax()) {
            if (!empty($payload)) {
                $list = ProductUploadProcess::where('status', STATUS_PRODUCT_UPLOAD_FAILED)->where('batch_payload', $payload);
            } else {
                $list = ProductUploadProcess::where('status', STATUS_PRODUCT_UPLOAD_FAILED);
            }
            return datatables($list)
                ->editColumn('image', function ($item) {
                    $product = json_decode($item->product);
                    if (isset($product[14])) {
                        return '<img src="' . $product[14] . '" onerror=\'this.src="' . adminAsset('images/no-image.png') . '"\' class="img-circle zoom-sm" width="60">';
                    } else {
                        return '<img src="' . adminAsset('images/no-image.png') . '" class="img-circle zoom-sm" width="60">';
                    }
                })->addColumn('name', function ($item) {
                    $product = json_decode($item->product);
                    return $product[0] ?? __('No name');
                })->addColumn('reference', function ($item) {
                    $product = json_decode($item->product);
                    return $product[1] ?? __('No reference');
                })->addColumn('description', function ($item) {
                    $product = json_decode($item->product);
                    return $product[2] ?? __('No description');
                })->addColumn('status', function ($item) {
                    $html = '<span class="badge badge-danger">' . __('Failed') . '</span>';
                    return $html;
                })->addColumn('action', function ($item) {
                    return '<button class="btn btn-dark btn-xs delete_item" data-id="' . $item->id . '"><i class="fa fa-trash"></i> ' . __('Delete') . '</button>';
                })->rawColumns(['image', 'status', 'action'])
                ->make(TRUE);
        }

        $data['payload'] = $payload;

        return view('admin.products.products.product_upload.product_upload_fails', $data);
    }

    public function deleteUploadFailProduct(Request $request)
    {
        try {
            ProductUploadProcess::where('id', $request->id)->delete();
            return jsonResponse(TRUE)->message(__('Product failed item deleted successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message(__('Product failed item delete failed.'));
        }
    }
}
