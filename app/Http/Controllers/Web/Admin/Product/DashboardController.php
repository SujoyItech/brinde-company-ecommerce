<?php

namespace App\Http\Controllers\Web\Admin\Product;


use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use App\Http\Services\Product\ProductService;
use App\Http\Services\Production\ProductionService;
use Illuminate\Http\Request;


class DashboardController extends Controller
{

    protected $product_service;

    public function __construct(ProductService $product_service)
    {
        $this->product_service = $product_service;
    }

    public function index(Request $request)
    {
        $data['product'] = $this->product_service->getDashboardData();
        return view('admin.products.dashboard.home', $data);
    }
}
