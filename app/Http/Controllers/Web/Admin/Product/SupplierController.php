<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\SupplierRequest;
use App\Http\Services\Product\SupplierService;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /*
     * @var SupplierService
     */
    private $supplierService;

    /**
     * SupplierController constructor.
     *
     * @param SupplierService $service
     */
    public function __construct(SupplierService $service)
    {
        $this->supplierService = $service;
    }

    public function index(Request $request)
    {
        $supplier_lists = $this->supplierService->getData([], [], ['created_at' => 'desc']);
        if ($request->ajax()) {
            return datatables($supplier_lists)
                ->editColumn('status', function ($item) {
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">' . __('Active') . '</span>' : '<span class="badge badge-warning">' . __('Inactive') . '</span>';
                })->editColumn('action', function ($item) {
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="' . $item->id . '"><i class="fa fa-edit"></i></a>';
                    $html .= '<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="' . $item->id . '"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status', 'action'])
                ->make(TRUE);
        }
        return view('admin.products.supplier.suppliers');
    }

    public function store(SupplierRequest $request)
    {
        if (!empty($request->id)) {
            return $this->supplierService->update($request->id, $request->except('id'));
        } else {
            return $this->supplierService->create($request->except('id'));
        }
    }

    public function edit(Request $request)
    {
        return view('admin.products.supplier.supplier_add', $this->supplierService->getSupplierData($request->id));
    }


    public function delete(Request $request)
    {
        return $this->supplierService->delete($request);
    }
}
