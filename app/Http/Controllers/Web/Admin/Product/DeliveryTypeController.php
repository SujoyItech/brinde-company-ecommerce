<?php

namespace App\Http\Controllers\Web\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Product\DeliveryRequest;
use App\Http\Services\Product\DeliveryTypeService;
use Illuminate\Http\Request;

class DeliveryTypeController extends Controller
{
    private $deliveryTypeService;

    public function __construct(DeliveryTypeService $service){
        $this->deliveryTypeService = $service;
    }

    public function index(Request $request){
        $delivery_type_lists = $this->deliveryTypeService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($delivery_type_lists)
                ->editColumn('icon',function ($item){
                    return '<img src="'.asset(get_image_path('delivery').'/'.$item->icon).'" class="img-circle" width="30">';
                }) ->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0);" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    return $html;
                })->rawColumns(['icon','status','action'])
                ->make(TRUE);
        }
        return view('admin.products.delivery.delivery_type');
    }

    public function edit(Request $request){
        return view('admin.products.delivery.delivery_type_add',$this->deliveryTypeService->getDeliveryTypeData($request->id));
    }

    public function store(DeliveryRequest $request){
        if(!empty($request->id)){
            return $this->deliveryTypeService->update($request->id,$request->except('id'));
        }else{
            return $this->deliveryTypeService->create($request->except('id'));
        }

    }

    public function delete(Request $request){
        return $this->deliveryTypeService->delete($request->id);
    }
}
