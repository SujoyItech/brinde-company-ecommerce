<?php

namespace App\Http\Controllers\Web\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\User\UserRequest;
use App\Http\Services\Product\BrandService;
use App\Http\Services\User\UserService;
use App\Models\Product\Brand;
use App\Models\Role\Role;
use App\Models\UserBrand;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /*
  * @var SupplierService
  */
    private $userService,$brandService;

    /**
     * SupplierController constructor.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service,BrandService $brand_service){
        $this->userService = $service;
        $this->brandService = $brand_service;
    }

    public function index(Request $request){
        $user_lists = $this->userService->getUserList();
        $data['roles'] = Role::all();
        if ($request->ajax()){
            return datatables($user_lists)
                ->editColumn('module_id',function ($item){
                    if ($item->module_id == MODULE_USER_ADMIN) return 'Admin';
                    elseif ($item->module_id == MODULE_PRODUCT_MANAGEMENT) return 'Product Manager';
                    elseif ($item->module_id == MODULE_PRODUCTION_MANAGEMENT) return 'Production Manager';
                    elseif ($item->module_id == MODULE_CRM_MANAGEMENT) return 'CRM Manager';
                    elseif ($item->module_id == MODULE_WEBSITE_MANAGEMENT) return 'Website Manager';
                })->editColumn('status',function ($item){
                    if($item->status == ACTIVE ) return '<span class="badge badge-success">'.__('Active').'</span>';
                    elseif($item->status == INACTIVE ) return '<span class="badge badge-primary">'.__('Inactive').'</span>';
                    elseif($item->status == USER_SUSPENDED ) return '<span class="badge badge-warning">'.__('Suspended').'</span>';
                    elseif($item->status == USER_BLOCKED ) return '<span class="badge badge-danger">'.__('Blocked').'</span>';
                })->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }
        $data['brands'] = Brand::where('status',STATUS_ACTIVE)->get();

        return view('admin.users.users',$data);
    }

    public function userSlugCheck(Request $request){
        return $this->userService->checkSlug($request->slug,$request->id);
    }

    public function store(UserRequest $request){
        if(!empty($request->id)){
            return $this->userService->updateUser($request->id,$request->except('id','brand_id'),$request->brand_id);
        }else{
            return $this->userService->createUser($request->except('id','brand_id'),$request->brand_id);
        }
    }

    public function edit(Request $request){
        $data = $this->userService->getUserData($request->id);
        $data['brands'] = Brand::where('status',STATUS_ACTIVE)->get();
        $data['user_brands'] = UserBrand::where('user_id',$request->id)->pluck('brand_id')->toArray();
        return view('admin.users.user_add',$data);
    }

    public function delete(Request $request){
        return $this->userService->delete($request->id);
    }

    public function userBrands(Request $request){
        $user_lists = $this->userService->getUserBrandList();
        if ($request->ajax()){
            return datatables($user_lists)
               ->editColumn('action',function ($item){
                    $brands = $this->brandService->getData(['status'=>STATUS_ACTIVE]);
                    $user_brands = !empty($item->brand_id) ? explode(',',$item->brand_id) : [];
                    $html = '';
                    if (!empty($brands))
                    {
                        foreach ($brands as $brand){
                            $html .= '<div class="custom-control custom-checkbox custom-control-inline">
                                          <input type="checkbox" class="custom-control-input check_brands" id="brand-'.$brand->id.'-'.$item->id.'"
                                                data-user-id="'.$item->id.'" data-brand-id="'.$brand->id.'" ';
                            if (in_array($brand->id,$user_brands)){
                                $html .= 'checked';
                            }
                            $html.='><label class="custom-control-label" for="brand-'.$brand->id.'-'.$item->id.'">'.$brand->name.'</label>
                                       </div>';
                        }
                    }

                    return $html;
                })->rawColumns(['action'])
                ->make(TRUE);
        }
        return view('admin.users.brands.user_brands');
    }

    public function userBrandCheck(Request $request){
        return $this->userService->userBrandCheck($request->all());
    }
}
