<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Auth\ForgetPasswordRequest;
use App\Http\Requests\Web\Auth\ForgotPasswordResetRequest;
use App\Http\Requests\Web\Auth\LoginRequest;
use App\Http\Services\Auth\AuthService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $authService;

    /**
     * SupplierController constructor.
     *
     * @param AuthService $service
     */
    public function __construct(AuthService $service){
        $this->authService = $service;
    }
    public function login() {
        if (Auth::user()) {
            if (check_permission(MODULE_SUPER_ADMIN)){
                return redirect()->route('crmManagerHome');
            }elseif (check_permission(MODULE_USER_ADMIN)){
                return redirect()->route('crmManagerHome');
            }elseif (check_permission(MODULE_PRODUCT_MANAGEMENT)){
                return redirect()->route('productManagerHome');
            }elseif (check_permission(MODULE_PRODUCTION_MANAGEMENT)){
                return redirect()->route('productionManagerHome');
            }elseif (check_permission(MODULE_CRM_MANAGEMENT)){
                return redirect()->route('crmManagerHome');
            }elseif (check_permission(MODULE_WEBSITE_MANAGEMENT)){
                return redirect()->route('websitePages');
            }else{
                Auth::logout();
                return view('auth.login');
            }
        } else {
            return view('auth.login');
        }
    }

    // Login post
    public function postLogin(LoginRequest $request) {
        $response = $this->authService->login($request);
        if ($response->getStatus() == TRUE){
            if (check_permission(MODULE_SUPER_ADMIN)){
                 return redirect()->intended('admin/crm-manager-home')->with(['success'=>__('Login successful as super admin')]);
            }elseif (check_permission(MODULE_USER_ADMIN)){
                return redirect()->intended('admin/crm-manager-home')->with(['success'=>__('Login successful as admin')]);
            }elseif (check_permission(MODULE_PRODUCT_MANAGEMENT)){
                return redirect()->intended('admin/product-manager-home')->with(['success'=>__('Login successful as product manager')]);
            }elseif (check_permission(MODULE_PRODUCTION_MANAGEMENT)){
                return redirect()->intended('admin/production-manager-home')->with(['success'=>__('Login successful as production manager')]);
            }elseif (check_permission(MODULE_CRM_MANAGEMENT)){
                return redirect()->intended('admin/crm-manager-home')->with(['success'=>__('Login successful as crm manager')]);
            }elseif (check_permission(MODULE_WEBSITE_MANAGEMENT)){
                return redirect()->intended('admin/website-pages')->with(['success'=>__('Login successful as website manager')]);
            }else{
                Auth::logout();
                return view('auth.login');
            }
        }else{
            Auth::logout();
            return redirect()->route('login')->with(['dismiss'=>$response->getMessage()]);
        }
    }


    //---- done
    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('landing');
    }

    public function forgetPassword() {
        return view('auth.forgot-password_1');
    }

    public function sendForgetPasswordMail(ForgetPasswordRequest $request) {
        $response = $this->authService->sendForgotPasswordMail($request);
        if ($response->getStatus() == TRUE) {
            return redirect()->route('login')->with('success', $response->getMessage());
        } else {
            return redirect()->back()->with('dismiss', $response->getMessage());
        }
    }

    public function resetPassword($reset_code){
        $remember_token = decrypt($reset_code);
        $response = $this->authService->resetPassword($remember_token);
        if ($response->getStatus() == TRUE){
            $data['remember_token'] = $remember_token;
            return view('auth.reset-password_1', $data);
        }else{
            return redirect()->route('login')->with(['dismiss'=>$response->getMessage()]);
        }
    }

    public function changePassword(ForgotPasswordResetRequest $request){
        $response = $this->authService->changePassword($request);
        if ($response->getStatus() == TRUE) {
            return redirect()->route('login')->with('success', $response->getMessage());
        } else {
            return redirect()->route('login')->with('dismiss', $response->getMessage());
        }
    }
}
