<?php

namespace App\Http\Controllers\Web\Payment;

use App\Http\Controllers\Controller;
use App\Http\Services\LoggerService;
use App\Http\Services\Payment\PaymentService;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $paymentService, $logger;

    public function __construct(PaymentService $paymentService) {
        $this->paymentService = $paymentService;
        $this->logger = new LoggerService(storage_logs().'ifthenpay_callback.log');
    }

    public function index(Request $request) {
        $response = $this->paymentService->generateReference($request);
        if($response['success']) {
            return view('payment.index', ['payRecord'=>$response['pay_record']]);
        } else{
            return $response['message'];
        }
    }

    public function ifthenpayCallback(Request $request) {
        $response = $this->paymentService->ifthenpayCallback($request);
        $this->logger->log('result', json_encode($response));
        return $response;
    }
}
