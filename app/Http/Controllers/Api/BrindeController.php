<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SubscriptionRequest;
use App\Http\Services\Api\BrindeService;
use Illuminate\Http\Request;

class BrindeController extends Controller
{
    private $brinde_service;
    public function __construct(BrindeService $brinde_service) {
        $this->brinde_service = $brinde_service;
    }

    public function subscribe(SubscriptionRequest $request){
        return $this->brinde_service->subscribe($request->all());
    }
    public function unSubscribe(Request $request){
        return $this->brinde_service->unSubscribe($request->email);
    }
    public function sendMessage(Request $request){
        return $this->brinde_service->sendMessage($request->all());
    }
    public function webExtraContent(){
        return $this->brinde_service->webExtraContent();
    }
    public function faqsApi(){
        return $this->brinde_service->faqsApi();
    }
    public function teamsApi(){
        return $this->brinde_service->teamsApi();
    }
    public function contactsApi(){
        return $this->brinde_service->contactsApi();
    }
    public function socialLinkApis(){
        return $this->brinde_service->socialLinkApis();
    }

    public function countryList(){
        return $this->brinde_service->countryList();
    }


}
