<?php

namespace App\Http\Controllers\Api\Website;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Website\WebsitePageRepository;
use App\Http\Services\Product\BrandService;
use App\Http\Services\Website\WebsitePageService;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    private $page_service,$brand_service;
    public function __construct(WebsitePageService $page_service,BrandService $brand_service){
        $this->page_service = $page_service;
        $this->brand_service = $brand_service;
    }

    public function homePageContent($brand_slug){
        $brand_id = $this->brand_service->getIdBySlug($brand_slug);
        return $this->page_service->getWebPageContent(NULL,'home',$brand_id);
    }
    public function webpageList($brand_slug){
        return $this->page_service->getWebPageList($brand_slug);
    }

    public function webpageContent($page_slug){
        return $this->page_service->getWebPageContent($page_slug);
    }
}
