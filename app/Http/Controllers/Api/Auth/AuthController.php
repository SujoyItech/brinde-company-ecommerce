<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;

use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\ForgotPasswordResetRequest;
use App\Http\Requests\Web\Auth\LoginRequest;
use App\Http\Requests\Web\Auth\RegistrationRequest;
use App\Http\Services\Api\Auth\AuthService;
use App\Models\CRM\CrmCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $service){
        $this->authService = $service;
    }

    public function login(LoginRequest $request){
        return $this->authService->login($request);
    }

    public function register(RegistrationRequest $request){
        return $this->authService->register($request->all());
    }

    public function customerVerifyEmail(Request $request){
        return $this->authService->customerVerifyEmail($request->code);
    }

    public function sendForgetPasswordMail(ForgetPasswordRequest $request){
        return $this->authService->sendForgetPasswordMail($request->email);
    }

    public function resetPassword(ForgotPasswordResetRequest $request){
        return $this->authService->resetPassword($request->all());
    }

}
