<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CRM\CrmCustomerRequest;
use App\Http\Services\CRM\CrmCustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CrmCustomerController extends Controller
{
    private $customer_service;
    public function __construct(CrmCustomerService $customer_service){
        $this->customer_service = $customer_service;
    }
    public function getCustomer(){
        return $this->customer_service->getCustomerProfile(Auth::user()->id);
    }

    public function updateCustomerProfile(CrmCustomerRequest $request){
        return $this->customer_service->updateCustomerProfile($request);
    }

    public function updateCustomerPassword(Request $request){
        return $this->customer_service->updateCustomerPassword($request->all());
    }
}
