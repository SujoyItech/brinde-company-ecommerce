<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Http\Services\Api\Product\ProductService;
use App\Http\Services\Product\BrandService;
use App\Http\Services\Product\CategoryService;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Product\Supplier;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public $productService,$categoryService,$brandService;
    public function __construct(ProductService $service,CategoryService $category_service,BrandService $brand_service){
        $this->productService = $service;
        $this->categoryService = $category_service;
        $this->brandService = $brand_service;
    }
    public function categoryList($brand_slug,$parent_slug=NULL){
        $parent_id = 0;

        if (!empty($parent_slug)){
            $category = Category::where('slug',$parent_slug)->first();
            $parent_id = $category->id ?? 0;
        }
        $brand_id = $this->brandService->getIdBySlug($brand_slug);
        return $this->productService->categoryList($brand_id,$parent_id);
    }

    public function product($slug){
        return $this->productService->product($slug);
    }
    public function productList($brand_slug='',Request $request){
        $search_data = $request->all();
        $search_data['category_id'] = isset($search_data['category_id']) && !empty($search_data['category_id']) ? $search_data['category_id'] : [];
        if (isset($request->parent_slug) && !empty($request->parent_slug)){
            $category = Category::where('slug',$request->parent_slug)->first();
            if (isset($search_data['category_id'])){
                array_push($search_data['category_id'],$category->id ?? '');
                $search_data['category_id'] = array_unique($search_data['category_id'],SORT_ASC);
            }
        }
        return $this->productService->productList($search_data,$brand_slug);
    }

    public function getColorList($parent_category_slug = NULL){
        $parent_category_id = 0;
        if (!empty($parent_category_slug)){
            $category = Category::where('slug',$parent_category_slug)->first();
            $parent_category_id = $category->id ?? 0;
        }

        return $this->productService->getColorList($parent_category_id);
    }

    public function getDeliveryTypes($parent_category_slug = NULL){
        $parent_category_id = 0;
        if (!empty($parent_slug)){
            $category = Category::where('slug',$parent_category_slug)->first();
            $parent_category_id = $category->id ?? 0;
        }
        return $this->productService->getDeliveryTypes($parent_category_id);
    }

    public function searchProduct($brand,$keywords = '',$order_by = ''){
        if (!empty($keywords)){
            $search_data['keywords'] = $keywords;
        }
        if (!empty($order_by)){
            $search_data['order_by'] = $order_by;
        }
        return $this->productService->productList($search_data,$brand);
    }
}
