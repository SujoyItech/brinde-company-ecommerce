<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function setLang($code) {
        $user_lang_update = User::where('id',Auth::user()->id)->update(['language' => $code]);
        if ($user_lang_update) {
            return redirect()->back()->with(['success' => __('Language Changed Successfully.')]);
        } else {
            return redirect()->back()->with(['dismiss' => __('Language Change Failed.')]);
        }
    }
}
