<?php

namespace App\Http\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class LanguageSettingController extends Controller
{
    public function languageSettings()
    {
        return view('admin.settings.language_settings');
    }

    public function addLanguage(){
        $language = $_GET['language'];
        $exitCode = Artisan::call('translation:add-language');
    }
}
