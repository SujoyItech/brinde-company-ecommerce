<?php

namespace App\Http\Requests\Web\Admin\CRM;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class CrmCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
//            'customer_code'=>'required|max:100',
            'name'=>'required|max:100',
            'contact_name'=>'required|max:180',
            'email' => [
                'required',
                'email',
                Rule::unique('crm_customers')->ignore($this->id, 'id')
            ],
            'crm_payment_condition_id' => 'required|exists:crm_payment_conditions,id',
            'status'=>'required'
        ];
        if($this->phone) {
            $rules['phone'] = 'max:25';
        }
        if(isset($this->quotation_id)) {
            $rules['assigned_salesman'] = 'required|exists:users,id';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'=>__('Company name is required.'),
            'name.max'=>__('Name can\'t be more than 100 character.'),
            'tin_number.required'=>__('Vat number is required.'),
            'status.required'=>__('Status field is required.')
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = ['success'=>false,
                'data'=>[],
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 200);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }

    }
}
