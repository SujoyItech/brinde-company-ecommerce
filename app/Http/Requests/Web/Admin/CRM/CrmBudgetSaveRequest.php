<?php

namespace App\Http\Requests\Web\Admin\CRM;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class CrmBudgetSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'quotation_id' => 'required|exists:crm_quotations,id',
            'expired_at' => 'required|date',
        ];
        if ($this->printing_enable == 'on') {
            $rules['printing_options.*'] = 'required|exists:crm_printing_options,id';
            $rules['budget_printing_comments'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'product_name.required'=>__('Item name is required.'),
            'product_name.max'=>__('Name can\'t be more than 100 character.'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = ['success'=>false,
                'data'=>[],
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 200);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }

    }
}
