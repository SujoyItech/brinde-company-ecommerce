<?php

namespace App\Http\Requests\Web\Admin\Product;

use App\Http\Requests\BaseValidation;
use Illuminate\Foundation\Http\FormRequest;

class ProductMediaRequest extends BaseValidation
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->has('image')) {
            $maxSize = isset(__options(['application_settings'])->product_image_max_size_in_mb) ? __options(['application_settings'])->product_image_max_size_in_mb : 1;
            $maxSize *= 1024;
            $rules['image'] = 'image|mimes:jpg,jpeg,png|max:'.$maxSize;
        } else if($this->has('media_url')) {
            $rules['media_url'] = 'required|max:255';
        }
        $rules['media_type'] = 'required|in:'.implode(',', array_keys(get_product_media_type()));
        return $rules;
    }
}
