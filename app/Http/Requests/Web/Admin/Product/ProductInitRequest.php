<?php

namespace App\Http\Requests\Web\Admin\Product;

use App\Http\Requests\BaseValidation;
use Illuminate\Foundation\Http\FormRequest;

class ProductInitRequest extends BaseValidation
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:100',
            'reference'=>'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>__('Name field is required.'),
            'name.max'=>__('Name can\'t be more than 100 character.'),
            'reference.required'=>__('Reference field is required.'),
            'reference.max'=>__('Reference can\'t be more than 100 character.'),
        ];
    }
}
