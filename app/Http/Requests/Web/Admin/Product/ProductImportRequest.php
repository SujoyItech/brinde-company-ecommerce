<?php

namespace App\Http\Requests\Web\Admin\Product;

use App\Http\Requests\BaseValidation;
use Illuminate\Foundation\Http\FormRequest;

class ProductImportRequest extends BaseValidation
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['file'] = 'required';
        $rules['supplier_id'] = 'required';
        return $rules;
    }
}
