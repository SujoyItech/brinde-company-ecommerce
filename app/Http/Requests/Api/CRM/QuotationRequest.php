<?php

namespace App\Http\Requests\Api\CRM;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class QuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'brand_slug' => 'required|exists:brands,slug',
            'company_name' => 'max:255',
            'contact_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'product_id.*' => 'required|exists:products,id',
            'combination_id.*' => 'required|exists:combinations,id',
            'combination_type_id.*' => 'required|exists:combination_types,id',
            'quantity.*' => 'required|min:1'
        ];
        if ($this->phone) {
            $rules['phone'] = 'max:15';
        }
        if ($this->attached_file) {
            $rules['attached_file'] = 'max:5048';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'contact_name.required' => __('Contact name is required'),
            'email.required' => __('Email is required'),
            'product_id' => __('Product is required'),
            'combination_id' => __('Product combination is required'),
            'combination_type_id' => __('Product combination type is required'),
            'quantity' => __('Quantity is required'),
            'phone.max' => __('Invalid Phone!'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = [
                'success'=>false,
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 200);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }

    }
}
