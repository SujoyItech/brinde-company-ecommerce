<?php

namespace App\Http\Services\Auth;

use App\Http\Repositories\Auth\AuthRepository;
use App\Http\Requests\Web\Auth\ForgetPasswordRequest;
use App\Http\Services\BaseService;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Auth/AuthRepository $repository
     */
    public function __construct(AuthRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function login(Request $request){

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return $this->statusCheck(auth()->user());
        } else {
            return jsonResponse(FALSE)->message(__('Email or Password Not matched!'));
        }
    }


    public function statusCheck($user){
        if ($user->status == ACTIVE){
            return jsonResponse(true);
        }elseif ($user->status == INACTIVE) {
            Auth::logout();
            return jsonResponse(FALSE)->message(__('Your account is inactive. Please change your password or contact with admin.'));
        }
        elseif ($user->status == USER_BLOCKED){
            Auth::logout();
            return jsonResponse(FALSE)->message(__('You are blocked. Contact with admin.'));
        } elseif ($user->status == USER_SUSPENDED) {
            Auth::logout();
            return jsonResponse(FALSE)->message(__('Your Account has been suspended. please contact with admin to active again!'));
        }else{
            Auth::logout();
            return jsonResponse(FALSE)->default();
        }
    }

    public function sendForgotPasswordMail($request) {
        try {
            $user = User::where(['email' => $request->email])->first();
            if ($user) {
                 $this->repo->sendForgotPasswordMail($user);
                 return jsonResponse(true)->message(__("Please check your email to recover password."));
            } else {
                return jsonResponse(FALSE)->message(__("Your email is not correct!."));
            }
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function resetPassword($remember_token){
        try {
            $user = $this->repo->getUserByToken($remember_token);
            if ($user) {
                return jsonResponse(true)->message(__("User get successfully."));
            } else {
                return jsonResponse(FALSE)->message(__("Invalid request!."));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function changePassword(Request $request){
        try {
            $user = User::where(['remember_token' => $request->remember_token])->first();
            if ($user) {
                $updated = $this->repo->changePassword($user,$request->password);
                if ($updated) {
                    if ($user->status == INACTIVE){
                        $this->repo->changeStatus($user,STATUS_ACTIVE);
                    }
                    return jsonResponse(true)->message(__("Password changed successfully."));
                } else {
                    return jsonResponse(FALSE)->message(__("Password not changed try again."));
                }
            } else {
                return jsonResponse(FALSE)->message(__("Sorry! user not found."));
            }
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }
}
