<?php

namespace App\Http\Services\CRM;

use App\Http\Repositories\CRM\CrmPaymentConditionRepository;
use App\Http\Services\BaseService;

class CrmPaymentConditionService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CRM/CrmPaymentConditionRepository $repository
     */
    public function __construct(CrmPaymentConditionRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getPaymentConditionData($id){

        if ($id !== NULL){
            $data['payment_condition'] = $this->repo->firstWhere(['id'=>$id]);
        }else{
            $data['payment_condition'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {

        try {
            $payment_condition = $this->repo->create($requestArray);
            if ( $payment_condition) {
                return jsonResponse(true)->message(__("Payment Condition has been created successfully."));
            }
            return jsonResponse(false)->message(__("Payment Condition create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Payment Condition has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $response = $this->repo->destroy($id);
            if ($response){
                return jsonResponse(TRUE)->message(__('Payment Condition deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Payment Condition delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
