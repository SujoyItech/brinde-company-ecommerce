<?php

namespace App\Http\Services\CRM;

use App\Http\Repositories\CRM\BudgetRepository;
use App\Http\Repositories\CRM\QuotationProductRepository;
use App\Http\Repositories\CRM\QuotationRepository;
use App\Http\Repositories\Product\BrandRepository;
use App\Http\Repositories\Product\ProductRepository;
use App\Http\Services\BaseService;
use App\Http\Services\CrmStatusChangeMailService;
use App\Http\Services\MailService;
use App\Jobs\CrmDailyMailJob;
use App\Models\CRM\CrmCustomer;
use App\Models\Crm\CrmQuotation;
use App\Models\Product\Combination;
use App\Models\Product\ProductCombination;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class BudgetService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CrmQuotation/BudgetRepository $repository
     */
    private $quotationProductRepo, $productRepo, $brandRepo, $budgetRepo, $mailService;

    public function __construct(
        QuotationRepository $repository,
        QuotationProductRepository $quotationProductRepo,
        ProductRepository $productRepo,
        BrandRepository $brandRepo,
        BudgetRepository $budgetRepo,
        MailService $mailService
    ) {
        $this->repo = $repository;
        $this->quotationProductRepo = $quotationProductRepo;
        $this->productRepo = $productRepo;
        $this->brandRepo = $brandRepo;
        $this->budgetRepo = $budgetRepo;
        $this->mailService = $mailService;
    }

    // Your methods for repository

    public function getSingleData($id)
    {

        if ($id !== NULL) {
            $data = $data = $this->repo->firstWhere(['id' => $id]);
        } else {
            $data = [];
        }
        return $data;
    }

    public function getCreatedBudget($code)
    {
        return $this->budgetRepo->getCreatedBudget($code);
    }

    public function getBudgetData($search_array = [])
    {
        return $this->budgetRepo->getBudgetData($search_array);
    }

    public function create($request)
    {
    }

    public function update(int $id, array $requestArray)
    {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse('', __("Your quotation updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    private function imageData($image, $id = NULL)
    {
        if ($id !== NULL) {
            $details = $this->getSingleData($id);
            return uploadImage($image, get_image_path('quotation'), $details->icon ?? '');
        } else {
            return uploadImage($image, get_image_path('quotation'));
        }
    }

    public function delete($id)
    {
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier) {
                return jsonResponse(TRUE)->message(__('Brand deleted successfully.'));
            } else {
                return jsonResponse(TRUE)->message(__('Brand delete failed.'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function createBudgetProduct($request)
    {
        try {
            $product = '';
            if (isset($request->product_id)) {
                $product = $this->productRepo->firstWhere(['id' => $request->product_id]);
                if (empty($product)) {
                    return sendResponseError('', __('Product not found'));
                }
            }
            $input = [
                'quotation_id'        => $request->quotation_id,
                'is_shipped_item'     => isset($request->is_shipped_item) ? $request->is_shipped_item : INACTIVE,
                'product_id'          => $request->product_id ?? NULL,
                'combination_id'      => $request->combination_id ?? NULL,
                'combination_type_id' => $request->combination_type_id ?? NULL,
                'comments'            => $request->comments ?? NULL,
                'additional_info'     => $request->additional_info ?? NULL,
                'quantity'            => $request->quantity,
                'product_name'        => !empty($product) ? $product->reference : $request->product_name,
                'product_reference'   => !empty($product) ? $product->reference : NULL,
                'unit_price'          => $request->unit_price,
                'total_price'         => $request->unit_price * $request->quantity,
                'vat'                 => $request->vat ?? VAT_AMOUNT,
                'total_vat'           => ($request->vat * ($request->unit_price * $request->quantity)) / 100,
            ];
            $response = $this->quotationProductRepo->create($input);
            $this->updateBudgetPrice($request->quotation_id);

            if ($response) {
                $data['products'] = $this->quotationProductRepo->getData(['quotation_id' => $request->quotation_id]);
                $view = '';
                $view .= View::make('admin.crm.budget.budget_view_product_item', $data);
            }

            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse($view, __("Product added to budget."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    public function updateBudgetProduct($request)
    {
        try {
            $input = [
                'combination_id'        => $request->combination_id ?? NULL,
                'comments'        => $request->comments ?? NULL,
                'additional_info' => $request->additional_info ?? NULL,
                'quantity'        => $request->quantity,
                'unit_price'      => $request->unit_price,
                'total_price'     => $request->unit_price * $request->quantity,
                'vat'             => $request->vat ?? VAT_AMOUNT,
                'total_vat'       => ($request->vat * ($request->unit_price * $request->quantity)) / 100,
            ];
            $response = $this->quotationProductRepo->updateModel($request->edit_id, $input);
            $this->updateBudgetPrice($request->quotation_id);

            $view = '';
            if ($response) {
                $data['products'] = $this->quotationProductRepo->getData(['quotation_id' => $request->quotation_id]);
                $view = '';
                $view .= View::make('admin.crm.budget.budget_view_product_item', $data);
            }

            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse($view, __("Product updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    public function duplicateBudgetProduct($request)
    {
        try {
            $product = $this->quotationProductRepo->firstWhere(['id' => $request->id]);
            if (isset($product)) {
                $input = [
                    'quotation_id'        => $product->quotation_id,
                    'is_shipped_item'     => $product->is_shipped_item,
                    'product_id'          => $product->product_id,
                    'combination_id'      => $product->combination_id,
                    'combination_type_id' => $product->combination_type_id,
                    'comments'            => $product->comments,
                    'additional_info'     => $product->additional_info,
                    'quantity'            => $product->quantity,
                    'product_name'        => $product->product_name,
                    'product_reference'   => $product->product_reference,
                    'unit_price'          => $product->unit_price,
                    'total_price'         => $product->total_price,
                    'vat'                 => $product->vat ?? VAT_AMOUNT,
                    'total_vat'           => $product->total_vat,
                ];
                $response = $this->quotationProductRepo->create($input);
                $this->updateBudgetPrice($product->quotation_id);

                $view = '';
                if ($response) {
                    $data['products'] = $this->quotationProductRepo->getData(['quotation_id' => $product->quotation_id]);
                    $view = '';
                    $view .= View::make('admin.crm.budget.budget_view_product_item', $data);
                }

                return !$response ? sendResponseError('', __("Operation failed.")) :
                    sendResponse($view, __("Product cloned successfully."));
            } else {
                return sendResponseError('', __('Data not found'));
            }
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    public function sendBudgetMail($request)
    {
        try {
            $quotation = CrmQuotation::select(
                'crm_quotations.*',
                'users.name as salesman_name',
                'users.email as salesman_email',
                'brands.id as brand_id',
                'brands.name as brand_name',
                'brands.brand_url',
                'brands.email as brand_email',
                'brands.phone as brand_phone',
                'brands.address as brand_address',
                'brands.privacy_policy_link as brand_privacy_policy_link',
                'brands.faq_link as brand_faq_link',
                'brands.color_code as brand_color_code',

            )
                ->leftJoin('users', 'crm_quotations.assigned_salesman', '=', 'users.id')
                ->leftJoin('brands', 'crm_quotations.brand_id', '=', 'brands.id')
                ->where('crm_quotations.id', $request->id)->first();

            if (isset($quotation)) {
                $userName = $quotation->contact_name;
                $userEmail = $quotation->email;
                $companyName = env('APP_NAME',);
                $subject = __('Budget Proposal | :companyName', ['companyName' => $companyName]);
                $data = [
                    'brand_id'         => $quotation->brand_id,
                    'brand_name'       => $quotation->brand_name,
                    'brand_url'        => $quotation->brand_url,
                    'brand_email'        => $quotation->brand_email,
                    'brand_phone'        => $quotation->brand_phone,
                    'brand_address'        => $quotation->brand_address,
                    'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                    'brand_faq_link'        => $quotation->brand_faq_link,
                    'brand_color_code'        => $quotation->brand_color_code,
                    'salesman_name' => $quotation->salesman_name,
                    'salesman_email' => $quotation->salesman_email,
                    'budget_reference' => $quotation->budget_reference,
                    'customer_name' => $quotation->contact_name,
                    'approve_link' => route('clientBudgetDetails', $quotation->budget_reference),
                    'date' => Carbon::now()->format('d-m-Y')
                ];
                dispatch(new CrmDailyMailJob('admin.mail.email.budget_mail', $userEmail, $data, $subject));
                $this->repo->updateModel($quotation->id, ['status' => STATUS_BUDGET_SENT]);
                return sendResponse('', __("Budget proposal sent successfully."));
            } else {
                return sendResponseError('', __("Quotation not found."));
            }
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    public function saveBudget($request)
    {
        try {
            $input = [
                'budget_printing_enable'   => $request->printing_enable == 'on' ? 1 : 0,
                'budget_printing_comments' => $request->budget_printing_comments ?? NULL,
                'expired_at'               => $request->expired_at ?? NULL,
                'budget_masking_enable'    => $request->masking_enable == 'on' ? 1 : 0,
                'budget_show_total_enable' => $request->show_total_enable == 'on' ? 1 : 0,
                'budget_model'             => $request->budget_model,
            ];

            $options = '';
            if (isset($request->printing_options[0])) {
                $options = implode(',', $request->printing_options);
            }
            $input['budget_printing_option'] = $options;

            $response = $this->budgetRepo->updateModel($request->quotation_id, $input);

            $this->updateBudgetPrice($request->quotation_id);
            if ($request->masking_enable == 'on') {
                $this->make_product_masking($request->quotation_id);
            }

            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse('', __("Budget updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    public function crmBudgetCreateSave(array $requestArray)
    {
        try {
            $customer = CrmCustomer::where('id', $requestArray['customer_id'])->first();
            $brand = $this->brandRepo->firstWhere(['id' => $requestArray['brand_id']]);
            $data = [
                'brand_id' => $brand->id,
                'quote_reference' => formatQuoteReference(textSlugify($brand->name), getNextPrimaryId('crm_quotations')),
                'need_payment' => $brand->is_reseller,
                'customer_id' => $customer->id,
                'company_name' => $customer->name,
                'contact_name' => $customer->contact_name,
                'email' => $customer->email,
                'phone' => $customer->phone
            ];
            $quotation = $this->repo->create($data);
            if ($quotation) {
                $salesman = User::where('id', Auth::user()->id)->first();
                $update_data = [
                    'assigned_salesman' => $salesman->id,
                    'budget_reference' => formatBudgetReference(referenceCodeEdit($quotation->quote_reference, 0), textSlugify($salesman->name), referenceCodeEdit($quotation->quote_reference, 1), referenceCodeEdit($quotation->quote_reference, 2)),
                    'customer_code' => customerReferenceWithSalesman(textSlugify($salesman->name), referenceCodeEdit($customer->customer_code, 0), referenceCodeEdit($customer->customer_code, 1), referenceCodeEdit($customer->customer_code, 2)),
                    'assignee_sales_admin' => Auth::id(),
                    'status' => STATUS_QUOTATION_SALESMAN_ASSIGNED
                ];
                $this->repo->updateModel($quotation->id, $update_data);
            }
            $quotation_data['data'] = $this->repo->firstWhere(['id' => $quotation->id]);
            return jsonResponse(TRUE)->message(__("New budget has been created successfully."))->data($quotation_data);
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message(__("New budget create failed."));
        }
    }

    public function deleteBudgetProduct($request)
    {
        try {
            $quotation = $this->quotationProductRepo->firstWhere(['id' => $request->id]);
            $delete = $this->quotationProductRepo->destroy($request->id);
            if ($delete) {
                $this->updateBudgetPrice($quotation->quotation_id);

                return sendResponse($request->id, __('Item deleted successfully'));
            } else {
                return sendResponseError('', __('Operation failed'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function sendBudgetModifyRequest($request)
    {
        try {
            $budget = $this->budgetRepo->firstWhere(['id' => $request->quotation_id]);

            $comment = array(
                'date_time' => date("Y-m-d h:i:s"),
                'comments'  => $request->budget_modification_comments
            );

            $comments = static::getComments($budget->budget_modification_comments, $comment);

            $input = [
                'budget_modification_comments' => $comments,
                'status'                       => STATUS_BUDGET_MODIFY_REQUEST
            ];

            $response = $this->budgetRepo->updateModel($request->quotation_id, $input);

            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse('', __("Budget modification request sent successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    private static function getComments($previous_comments, $comment)
    {
        $comments = $previous_comments != NULL ? json_decode($previous_comments) : [];
        array_push($comments, $comment);

        return json_encode($comments);
    }

    public function approveBudget($request)
    {
        try {
            $budget = $this->repo->firstWhere(['id' => $request->id]);
            if ($budget->status >= STATUS_BUDGET_APPROVED) {
                return sendResponseError('', __('Sorry this budget already approved'));
            }
            $input = [
                'status' => STATUS_BUDGET_APPROVED,
                'approved_by' => STATUS_APPROVED_BY_CUSTOMER
            ];

            $response = $this->budgetRepo->updateModel($request->id, $input);

            $this->budgetApproveStatusMail($budget->id);
            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse($budget, __("Budget approved successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    private function budgetApproveStatusMail($id)
    {
        CrmStatusChangeMailService::productBudgetApprovalStatus($id);
    }

    public function getBudgetProductPrice($budget_id)
    {
        $products = $this->quotationProductRepo->getBudgetProducts($budget_id);
        $data['total_price'] = 0;
        $data['total_vat'] = 0;
        if (isset($products[0])) {
            foreach ($products as $product) {
                $data['total_price'] = $data['total_price'] + $product->total_price;
                $data['total_vat'] = $data['total_vat'] + $product->total_vat;
            }
        }

        return $data;
    }

    public function updateBudgetPrice($budget_id)
    {
        $product = $this->getBudgetProductPrice($budget_id);
        $input = [
            'total_price' => $product['total_price'],
            'total_vat'   => $product['total_vat'],
            'grand_total' => $product['total_vat'] + $product['total_price'],
        ];

        $response = $this->budgetRepo->updateModel($budget_id, $input);
    }

    private function make_product_masking($quotation_id)
    {
        try {
            $products = $this->quotationProductRepo->getBudgetProducts($quotation_id);
            if (isset($products[0])) {
                foreach ($products as $product) {
                    $ranString = Str::random(15);
                    $this->quotationProductRepo->updateModel($product->id, ['product_mask_reference' => $ranString]);
                }
            }
        } catch (\Exception $e) {
        }
    }

    public function searchProduct($request)
    {
        $response = $this->productRepo->getProductSearchedData($request->search_item);
        try {

            $data['quotation_id'] = $request->quotation_id;
            $data['products'] = $response;
            $view = '';
            $view .= View::make('admin.crm.budget.searched_product_list', $data);
            return response()->json($view);
        } catch (\Exception $e) {
            //--
        }
    }

    public function budgetProductEdit($request)
    {
        try {

            if (isset($request->new_product) && ($request->new_product == 'new')) {
                $response = $this->productRepo->firstWhere(['id' => $request->id]);
                $product_combination = ProductCombination::where(['product_id' => $request->id, 'combination_type_id' => 1])->pluck('combination_id')->toArray();
                $combination = Combination::whereIn('id', $product_combination)->get();
                $data['colors'] = $combination;
                $data['product'] = $response;
                return view('admin.crm.budget.budget_product_add_modal_data', $data);
            } else {
                $response = $this->quotationProductRepo->firstWhere(['id' => $request->id]);
                if (!empty($response->product_id)) {
                    $product_combination = ProductCombination::where(['product_id' => $response->product_id, 'combination_type_id' => 1])->pluck('combination_id')->toArray();
                    $combination = Combination::whereIn('id', $product_combination)->get();
                } else {
                    $combination = Combination::where('combination_type_id', 1)->get();
                }

                $data['colors'] = $combination;

                $data['product'] = $response;
                return view('admin.crm.budget.budget_product_edit_modal_data', $data);
            }
        } catch (\Exception $e) {
        }
    }

    public function makeBudgetOrder($request)
    {
        try {
            $budget = $this->budgetRepo->firstWhere(['id' => $request->id]);
            if ($budget->status >= STATUS_BUDGET_ORDERED) {
                return sendResponseError('', __('Sorry this budget already sent to order'));
            }
            if ($budget->status != STATUS_BUDGET_APPROVED) {
                return sendResponseError('', __('Sorry this budget is not approved , so you can not make it order'));
            }

            $input = [
                'in_staging'      => QUOTATION_STAGE_IN_PURCHASE,
                'status'          => STATUS_BUDGET_ORDERED,
                'order_reference' => formatOrderReference(referenceCodeEdit($budget->quote_reference, 2))
            ];

            $response = $this->budgetRepo->updateModel($request->id, $input);
            $budget = $this->budgetRepo->firstWhere(['id' => $request->id]);
            $this->makeOrderMail($budget->id);
            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse($budget, __("Budget send to order successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    private function makeOrderMail($id)
    {
        CrmStatusChangeMailService::makeOrderMail($id);
    }

    public function crmBudgetApprove($request)
    {
        try {
            $budget = $this->repo->firstWhere(['id' => $request->id]);
            if ($budget->status >= STATUS_BUDGET_APPROVED) {
                return jsonResponse(FALSE)->message(__('Sorry this budget already approved'));
            }
            $input = [
                'status' => STATUS_BUDGET_APPROVED,
                'approved_by' => STATUS_APPROVED_BY_MANAGER
            ];

            $response = $this->repo->updateModel($request->id, $input);
            return !$response ? jsonResponse(FALSE)->message(__("Operation failed.")) :
                jsonResponse(TRUE)->message(__("Budget approved successfully."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->message($e->getMessage());
        }
    }
}
