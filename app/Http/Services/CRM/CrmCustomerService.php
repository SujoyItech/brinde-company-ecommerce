<?php

namespace App\Http\Services\CRM;

use App\Http\Repositories\CRM\CrmCustomerRepository;
use App\Http\Repositories\CRM\QuotationRepository;
use App\Http\Services\BaseService;
use App\Http\Services\MailService;
use App\Http\Services\User\UserService;
use App\Jobs\CrmDailyMailJob;
use App\Models\CRM\CrmCustomer;
use App\Models\CRM\CrmQuotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CrmCustomerService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CRM/CrmCustomerRepository $repository
     */
    private $quotationService, $userService;
    public function __construct(CrmCustomerRepository $repository, QuotationService $quotationService, UserService $userService)
    {
        $this->repo = $repository;
        $this->quotationService = $quotationService;
        $this->userService = $userService;
    }

    // Your methods for repository

    public function getSingleData($id){

        if ($id !== NULL){
            $data = $this->repo->firstWhere(['customer_code' => $id]);
        }else{
            $data = (object)[];
        }
        return $data;
    }
    public function getSingleCustomer($id){

        if ($id !== NULL){
            $data = $this->repo->firstWhere(['id' => $id]);
        }else{
            $data = (object)[];
        }
        return $data;
    }

    public function customerOrderDetails($customer_id,$search_array=[]){
        $lists = CrmQuotation::select('*')->where('customer_id',$customer_id);
        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['value'] !== NULL){
                    $lists->where('crm_quotations.'.$search['name'],$search['value']);
                }
            }
        }
        return $lists->get();
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $data = [
                'customer_code' => formatCustomerReference(textSlugify($request->contact_name), getNextPrimaryId('crm_customers')),
                'name' => $request->name,
                'contact_name' => $request->contact_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt('123456'),
                'remember_token' => md5($request->email . uniqid() . randomString(5)),
                'crm_payment_condition_id' => $request->crm_payment_condition_id,
                'address' => $request->address,
                'postcode' => $request->postcode,
                'locality' => $request->locality,
                'country' => $request->country,
                'tin_number' => $request->tin_number,
                'tax_address' => $request->tax_address,
                'fiscal_postcode' => $request->fiscal_postcode,
                'tax_location' => $request->tax_location,
                'tax_country' => $request->tax_country,
                'status' => $request->status,
                'email_verified' => TRUE,
                'salesman_id' => $request->assigned_salesman
            ];

            $item = $this->repo->create($data);
            if(isset($item)) {
                $this->createCustomerMail($item);
                if(isset($request->quotation_id)) {
                    $quotation = $this->quotationService->getSingleData($request->quotation_id);
                    if(isset($request->assigned_salesman)) {
                        $salesman = $this->userService->getUserData($request->assigned_salesman)['user'];
                        $quotationArray = [
                            'assigned_salesman' => $request->assigned_salesman,
                            'customer_id' => $item->id,
                            'status' => STATUS_QUOTATION_SALESMAN_ASSIGNED,
                            'customer_code' => customerReferenceWithSalesman(textSlugify($salesman->name),referenceCodeEdit($item->customer_code,0),referenceCodeEdit($item->customer_code,1),referenceCodeEdit($item->customer_code,2)) ,
                            'budget_reference' => formatBudgetReference(referenceCodeEdit($quotation->quote_reference,0), textSlugify($salesman->name),referenceCodeEdit($quotation->quote_reference,1), referenceCodeEdit($quotation->quote_reference,2)),
                        ];
                        if(empty($quotation->assignee_sales_admin)) {
                            $quotationArray['assignee_sales_admin'] = Auth::id();
                        }
                        $this->quotationService->update($request->quotation_id, $quotationArray);
                    }
                }
                DB::commit();
                return sendResponse($item,__("New customer created successfully."));
            }
            DB::rollBack();
            return sendResponseError('',__("Operation failed."));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('',$e->getMessage());
        }
    }

    private function createCustomerMail($user) {
        if ($user) {
            $userEmail = $user->email;
            $subject = __('Customer create mail');
            $data['user'] = $user;
            $data['title'] = __('Your are successfully registered as customer.');
            $data['message'] = __('Please reset your password from forget password to login.');
            dispatch(new CrmDailyMailJob('admin.mail.email.customer.create_customer_mail', $userEmail, $data, $subject));
        }
    }

    public function assignSalesman($request)
    {
        DB::beginTransaction();
        try {
            if(isset($request->quotation_id)) {
                $quotation = $this->quotationService->getSingleData($request->quotation_id);
                if(isset($request->assigned_salesman)) {
                    $salesman = $this->userService->getUserData($request->assigned_salesman)['user'];
                    $quotationArray = [
                        'assigned_salesman' => $request->assigned_salesman,
                        'budget_reference' => formatBudgetReference(referenceCodeEdit($quotation->quote_reference,0), textSlugify($salesman->name),referenceCodeEdit($quotation->quote_reference,1), referenceCodeEdit($quotation->quote_reference,2)),
                    ];
                    if (!empty($quotation->assigned_salesman)) {
                        $quotationArray['customer_code'] = customerReferenceWithSalesman(textSlugify($salesman->name),referenceCodeEdit($quotation->customer_code,1),referenceCodeEdit($quotation->customer_code,2),referenceCodeEdit($quotation->customer_code,3));
                    } else {
                        $quotationArray['customer_code'] = customerReferenceWithSalesman(textSlugify($salesman->name),referenceCodeEdit($quotation->customer_code,0),referenceCodeEdit($quotation->customer_code,1),referenceCodeEdit($quotation->customer_code,2));
                    }
                    if(empty($quotation->assignee_sales_admin)) {
                        $quotationArray['assignee_sales_admin'] = Auth::id();
                    }
                    if ($quotation->status < STATUS_QUOTATION_SALESMAN_ASSIGNED) {
                        $quotationArray['status'] = STATUS_QUOTATION_SALESMAN_ASSIGNED;
                    }
                    $this->quotationService->update($request->quotation_id, $quotationArray);
                    $this->repo->updateModel($quotation->customer_id,['salesman_id'=>$request->assigned_salesman]);
                }
                $quotation = $this->quotationService->getSingleData($request->quotation_id);
                DB::commit();
                return sendResponse($quotation,__("Salesman assigned successfully."));
            }

            DB::rollBack();
            return sendResponseError('',__("Operation failed."));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('',$e->getMessage());
        }
    }

    public function updateCustomer($id, $request)
    {
        DB::beginTransaction();
        try {
            $requestArray = [
                'name' => $request->name,
                'contact_name' => $request->contact_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'crm_payment_condition_id' => $request->crm_payment_condition_id,
                'address' => $request->address,
                'postcode' => $request->postcode,
                'locality' => $request->locality,
                'country' => $request->country,
                'tin_number' => $request->tin_number,
                'tax_address' => $request->tax_address,
                'fiscal_postcode' => $request->fiscal_postcode,
                'tax_location' => $request->tax_location,
                'tax_country' => $request->tax_country,
                'status' => $request->status,
                'salesman_id' => $request->assigned_salesman
            ];
            $update = $this->update($id, $requestArray);
            if ($update) {
                if(isset($request->quotation_id)) {
                    $quotation = $this->quotationService->getSingleData($request->quotation_id);
                    if(isset($request->assigned_salesman)) {
                        $salesman = $this->userService->getUserData($request->assigned_salesman)['user'];
                        $quotationArray = [
                            'assigned_salesman' => $request->assigned_salesman,
                            'budget_reference' => formatBudgetReference(referenceCodeEdit($quotation->quote_reference,0), textSlugify($salesman->name),referenceCodeEdit($quotation->quote_reference,1), referenceCodeEdit($quotation->quote_reference,2)),
                        ];
                        if (!empty($quotation->assigned_salesman)) {
                            $quotationArray['customer_code'] = customerReferenceWithSalesman(textSlugify($salesman->name),referenceCodeEdit($quotation->customer_code,1),referenceCodeEdit($quotation->customer_code,2),referenceCodeEdit($quotation->customer_code,3));
                        } else {
                            $quotationArray['customer_code'] = customerReferenceWithSalesman(textSlugify($salesman->name),referenceCodeEdit($quotation->customer_code,0),referenceCodeEdit($quotation->customer_code,1),referenceCodeEdit($quotation->customer_code,2));
                        }
                        if (empty($quotation->assignee_sales_admin)) {
                            $quotationArray['assignee_sales_admin'] = Auth::id();
                        }
                        if ($quotation->status < STATUS_QUOTATION_SALESMAN_ASSIGNED) {
                            $quotationArray['status'] = STATUS_QUOTATION_SALESMAN_ASSIGNED;
                        }

                        $this->quotationService->update($request->quotation_id, $quotationArray);
                    }
                }
                DB::commit();
                return sendResponse('',__("Customer updated successfully."));
            }
            DB::rollBack();
            return sendResponseError('',__("Operation failed."));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('',$e->getMessage());
        }

    }

    public function updateQuotationCustomer(){

    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? sendResponseError('',__("Operation failed.")) :
            sendResponse('',__("Customer has been updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('',$e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $response = $this->repo->destroy($id);
            if ($response){
                return jsonResponse(TRUE)->message(__('Customer deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Customer delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }

    public function getCustomerProfile($id){
        try {

            $customer = $this->repo->getSingleRowData(['id'=>$id],['name','contact_name','email','phone','address',
                                                                           'postcode','locality','country','tin_number',
                                                                           'tax_address','fiscal_postcode','tax_location','tax_country',
                                                                           'image','status']);
            if (!empty($customer->image)){
                $customer->image = asset(get_image_path('customers').$customer->image);
            }
            $data['customer'] = $customer;
            return jsonResponse(TRUE)->message(__('Customer get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('No customer data found'));
        }
    }

    public function updateCustomerProfile(Request $request){
        try {
            $requestArray = $request->except('image');
            if (isset($request->image) && $request->hasFile('image')) {
                if (!empty(Auth::user()->image)){
                    deleteOnlyImage(Auth::user()->image,get_image_path('customers'));
                }
                $requestArray['image'] = uploadImage($request->image,get_image_path('customers'));
            }
            $response = $this->repo->updateModel(Auth::user()->id,$requestArray);
            return jsonResponse(TRUE)->message(__('Profile updated successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function updateCustomerPassword(array $requestArray){
        try {
            if (Hash::check($requestArray['old_password'],Auth::user()->password)){
                $update_data = [
                    'password' => bcrypt($requestArray['password'])
                ];
                $response = $this->repo->updateModel(Auth::user()->id,$update_data);
                return jsonResponse(TRUE)->message(__('Password updated successfully.'));
            }else{
                return jsonResponse(FALSE)->message(__('Incorrect old password.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
}
