<?php

namespace App\Http\Services\CRM;

use App\Http\Repositories\CRM\CrmPrintingOptionRepository;
use App\Http\Services\BaseService;

class CrmPrintingOptionService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CRM/CrmPrintingOptionRepository $repository
     */
    public function __construct(CrmPrintingOptionRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getPrintingOptionData($id){

        if ($id !== NULL){
            $data['printing_option'] = $this->repo->firstWhere(['id'=>$id]);
        }else{
            $data['printing_option'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {

        try {
            $payment_condition = $this->repo->create($requestArray);
            if ( $payment_condition) {
                return jsonResponse(true)->message(__("Printing Option has been created successfully."));
            }
            return jsonResponse(false)->message(__("Printing Option create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Printing Option has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $response = $this->repo->destroy($id);
            if ($response){
                return jsonResponse(TRUE)->message(__('Printing Option deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Printing Option delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
