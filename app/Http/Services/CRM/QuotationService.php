<?php

namespace App\Http\Services\CRM;

use App\Http\Repositories\CRM\QuotationProductRepository;
use App\Http\Repositories\CRM\QuotationRepository;
use App\Http\Repositories\Product\BrandRepository;
use App\Http\Repositories\Product\ProductRepository;
use App\Http\Services\BaseService;
use App\Models\CRM\CrmCustomer;
use App\Models\Crm\CrmQuotation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class QuotationService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CrmQuotation/QuotationRepository $repository
     */
    private $quotionProductRepo, $productRepo, $brandRepo;
    public function __construct(QuotationRepository $repository,
                                QuotationProductRepository $quotionProductRepo,
                                ProductRepository $productRepo,
                                BrandRepository $brandRepo
    )
    {
        $this->repo = $repository;
        $this->quotionProductRepo = $quotionProductRepo;
        $this->productRepo = $productRepo;
        $this->brandRepo = $brandRepo;
    }

    // Your methods for repository

    public function getDashboardData($brand_id = []){
        return $this->repo->getDashboardData($brand_id);
    }

    public function getAllQuotationList($search_array = []) {
        return $this->repo->getAllQuotationList($search_array);
    }

    public function getSingleData($id){

        if ($id !== NULL){
            $data = $data = $this->repo->firstWhere(['id' => $id]);
        }else{
            $data = [];
        }
        return $data;
    }

    public function getQuotationData($search_array=[]) {
        return $this->repo->getQuotationData($search_array);
    }

    public function create($request) {

        try {
            DB::beginTransaction();
            $customer = CrmCustomer::where(['email'=>$request->email])->first();
            if (isset($customer) && $customer->status == INACTIVE) {
                return sendResponseError('',__("This customer is inactive.Please contact with admin."));
            }
            $brand = $this->brandRepo->firstWhere(['slug' => $request->brand_slug]);
            $data = [
                'brand_id' => $brand->id,
                'quote_reference' => formatQuoteReference(textSlugify($brand->name), getNextPrimaryId('crm_quotations')),
                'need_payment' => $brand->is_reseller,
                'company_name' => $request->company_name,
                'contact_name' => $request->contact_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'comments' => $request->comments,
            ];
            if ($request->hasFile('attached_file')){
                $data['attached_file'] = uploadFile($request->attached_file,get_image_path('quotation'));
            }
            $quotation = $this->repo->create($data);
            if ( $quotation) {

                if (isset($customer) && !empty($customer->salesman_id)) {
                    $this->assignedSalesMan($quotation,$customer);
                }
                $this->createCrmProducts($request,$quotation);
                DB::commit();
                return sendResponse($quotation,__("Your quotation request submitted successfully."));
            }
            return sendResponseError('',__("Operation failed."));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('',$e->getMessage());
        }
    }

    private function assignedSalesMan($quotation,$customer){
        $salesman = User::where(['id'=>$customer->salesman_id,'status'=>STATUS_ACTIVE])->first();
        if (isset($salesman)) {
            $quotationArray = [
                'customer_id' => $customer->id,
                'assigned_salesman' => $customer->salesman_id,
                'budget_reference' => formatBudgetReference(referenceCodeEdit($quotation->quote_reference,0), textSlugify($salesman->name),referenceCodeEdit($quotation->quote_reference,1), referenceCodeEdit($quotation->quote_reference,2)),
                'customer_code' => customerReferenceWithSalesman(textSlugify($salesman->name),referenceCodeEdit($customer->customer_code,0),referenceCodeEdit($customer->customer_code,1),referenceCodeEdit($customer->customer_code,2)),
                'assignee_sales_admin' => Auth::id(),
                'status' => STATUS_QUOTATION_SALESMAN_ASSIGNED
            ];
            $this->repo->updateModel($quotation->id, $quotationArray);
        }
    }

    private function createCrmProducts($request,$quotation){
        $product_id = !empty($request->product_id) ? explode(',',$request->product_id) : [];
        $combination_id = !empty($request->combination_id) ? explode(',',$request->combination_id) : [];
        $combination_type_id = !empty($request->combination_type_id) ? explode(',',$request->combination_type_id) : [];
        $product_comment = !empty($request->product_comment) ? explode(',',$request->product_comment) : [];
        $quantity = !empty($request->quantity) ? explode(',',$request->quantity) : [];

        if(!empty($product_id[0])) {
            foreach ($product_id as $key => $value){
                $product = $this->productRepo->firstWhere(['id' => $value]);
                $unit_price = get_product_unit_price($value, $quantity[$key]);
                $input = [
                    'quotation_id' => $quotation->id,
                    'is_shipped_item' => 1,
                    'product_id' => $value,
                    'combination_id' => $combination_id[$key],
                    'combination_type_id' => $combination_type_id[$key],
                    'comments' => isset($product_comment[$key]) ? $product_comment[$key] : '',
                    'quantity' =>   (int)$quantity[$key],
                    'product_name' => $product->name,
                    'product_reference' => $product->reference,
                    'unit_price' => $unit_price,
                    'total_price' => $unit_price * (int)$quantity[$key],
                    'vat' => VAT_AMOUNT,
                    'total_vat' => (VAT_AMOUNT * ($unit_price * (int)$quantity[$key]))/100,
                ];
                $this->quotionProductRepo->create($input);
            }
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? sendResponseError('',__("Operation failed.")) :
                sendResponse('',__("Your quotation updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('',$e->getMessage());
        }
    }

    private function imageData($image,$id=NULL){
        if ($id !== NULL){
            $details =  $this->getSingleData($id);
            return uploadImage($image,get_image_path('quotation'),$details->icon ?? '');
        }else{
            return uploadImage($image,get_image_path('quotation'));
        }
    }

    public function delete($id){
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier){
                return jsonResponse(TRUE)->message(__('Brand deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Brand delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
