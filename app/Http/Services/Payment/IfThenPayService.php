<?php

namespace App\Http\Services\Payment;


use Illuminate\Support\Facades\Http;

class IfThenPayService
{
    private $entity;
    private $subEntity;

    public function __construct()
    {
        $this->entity = env('IFTHENPAY_ENTITY');
        $this->subEntity = env('IFTHENPAY_SUB_ENTITY');
    }

    //HOME TREATMENT REGIONAL DEFINITIONS
    private function format_number($number)
    {
        $verifySepDecimal = number_format(99, 2);

        $valorTmp = $number;

        $sepDecimal = substr($verifySepDecimal, 2, 1);

        $hasSepDecimal = True;

        $i = (strlen($valorTmp) - 1);

        for ( ; $i != 0; $i -= 1) {
            if (substr($valorTmp, $i, 1) == "." || substr($valorTmp, $i, 1) == ",") {
                $hasSepDecimal = True;
                $valorTmp = trim(substr($valorTmp, 0, $i)) . "@" . trim(substr($valorTmp, 1 + $i));
                break;
            }
        }

        if ($hasSepDecimal != True) {
            $valorTmp = number_format($valorTmp, 2);

            $i = (strlen($valorTmp) - 1);

            for ( ; $i != 1; $i--) {
                if (substr($valorTmp, $i, 1) == "." || substr($valorTmp, $i, 1) == ",") {
                    $hasSepDecimal = True;
                    $valorTmp = trim(substr($valorTmp, 0, $i)) . "@" . trim(substr($valorTmp, 1 + $i));
                    break;
                }
            }
        }

        for ($i = 1; $i != (strlen($valorTmp) - 1); $i++) {
            if (substr($valorTmp, $i, 1) == "." || substr($valorTmp, $i, 1) == "," || substr($valorTmp, $i, 1) == " ") {
                $valorTmp = trim(substr($valorTmp, 0, $i)) . trim(substr($valorTmp, 1 + $i));
                break;
            }
        }

        if (strlen(strstr($valorTmp, '@')) > 0) {
            $valorTmp = trim(substr($valorTmp, 0, strpos($valorTmp, '@'))) . trim($sepDecimal) . trim(substr($valorTmp, strpos($valorTmp, '@') + 1));
        }

        return $valorTmp;
    }
    //END TREATMENT REGIONAL DEFINITIONS


    //HOME REF MULTIBANCO
    public function generateMbRef($order_id, $amount)
    {
        $chk_val = 0;

        $order_id = "0000" . $order_id;

        if (strlen($this->entity) < 5) {
            return ['success'=>false, 'message'=> __('Sorry, you must provide a valid entity')];

        } else if (strlen($this->entity) > 5) {
            return ['success'=>false, 'message'=> __('Sorry, you must provide a valid entity')];
        }
        if (strlen($this->subEntity) == 0) {
            return ['success'=>false, 'message'=>__('Sorry, you must provide a valid sub-entity')];
        }

        $amount = sprintf("%01.2f", $amount);

        $amount = $this->format_number($amount);

        if ($amount < 1) {
            return ['success'=>false, 'message'=> __('We are sorry but it is impossible to generate an MB reference for values below 1 Euro')];
        }

        $notice = INACTIVE;
        if ($amount >= 1000000) {
            //'Fractional payment for exceeding the limit value for payments in the Multibanco system'
            $notice = ACTIVE;
        }

        if (strlen($this->subEntity) == 1) {
            //Only the 6 characters to the right of order_id are considered
            $order_id = substr($order_id, (strlen($order_id) - 6), strlen($order_id));
            $chk_str = sprintf('%05u%01u%06u%08u', $this->entity, $this->subEntity, $order_id, round($amount * 100));
        } else if (strlen($this->subEntity) == 2) {
            //Only the 5 characters to the right of order_id are considered
            $order_id = substr($order_id, (strlen($order_id) - 5), strlen($order_id));
            $chk_str = sprintf('%05u%02u%05u%08u', $this->entity, $this->subEntity, $order_id, round($amount * 100));
        } else {
            //Only the 4 characters to the right of order_id are considered
            $order_id = substr($order_id, (strlen($order_id) - 4), strlen($order_id));
            $chk_str = sprintf('%05u%03u%04u%08u', $this->entity, $this->subEntity, $order_id, round($amount * 100));
        }

        //check digits calculation

        $chk_array = array(3, 30, 9, 90, 27, 76, 81, 34, 49, 5, 50, 15, 53, 45, 62, 38, 89, 17, 73, 51);

        for ($i = 0; $i < 20; $i++) {
            $chk_int = substr($chk_str, 19 - $i, 1);
            $chk_val += ($chk_int % 10) * $chk_array[$i];
        }

        $chk_val %= 97;

        $chk_digits = sprintf('%02u', 98 - $chk_val);

        return [
            'success' => true,
            'notice' => $notice,
            'entity' => $this->entity,
            'sub_entity' => $this->subEntity,
            'reference' => substr($chk_str, 5, 3) . " " . substr($chk_str, 8, 3) .
                " " . substr($chk_str, 11, 1) . $chk_digits,
            'amount' => number_format($amount, 2, ',', ' ')
        ];
    }

    public function getPaymentsJsonV2($keyBackoffice, $entity, $subEntity, $reference = '',  $amount = '') {
        $url = 'https://www.ifthenpay.com/IfmbWS/WsIfmb.asmx/GetPaymentsJsonV2?chavebackoffice='.$keyBackoffice.
            '&entidade='.$entity.'&subentidade='.$subEntity.'&dtHrInicio=&dtHrFim=&referencia='.$reference.'&valor='.$amount.
            '&sandbox='.env('IFTHENPAY_ENV');
        return Http::get($url);
    }
}
