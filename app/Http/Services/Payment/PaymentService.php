<?php

namespace App\Http\Services\Payment;

use App\Http\Repositories\CRM\QuotationRepository;
use App\Http\Repositories\IfthenpayRecordRepository;
use App\Http\Services\BaseService;
use App\Http\Services\LoggerService;
use Illuminate\Support\Facades\DB;


class PaymentService extends BaseService
{
    private $ifThenPayService, $quotationRepository, $ifthenpayRecordRepository, $logger;

    public function __construct(IfThenPayService $ifThenPayService, QuotationRepository $quotationRepository,
                                IfthenpayRecordRepository $ifthenpayRecordRepository)
    {
        $this->logger = new LoggerService(storage_logs().'ifthenpay_callback.log');
        $this->ifThenPayService = $ifThenPayService;
        $this->quotationRepository = $quotationRepository;
        $this->ifthenpayRecordRepository = $ifthenpayRecordRepository;
    }

    public function generateReference($request)
    {
        try {
            $quotation_id = decrypt($request->quotation_id);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => __('Link expired.')];
        }
        //$quotation_id = $request->quotation_id;
        $condition = array(
            'id' => $quotation_id,
            'status' => STATUS_BUDGET_APPROVED,
            'payment_status' => ['!=', PAYMENT_PAID]
        );
        $quotation = $this->quotationRepository->getSingleRowData($condition);
        if (!$quotation) return ['success' => false, 'message' => __('Link expired.')];

        $payRecord = $this->ifthenpayRecordRepository->firstWhere(['crm_quotation_id' => $quotation_id]);
        if ($payRecord) return ['success' => true, 'pay_record' => $payRecord];

        $totalAmount = $quotation->grand_total;
        /*foreach ($quotation->quotation_products as $product) {
            if ($product->status == ACTIVE) {
                $totalAmount += $product->total_price + $product->total_vat;
            }
        }*/
        $orderId = randomString(4, TRUE);
        while (true) {
            $record = $this->ifthenpayRecordRepository->firstWhere(['order_id' => $orderId]);
            if (!$record) break;
            $orderId = randomString(4, TRUE);
        }
        $response = $this->ifThenPayService->generateMbRef($orderId, $totalAmount);
        if ($response['success']) {
            try {
                DB::beginTransaction();
                $payRecord = $this->ifthenpayRecordRepository->create([
                    'crm_quotation_id' => $quotation_id,
                    'order_id' => $orderId,
                    'entity' => $response['entity'],
                    'sub_entity' => $response['sub_entity'],
                    'showable_reference' => $response['reference'],
                    'reference' => str_replace(' ', '', $response['reference']),
                    'notice' => $response['notice'],
                    'amount' => $totalAmount,
                    'key_backoffice' => env('IFTHENPAY_KEY_BACK_OFFICE'),
                    'budget_reference'=> $quotation->budget_reference
                ]);
                $this->quotationRepository->updateWhere(['id' => $quotation_id], ['payment_status' => REFERENCE_GENERATED, 'mb_reference'=>$payRecord->reference]);
                DB::commit();
                return ['success' => true, 'pay_record' => $payRecord];
            } catch (\Exception $e) {
                DB::rollBack();
                return ['success' => false, 'message' => __('Something went wrong.')];
            }
        } else {
            return $response;
        }
    }

    public function ifthenpayCallback($request)
    {
        $this->logger->log('--','--');
        $this->logger->log('--','--');
        $this->logger->log('callback-request-body', json_encode($request->all()));
        $this->logger->log('request-chave', $request->chave);
        $this->logger->log('env-chave', env('IFTHENPAY_CALLBACK_KEY'));
        $this->logger->log('ifthenpay-env(1 for sandbox, 0 for production)', env('IFTHENPAY_ENV'));
        if(env('IFTHENPAY_CALLBACK_KEY') != $request->chave) {
            return ['success'=>false, 'message'=>__('Callback key doesn\'t match.')];
        }
        $payRecord = $this->ifthenpayRecordRepository->firstWhere(['reference'=>$request->referencia, 'callback_status'=>NOT_CALLED_YET]);
        if(!$payRecord) {
            return ['success'=>false, 'message'=>__('Invalid Reference number.')];
        }
        $this->logger->log('ifthenpay-table-recod', json_encode($payRecord));

        $response = $this->ifThenPayService->getPaymentsJsonV2($payRecord->key_backoffice, $payRecord->entity, $payRecord->sub_entity, $payRecord->reference);
        $this->logger->log('getPayment-response-body', $response->body());
        $payments = $response->json();

        try {
            if($payments[0]['MensagemErro']) throw new \Exception($payments[0]['MensagemErro']);
            $amount = 0;
            foreach ($payments as $payment) {
                $amount += floatval(str_replace(',','.', $payment['Valor'], ));
            }
            if($amount < $payRecord->amount) {
                return ['success'=>false, 'amount_paid'=>$amount, 'amount_from_db'=>$payRecord->amount, 'message'=>__('Amount doesn\'t match.')];
            }
            DB::beginTransaction();
            $this->quotationRepository->updateWhere(['id'=> $payRecord->crm_quotation_id], ['payment_status'=> PAYMENT_PAID]);
            $this->ifthenpayRecordRepository->updateWhere(['crm_quotation_id'=>$payRecord->crm_quotation_id], ['callback_status'=>CALLED]);
            DB::commit();
            return ['success'=>true, 'reference'=>$payRecord->reference];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['success'=>false, 'message'=>__('Something went wrong. ').$e->getMessage()];
        }
    }

}
