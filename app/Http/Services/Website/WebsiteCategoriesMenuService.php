<?php

namespace App\Http\Services\Website;

use App\Http\Repositories\Product\CategoryRepository;
use App\Http\Repositories\Website\WebsiteCategoriesMenuRepository;
use App\Http\Services\BaseService;


class WebsiteCategoriesMenuService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Website/WebsiteCategoriesRepository $repository
     */

    private $category_repository;

    public function __construct(WebsiteCategoriesMenuRepository $repository, CategoryRepository $category_repository)
    {
        $this->repo = $repository;
        $this->category_repository = $category_repository;
    }

    // Your methods for repository

    public function getCategoryImageAddModalData(array $requestArray){
        $data['category'] = $this->category_repository->firstWhere(['id'=>$requestArray['category_id']]);
        $data['brand_id'] = $requestArray['brand_id'];
        if (isset($requestArray['id'])) {
            $data['category_image'] = $this->repo->firstWhere(['id'=>$requestArray['id']]);
        }
        return view('admin.website_manager.categories_menu.add_image_modal',$data);
    }

    public function create(array $input) {
        try {
            if (isset($input['image']) && !empty($input['image'])) {
                $input['image'] = uploadImage($input['image'],get_image_path('category'));
            }
            $response = $this->repo->create($input);
            if ($response) {
                return jsonResponse(true)->message(__("Category image has been added successfully."));
            }else{
                return jsonResponse(FALSE)->message(__("Category image added failed."));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function update(int $id, array $input) {
        try {
            if (isset($input['image']) && !empty($input['image'])) {
                $category_image = $this->repo->firstWhere(['id'=>$id]);
                deleteImageFile(get_image_path('category'),$category_image->image);
                $input['image'] = uploadImage($input['image'],get_image_path('category'));
            }
            $response = $this->repo->updateModel($id,$input);
            if ($response) {
                return jsonResponse(true)->message(__("Category image has been updated successfully."));
            }else{
                return jsonResponse(FALSE)->message(__("Category image update failed."));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function delete($id){
        try {
            $response = $this->repo->destroy($id);
            if ($response) {
                return jsonResponse(true)->message(__("Category image has been deleted successfully."));
            }else{
                return jsonResponse(FALSE)->message(__("Category image delete failed."));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

}
