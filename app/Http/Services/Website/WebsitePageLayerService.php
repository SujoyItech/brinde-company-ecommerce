<?php

namespace App\Http\Services\Website;

use App\Http\Repositories\Website\WebsitePageLayerDetailsRepository;
use App\Http\Repositories\Website\WebsitePageLayerRepository;
use App\Http\Services\BaseService;
use App\Models\Website\WebsitePageLayerDetails;
use http\Env\Request;

class WebsitePageLayerService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Website/WebsitePageLayerRepository $repository
     */
    private $layer_details_repository;

    public function __construct(WebsitePageLayerRepository $repository,WebsitePageLayerDetailsRepository $layer_details_repository)
    {
        $this->repo = $repository;
        $this->layer_details_repository = $layer_details_repository;
    }

    public function edit(array $requestArray){
        $data = [];
        if (isset($requestArray['id'])) {
            $data['page_layer'] = $this->repo->firstWhere(['id'=>$requestArray['id']]);
        }
        $data['website_pages_id'] = $requestArray['website_pages_id'];
        return view('admin.website_manager.layers.create_layer',$data);
    }

    public function create(array $requestArray) {
        try {
            $requestArray['order'] = 0 ;
            $response = $this->repo->create($requestArray);
            if ( $response) {
                return jsonResponse(true)->message(__("Web Page Layer has been created successfully."));
            }
            return jsonResponse(false)->message(__("Web Page Layer create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function update($id,array $requestArray) {
        try {
            $response = $this->repo->updateModel($id,$requestArray);
            if ( $response) {
                return jsonResponse(true)->message(__("Web Page Layer has been updated successfully."));
            }
            return jsonResponse(false)->message(__("Web Page Layer update failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function updateOrder(array $requestArray) {
        if (!empty($requestArray['data'])){
            $requestArray['data'] = json_decode($requestArray['data']);
        }else{
            $requestArray['data'] = [];
        }
        try {
            $response = $this->repo->updateOrder($requestArray);
            if ( $response) {
                return jsonResponse(true)->message(__("Web Page order updated successfully."));
            }
            return jsonResponse(false)->message(__("Web Page order update failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function websitePageLayerStatusChange(array $requestArray){
        $data = [];
        if ($requestArray['status'] == STATUS_ACTIVE){
            $data['status'] = INACTIVE;
            $status = __('Inactive');
        }else{
            $data['status'] = STATUS_ACTIVE;
            $status = __('Active');
        }
        try {
            $response = $this->repo->updateModel($requestArray['id'],$data);
            if ( $response) {
                return jsonResponse(true)->message(__("Web Page layer ".$status." successfully."));
            }
            return jsonResponse(false)->message(__("Web Page layer ".$status." failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function websitePageLayerDelete(array $requestArray){
        $data = [];
        try {
            $response = $this->repo->destroy($requestArray['id']);
            if ( $response) {
                return jsonResponse(true)->message(__("Web Page layer deleted successfully."));
            }
            return jsonResponse(false)->message(__("Web Page layer deleted failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }


}
