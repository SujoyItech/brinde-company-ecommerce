<?php

namespace App\Http\Services\Website;

use App\Http\Repositories\Api\Product\ProductRepository;
use App\Http\Repositories\Product\BrandRepository;
use App\Http\Repositories\Website\WebsitePageRepository;
use App\Http\Services\BaseService;
use App\Models\Product\Category;
use Illuminate\Support\Facades\Storage;

class WebsitePageService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Website/WebsitePageRepository $repository
     */
    private $brand_repository,$product_api_repository;

    public function __construct(WebsitePageRepository $repository,BrandRepository $brand_repository,ProductRepository $product_api_repository)
    {
        $this->repo = $repository;
        $this->brand_repository = $brand_repository;
        $this->product_api_repository = $product_api_repository;
    }

    public function getWebPageList($brand_slug){
        $brand_id = $this->brand_repository->getIdBySlug($brand_slug);
        $data['website_pages'] = $this->repo->getWebPageList($brand_id);
        return jsonResponse()->data($data)->message(__('Website pages Fetched Successful'));
    }

    public function getWebPageContent($page_slug=NULL, $page_type=NULL,$brand_id = NULL){
        $data = array();
        $contents = $this->repo->getWebPageContent($page_slug,$page_type,$brand_id);
        $categories = Category::where(['status'=>STATUS_ACTIVE])->get();
        $data = [];
        foreach ($contents as $content){
            $data['page_info']['id'] = $content->id;
            $data['page_info']['url'] = $content->url;
            $data['page_info']['page_type'] = $content->page_type;
            $data['page_info']['title'] = $content->title;
            $data['page_info']['subtitle'] = $content->subtitle;
            $data['layer_info'][$content->layer_id]['layer_id'] = $content->layer_id;
            $data['layer_info'][$content->layer_id]['order'] = $content->layer_order;
            $data['layer_info'][$content->layer_id]['layer_type'] = $content->layer_type;
            $data['layer_info'][$content->layer_id]['layer_title'] = $content->layer_title;
            $data['layer_info'][$content->layer_id]['layer_subtitle'] = $content->layer_subtitle;
            $data['layer_info'][$content->layer_id]['layout_number'] = $content->layout_number;
            $data['layer_info'][$content->layer_id]['padding_x'] = $content->padding_x;
            $data['layer_info'][$content->layer_id]['padding_top'] = $content->padding_top;
            $data['layer_info'][$content->layer_id]['padding_bottom'] = $content->padding_bottom;
            if (!empty($content->layer_details_id)) {
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['layer_details_id'] = $content->layer_details_id;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['order'] = $content->layer_details_order;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['layer_details_title'] = $content->layer_details_title;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['description_title'] = $content->description_title;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['description_body'] = $content->description_body;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['category_id'] = $content->category_id;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['category_slug'] = $this->getCategorySlugById($categories,$content->category_id);
                if ($content->layer_type == 'product') {
                    $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['product_image_url'] = Storage::url('/');
                    $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['products'] = $this->getCategoryProducts($content->category_product_slugs);
                }
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['featured_image'] = !empty($content->featured_image) ? adminAsset('images/website_manager/'.$content->layer_type.'/'.$content->featured_image) : NULL;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['url_slug'] = $content->url_slug;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['url_target_type'] = $content->url_target_type;
                $data['layer_info'][$content->layer_id]['layout_content'][$content->layer_details_id]['layout_is_image'] = $content->layout_is_image;
            }else{
                $data['layer_info'][$content->layer_id]['layout_content'] = [];
            }

        }
        return jsonResponse()->data($data)->message(__('Content Fetched Successful'));
    }

    private function getCategorySlugById($categories,$id){
        $tree = generateParentTree($categories,$id);
        if (isset($tree[0])){
            $slug = $this->generateParentSlug($tree[0]);
        }else{
            $slug = '';
        }
        return $slug;
    }


    private function generateParentSlug($element,$slug=''){
        if (isset($element->parent[0])) {
            $slug = $this->generateParentSlug($element->parent[0],$slug).'/'.$element->slug;
        }else{
            $slug = $element->slug;
        }
        return $slug;
    }

    private function getCategoryProducts($product_slug){
        if (!empty($product_slug)) {
            $product_array = explode(',',$product_slug);
            $search_data['product_slug'] = $product_array;
            $products = $this->product_api_repository->getAllProductListApi($search_data);
            return $this->product_api_repository->getProductListDataArray($products);
        }else{
            return '';
        }
    }

    public function getPageQuery($band_array,$brand_id) {
        return $this->repo->getPageQuery($band_array,$brand_id);
    }

    public function getPageData($id){
        if ($id !== NULL){
            $data['page'] = $this->repo->getPageDetails($id);
        }else{
            $data['page'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {
        try {
            $response = $this->repo->create($requestArray);
            if ( $response) {
                return jsonResponse(true)->message(__("Page has been created successfully."));
            }
            return jsonResponse(false)->message(__("Page create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Page has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $response = $this->repo->destroy($id);
            if ($response){
                return jsonResponse(TRUE)->message(__('Page deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Page delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function websitePageStatusChange(array $requestArray){
        try {
            $response = $this->repo->websitePageStatusChange($requestArray);
            if ($response){
                return jsonResponse(TRUE)->message(__('Status changed successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Status changed failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

}
