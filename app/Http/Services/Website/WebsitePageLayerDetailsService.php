<?php

namespace App\Http\Services\Website;

use App\Http\Repositories\Product\CategoryRepository;
use App\Http\Repositories\Website\WebsitePageLayerDetailsRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Product\ProductService;
use App\Models\Website\WebsitePageLayerDetails;

class WebsitePageLayerDetailsService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Website/WebsitePageLayerContentRepository $repository
     */
    private $category_repository,$page_layer_service,$product_service;
    public function __construct(
        WebsitePageLayerDetailsRepository $repository,
        CategoryRepository $category_repository,
        WebsitePageLayerService $page_layer_service,
        ProductService $product_service
    )
    {
        $this->repo = $repository;
        $this->category_repository = $category_repository;
        $this->page_layer_service = $page_layer_service;
        $this->product_service = $product_service;
    }

    public function getLayerContentData($where=[]){
        return $this->repo->getLayerContentData($where);
    }

    public function getLayerContentSingleData($where=[]){
        return $this->repo->getLayerContentSingleData($where);
    }

    public function showLayerContentModal(array $requestArray){
        $data['website_pages_layers_id'] = $requestArray['website_pages_layers_id'];
        $data['type'] = $requestArray['type'];
        $data['order'] = $requestArray['order'] ?? 0;
        $layer = $this->page_layer_service->firstWhere(['id'=>$requestArray['website_pages_layers_id']]);
        $data['layer'] = $layer;
        if ($requestArray['type'] == 'category' || $requestArray['type'] == 'product'){
            $data['categories'] = $this->category_repository->getData(['status'=>STATUS_ACTIVE]);
        }
        if (isset($requestArray['id'])){
            $data['layer_content'] = $this->repo->firstWhere(['id'=>$requestArray['id']]);
            if ($requestArray['type'] == 'product') {
                $data['layer_content']['products_by_category'] = isset($data['layer_content']) && !empty($data['layer_content']->category_product_slugs) ?
                    $this->product_service->getDataWhereIn('slug',explode(',',$data['layer_content']->category_product_slugs)) : [];
            }
        }
        return view('admin.website_manager.layers.layer_forms.'.$requestArray['type'],$data);
    }

    public function createLayerContent(array $requestArray,$type) {
        try {
            if (isset($requestArray['category_product_slugs'])){
                $requestArray['category_product_slugs'] = implode(',',$requestArray['category_product_slugs']);
            }
            $insert_data = $this->prepareLayerContentInsertData($requestArray,$type);
            $response = $this->repo->create($insert_data);
            if ( $response) {
                $update_data = [];
                $update_data['id'] = $response->id;
                if (isset($requestArray['featured_image'])){
                    $update_data['featured_image'] = $requestArray['featured_image'];
                }
                if (isset($requestArray['layout_is_image'])){
                    $update_data['layout_is_image'] = $requestArray['layout_is_image'];
                }
                $this->updateLayerContent($response->id,$update_data,$type);
                return jsonResponse(true)->message(__("Layer content has been created successfully."));
            }
            return jsonResponse(false)->message(__("Layer content create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function updateLayerContent(int $id, array $requestArray,$type) {
        try {
            $requestArray['id'] = $id;
            if (isset($requestArray['category_product_slugs'])){
                $requestArray['category_product_slugs'] = implode(',',$requestArray['category_product_slugs']);
            }
            $update_data = $this->prepareLayerContentInsertData($requestArray,$type);
            $response = $this->repo->updateModel($id, $update_data);
            return jsonResponse(true)->message(__("Layer content has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    private function prepareLayerContentInsertData(array $requestArray,$type){
        if (!empty($requestArray['id'])){
            $layer_content = $this->repo->firstWhere(['id'=>$requestArray['id']]);
            if (isset($requestArray['featured_image']) && !empty($requestArray['featured_image'])){
                $image_path = 'admin/images/website_manager/'.$type.'/';
                if (!empty($layer_content->featured_image)){
                    deleteOnlyImage(public_path($image_path),$layer_content->featured_image);
                }
                $requestArray['featured_image'] = uploadImage($requestArray['featured_image'],$image_path,$layer_content->id);
            }
            if (isset($requestArray['layout_is_image'])){
                $requestArray['layout_is_image'] = 1;
            }else{
                $requestArray['layout_is_image'] = 0;
            }

        }else{
            $requestArray['featured_image'] = '';
        }
        return $requestArray;
    }


    public function layerContentDelete(array $requestArray){
        try {
            $layer_content = $this->repo->layerDeatailsWithLayer($requestArray['id']);
            $response = $this->repo->destroy($requestArray['id']);
            if ( $response) {
                $image_path = 'admin/images/website_manager/'.$layer_content->layer_type.'/';
                if (!empty($layer_content->featured_image)){
                    deleteOnlyImage(public_path($image_path),$layer_content->featured_image);
                }
                if ($layer_content->layer_type == 'banner') {
                    $this->page_layer_service->update($layer_content->layer_id,['status'=>INACTIVE]);
                }
                return jsonResponse(true)->message(__("Web Page Layer content has been deleted successfully."));
            }
            return jsonResponse(false)->message(__("Web Page Layer content delete failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function getCategorySlugById($id){
        try {
            $categories = $this->category_repository->getData(['status'=>STATUS_ACTIVE]);
            $tree = generateParentTree($categories,$id);
            $slug = $this->generateParentSlug($tree[0]);
            $data['slug']= $slug;
            return jsonResponse(true)->message(__("Slug get successfully."))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(true)->message(__('Slug get failed'));
        }
    }


    private function generateParentSlug($element,$slug=''){
        if (isset($element->parent[0])) {
            $slug = $this->generateParentSlug($element->parent[0],$slug).'/'.$element->slug;
        }else{
            $slug = $element->slug;
        }
        return $slug;
    }



}
