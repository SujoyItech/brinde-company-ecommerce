<?php


namespace App\Http\Services\RoleManagementServices;


use App\Http\Boilerplate\CustomResponse;
use App\Http\Repositories\RoleManagementRepositories\RolePermissionRepository;
use App\Http\Services\BaseService;
use Illuminate\Support\Facades\DB;

class RolePermissionService extends BaseService {

    /**
     * RolePermissionService constructor.
     *
     * @param RolePermissionRepository $rolePermissionRepository
     */
    public function __construct(RolePermissionRepository $rolePermissionRepository) {
        $this->repo = $rolePermissionRepository;
    }

    /**
     * @param array $requestArray
     *
     * @return array
     */
    protected function _prepareDataForRolePermission(array $requestArray) : array {
        $rolePermissionData = [];
        foreach ($requestArray['permission_ids'] as $permissionId) {
            $data = [
                'permission_id' => $permissionId,
                'role_id' => $requestArray['role_id'],
            ];
            array_push($rolePermissionData, $data);
        }

        return $rolePermissionData;
    }

    /**
     * @param array $requestArray
     *
     * @return CustomResponse|string
     */
    public function updateRolePermission(array $requestArray) {
        try {
            $data = $this->_prepareDataForRolePermission($requestArray);
            DB::beginTransaction();
            $deleteResponse = $this->repo->deleteWhere(['role_id' => $requestArray['role_id']]);
            $createResponse = $this->repo->insert($data);
            if (is_null($createResponse)) {
                throw new \Exception(__('Something went wrong'));
            }
            DB::commit();

            return jsonResponse(true)->message(__('Role permission has been updated successfully'));

        } catch (\Exception $e) {
            DB::rollBack();

            return jsonResponse(false)->default();
        }
    }
}
