<?php


namespace App\Http\Services\RoleManagementServices;


use App\Http\Boilerplate\CustomResponse;
use App\Http\Repositories\RoleManagementRepositories\PermissionRepository;
use App\Http\Services\BaseService;

class PermissionService extends BaseService {

    /**
     * PermissionService constructor.
     *
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository) {
        $this->repo = $permissionRepository;
    }

    /**
     * @return CustomResponse|string
     */
    public function permissionWithRole() {
        try {
            $response = $this->repo->permissionWithRole();
            return !is_null($response) ?
                jsonResponse(true)->message(__('Successful'))->data(['data' => $response]) :
                jsonResponse(false)->default()->data(["data" => []]);
        } catch (\Exception $e) {
            return jsonResponse(false)->default()->data(["data" => []]);
        }
    }
}
