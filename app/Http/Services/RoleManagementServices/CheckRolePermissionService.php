<?php


namespace App\Http\Services\RoleManagementServices;


use App\Http\Services\BaseService;

class CheckRolePermissionService extends BaseService {

    /**
     * @var RolePermissionService
     */
    protected $rolePermissionService;
    /**
     * @var PermissionService
     */
    protected $permissionService;

    /**
     * CheckRolePermissionService constructor.
     *
     * @param RolePermissionService $rolePermissionService
     * @param PermissionService $permissionService
     */
    public function __construct(RolePermissionService $rolePermissionService, PermissionService $permissionService) {
        $this->rolePermissionService = $rolePermissionService;
        $this->permissionService = $permissionService;
    }

    /**
     * @param int $roleId
     * @param string $routeName
     *
     * @return bool
     */
    public function checkIfRoleHasPermission(int $roleId, string $routeName) {
        $permission = $this->permissionService->firstWhere(['route_name' => $routeName]);
        $roleHasPermission = $this->rolePermissionService->firstWhere(['role_id' => $roleId, 'permission_id' => $permission->id]);
        return is_null($roleHasPermission) ? false : true;
    }
}
