<?php

namespace App\Http\Services;

use App\Http\Repositories\SettingRepository;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Setting;
use App\Models\Team;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class SettingService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param SettingRepository $repository
     */
    public function __construct(SettingRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function adminSettingsSave(array $requestArray){
        try {
            $setting = FALSE;
            if ($requestArray['option_type'] == 'text'){
                $setting = $this->insert_or_update($requestArray['option_group'],$requestArray['option_key'],$requestArray['option_value']);
            }elseif($requestArray['option_type'] == 'file'){
                $check_logo = Setting::where(['option_key'=>$requestArray['option_key'],'option_group'=>$requestArray['option_group']])->first();
                $option_value = uploadImage($requestArray['option_value'],get_image_path('settings'),$check_logo->option_value ?? '');
                $setting = $this->insert_or_update($requestArray['option_group'],$requestArray['option_key'],$option_value);
            }
            if ($setting){
                return jsonResponse(true)->message(__("Setting saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Setting saved failed."));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__("Setting saved failed."));
        }

    }

    private function insert_or_update($option_group,$option_key,$option_value){
        return $this->repo->updateOrCreate(['option_group'=>$option_group,'option_key'=>$option_key], ['option_group'=>$option_group,'option_key'=>$option_key,'option_value'=>$option_value]);
    }

    public function runCommand($requestArray){
        try {
            Artisan::call($requestArray['type']);
            $message = Artisan::output();
            return jsonResponse(true)->message($message);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }

    }

    public function createCommand($requestArray){
        try {
            Artisan::call($requestArray['type']);
            $message = Artisan::output();
            return jsonResponse(true)->message($message);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }

    }

    public function updateFaqs($id,array $requestArray){
        try {
            Faq::where('id',$id)->update($requestArray);
            return jsonResponse(TRUE)->message(__('Faqs updated successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Faqs update failed.'));
        }

    }

    public function createFaqs(array $requestArray){
        try {
            Faq::create($requestArray);
            return jsonResponse(TRUE)->message(__('Faqs created successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Faqs create failed.'));
        }
    }

    public function deleteFaqs($id){
        try {
            Faq::where('id',$id)->delete();
            return jsonResponse(TRUE)->message(__('Faqs deleted successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Faqs delete failed.'));
        }
    }

    public function updateTeams($id,array $requestArray){
        try {
            Team::where('id',$id)->update($requestArray);
            return jsonResponse(TRUE)->message(__('Teams updated successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Teams update failed.'));
        }

    }

    public function createTeams(array $requestArray){
        try {
            Team::create($requestArray);
            return jsonResponse(TRUE)->message(__('Teams created successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Teams create failed.'));
        }
    }

    public function deleteTeams($id){
        try {
            Team::where('id',$id)->delete();
            return jsonResponse(TRUE)->message(__('Teams deleted successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Teams delete failed.'));
        }
    }

    public function updateContact($id,array $requestArray){
        try {
            Contact::where('id',$id)->update($requestArray);
            return jsonResponse(TRUE)->message(__('Contact updated successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Contact update failed.'));
        }

    }

    public function createContact(array $requestArray){
        try {
            Contact::create($requestArray);
            return jsonResponse(TRUE)->message(__('Contact created successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Contact create failed.'));
        }
    }

    public function deleteContact($id){
        try {
            Contact::where('id',$id)->delete();
            return jsonResponse(TRUE)->message(__('Contact deleted successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message(__('Contact delete failed.'));
        }
    }

    public function order(array $requestArray){
        try {
            $column = 'order';
            if (!empty($requestArray['column']))
                $column = $requestArray['column'];

            $order = array_filter(explode(',', $requestArray['ids']));

            if (is_array($order)) {
                foreach ($order as $key => $item) {
                    DB::table($requestArray['table'])->where(['id' => $item])->update([$column => $key]);
                }
            }
            return jsonResponse(TRUE)->message(__('Order updated successfully.'));
        } catch (\Exception $exception) {
            return jsonResponse(TRUE)->default();
        }
    }

}
