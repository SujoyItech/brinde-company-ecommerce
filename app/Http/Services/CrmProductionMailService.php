<?php

namespace App\Http\Services;


use App\Jobs\CrmDailyMailJob;
use App\Models\CRM\CrmQuotation;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CrmProductionMailService extends BaseService
{

    // Your methods for repository

    public static function newQuotation(){
        $new_quotations = CrmQuotation::select('brands.name as brand_name',DB::raw("COUNT(*) as new_quotation"))
            ->join('brands','crm_quotations.brand_id','=','brands.id')
            ->where('crm_quotations.status','=',STATUS_QUOTATION_NEW)
            ->groupBy('crm_quotations.brand_id')
            ->get();
        $sales_admin = User::where(['status'=>ACTIVE,'role'=>ROLE_SALES_MANAGER])->get();

        if (isset($new_quotations)) {
            $mail_body = [];
            foreach ($new_quotations as $quotation){
                foreach ($sales_admin as $admin){
                    $mail_body['user_name'] = $admin->name;
                    $mail_body['title'] = $quotation->new_quotation .__(' new quotations has been created for brand ').$quotation->brand_name ?? '';
                    dispatch(new CrmDailyMailJob('admin.mail.email.crm_daily_mail',$admin->email,$mail_body,__('New quotation mail form '.$quotation->brand_name ?? '')));
                }
            }
        }


    }

    public static function newlyAssignedQuotation(){
        $newly_assigned_quotation = CrmQuotation::select('users.name','users.email',DB::raw("COUNT(*) as new_assigned_quotations"))
            ->join('users','crm_quotations.assigned_salesman','=','users.id')
            ->where('crm_quotations.status',STATUS_QUOTATION_SALESMAN_ASSIGNED)
            ->groupBy('crm_quotations.assigned_salesman')
            ->get();
        if (isset($newly_assigned_quotation)) {
            foreach ($newly_assigned_quotation as $assigned_quotation){
                $mail_body['user_name'] = $assigned_quotation->name;
                $mail_body['title'] = $assigned_quotation->new_assigned_quotations .__(' new quotations has been assigned for you.');
                dispatch(new CrmDailyMailJob('admin.mail.email.crm_daily_mail',$assigned_quotation->email,$mail_body,__('New quotation assigned for you')));
            }
        }
    }

    public static function approvedBudget() {
        $approved_budgets = CrmQuotation::select('crm_quotations.budget_reference as budget_reference',
            'salesman_user.name as salesman_name','salesman_user.email as salesman_email',
            'salesadmin_user.name as salesadmin_name','salesadmin_user.email as salesadmin_email')
            ->join('users as salesman_user','crm_quotations.assigned_salesman','=','salesman_user.id')
            ->join('users as salesadmin_user','crm_quotations.assignee_sales_admin','=','salesadmin_user.id')
            ->where('crm_quotations.status',STATUS_BUDGET_APPROVED)->get();

        if (isset($approved_budgets)) {
            foreach ($approved_budgets as $approved_budget){
                $mail_body_salesman['budget_reference'] = $approved_budget->budget_reference;
                $mail_body_salesman['user_name'] = $approved_budget->salesman_name;
                $mail_body_salesman['email'] = $approved_budget->salesman_email;
                $mail_body_salesman['title'] = __('A new budget of reference no .').$approved_budget->budget_reference ?? '' .__('has been approved');
                dispatch(new CrmDailyMailJob('',$approved_budget->salesman_email,$mail_body_salesman,__('New budget approved.')));

                $mail_body_salesadmin['budget_reference'] = $approved_budget->budget_reference;
                $mail_body_salesadmin['user_name'] = $approved_budget->salesadmin_name;
                $mail_body_salesadmin['email'] = $approved_budget->salesadmin_email;
                $mail_body_salesman['title'] = __('A new budget of reference no .').$approved_budget->budget_reference ?? '' .__('has been approved');
                dispatch(new CrmDailyMailJob('',$approved_budget->salesadmin_email,$mail_body_salesadmin,__('New budget approved.')));
            }
        }
    }

    public static function newOrder(){

    }
}
