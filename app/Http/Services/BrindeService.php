<?php

namespace App\Http\Services;


use App\Jobs\CrmDailyMailJob;
use App\Models\Message;
use App\Models\Subscriber;

class BrindeService extends BaseService
{
    public function deleteSubscriber($id){
        try {
            Subscriber::where('id',$id)->delete();
            return jsonResponse(TRUE)->message(__('Subscription deleted successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function sendMailToAllSubscriber(array $requestArray){
        try {
            $subject = $requestArray['subject'];
            $data['message'] = $requestArray['message'];
            $email = Subscriber::where('status',ACTIVE)->pluck('email')->toArray();
            $email_segment = array_chunk($email,100);
            foreach ($email_segment as $collection){
                dispatch(new CrmDailyMailJob('admin.mail.email.subscriber_mail',$collection,$data,$subject));
            }
            return jsonResponse(TRUE)->message(__('Message sent successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function deleteMessage($id){
        try {
            Message::where('id',$id)->delete();
            return jsonResponse(TRUE)->message(__('Message deleted successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function replyMessageToUser(array $requestArray){
        try {
            $message = Message::where('id',$requestArray['id'])->first();
            $subject = $requestArray['subject'] ?? $message->subject;
            $email =   $message->email;
            $data['message'] = $requestArray['message'];
            dispatch(new CrmDailyMailJob('admin.mail.email.subscriber_mail',$email,$data,$subject));
            Message::where('id',$message->id)->update(['status'=>ACTIVE]);
            return jsonResponse(TRUE)->message(__('Message replied successfully.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

}
