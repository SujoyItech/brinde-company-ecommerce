<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\Product\BrandRepository;
use App\Http\Repositories\Product\CategoryRepository;
use App\Http\Repositories\Product\CombinationRepository;
use App\Http\Repositories\Product\CombinationTypeRepository;
use App\Http\Repositories\Product\DeliveryTypeRepository;
use App\Http\Repositories\Product\ProductPricingRepository;
use App\Http\Repositories\Product\SupplierRepository;
use App\Http\Repositories\Product\TagRepository;
use App\Imports\BrindeImport;
use App\Jobs\ProcessProductExcelToDatabase;
use App\Jobs\ProcessProductUpload;
use App\Models\Product\Product;
use App\Models\Product\ProductBrand;
use App\Models\Product\ProductCombination;
use App\Models\Product\ProductUpload;
use App\Models\Product\ProductUploadProcess;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Services\BaseService;
use App\Http\Repositories\Product\ProductRepository;
use App\Http\Repositories\Product\ProductTagRepository;
use App\Http\Repositories\Product\ProductBrandRepository;
use App\Http\Repositories\Product\ProductCategoryRepository;
use App\Http\Repositories\Product\ProductCombinationRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Util\Filesystem;


class ProductService extends BaseService {
    private $productBrandRepo, $productCategoryRepo, $productCombinationRepo, $productTagRepo, $combinationTypeRepository;
    private $deliveryTypeRepo, $supplierRepo, $categoryRepository, $tagRepository, $brandRepo, $productPricingRepo, $combinationRepository;

    public function __construct(
        ProductRepository $repository,
        ProductBrandRepository $productBrandRepository,
        ProductCategoryRepository $productCategoryRepository,
        ProductCombinationRepository $productCombinationRepository,
        ProductTagRepository $productTagRepository,
        DeliveryTypeRepository $deliveryTypeRepository,
        SupplierRepository $supplierRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository,
        BrandRepository $brandRepo,
        ProductPricingRepository $productPricingRepo,
        CombinationTypeRepository $combinationTypeRepository,
        CombinationRepository $combinationRepository
    ) {
        $this->repo = $repository;
        $this->productBrandRepo = $productBrandRepository;
        $this->productCategoryRepo = $productCategoryRepository;
        $this->productCombinationRepo = $productCombinationRepository;
        $this->productTagRepo = $productTagRepository;
        $this->deliveryTypeRepo = $deliveryTypeRepository;
        $this->supplierRepo = $supplierRepository;
        $this->categoryRepository = $categoryRepository;
        $this->tagRepository = $tagRepository;
        $this->brandRepo = $brandRepo;
        $this->productPricingRepo = $productPricingRepo;
        $this->combinationRepository = $combinationRepository;
        $this->combinationTypeRepository = $combinationTypeRepository;
    }

    public function getDashboardData() {
        return $this->repo->getDashboardData();
    }

    public function getProductData($request, $search_array=[]) {
        return $this->repo->getProductData($request, $search_array);
    }

    public function showProductTable($product_list) {
        return datatables($product_list)
            ->editColumn('icon', function ($item) {
                if ($item->media_type == 'internal_image') {
                    $html = '<img src="' .Storage::url($item->featured_image). '" onerror=\'this.src="' . adminAsset('images/no-image.png') . '"\' class="img-circle zoom-sm" width="60">';
                }else{
                    $html = '<img src="' .$item->featured_image. '" onerror=\'this.src="' . adminAsset('images/no-image.png') . '"\' class="img-circle zoom-sm" width="60">';
                }
                return $html;
            })
            ->editColumn('description', function ($item) {
                return substr($item->description, 0, 200);

            })
            ->editColumn('tag_names', function ($item) {
                $tags = [];
                $local = 'en';
                $all_tags = json_decode('[' . $item->tag_names . ']');
                foreach ($all_tags as $all_tag) {
                    $tags[] = '<span class="badge badge-secondary text-lg">' . $all_tag->$local . '</span>';
                }
                return implode(' ', $tags);

            })
            ->editColumn('colors', function ($item) {
                if (isset($item->colors)) {
                    $colorNameString = '';
                    $colorCodes = [];
                    $colorData = explode('***', $item->colors);
                    foreach ($colorData as $key => $data) {
                        $arr = explode('--', $data);
                        if ($key > 0) {
                            $colorNameString .= ',';
                        }
                        $colorNameString .= $arr[2];
                        $colorCodes[] = $arr[1];
                    }

                    //$local = app()->getLocale();
                    $colors = json_decode('[' . $colorNameString . ']');
                    $colorBlocks = [];
                    foreach ($colors as $key => $color) {
                        // $color_name =  . $color->$local;
                        if ($colorCodes[$key] != 'NO_COLOR') {
                            // '<span class="badge badge-secondary text-lg">'.$color->$local.'</span>';
                            $colorBlocks[] = '<span class="badge mr-1" style="background-color:' . $colorCodes[$key] . '">&nbsp;&nbsp;</span>';
                        }
                        /*else {
                            $colorBlocks[] = $color_name;
                        }*/
                    }
                    return implode(' ', $colorBlocks);
                } else {
                    return 'N/A';
                }

            })
            /*->editColumn('size', function ($item) {
                if (isset($item->size)) {
                    $sizeNameString = '';
                    $sizeData = explode('***', $item->size);
                    foreach ($sizeData as $key => $data) {
                        $arr = explode('--', $data);
                        if ($key > 0) {
                            $sizeNameString .= ',';
                        }
                        $sizeNameString .= $arr[1];
                    }

                    $local = app()->getLocale();
                    $sizes = json_decode('[' . $sizeNameString . ']');
                    $sizeBlocks = [];
                    foreach ($sizes as $key => $size) {
                        $sizeBlocks[] = '<span class="badge badge-secondary text-lg">' . $size->$local . '</span>';
                    }
                    return implode(' ', $sizeBlocks);
                } else {
                    return 'N/A';
                }
            })*/
            ->editColumn('status', function ($item) {
                $statusText = '';
                $statusClass = '';
                if ($item->status == ACTIVE) {
                    $statusText = __('Active');
                    $statusClass = 'success';
                } else if ($item->status == INACTIVE) {
                    $statusText = __('Inactive');
                    $statusClass = 'danger';
                } else if ($item->status == PRODUCT_DRAFT) {
                    $statusText = __('In draft');
                    $statusClass = 'secondary';
                }
                return '<div class="btn-group mb-2">
                                <button type="button" class="btn btn-xs btn-' . $statusClass . ' dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ' . $statusText . '&nbsp<i class="fas fa-caret-down"></i>
                                </button>
                                <div class="dropdown-menu" style="">
                                    <a class="dropdown-item status_change" data-type="' . ACTIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('Active') . '</a>
                                    <a class="dropdown-item status_change" data-type="' . INACTIVE . '" data-id="' . $item->id . '" href="javascript:">' . __('Inactive') . '</a>
                                </div>
                            </div>';
                })
            ->editColumn('action', function ($item) {
                $html = '<a href="' . route('editProduct', $item->reference) . '" class="btn btn-xs btn-outline-info waves-effect waves-light m-1 edit_item" data-id="' . $item->id . '">' . __('Add/Edit') . '</a>';
                $html .= '<button class="btn btn-xs btn-outline-warning waves-effect waves-light duplicate_item m-1" data-id="' . $item->id . '" data-name="' . $item->name . '">' . __('Duplicate') . '</button>';

                return $html;
            })

            ->rawColumns(['tag_names', 'icon', 'colors', 'size', 'status', 'action'])

            ->make(TRUE);
    }

    public function checkSlug(Request $request) {
        $slug = $this->repo->getSlug($request->slug, $request->id);
        try {
            if ($slug) {
                return jsonResponse(FALSE)->message(__('Unique and valid Slug required'));
            } else {
                return jsonResponse(TRUE)->message(__('Valid Slug'));
            }

        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function checkReference(Request $request) {
        $reference = $this->repo->getReference($request->reference, $request->id);
        try {
            if ($reference) {
                return sendResponseError('', __('Unique and valid Reference required.'));
            } else {
                return sendResponse('', __('Valid Reference.'));
            }

        } catch (\Exception $exception) {
            return sendResponseError();
        }
    }

    public function saveBrands(array $requestArray) {
        try {
            $product = $this->productBrandRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Brands has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Brands save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveCategories(array $requestArray) {
        try {
            $product = $this->productCategoryRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Categories has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Categories save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveCombinations(array $requestArray) {
        try {
            $product = $this->productCombinationRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Combinations has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Combinations save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function saveTags(array $requestArray) {
        try {
            $product = $this->productTagRepo->create($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product Tags has been saved successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product Tags save failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function statusChange(array $requestArray) {
        try {
            $product = $this->repo->changeStatus($requestArray);
            if ($product) {
                return jsonResponse(TRUE)->message(__("Product status has been changed successfully."));
            }
            return jsonResponse(FALSE)->message(__("Product status change failed."));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->default();
        }
    }

    public function initProduct($request): \Illuminate\Http\JsonResponse {
        $ref = clean_reference($request->reference);
        try {
            $product = $this->prepareDataAndCreateProduct($request);
            if (!$product) throw new \Exception();
            return sendResponse(['reference' => $product->reference], __('Product has been initiated successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('', __('Product initialization failed. ') . $e->getMessage());
        }
    }

    private function prepareDataAndCreateProduct($request, $callFromDuplicate = FALSE) {
        $ref = clean_reference($request->reference);
        $data = [
            'name'      => $request->name,
            'reference' => $ref,
            'slug'      => create_slug($request->name) . '-' . $ref,
            'status'    => PRODUCT_DRAFT
        ];
        if ($callFromDuplicate) {
            $oldProduct = $this->repo->firstWhere(['id' => $request->old_product_id]);
            if (!$oldProduct) return ['message' => __('No product found to duplicate.')];
            $data['supplier_id'] = $oldProduct->supplier_id;
            $data['delivery_type_id'] = $oldProduct->delivery_type_id;
            $data['description'] = $oldProduct->description;
            $data['additional_info'] = $oldProduct->additional_info;
            $data['meta_keywords'] = $oldProduct->meta_keywords;
            $data['meta_description'] = $oldProduct->meta_description;
            $data['meta_title'] = $oldProduct->meta_title;
        }
        return $this->repo->create($data);
    }


    /**
     * @param $request
     *
     * @return array
     */
    public function getProductEditPageData($request): array {
        $delivery_types = $this->deliveryTypeRepo->getData(['status' => STATUS_ACTIVE]);
        $suppliers = $this->supplierRepo->getData(['status' => STATUS_ACTIVE]);
        $product = $this->repo->firstWhere(['reference' => $request->reference]);
        if (!$product) {
            abort(404);
        }
        $categories = buildTree($this->categoryRepository->getData(['status' => STATUS_ACTIVE], NULL, ['sort_number' => 'asc']));
        $tags = $this->tagRepository->getData(['status' => STATUS_ACTIVE]);
        $brands = $this->brandRepo->getData(['status' => ACTIVE], NULL, ['is_reseller' => 'asc']);
        $combinationTypes = $this->combinationTypeRepository->getData();
        $combinations = $this->combinationRepository->getData();
        $productCombinations = $this->productCombinationRepo->getData(['product_id' => $product->id, 'status' => ACTIVE], NULL, ['combination_type_id' => 'desc']);
        $productCategories = $this->productCategoryRepo->getQuery(['product_id' => $product->id])->pluck('category_id')->toArray();
        return [
            'product'             => $product,
            'deliveryTypes'       => $delivery_types,
            'suppliers'           => $suppliers,
            'categories'          => $categories,
            'tags'                => $tags,
            'productCategories'   => $productCategories,
            'productTags'         => $product->product_tags,
            'supplier'            => $product->supplier,
            'brands'              => $brands,
            'pricings'            => $product->pricings,
            'combinationTypes'    => $combinationTypes,
            'combinations'        => $combinations,
            'productCombinations' => $productCombinations
        ];
    }

    public function saveProduct($request): \Illuminate\Http\JsonResponse {
        try {
            $product = $this->repo->updateWhere(['id' => $request->id], [
                'name'             => $request->name,
                'supplier_id'      => $request->supplier_id,
                'delivery_type_id' => $request->delivery_type_id,
                'description'      => $request->description,
                'additional_info'  => $request->additional_info,
                'status'           => isset($request->status) ? ACTIVE : INACTIVE
            ]);
            if (!empty($request->brand_id)) {
                $product_brands = $this->prepareProductBrandData($request->id,$request->brand_id);
                ProductBrand::where('product_id',$request->id)->delete();
                ProductBrand::insert($product_brands);
            }
            if (!$product) throw new \Exception();
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('', __('Save failed. ') . $e->getMessage());
        }
    }

    private function prepareProductBrandData($product_id,$brands){
        $product_brand = [];
        foreach ($brands as $key=>$brand_id){
            $product_brand[$key]['product_id'] = $product_id;
            $product_brand[$key]['brand_id'] = $brand_id;
        }
        return $product_brand;
    }

    public function saveProductCategoriesAndTags($request): \Illuminate\Http\JsonResponse {
        $categoryIds = $request->category_id;
        $tagIds = $request->tag_id;
        $productId = $request->id;
        try {
            if (!$categoryIds && !$tagIds) return sendResponseError('', __('No data send.'));
            $response = $this->saveProductCategories($categoryIds, $productId);
            if (!$response['success']) throw new \Exception($response['message']);
            $response = $this->saveProductTags($tagIds, $productId);
            if (!$response['success']) throw new \Exception($response['message']);
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('', __('Something went wrong. ') . $e->getMessage());
        }
    }

    private function saveProductCategories($categoryIds, $productId) {
        if (!$categoryIds) return ['success' => TRUE];
        $productCategories = [];
        foreach ($categoryIds as $categoryId) {
            $productCategories[] = ['product_id' => $productId, 'category_id' => $categoryId];
        }
        try {
            DB::beginTransaction();
            $this->productCategoryRepo->deleteWhere(['product_id' => $productId]);
            $this->productCategoryRepo->insert($productCategories);
            DB::commit();
            return ['success' => TRUE];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['success' => FALSE, 'message' => $e->getMessage()];
        }
    }

    private function saveProductTags($tagIds, $productId) {
        if (!$tagIds) return ['success' => TRUE];
        $productTags = [];
        foreach ($tagIds as $tagId) {
            $productTags[] = ['product_id' => $productId, 'tag_id' => $tagId];
        }
        try {
            DB::beginTransaction();
            $this->productTagRepo->deleteWhere(['product_id' => $productId]);
            $this->productTagRepo->insert($productTags);
            DB::commit();
            return ['success' => TRUE];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['success' => FALSE, 'message' => $e->getMessage()];
        }
    }

    public function saveProductPricing($request) {
        $compares = $request->compare;
        $quantities = $request->quantity;
        $prices = $request->price;
        $productId = $request->product_id;
        if (!$compares || !$quantities || !$prices) {
            return sendResponseError('', __('Send all data.'));
        }
        $productPricings = [];
        foreach ($compares as $index => $compare) {
            $productPricings[] = ['product_id' => $productId, 'compare' => $compares[$index], 'quantity' => $quantities[$index], 'price' => $prices[$index],
                                  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
        }
        try {
            DB::beginTransaction();
            $this->productPricingRepo->deleteWhere(['product_id' => $productId]);
            $this->productPricingRepo->insert($productPricings);
            $min_price = !empty($prices) ? min($prices) : 0;
            Product::where('id',$productId)->update(['min_price'=>$min_price]);
            DB::commit();
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('', __('Something went wrong.'));
        }
    }

    public function saveProductCombinations($request) {
        $productId = $request->product_id;
        $combinationTypeId = $request->combination_type_id;
        $combinationIds = is_array($request->combination_ids) ? $request->combination_ids : [];
        //if(!$combinationIds) return sendResponseError('', __('No data send.'));
        if ($combinationTypeId == COLOR_ID) {
            return $this->saveColorCombinations($productId, $combinationIds);
        } else {
            return $this->saveOtherCombinations($productId, $combinationTypeId, $combinationIds);
        }
    }

    private function saveColorCombinations($productId, $reqCombinationIds) {
        $newProductCombinations = [];
        $existingCombinationIds = $this->productCombinationRepo->getData(['product_id' => $productId, 'combination_type_id' => COLOR_ID], ['combination_id'])
                                                               ->pluck('combination_id')->toArray();
        $newCombinationIds = array_diff($reqCombinationIds, $existingCombinationIds);
        foreach ($newCombinationIds as $combinationId) {
            $newProductCombinations[] = ['product_id' => $productId, 'combination_type_id' => COLOR_ID, 'combination_id' => $combinationId,
                                         'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
        }
        try {
            DB::beginTransaction();
            $this->productCombinationRepo->updateWhereNotInAndWhere('combination_id', array_values($reqCombinationIds),
                ['product_id' => $productId, 'combination_type_id' => COLOR_ID], ['status' => INACTIVE]);
            $this->productCombinationRepo->updateWhereInAndWhere('combination_id', array_values($reqCombinationIds),
                ['product_id' => $productId, 'combination_type_id' => COLOR_ID], ['status' => ACTIVE]);
            $this->productCombinationRepo->insert($newProductCombinations);
            DB::commit();
            return $this->renderMediaCombinations($productId);
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('', __('Something went wrong.') . $e->getMessage());
        }
    }

    private function renderMediaCombinations($productId) {
        $updatedMediaCombinations = $this->productCombinationRepo->getData(['product_id' => $productId, 'status' => ACTIVE], NULL, ['combination_type_id' => 'desc']);
        $html = view('admin.products.products.add_product.combination.media_combinations', ['productCombinations' => $updatedMediaCombinations])->render();
        return sendResponse(['html' => $html], __('Saved successfully.'));
    }

    private function saveOtherCombinations($productId, $combinationTypeId, $combinationIds) {
        $productCombinations = [];
        foreach ($combinationIds as $combinationId) {
            $productCombinations[] = ['product_id' => $productId, 'combination_type_id' => $combinationTypeId, 'combination_id' => $combinationId,
                                      'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
        }
        try {
            DB::beginTransaction();
            $this->productCombinationRepo->deleteWhere(['product_id' => $productId, 'combination_type_id' => $combinationTypeId]);
            $this->productCombinationRepo->insert($productCombinations);
            DB::commit();
            return sendResponse('', __('Saved successfully.'));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('', __('Something went wrong.') . $e->getMessage());
        }
    }

    public function saveProductMedia($request) {
        $data['product_id'] = $request->product_id;
        $data['media_type'] = $request->media_type;
        $data['id'] = $request->id;
        if ($request->image) {
            $response = $this->uploadCombinationImage($request->file('image'));
            if ($response['success'] && $response['url']) {
                $data['media_url'] = $response['url'];
            } else if (!$response['success']) {
                return sendResponseError('', $response['message']);
            }
        } else if ($request->media_url) {
            $data['media_url'] = $request->media_url;
        }

        try {
            $productCombination = $this->productCombinationRepo->firstWhere(['id' => $data['id'], 'media_type' => INTERNAL_IMAGE]);

            $ok = $this->productCombinationRepo->updateOrCreate(['id' => $data['id']], $data);
            if (!$ok) throw new \Exception('Operation Failed');

            //remove if previous image exist
            if (($request->image && $productCombination) || ($request->media_url && $productCombination)) {
                $this->removePreviousCombinationImageFile($productCombination);
            }
            //

            return $this->renderMediaCombinations($data['product_id']);
        } catch (\Exception $e) {
            return sendResponseError('', __('Something went wrong. ') . $e->getMessage());
        }
    }

    private function uploadCombinationImage($image) {
        try {
            if (isset($image)) {
                $folder = 'products/images';
                $url = $image->store($folder,);
                return ['success' => TRUE, 'url' => $url];
            }
            return ['success' => TRUE];
        } catch (\Exception $e) {
            return ['success' => FALSE, 'message' => $e->getMessage()];
        }
    }

    public function removePreviousCombinationImageFile($productCombination) {
        if ($productCombination->media_url) {
            Storage::delete($productCombination->media_url);
        }
    }

    public function saveProductFeaturedMedia($request) {
        $productId = $request->product_id;
        $productCombinationId = $request->product_combination_id;
        try {
            $this->productCombinationRepo->saveFeatured($productId, $productCombinationId);
            return $this->renderMediaCombinations($productId);
        } catch (\Exception $e) {
            return sendResponseError('', '', $e->getMessage());
        }
    }
    public function deleteProductCombination($id) {
        try {
            $productCombination = ProductCombination::where('id',$id)->first();
            $this->productCombinationRepo->destroy($id);
            if ($productCombination->media_url) {
                Storage::delete($productCombination->media_url);
            }
            return jsonResponse(TRUE)->message(__('Media deleted successfully'));
        } catch (\Exception $e) {
            return jsonResponse(FALSE)->message($e->getMessage());
        }
    }

    public function duplicateProduct($request) {
        try {
            DB::beginTransaction();
            $product = $this->prepareDataAndCreateProduct($request, TRUE);
            if (isset($product['message'])) return sendResponseError('', $product['message']);

            $this->productCategoryRepo->duplicateProductCategories($product->id, $request->old_product_id);
            $this->productTagRepo->duplicateProductTags($product->id, $request->old_product_id);
            $this->productCombinationRepo->duplicateProductCombinations($product->id, $request->old_product_id);
            $this->productPricingRepo->duplicateProductPricings($product->id, $request->old_product_id);
            DB::commit();
            return sendResponse(['reference' => $product->reference], __('Product has been duplicated successfully.'));
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('', '', $e->getMessage());
        }

    }

    public function getProductSearchByCategory($category_id, $searchItem) {
        return $this->repo->getProductSearchByCategory($category_id, $searchItem);
    }

    public function getProductByCategory($category_id) {
        return $this->repo->getProductByCategory($category_id);
    }

    public function getProductListByWhereIn($id, $slug) {
        return $this->repo->getProductListByWhereIn($id, $slug);
    }


}
