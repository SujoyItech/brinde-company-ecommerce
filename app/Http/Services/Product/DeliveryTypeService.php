<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\Product\DeliveryTypeRepository;
use App\Http\Services\BaseService;
use Illuminate\Http\Request;

class DeliveryTypeService extends BaseService
{

    /**
     * DeliveryTypeService constructor.
     * @param DeliveryTypeRepository $repository
     */
    public function __construct(DeliveryTypeRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getDeliveryTypeData($id){

        if ($id !== NULL){
            $data['delivery_type'] = $this->repo->getDeliveryDetails($id);
        }else{
            $data['delivery_type'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {

        try {
            if (isset($requestArray['icon']) && !empty($requestArray['icon'])){
                $requestArray['icon'] = $this->imageData($requestArray['icon']);
            }
            $delivery = $this->repo->create($requestArray);
            if ( $delivery) {
                return jsonResponse(true)->message(__("Delivery type has been created successfully."));
            }
            return jsonResponse(false)->message(__("Delivery type create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            if (isset($requestArray['icon']) && !empty($requestArray['icon'])){
                $requestArray['icon'] = $this->imageData($requestArray['icon'],$id);
            }
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Delivery type has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    private function imageData($image,$id=NULL){
        if ($id !== NULL){
            $details =  $this->repo->getDeliveryDetails($id);
            return uploadImage($image,get_image_path('delivery'),$details->icon ?? '');
        }else{
            return uploadImage($image,get_image_path('delivery'));
        }
    }

    public function delete($id){
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier){
                return jsonResponse(TRUE)->message(__('Delivery type deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Delivery type delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
