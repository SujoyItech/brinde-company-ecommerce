<?php

namespace App\Http\Services\Product;


use App\Imports\BrindeImport;
use App\Jobs\ProcessProductUpload;
use App\Models\Product\Brand;
use App\Models\Product\Category;
use App\Models\Product\Combination;
use App\Models\Product\Product;
use App\Models\Product\ProductBrand;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductCombination;
use App\Models\Product\ProductPricing;
use App\Models\Product\ProductTag;
use App\Models\Product\ProductUpload;
use App\Models\Product\ProductUploadProcess;
use App\Models\Product\Tag;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ProductImportService
{
    private $category_array,$tags_array;

    public function __construct(){
        $this->category_array = Category::where('status',STATUS_ACTIVE)->pluck('id')->toArray();
        $this->tags_array = Tag::where('status',STATUS_ACTIVE)->pluck('id')->toArray();
    }

    public static function productUpload(array $requestArray){
        try {
            $directory = get_file_path().$requestArray['supplier_id'].'/';
            $file_name = 'product_'.$requestArray['supplier_id'].'_'.date("Y-m-d-h-is",time());
            $uploaded_file = uploadFile($requestArray['file'],$directory,$file_name);
            $insert = [
                'supplier_id' => $requestArray['supplier_id'],
                'file' => $directory.$uploaded_file,
                'uploader_id' => Auth::user()->id,
                'total_product' => 0,
                'uploaded_product' => 0,
                'upload_time' => Carbon::now()
            ];
            $product = ProductUpload::create($insert);
            if ($product) {
                self::processProductExcel($product->id);
                return jsonResponse(TRUE)->message(__('Product file uploaded successfully.'));
            }else{
                return jsonResponse(FALSE)->message(__('Product file upload failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }

    public static function processProductExcel($id){
        $uploaded_product = ProductUpload::where(['id'=>$id])->first();
        if (isset($uploaded_product->file)) {
            Excel::import(new BrindeImport($uploaded_product->id),public_path($uploaded_product->file));
        }
    }

    public static function productUploadProcess($batch_payload){
        try {
            self::productUploadComplete($batch_payload);
            return jsonResponse(TRUE)->message(__('Product Upload Processing Start.'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }

    }

    public static function productUploadComplete($batch_payload){
        $supplier_id = ProductUpload::where('id',$batch_payload)->first()->supplier_id;
        $products_query = ProductUploadProcess::where(['batch_payload'=>$batch_payload,'status'=>STATUS_PRODUCT_UPLOAD_UNREAD]);
        $products_query->chunk(100, function ($collect) use ($batch_payload,$supplier_id) {
            dispatch(new ProcessProductUpload($collect,$batch_payload,$supplier_id));
//            $product_import_service = new ProductImportService();
//            $product_import_service->importProducts($collect,$batch_payload,$supplier_id);
        });
        ProductUpload::where('id',$batch_payload)->update(['status'=>STATUS_PRODUCT_PROCESSING_COMPLETED]);

    }

    public function importProducts($rows,$batch_payload,$supplier_id) {
        try {
            $combination_array = [];
            $combination = Combination::select('id','name','combination_type_id')->get();
            foreach ($combination as $comb){
                $combination_array[$comb->name]['id'] = $comb->id;
                $combination_array[$comb->name]['combination_type_id'] = $comb->combination_type_id;
            }
            $index = 0;
            foreach ($rows as $row){
                try {
                    $product = json_decode($row->product);
                    $columnList = json_decode($row->product_column);
                    $reference = $this->generateReference($product[1]);
                    $priceArray = [
                        'equal' => $product[11],
                        'less' => $product[12],
                        'more' => $product[13]
                    ];
                    $insert = [
                        'name' => $product[0],
                        'slug' => $this->generateSlug($product[0],$reference),
                        'reference' => $reference,
                        'description' => $product[2],
                        'additional_info' => $product[3],
                        'supplier_id' => $supplier_id,
                        'delivery_type_id' => $product[5],
                        'min_price' => $this->getMinPrice($priceArray),
                        'meta_title' => $product[7],
                        'meta_keywords' => $product[8],
                        'meta_description' => $product[9],
                        'batch_payload' => $row->batch_payload,
                        'status' => $product[10]
                    ];
                    $new_product = Product::create($insert);
                    $this->productPriceUpdate($new_product->id,$priceArray);
                    $this->productTagUpdate($new_product->id,$product[6]);
                    $this->productCategoryUpdate($new_product->id,$product[4]);
                    $this->productMediaUpdate($new_product->id,$columnList,$product,$combination_array);
                    $this->productBrandUpdate($new_product->id);
                    ProductUploadProcess::where('id',$row->id)->delete();
                    $index ++;
                }catch (\Exception $product_exception){
                    ProductUploadProcess::where('id',$row->id)->update(['status'=>STATUS_PRODUCT_UPLOAD_FAILED]);
                }
            }
            ProductUpload::where('id',$batch_payload)->increment('uploaded_product',$index);

        }catch (\Exception $exception){
        }
    }

    private function generateReference($reference){
        $check_product = Product::where('reference',$reference)->first();
        if (isset($check_product)) {
            $reference = $reference.'-'.randomNumber(10);
        }
        return $reference;
    }
    private function generateSlug($name,$reference){
        $slug = create_slug($name).'-'.$reference;
        return $slug;
    }

    private function productCategoryUpdate($product_id,$category_id) {
        if (!empty($category_id)) {
            $insert_data = [];
            $category_array = explode(',',$category_id);
            foreach ($category_array as $key=>$category){
                if (in_array($category,$this->category_array)) {
                    $insert_data[$key]['product_id'] = $product_id;
                    $insert_data[$key]['category_id'] = $category;
                }
            }
            ProductCategory::insert($insert_data);
        }
    }

    private function productPriceUpdate($product_id,array $priceArray){
        $equal_price =  preg_replace("/\r|\n/", "", $priceArray['equal']);
        $less_price = preg_replace("/\r|\n/", "", $priceArray['less']);
        $more_price = preg_replace("/\r|\n/", "", $priceArray['more']);
        $equal_price_array = $this->preparePrice($product_id,$equal_price,'equal');
        $less_price_array = $this->preparePrice($product_id,$less_price,'less');
        $more_price_array = $this->preparePrice($product_id,$more_price,'more');
        $product_price = array_merge($equal_price_array,$less_price_array,$more_price_array);
        ProductPricing::insert($product_price);
    }

    private function getMinPrice($priceArray){
        $equal_price =  preg_replace("/\r|\n/", "", $priceArray['equal']);
        $less_price = preg_replace("/\r|\n/", "", $priceArray['less']);
        $more_price = preg_replace("/\r|\n/", "", $priceArray['more']);
        $equal_price_array = $this->buildArrayFromString($equal_price);
        $less_price_array = $this->buildArrayFromString($less_price);
        $more_price_array = $this->buildArrayFromString($more_price);
        $all_price = array_merge($equal_price_array,$less_price_array,$more_price_array);
        $min_price = !empty($all_price) ? min($all_price) : 0 ;
        return $min_price;
    }

    private function buildArrayFromString($string){
        $array = explode(',',$string);
        $new_array = [];
        foreach ($array as $key=>$value){
            $new_array [] = (float)explode('=',$value)[1];
        }
        return $new_array;
    }

    private function preparePrice($product_id,$price_data,$compare){
        $price_array = explode(',',$price_data);
        $insert_data = [];
        foreach ($price_array as $key=>$price){
            $quantity_price_map = explode('=',$price);
            $insert_data[$key]['product_id'] = $product_id;
            $insert_data[$key]['compare'] = $compare;
            $insert_data[$key]['quantity'] = $quantity_price_map[0];
            $insert_data[$key]['price'] = $quantity_price_map[1];
        }
        return $insert_data;
    }

    private function productTagUpdate($product_id,$tag_data) {
        if (!empty($tag_data)) {
            $insert_data = [];
            $tag_array = explode(',',$tag_data);
            foreach ($tag_array as $key=>$tag){
                if (in_array($tag,$this->tags_array)) {
                    $insert_data[$key]['product_id'] = $product_id;
                    $insert_data[$key]['tag_id'] = $tag;
                }
            }
            ProductTag::insert($insert_data);
        }

    }

    private function productMediaUpdate($product_id,$row_header,$row,$combination_array){
        $insert_data = [];
        $index = 0;
        foreach ($row_header as $key => $header){
            $media =  $row[$key];
            if ($media != NULL) {
                if ($header == 'media_360') {
                    $insert_data[$index]['product_id'] = $product_id;
                    $insert_data[$index]['combination_type_id'] = NULL;
                    $insert_data[$index]['combination_id'] = NULL;
                    $insert_data[$index]['media_type'] = '360_url';
                    $insert_data[$index]['media_url'] = $media;
                    $insert_data[$index]['is_featured'] = INACTIVE;
                }elseif ($header == 'media_video') {
                    $insert_data[$index]['product_id'] = $product_id;
                    $insert_data[$index]['combination_type_id'] = NULL;
                    $insert_data[$index]['combination_id'] = NULL;
                    $insert_data[$index]['media_type'] = 'video_url';
                    $insert_data[$index]['media_url'] = $media;
                    $insert_data[$index]['is_featured'] = INACTIVE;
                }elseif ($header == 'media_default') {
                    $insert_data[$index]['product_id'] = $product_id;
                    $insert_data[$index]['combination_type_id'] = NULL;
                    $insert_data[$index]['combination_id'] = NULL;
                    $insert_data[$index]['media_type'] = 'external_image';
                    $insert_data[$index]['media_url'] = $media;
                    $insert_data[$index]['is_featured'] = TRUE;
                }elseif (str_contains($header,'media_color')) {
                    $color_name = substr($header,12);
                    if (isset($combination_array[$color_name])) {
                        $insert_data[$index]['product_id'] = $product_id;
                        $insert_data[$index]['combination_type_id'] = $combination_array[$color_name]['combination_type_id'];
                        $insert_data[$index]['combination_id'] = $combination_array[$color_name]['id'];
                        $insert_data[$index]['media_type'] = 'external_image';
                        $insert_data[$index]['media_url'] = $media;
                        $insert_data[$index]['is_featured'] = FALSE;
                    }
                }elseif (str_contains($header,'ask_size')){
                    $size_name = substr($header,9);
                    if (isset($combination_array[$size_name])) {
                        $insert_data[$index]['product_id'] = $product_id;
                        $insert_data[$index]['combination_type_id'] = $combination_array[$size_name]['combination_type_id'];
                        $insert_data[$index]['combination_id'] = $combination_array[$size_name]['id'];
                        $insert_data[$index]['media_type'] = NULL;
                        $insert_data[$index]['media_url'] = NULL;
                        $insert_data[$index]['is_featured'] = FALSE;
                    }
                }
                $index ++;
            }
        }
        ProductCombination::insert($insert_data);
    }

    public function productBrandUpdate($product_id){
        $brands = Brand::where('status',ACTIVE)->get();
        $product_brand = [];
        if (isset($brands)) {
            foreach ($brands as $key=>$brand){
                $product_brand[$key]['product_id']=$product_id;
                $product_brand[$key]['brand_id']=$brand->id;
            }
            ProductBrand::insert($product_brand);
        }
    }
}
