<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\Product\SupplierRepository;
use App\Http\Services\BaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Product/SupplierRepository $repository
     */
    public function __construct(SupplierRepository $repository)
    {
        $this->repo = $repository;
    }

    public function getSupplierData($id){
        if ($id !== NULL){
            $data['supplier'] = $this->repo->getSuppliersDetails($id);
        }else{
            $data['supplier'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {

        try {
            $requestArray['created_by'] = Auth::user()->id;
            $supplier = $this->repo->create($requestArray);
            if ( $supplier) {
                return jsonResponse(true)->message(__("Supplier has been created successfully."));
            }
            return jsonResponse(false)->message(__("Supplier create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Supplier has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete(Request $request){
        try {
            $supplier = $this->repo->destroy($request->id);
            if ($supplier){
                return jsonResponse(TRUE)->message(__('Supplier deleted successfully.'));
            }else{
                return jsonResponse(TRUE)->message(__('Supplier delete failed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }

    // Your methods for repository
}
