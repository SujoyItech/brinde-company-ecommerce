<?php

namespace App\Http\Services;

use App\Models\User;
use App\Jobs\CrmDailyMailJob;
use Illuminate\Support\Carbon;
use App\Models\CRM\CrmQuotation;

class CrmStatusChangeMailService
{
    public static function productFullArrivalStatus($id)
    {
        $quotation = self::quotationDetails($id);
        if (isset($quotation)) {
            $userEmail = $quotation->salesman_email;
            $subject = $quotation->brand_name . __(' products fully arrived');
            $data = [
                'brand_id'         => $quotation->brand_id,
                'brand_name'       => $quotation->brand_name,
                'brand_url'        => $quotation->brand_url,
                'brand_email'        => $quotation->brand_email,
                'brand_phone'        => $quotation->brand_phone,
                'brand_address'        => $quotation->brand_address,
                'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                'brand_faq_link'        => $quotation->brand_faq_link,
                'brand_color_code'        => $quotation->brand_color_code,
                'customer_name'    => $quotation->contact_name,
                'comments'    => $quotation->comments,
                'salesman_name'    => $quotation->salesman_name,
                'salesman_email'   => $quotation->salesman_email,
                'order_reference' => $quotation->order_reference,
                'products' => $quotation->quotation_products,
                'arrival_page_link' => route('productionPurchaseDetails', ['code' => $quotation->purchase_reference])
            ];
            dispatch(new CrmDailyMailJob('admin.mail.email.crm_status.arrival_mail', $userEmail, $data, $subject));
        }
    }
    public static function productBudgetApprovalStatus($id)
    {
        $quotation = self::quotationDetails($id);
        if (isset($quotation)) {
            $userEmail = $quotation->salesman_email;
            $subject = __('Budget approved by customer.');
            $data = [
                'brand_id'         => $quotation->brand_id,
                'brand_name'       => $quotation->brand_name,
                'brand_url'        => $quotation->brand_url,
                'brand_email'        => $quotation->brand_email,
                'brand_phone'        => $quotation->brand_phone,
                'brand_address'        => $quotation->brand_address,
                'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                'brand_faq_link'        => $quotation->brand_faq_link,
                'brand_color_code'        => $quotation->brand_color_code,
                'customer_name'    => $quotation->contact_name,
                'comments'    => $quotation->comments,
                'salesman_name'    => $quotation->salesman_name,
                'salesman_email'   => $quotation->salesman_email,
                'budget_reference' => $quotation->budget_reference,
                'budget_page_link' => route('crmBudgetDetails', ['code' => $quotation->budget_reference])
            ];
            dispatch(new CrmDailyMailJob('admin.mail.email.crm_status.budget_approval_mail', $userEmail, $data, $subject));
        }
    }
    public static function makeOrderMail($id)
    {
        $quotation = self::quotationDetails($id);
        $logistic_email = User::where(['role' => LOGISTIC_MANAGER])->pluck('email')->toArray();
        if (isset($quotation) && !empty($logistic_email)) {
            $subject = __('New order created for .') . $quotation->brand_name;
            $data = [
                'brand_id'         => $quotation->brand_id,
                'brand_name'       => $quotation->brand_name,
                'brand_url'        => $quotation->brand_url,
                'brand_email'        => $quotation->brand_email,
                'brand_phone'        => $quotation->brand_phone,
                'brand_address'        => $quotation->brand_address,
                'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                'brand_faq_link'        => $quotation->brand_faq_link,
                'brand_color_code'        => $quotation->brand_color_code,
                'customer_name'    => $quotation->contact_name,
                'comments'    => $quotation->comments,
                'salesman_name'    => $quotation->salesman_name,
                'salesman_email'   => $quotation->salesman_email,
                'order_reference' => $quotation->order_reference,
                'order_page_link' => route('productionOrderDetails', ['code' => $quotation->order_reference])
            ];
            dispatch(new CrmDailyMailJob('admin.mail.email.crm_status.order_mail', $logistic_email, $data, $subject));
        }
    }

    public static function sendToProductionMail($id)
    {
        $quotation = self::quotationDetails($id);
        $production_email = User::where(['role' => PRODUCTION_MANAGER])->pluck('email')->toArray();
        if (isset($quotation) && !empty($production_email)) {
            $subject = __('New order request for production.');
            $data = [
                'brand_id'         => $quotation->brand_id,
                'brand_name'       => $quotation->brand_name,
                'brand_url'        => $quotation->brand_url,
                'brand_email'        => $quotation->brand_email,
                'brand_phone'        => $quotation->brand_phone,
                'brand_address'        => $quotation->brand_address,
                'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                'brand_faq_link'        => $quotation->brand_faq_link,
                'brand_color_code'        => $quotation->brand_color_code,
                'customer_name'    => $quotation->contact_name,
                'comments'    => $quotation->comments,
                'salesman_name'    => $quotation->salesman_name,
                'salesman_email'   => $quotation->salesman_email,
                'production_reference' => $quotation->production_reference,
                'production_page_link' => route('productionDetails', ['code' => $quotation->production_reference])
            ];
            dispatch(new CrmDailyMailJob('admin.mail.email.crm_status.production_mail', $production_email, $data, $subject));
        }
    }
    public static function sendToShipmentMail($id)
    {
        $quotation = self::quotationDetails($id);
        $salesman_email = $quotation->salesman_email;
        if (isset($quotation) && !empty($salesman_email)) {
            $subject = __('New order request for shipment.');
            $data = [
                'brand_id'         => $quotation->brand_id,
                'brand_name'       => $quotation->brand_name,
                'brand_url'        => $quotation->brand_url,
                'brand_email'        => $quotation->brand_email,
                'brand_phone'        => $quotation->brand_phone,
                'brand_address'        => $quotation->brand_address,
                'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                'brand_faq_link'        => $quotation->brand_faq_link,
                'brand_color_code'        => $quotation->brand_color_code,
                'customer_name'    => $quotation->contact_name,
                'comments'    => $quotation->comments,
                'salesman_name'    => $quotation->salesman_name,
                'salesman_email'   => $quotation->salesman_email,
                'order_reference' => $quotation->order_reference,
                'shipment_page_link' => route('productionShipmentDetails', ['code' => $quotation->order_reference])
            ];
            dispatch(new CrmDailyMailJob('admin.mail.email.crm_status.shipment_mail', $salesman_email, $data, $subject));
        }
    }
    public static function sendProductionCompletedMail($id)
    {
        $quotation = self::quotationDetails($id);
        $salesman_email = $quotation->salesman_email;
        if (isset($quotation) && !empty($salesman_email)) {
            $subject = __('Production has been completed.');
            $data = [
                'brand_id'         => $quotation->brand_id,
                'brand_name'       => $quotation->brand_name,
                'brand_url'        => $quotation->brand_url,
                'brand_email'        => $quotation->brand_email,
                'brand_phone'        => $quotation->brand_phone,
                'brand_address'        => $quotation->brand_address,
                'brand_privacy_policy_link'        => $quotation->brand_privacy_policy_link,
                'brand_faq_link'        => $quotation->brand_faq_link,
                'brand_color_code'        => $quotation->brand_color_code,
                'customer_name'    => $quotation->contact_name,
                'comments'    => $quotation->comments,
                'salesman_name'    => $quotation->salesman_name,
                'salesman_email'   => $quotation->salesman_email,
                'order_reference' => $quotation->order_reference,
                'production_reference' => $quotation->production_reference,
                'production_completed_page_link' => route('productionCompleteDetails', ['code' => $quotation->production_reference]),
                'date' => Carbon::now()->format('d-m-Y')
            ];
            dispatch(new CrmDailyMailJob('admin.mail.email.crm_status.production_completed_mail', $salesman_email, $data, $subject));
        }
    }

    private static function quotationDetails($id)
    {
        return CrmQuotation::select(
            'crm_quotations.*',
            'users.name as salesman_name',
            'users.email as salesman_email',
            'brands.id as brand_id',
            'brands.name as brand_name',
            'brands.brand_url',
            'brands.email as brand_email',
            'brands.phone as brand_phone',
            'brands.address as brand_address',
            'brands.privacy_policy_link as brand_privacy_policy_link',
            'brands.faq_link as brand_faq_link',
            'brands.color_code as brand_color_code',
        )
            ->leftJoin('users', 'crm_quotations.assigned_salesman', '=', 'users.id')
            ->leftJoin('brands', 'crm_quotations.brand_id', '=', 'brands.id')
            ->where('crm_quotations.id', $id)
            ->first();
    }
}
