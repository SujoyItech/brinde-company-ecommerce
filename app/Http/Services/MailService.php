<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailService extends BaseService {
    protected $defaultEmail;
    protected $defaultName;

    public function __construct() {
        $this->defaultEmail = env('MAIL_FROM_ADDRESS');
        $this->defaultName = env('APP_NAME');
    }

    public static function sendResetPasswordMailProcess($email, $body, $subject) {
        Mail::send('admin.mail.email.reset_password_mail', ['body' => $body], function ($messages) use ($email, $subject) {
            $messages->to($email)->subject($subject);
        });
    }

    public function send($template = '', $data = [], $to = '', $name = '', $subject = '') {
        try {
            Mail::send($template, $data, function ($message) use ($name, $to, $subject) {
                $message->to($to, $name)->subject($subject)->replyTo(
                    $this->defaultEmail, $this->defaultName
                );
                $message->from($this->defaultEmail, $this->defaultName);
            });
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }

    public static function sendMailProcess($template,$body,$email,$subject){
        Mail::send($template, ['body' => $body], function ($messages) use ($email, $subject) {
            $messages->to($email)->subject($subject);
        });
    }
}
