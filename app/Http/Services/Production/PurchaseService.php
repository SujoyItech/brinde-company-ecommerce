<?php

namespace App\Http\Services\Production;

use App\Http\Repositories\CRM\QuotationProductRepository;
use App\Http\Repositories\Production\PurchaseRepository;
use App\Http\Services\BaseService;
use App\Http\Services\CrmStatusChangeMailService;
use App\Models\Crm\CrmQuotation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class PurchaseService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CrmQuotation/PurchaseRepository $repository
     */
    private $repository, $quotationProductRepo;

    public function __construct(PurchaseRepository $repository,
                                QuotationProductRepository $quotationProductRepo)
    {
        $this->repo = $repository;
        $this->quotationProductRepo = $quotationProductRepo;
    }

    // Your methods for repository

    public function getSingleData($id)
    {

        if ($id !== NULL) {
            $data = $data = $this->repo->firstWhere(['id' => $id]);
        } else {
            $data = [];
        }
        return $data;
    }

    public function getOrderData($search_array = [])
    {
        return $this->repo->getOrderData($search_array);
    }

    public function create($request)
    {
    }

    public function update(int $id, array $requestArray)
    {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse('', __("Updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }


    public function purchaseProduct($request)
    {
        $response = sendResponseError('', __("Operation failed"));
        try {
            $product = $this->quotationProductRepo->firstWhere(['id' => $request->id]);
            if (isset($product)) {
                if($product->purchased == STATUS_PURCHASED) {
                    $response = sendResponseError('', __("This product already purchased"));
                }
                $data = [
                    'purchased' => STATUS_PURCHASED
                ];
                $update = $this->quotationProductRepo->updateModel($request->id, $data);
                if ($update) {
                    $this->changeOrderPurchaseStatus($product->quotation_id);
                    $updated_product = $this->quotationProductRepo->firstWhere(['id' => $request->id]);
                    $updated_order = $this->repo->firstWhere(['id' => $product->quotation_id]);
                    $status = [
                        'order_status' => $updated_order->status,
                        'product_purchase_status' => $updated_product->purchased,
                        'product_id' => $updated_product->id,
                    ];
                    $response = sendResponse($status, __("Product purchased successfully"));
                }

            } else {
                $response = sendResponseError('', __("Item not found"));
            }
        } catch (\Exception $e) {
            $response = sendResponseError('', $e->getMessage());
        }
        return $response;
    }

    private function changeOrderPurchaseStatus($quotation_id)
    {
        $quotation = $this->repo->find($quotation_id);
        $products = $this->quotationProductRepo->getData(['quotation_id' => $quotation_id]);
        if (isset($products[0])) {
            $product_size = sizeof($products);
            $purchase_size = 0;
            foreach ($products as $product) {
                if ($product->purchased == STATUS_PURCHASED) {
                    $purchase_size = $purchase_size + 1;
                }
            }
            if ($purchase_size > 0) {
                if ($purchase_size == $product_size) {
                    $this->repo->updateModel($quotation_id, [
                        'status' => STATUS_ORDER_PURCHASED_DONE,
                        'in_staging' => QUOTATION_STAGE_IN_ARRIVAL,
                        'purchase_reference' => formatPurchaseReference(referenceCodeEdit($quotation->quote_reference,2))
                    ]);
                } else {
                    $this->repo->updateModel($quotation_id, ['status' => STATUS_ORDER_PARTIALLY_PURCHASED, 'in_staging' => QUOTATION_STAGE_IN_PURCHASE]);
                }
            } else {
                $this->repo->updateModel($quotation_id, ['status' => STATUS_BUDGET_ORDERED, 'in_staging' => QUOTATION_STAGE_IN_PURCHASE]);
            }
        }
    }

    public function updateProductionOrder($request)
    {
        $response = sendResponseError('', __("Invalid request"));
        $quotation = $this->repo->find($request->quotation_id);
        DB::beginTransaction();
        try {
            if (isset($request->product_id[0])) {
                $product_size = sizeof($request->product_id);
                $purchase_size = 0;
                foreach ($request->product_id as $key => $value) {
                    if(isset($request->purchased[$value])) {
                        $this->quotationProductRepo->updateModel($value, ['purchased' => STATUS_PURCHASED]);
                    } else {
                        $this->quotationProductRepo->updateModel($value, ['purchased' => STATUS_NOT_PURCHASED]);
                    }
                }
                if(isset($request->purchased)) {
                    $purchase_size = sizeof($request->purchased);
                }
                if ($purchase_size > 0) {
                    if ($purchase_size == $product_size) {
                        $this->repo->updateModel($request->quotation_id, [
                            'status' => STATUS_ORDER_PURCHASED_DONE,
                            'in_staging' => QUOTATION_STAGE_IN_ARRIVAL,
                            'purchase_reference' => formatPurchaseReference(referenceCodeEdit($quotation->quote_reference,2))
                        ]);
                    } else {
                        $this->repo->updateModel($request->quotation_id, ['status' => STATUS_ORDER_PARTIALLY_PURCHASED, 'in_staging' => QUOTATION_STAGE_IN_PURCHASE]);
                    }
                } else {
                    $this->repo->updateModel($request->quotation_id, ['status' => STATUS_BUDGET_ORDERED, 'in_staging' => QUOTATION_STAGE_IN_PURCHASE]);
                }
                $data['products'] = $this->quotationProductRepo->getData(['quotation_id' => $request->quotation_id]);
                $result['order_status'] = $updated_order = $this->repo->firstWhere(['id' => $request->quotation_id])->status;
                $result['view'] = '';
                $result['view'] .= view::make('admin.production.purchase.updated_product_list', $data);

                $response = sendResponse($result, __("Order Updated successfully"));
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $response = sendResponseError('', $e->getMessage());
        }
        DB::commit();
        return $response;
    }

    public function updateProductionComplete($request)
    {
        $response = sendResponseError('', __("Invalid request"));
        $quotation = $this->repo->find($request->quotation_id);
        DB::beginTransaction();
        try {
            if (isset($request->product_id[0])) {
                $product_size = sizeof($request->product_id);
                $completed_size = 0;
                foreach ($request->product_id as $key => $value) {
                    if(isset($request->completed[$value])) {
                        $this->quotationProductRepo->updateModel($value, ['completed' => STATUS_COMPLETED]);
                    } else {
                        $this->quotationProductRepo->updateModel($value, ['completed' => STATUS_NOT_COMPLETED]);
                    }
                }
                if(isset($request->completed)) {
                    $completed_size = sizeof($request->completed);
                }
                if ($completed_size > 0) {
                    if ($completed_size == $product_size) {
                        $this->repo->updateModel($request->quotation_id, [
                            'status' => STATUS_PRODUCTION_COMPLETED,
                            'in_staging' => QUOTATION_STAGE_IN_PRODUCTION
                        ]);
                        $this->sendProductionCompletedMail($request->quotation_id);
                    } else {
                        $this->repo->updateModel($request->quotation_id, ['status' => STATUS_PRODUCTION_PARTIALLY_COMPLETED, 'in_staging' => QUOTATION_STAGE_IN_PRODUCTION]);
                    }
                } else {
                    $this->repo->updateModel($request->quotation_id, ['status' => STATUS_PRODUCTION_NOT_COMPLETED, 'in_staging' => QUOTATION_STAGE_IN_PRODUCTION]);
                }
                $data['products'] = $this->quotationProductRepo->getData(['quotation_id' => $request->quotation_id]);
                $result['order_status'] = $updated_order = $this->repo->firstWhere(['id' => $request->quotation_id])->status;
                $result['view'] = '';
                $result['view'] .= view::make('admin.production.production.production_item_list', $data);

                $response = sendResponse($result, __("Production Updated successfully"));
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $response = sendResponseError('', $e->getMessage());
        }
        DB::commit();
        return $response;
    }

    private function sendProductionCompletedMail($id){
        CrmStatusChangeMailService::sendProductionCompletedMail($id);
    }
}


