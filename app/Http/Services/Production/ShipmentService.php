<?php

namespace App\Http\Services\Production;

use App\Http\Repositories\Production\PurchaseRepository;
use App\Http\Repositories\Production\ShipmentRepository;
use App\Http\Services\BaseService;
use App\Models\Crm\CrmQuotation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ShipmentService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CrmQuotation/PurchaseRepository $repository
     */
    private $repository;

    public function __construct(ShipmentRepository $repository
    )
    {
        $this->repo = $repository;
    }

    // Your methods for repository

    public function getSingleData($id)
    {

        if ($id !== NULL) {
            $data = $data = $this->repo->firstWhere(['id' => $id]);
        } else {
            $data = [];
        }
        return $data;
    }

    public function getOrderData($search_array = [])
    {
        return $this->repo->getOrderData($search_array);
    }

    public function create($request)
    {
    }

    public function update(int $id, array $requestArray)
    {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? sendResponseError('', __("Operation failed.")) :
                sendResponse('', __("Updated successfully."));
        } catch (\Exception $e) {
            return sendResponseError('', $e->getMessage());
        }
    }

    private function imageData($image, $id = NULL)
    {
        if ($id !== NULL) {
            $details = $this->getSingleData($id);
            return uploadImage($image, get_image_path('quotation'), $details->icon ?? '');
        } else {
            return uploadImage($image, get_image_path('quotation'));
        }
    }

    public function delete($id)
    {
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier) {
                return jsonResponse(TRUE)->message(__('Order deleted successfully.'));
            } else {
                return jsonResponse(TRUE)->message(__('Order delete failed.'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->default();
        }

    }

    public function productionShipmentComplete($request)
    {
        $quotation = $this->repo->getSingleRowData(['id' => $request->id, 'status'=>STATUS_IN_SHIPMENT]);
        if(!$quotation) return sendResponseError('', __('Invalid quotation.'));
        if($quotation && $quotation->status >= STATUS_SHIPPED_COMPLETED) {
            return sendResponseError('', __('Already sent to shipment completed.'));
        }
        try {
            $ok = $this->repo->updateWhere(['id' => $quotation->id], [
                'status' => STATUS_SHIPPED_COMPLETED,
                'in_staging' => QUOTATION_STAGE_IN_SHIPMENT
            ]);
            if (!$ok) throw new \Exception();
            return sendResponse('',__('Sent to Shipment Complete successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('','', $e->getMessage());
        }
    }
}
