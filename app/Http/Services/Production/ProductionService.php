<?php

namespace App\Http\Services\Production;

use App\Http\Repositories\Production\ProductionRepository;
use App\Http\Services\BaseService;
use App\Models\Crm\CrmQuotation;
use App\Models\Product\Product;
use phpDocumentor\Reflection\Type;

class ProductionService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CrmQuotation/PurchaseRepository $repository
     */
    private $repository;

    public function __construct(ProductionRepository $repository
    )
    {
        $this->repo = $repository;
    }

    public function getOrderData($search_array = [])
    {
        return $this->repo->getOrderData($search_array);
    }

    public function getProductionCompleteData($search_array = [])
    {
        return $this->repo->getProductionCompleteData($search_array);
    }

    public function getDashboardData(){
        return $this->repo->getDashboardData();
    }

    public  function getProductionCount($type=''){
        if ($type == 'completed') {
            return $this->repo->getDashboardData()->production_completed ?? 0;
        }elseif ($type == 'production_running'){
            return $this->repo->getDashboardData()->production_running ?? 0;
        }elseif ($type == 'in_shipment'){
            return $this->repo->getDashboardData()->in_shipment ?? 0;
        }elseif ($type == 'expire_today'){
            return $this->repo->getDashboardData()->expire_today ?? 0;
        }elseif ($type == 'expire_tomorrow'){
            return $this->repo->getDashboardData()->expire_tomorrow ?? 0;
        }elseif ($type == 'expired'){
            return $this->repo->getDashboardData()->expired ?? 0;
        }else{
            return $this->repo->getDashboardData();
        }
    }

    public function productionExpireToday($search_data=[]){
        return $this->repo->getDataFromOrderToProduction(['crm_quotations.expired_at'=> date('Y-m-d')],$search_data);
    }

    public function productionExpireTomorrow($search_data=[]){
        return $this->repo->getDataFromOrderToProduction(['crm_quotations.expired_at'=> date("Y-m-d", strtotime('tomorrow'))],$search_data);
    }

    public function productionExpired($search_data=[]){
        return $this->repo->getDataFromOrderToProduction(['crm_quotations.expired_at'=>['<',date("Y-m-d")]],$search_data);
    }

}
