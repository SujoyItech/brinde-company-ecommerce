<?php

namespace App\Http\Services\Production;

use App\Http\Repositories\CRM\QuotationProductRepository;
use App\Http\Repositories\Production\CrmQuotationProductArrivalLogRepository;
use App\Http\Repositories\Production\LogisticRepository;
use App\Http\Services\BaseService;
use App\Http\Services\CrmStatusChangeMailService;
use App\Models\Crm\CrmQuotation;
use App\Models\CRM\CrmQuotationProduct;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LogisticService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param CrmQuotation/PurchaseRepository $repository
     */
    private $crmProductRepo, $crmProductArrivalRepo;

    public function __construct(LogisticRepository $repository, QuotationProductRepository $crmProductRepo,
                                CrmQuotationProductArrivalLogRepository $crmProductArrivalRepo)
    {
        $this->repo = $repository;
        $this->crmProductRepo = $crmProductRepo;
        $this->crmProductArrivalRepo = $crmProductArrivalRepo;
    }


    public function getOrderData($search_array = [])
    {
        return $this->repo->getOrderData($search_array);
    }

    public function saveArrivalQuantity($request) {
        $crmProductId = $request->crm_product_id;
        $arrivalQuantities = $request->arrival_quantity;
        $arrivedAts = $request->arrived_at;
        $notes = $request->note;
        $crmProduct = $this->crmProductRepo->find($crmProductId);
        if (!$crmProduct) return sendResponseError('', __('Invalid product.'));
        if (!$arrivalQuantities) return sendResponseError('', __('No quantity send.'));
        $crmProductArrivalLogs = [];
        $crmProductArriveStatus = STATUS_PARTIALLY_ARRIVED;
        $quantitySum = 0;
        foreach ($arrivalQuantities as $key => $arrivalQuantity) {
            if(!$arrivalQuantity) continue;
            $crmProductArrivalLogs[] = [
                'crm_quotation_product_id' => $crmProductId,
                'arrival_quantity' => $arrivalQuantity,
                'arrived_at'=> $arrivedAts[$key],
                'note' => $notes[$key],
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ];
            $quantitySum += $arrivalQuantity;
        }
        if (count($crmProductArrivalLogs) == 0) return sendResponseError('', __('No valid quantity sent.'));
        if($crmProduct->quantity < $quantitySum) {
            return sendResponseError('', __('Arrived quantity can\'t be greater than purchase quantity.'));
        } else if($crmProduct->quantity == $quantitySum) {
            $crmProductArriveStatus = STATUS_ARRIVED;
        }
        try {
            DB::beginTransaction();
            $this->crmProductArrivalRepo->deleteWhere(['crm_quotation_product_id' => $crmProductId]);
            $this->crmProductArrivalRepo->insert($crmProductArrivalLogs);
            $this->crmProductRepo->updateWhere(['id'=>$crmProductId], ['arrived'=>$crmProductArriveStatus]);
            $quotation = $this->repo->find($request->quotation_id);
            $products = $quotation->quotation_products;
            $quotationStatus = STATUS_ORDER_PARTIALLY_ARRIVED;
            $this->sendArrivalStatus($quotation->id);
            $allArrived = $this->allArriveCheck($products);
            if ($allArrived) {
                $quotationStatus = STATUS_ORDER_ARRIVED_DONE;
            }
            if($quotation->status != $quotationStatus) {
                $ok = $this->repo->updateWhere(['id' => $request->quotation_id], ['status' => $quotationStatus]);
                $quotation->status = $quotationStatus;
                if (!$ok) throw new \Exception();
            }
            DB::commit();
            return $this->renderProductListAndSubmitButton($quotation, $products);
        } catch (\Exception $e) {
            DB::rollBack();
            return sendResponseError('','', $e->getMessage());
        }
    }

    private function sendArrivalStatus($id){
        CrmStatusChangeMailService::productFullArrivalStatus($id);
    }

    private function renderProductListAndSubmitButton($quotation, $products) {
        $productList = view('admin.production.logistic.purchase_product_list', ['products' => $products])->render();
        if ($quotation->budget_printing_enable == TRUE){
            $sendProduction = view('admin.production.logistic.send_production_button', ['item' => $quotation])->render();
        }else{
            $sendProduction = view('admin.production.logistic.send_shipment_button', ['item' => $quotation])->render();
        }
        return sendResponse(['product_list' => $productList, 'send_production_button' => $sendProduction], __('Saved successfully.'));
    }

    private function allArriveCheck($products) {
        $allArrived = true;
        foreach ($products as $product) {
            if($product->arrived != STATUS_ARRIVED) {
                $allArrived = false;
                break;
            }
        }
        return $allArrived;
    }

    public function sendToProduction($code) {
        $quotation = $this->repo->getSingleRowData(['purchase_reference' => $code, 'status'=>['>=',STATUS_ORDER_PURCHASED_DONE]]);
        if(!$quotation) return sendResponseError('', __('Invalid quotation.'));
        if($quotation && $quotation->status > STATUS_ORDER_ARRIVED_DONE){
            return sendResponseError('', __('Already sent to production.'));
        }
        $products = $quotation->quotation_products;
        $allArrived = $this->allArriveCheck($products);
        try {
            if ($allArrived) {
                $ok = $this->repo->updateWhere(['id' => $quotation->id], [
                    'status' => STATUS_ORDER_SENT_TO_PRODUCTION,
                    'in_staging' => QUOTATION_STAGE_IN_PRODUCTION,
                    'production_reference' => formatProductionReference(referenceCodeEdit($quotation->quote_reference, 2))
                ]);
                if (!$ok) throw new \Exception();
            } else {
                return sendResponseError('', __('All products are not arrived.'));
            }
            $this->sendToProductionMail($quotation->id);
            return sendResponse(['redirect'=>route('productionPurchaseList')],__('Sent to production successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('','', $e->getMessage());
        }
    }

    private function sendToProductionMail($id){
        CrmStatusChangeMailService::sendToProductionMail($id);
    }

    public function sendToShipment($id) {
        $quotation = $this->repo->getSingleRowData(['id' => $id, 'status'=>['>=',STATUS_ORDER_PURCHASED_DONE]]);
        if(!$quotation) return sendResponseError('', __('Invalid quotation.'));
        if($quotation && $quotation->in_staging == QUOTATION_STAGE_IN_SHIPMENT) {
            return sendResponseError('', __('Already sent to shipment.'));
        }
        $products = $quotation->quotation_products;
        $allArrived = $this->allArriveCheck($products);
        try {
            if ($allArrived) {
                $ok = $this->repo->updateWhere(['id' => $quotation->id], [
                    'status' => STATUS_IN_SHIPMENT,
                    'in_staging' => QUOTATION_STAGE_IN_SHIPMENT
                ]);
                if (!$ok) throw new \Exception();
            } else {
                return sendResponseError('', __('All products are not arrived.'));
            }
            $this->sendToShipmentMail($quotation->id);
            return sendResponse('',__('Sent to Shipment successfully.'));
        } catch (\Exception $e) {
            return sendResponseError('','', $e->getMessage());
        }
    }

    private function sendToShipmentMail($id){
        CrmStatusChangeMailService::sendToShipmentMail($id);
    }

}
