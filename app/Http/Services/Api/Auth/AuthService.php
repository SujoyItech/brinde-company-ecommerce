<?php

namespace App\Http\Services\Api\Auth;

use App\Http\Repositories\Api\Auth\AuthRepository;
use App\Http\Services\BaseService;
use App\Http\Services\MailService;
use App\Models\CRM\CrmCustomer;
use App\Models\CRM\CrmCustomerVerificationCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Api/Auth/AuthRepository $repository
     */
    public function __construct(AuthRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository
    public function login(Request $request){

        $customer = CrmCustomer::where('email',$request->email)->first();
        if (isset($customer) && Hash::check($request->password,$customer->password)) {
            $status = $this->statusCheck($customer);
            if ($status->getStatus() == TRUE){
                $token_info = $this->accessTokenProcess($customer, $request);
                $user_data = [
                    'access_token'   => $token_info['access_token'],
                ];
                return jsonResponse(TRUE)->message(__('User get successfully!'))->data($user_data);
            }else{
                $user_data['user'] = $customer;
                if ($customer->email_verified == FALSE) {
                    return jsonResponse(TRUE)->message($status->getMessage())->data($user_data);
                }else{
                    return jsonResponse(FALSE)->message($status->getMessage())->data($user_data);
                }
            }

        } else {
            return jsonResponse(FALSE)->message(__('Email or Password Not matched!'));
        }
    }

    public function register(array $requestArray){
        DB::beginTransaction();
        try {
            $customer = CrmCustomer::create($this->getRegistrationData($requestArray));
            if (isset($customer)) {
                $this->sendCustomerVerificationMail($customer);
            }
            DB::commit();
            return jsonResponse(TRUE)->message(__('Registration successful.Please verify your email to login.'));
        }catch (\Exception $exception){
            DB::rollBack();
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    private function getRegistrationData(array $requestArray){
        $customer_data =[
            'name' => $requestArray['name'],
            'contact_name' => $requestArray['contact_name'],
            'customer_code'=> formatCustomerReference(textSlugify($requestArray['contact_name']), getNextPrimaryId('crm_customers')),
            'email' => $requestArray['email'],
            'phone' => $requestArray['phone'],
            'password' => bcrypt($requestArray['password']),
            'tin_number' => $requestArray['tin_number'],
            'postcode' => $requestArray['postcode'],
            'locality' => $requestArray['locality'],
            'address' => $requestArray['address'],
            'remember_token' => md5($requestArray['email'] . uniqid() . randomString(5)),
            'status' => INACTIVE
        ];
        return $customer_data;
    }

    public function sendCustomerVerificationMail($customer) {
        $mail_key = randomNumber(6);
        $insert_code = [
            'customer_id' => $customer->id,
            'code' => $mail_key,
            'type' => 1,
            'status' => INACTIVE,
            'expired_at' => date('Y-m-d', strtotime('+15 days'))
        ];
        CrmCustomerVerificationCode::where('customer_id',$customer->id)->delete();
        CrmCustomerVerificationCode::create($insert_code);
        $userName = $customer->name;
        $userEmail = $customer->email;
        $subject = __('Top Brinde Email Verification.');
        $userData['message'] = __('Hello! ') . $userName . __(' Please Verify Your Email.');
        $userData['verification_code'] = $mail_key;
        $userData['email'] = $userEmail;
        $userData['name'] = $userName;
        MailService::sendMailProcess('admin.mail.email.send_verification_mail',$userData,$userEmail,$subject);
    }

    public function customerVerifyEmail($code){
        if (!empty($code)) {
            $customer_verification = CrmCustomerVerificationCode::where(['code' => $code])->first();
            if ($customer_verification) {
                DB::beginTransaction();
                try {
                    CrmCustomerVerificationCode::where(['id' => $customer_verification->id])->update(['status' => STATUS_SUCCESS]);
                    $customer = CrmCustomer::where('id',$customer_verification->customer_id)->first();
                    if (!empty($customer)) {
                        if ($customer->email_verified == INACTIVE) {
                            CrmCustomer::where('id',$customer->id)->update(['email_verified_at'=>Carbon::now(),'email_verified' => STATUS_ACTIVE, 'status' => STATUS_ACTIVE]);
                            $response = jsonResponse(TRUE)->message(__('Email successfully verified.'));
                        } else {
                            $response = jsonResponse(TRUE)->message(__('You already verified email!'));
                        }
                    } else {
                        $response = jsonResponse(FALSE)->message(__('Email Verification Failed'));
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    $response =  jsonResponse(FALSE)->message($e->getMessage());
                }
            } else {
                $response =  jsonResponse(FALSE)->message(__('Verification Code Not Found!'));
            }
            return $response;
        }else{
            return jsonResponse(FALSE)->message(__('Verification Code Not Found!'));
        }
    }

    public function statusCheck($user){
        try {
            if ($user->email_verified == INACTIVE) {
                $this->sendCustomerVerificationMail($user);
                return jsonResponse(FALSE)->message(__('Your account is not verified. Please verify your email.'));
            }else{
                if ($user->status == STATUS_ACTIVE){
                    return jsonResponse(true);
                }elseif ($user->status == INACTIVE) {
                    return jsonResponse(FALSE)->message(__('Your account is inactive. Please change your password or contact with admin.'));
                }
                elseif ($user->status == USER_BLOCKED){
                    return jsonResponse(FALSE)->message(__('You are blocked. Contact with admin.'));
                } elseif ($user->status == USER_SUSPENDED) {
                    return jsonResponse(FALSE)->message(__('Your Account has been suspended. please contact with admin to active again!'));
                }else{
                    return jsonResponse(true)->default();
                }
            }

        }catch (\Exception $exception){
            return jsonResponse(true)->default();
        }
    }

    public function accessTokenProcess($user, $request) {
        $data = [];
        $token_data = $user->createToken($request->email)->toArray();
        $data['access_token'] = $token_data['accessToken'];
        $token_attribute = $token_data['token']->toArray();
        $data['access_token_id'] = $token_attribute['id'];
        if ($request->device_token != '') {
            DB::table('oauth_access_tokens')
              ->where('id', $data['access_token_id'])
              ->update(['device_token' => $request->device_token, 'driver' => $request->driver]);
        }
        return $data;
    }

    public function sendForgetPasswordMail($email) {
        $customer = CrmCustomer::where('email', $email)->first();
        if (isset($customer)) {
            DB::beginTransaction();
            try {
                $reset_key = randomNumber(6);
                $customer->update(['reset_password_code' => $reset_key]);
                $this->sendRestPasswordMail($customer, $reset_key);
                DB::commit();
                return jsonResponse(TRUE)->message(__('Password reset mail sent successfully.'));
            } catch (\Exception $exception) {
                DB::rollBack();
                return jsonResponse(FALSE)->message($exception->getMessage());
            }
        }else{
            return jsonResponse(FALSE)->message(__('Sorry! no user found'));
        }
    }

    private function sendRestPasswordMail($customer,$reset_key){
        $userName = $customer->name;
        $userEmail = $customer->email;
        $subject = __('Top Brinde Reset Password.');
        $userData['message'] = __('Hello! ') . $userName . __(' Please Reset Your Password By Using This Code.');
        $userData['reset_password_code'] = $reset_key;
        $userData['email'] = $userEmail;
        $userData['name'] = $userName;
        MailService::sendMailProcess('admin.mail.email.customer.reset_password_mail',$userData,$userEmail,$subject);
    }

    public function resetPassword(array $requestArray){
        if (!empty($requestArray['reset_password_code'])) {
            DB::beginTransaction();
            try {
                $customer = CrmCustomer::where(['reset_password_code'=>$requestArray['reset_password_code']])->first();
                if (isset($customer)) {
                    $customer->update(['password'=>bcrypt($requestArray['password']),'reset_password_code'=>'']);
                    DB::commit();
                    return jsonResponse(TRUE)->message(__('Password changed successfully.'));
                }else{
                    return jsonResponse(FALSE)->message(__('Code is not correct.Please enter valid code.'));
                }
            }catch (\Exception $exception){
                DB::rollBack();
                return jsonResponse(FALSE)->message($exception->getMessage());
            }
        }else {
            return jsonResponse(FALSE)->message('Sorry! code not found.');
        }
    }
}
