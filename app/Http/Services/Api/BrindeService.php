<?php

namespace App\Http\Services\Api;


use App\Models\Contact;
use App\Models\Faq;
use App\Models\Message;
use App\Models\Subscriber;
use App\Models\Team;

class BrindeService
{
    public function subscribe(array $requestArray){
        try {
            $requestArray['status'] = ACTIVE;
            $response = Subscriber::updateOrInsert(['email'=>$requestArray['email']],$requestArray);
            if ($response) {
                return jsonResponse(TRUE)->message(__('Email subscribed successfully.'));
            }
            return jsonResponse(FALSE)->message(__('Email subscribe failed'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function unSubscribe($email){
        try {
            $subscriber = Subscriber::where('email',$email)->first();
            if (isset($subscriber)) {
                Subscriber::where(['email'=>$email])->update(['status'=>INACTIVE]);
                return jsonResponse(TRUE)->message(__('Email unsubscribed successfully.'));
            }else{
                return jsonResponse(FALSE)->message(__('This email is not subscribed.'));
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function sendMessage(array $requestArray){
        try {
            $requestArray['status'] = INACTIVE;
            $response = Message::create($requestArray);
            if ($response) {
                return jsonResponse(TRUE)->message(__('Message sent successfully.'));
            }
            return jsonResponse(FALSE)->message(__('Message send failed'));
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }
    public function webExtraContent(){
        try {
            $settings = __options(['web_content_settings']);
            $data['terms_condition'] = $settings->terms_condition ?? '';
            $data['privacy_policy'] = $settings->privacy_policy ?? '';
            $data['about_us'] = $settings->about_us ?? '';
            $data['why_choose_us'] = $settings->why_choose_us ?? '';
            return jsonResponse(TRUE)->message(__('Web content extra data get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function faqsApi(){
        try {
            $data['faqs'] = Faq::where('status',ACTIVE)->get();
            return jsonResponse(TRUE)->message(__('Faqs get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }
    public function teamsApi(){
        try {
            $data['teams'] = Team::where('status',ACTIVE)->get();
            return jsonResponse(TRUE)->message(__('Teams data get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }
    public function contactsApi(){
        try {
            $data['contacts'] = Contact::where('status',ACTIVE)->get();
            return jsonResponse(TRUE)->message(__('Contact data get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function socialLinkApis(){
        try {
            $data['social_links'] = __options(['social_settings']);
            return jsonResponse(TRUE)->message(__('Social data get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }
    public function countryList(){
        try {
            $data['countries'] = countries();
            return jsonResponse(TRUE)->message(__('Country data get successfully.'))->data($data);
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }
}
