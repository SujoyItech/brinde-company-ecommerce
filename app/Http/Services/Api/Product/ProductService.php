<?php

namespace App\Http\Services\Api\Product;


use App\Http\Repositories\Api\Product\ProductRepository;
use App\Http\Repositories\Product\CategoryRepository;

use App\Http\Repositories\Product\CombinationRepository;
use App\Http\Repositories\Product\DeliveryTypeRepository;
use App\Http\Repositories\Product\ProductTagRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Product\CategoryService;
use PhpParser\Node\Expr\Array_;

class ProductService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Api/Product/ProductRepository $repository
     */

    private $product_api_repository,$category_service,$category_repository,$combination_repository,$delivery_type_repo,$tag_repo;

    public function __construct(
        ProductRepository $product_repository,
        ProductTagRepository $tag_repository,
        CategoryRepository $category_repository,
        CategoryService $category_service,
        CombinationRepository $combination_repository,
        DeliveryTypeRepository $delivery_type_repo
    )
    {
        $this->product_api_repository = $product_repository;
        $this->category_repository = $category_repository;
        $this->category_service = $category_service;
        $this->combination_repository = $combination_repository;
        $this->delivery_type_repo = $delivery_type_repo;
        $this->tag_repo = $tag_repository;
    }

    public function categoryList($brand_id,$parent_id){
        try {
            $categories = $this->category_repository->getBrandCategories($brand_id);
            if (!empty($categories[0])) {
                $prepared_category = $this->category_service->prepareBrandCategoryData($categories);
                $data['image_url']= asset(get_image_path('category'));
                $data['categories']= buildTreeForArray($prepared_category,$parent_id);
                return jsonResponse(TRUE)->message(__('Category get successfully.'))->data($data);
            } else {
                return jsonResponse(TRUE)->message(__('Category not found'));
            }

        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function product($slug){
        try {
            $product = $this->product_api_repository->getProductDataApiById($slug);
            if (isset($product)) {
                $product_id = $product->id;
                $tags =  $this->tag_repo->getData(['product_id'=>$product_id],['tag_id']);
                $tags_array = [];
                if (isset($tags)){
                    $tag_data = [];
                    foreach ($tags as $tag){
                        $tag_data[] = $tag->tag_id;
                    }
                    $tags_array = array_values(array_unique($tag_data));
                }
                $search_data['tag_id'] =$tags_array;
                $search_data['exclude_id'] = [$product_id];

                $data['products'] = $this->product_api_repository->getProductDataById($product);
                $similiar_product = $this->product_api_repository->getAllProductListApi($search_data,6);
                $data['similiar_product']= $this->product_api_repository->getProductListData($similiar_product);
                return jsonResponse(TRUE)->message(__('Product get successfully.'))->data($data);
            }else {
                return jsonResponse(TRUE)->message(__('Product not found!'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function productList($search_data,$brand_slug=''){
        $products = $this->product_api_repository->getAllProductListApi($search_data,20, $brand_slug);
        try {
            if (isset($products)) {
                $data['products'] = $this->product_api_repository->getProductListData($products);
                return jsonResponse(TRUE)->message(__('Products get successfully.'))->data($data);
            } else {
                return jsonResponse(TRUE)->message(__('Products not found'));
            }

        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function getColorList($parent_category_id){
        try {
            $colors= $this->combination_repository->getColorDataApi($parent_category_id);
            if (!empty($colors[0])) {
                $color_array = [];
                foreach ($colors as $key => $color) {
                    $color_array[$key]['id'] = $color->combination_id;
                    $color_array[$key]['name'] =  json_decode($color->color_name)->en ?? '';
                    $color_array[$key]['class_name'] = $color->class_name ;
                    $color_array[$key]['color_code'] = $color->color_code;
                }
                $data['colors'] = $color_array;
                return jsonResponse(TRUE)->message(__('Colors get successfully.'))->data($data);
            } else {
                return jsonResponse(TRUE)->message(__('Color not found.'));
            }

        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }

    public function getDeliveryTypes($parent_category_id){
        try {
            $delivery_types = $this->delivery_type_repo->getDeliveryTypesApi($parent_category_id);
            if (!empty($delivery_types[0])) {
                $data['delivery_types'] = $this->delivery_type_repo->getDeliveryTypesData($delivery_types);
                return jsonResponse(TRUE)->message(__('Delivery types get successfully.'))->data($data);
            } else {
                return jsonResponse(TRUE)->message(__('Delivery type not found.'));
            }
        } catch (\Exception $exception) {
            return jsonResponse(FALSE)->message($exception->getMessage());
        }
    }
}
