<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\ProductCategory;
use Illuminate\Support\Facades\DB;

class ProductCategoryRepository extends BaseRepository
{
    /**
     * ProductCategoryRepository constructor.
     * @param ProductCategory $model
     */
    public function __construct(ProductCategory $model)
    {
        parent::__construct($model);
    }

    public function duplicateProductCategories($newProductId, $oldProductId) {
        $query = "INSERT INTO `product_categories` (`product_id`, `category_id`) SELECT :product_id as `product_id`,
                        `category_id` FROM `product_categories` WHERE `product_id`=:old_product_id;";
        return DB::statement($query, ['product_id'=>$newProductId, 'old_product_id'=>$oldProductId]);
    }
}
