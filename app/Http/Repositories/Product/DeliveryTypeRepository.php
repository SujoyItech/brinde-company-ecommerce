<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Category;
use App\Models\Product\DeliveryType;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;

class DeliveryTypeRepository extends BaseRepository
{

    /**
     * DeliveryTypeRepository constructor.
     * @param DeliveryType $model
     */
    public function __construct(DeliveryType $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getDeliveryDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }

    public function getDeliveryTypesApi($parent_category_id){
        $categories = [];
        if ($parent_category_id !== NULL){
            $categories = Category::where(function ($query) use ($parent_category_id){
                $query->where('id',$parent_category_id)->orWhere('parent_id',$parent_category_id);
            })->where('status',STATUS_ACTIVE)->get()->toArray();
        }
        $query = Product::select('delivery_types.id','delivery_types.name','delivery_types.time','delivery_types.icon')
                        ->join('delivery_types',function ($join){
                            $join->on('products.delivery_type_id','=','delivery_types.id')
                                 ->where('delivery_types.status','=',DB::raw(STATUS_ACTIVE))->where('delivery_types.deleted_at',NULL);
                        })
                        ->join('product_categories','products.id','=','product_categories.product_id');
        if(!empty($parent_category_id)){
            $query = $query->whereIn('product_categories.category_id',$categories);
        }
        $query = $query->groupBy('delivery_types.id')->get();
        return $query;

    }

    public function getDeliveryTypesData($delivery_types){
        $delivery_type_list = [];
        foreach ($delivery_types as $key=>$delivery_type){
            $delivery_type_list[$key]['id'] = $delivery_type->id;
            $delivery_type_list[$key]['name'] = $delivery_type->name;
            $delivery_type_list[$key]['time'] = $delivery_type->time;
            $delivery_type_list[$key]['icon'] = !empty($delivery_type->icon) ? asset(get_image_path('delivery')).'/'.$delivery_type->icon : '';
        }
        return $delivery_type_list;
    }
}
