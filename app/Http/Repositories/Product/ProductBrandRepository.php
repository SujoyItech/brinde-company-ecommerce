<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\ProductBrand;

class ProductBrandRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product\ProductBrand $model
       */
    public function __construct(ProductBrand $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
