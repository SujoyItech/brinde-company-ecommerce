<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\CombinationType;

class CombinationTypeRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/CombinationType $model
       */
    public function __construct(CombinationType $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getCombinationTypeDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }
}
