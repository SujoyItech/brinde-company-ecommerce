<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Supplier;

class SupplierRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/Supplier $model
       */
    public function __construct(Supplier $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getSuppliersDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }
}
