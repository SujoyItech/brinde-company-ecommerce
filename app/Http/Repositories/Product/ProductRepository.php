<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\IFTTTHandler;

class ProductRepository extends BaseRepository {
    /**
     * Instantiate repository
     *
     * @param Product/Product $model
     */
    public function __construct(Product $model) {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getDashboardData() {
        $data['no_image_query'] = DB::select(DB::raw("SELECT
            COUNT(*) AS count_number
            FROM (
            SELECT products.id
            FROM products
            LEFT JOIN product_combinations ON products.id = product_combinations.product_id
            GROUP BY products.id
            HAVING SUM( product_combinations.media_url IS NULL ) > 0 AND SUM( product_combinations.media_url IS NOT NULL ) = 0
            ) AS no_image_table"))[0]->count_number;

        $data['no_color_query'] = DB::select(DB::raw("SELECT
            COUNT(*) AS count_number
            FROM (
            SELECT products.id
            FROM products
            LEFT JOIN product_combinations ON products.id = product_combinations.product_id AND product_combinations.combination_type_id = 1
            GROUP BY products.id
            HAVING SUM( product_combinations.combination_id IS NULL ) > 0 AND SUM( product_combinations.combination_id IS NOT NULL ) = 0
            ) AS no_color_table"))[0]->count_number;

        $data['no_category_query'] = DB::table('products')->select(DB::raw("COUNT(products.id) as no_category"))
            ->leftJoin('product_categories','products.id','=','product_categories.product_id')
            ->where('product_categories.category_id',NULL)->first()->no_category;

        $data['no_tags_query'] = DB::table('products')->select(DB::raw("COUNT(products.id) as no_tags"))
            ->leftJoin('product_tags','products.id','=','product_tags.product_id')
            ->where('product_tags.tag_id',NULL)->first()->no_tags;

        return $data;
    }

    public function getProductData($request, $search_array, $whereIn=[]) {
        $query = DB::table('products');
        $query->select('products.*', 'delivery_types.name AS delivery_type_name',
            'suppliers.name AS supplier_name',
            DB::raw("GROUP_CONCAT(DISTINCT product_image.media_type) AS media_type,
             GROUP_CONCAT(DISTINCT product_image.media_url) AS featured_image"),
            DB::raw("GROUP_CONCAT(DISTINCT categories.name) AS category_names,
            GROUP_CONCAT(DISTINCT brands.name) AS brand_names, GROUP_CONCAT(DISTINCT tags.name) AS tag_names"),
            DB::raw("GROUP_CONCAT(
                                    DISTINCT(
                                        CONCAT(IFNULL(comb_for_col.class_name,'NO_CLASS'),
                                        '--',
                                        IFNULL(comb_for_col.color_code,'NO_COLOR'),
                                        '--',
                                        comb_for_col.name)
                                    ) SEPARATOR '***'
                                ) AS colors")
/*            DB::raw("GROUP_CONCAT(
                                    DISTINCT(
                                        CONCAT(IFNULL(comb_for_sz.class_name,'NO_CLASS'),
                                        '--',
                                        comb_for_sz.name)
                                    ) SEPARATOR '***'
                                ) AS size"),*/
        );
        $query->leftJoin('suppliers', function ($join) {
            $join->on('products.supplier_id', '=', 'suppliers.id')
                 ->where('suppliers.deleted_at', '=', NULL);
        });
        $query->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id');
        $query->leftJoin('delivery_types', function ($join) {
            $join->on('products.delivery_type_id', '=', 'delivery_types.id')
                 ->where('delivery_types.deleted_at', '=', NULL);
        });
        $query->leftJoin('product_brands', 'products.id', '=', 'product_brands.product_id');
        $query->leftJoin('product_tags', 'products.id', '=', 'product_tags.product_id');
        //for colors
        $query->leftJoin('product_combinations as product_image', function ($join) {
            $join->on('products.id', '=', 'product_image.product_id')
                 ->where('product_image.is_featured', '=', DB::raw(ACTIVE));
        });
        $query->leftJoin('product_combinations as prd_comb_col', function ($join) {
            $join->on('products.id', '=', 'prd_comb_col.product_id')
                 ->where('prd_comb_col.combination_type_id', '=', DB::raw(COLOR_ID))
                 ->where('prd_comb_col.status', '=', DB::raw(ACTIVE));
        });
        $query->leftJoin('combinations as comb_for_col', function ($join) {
            $join->on('comb_for_col.id', '=', 'prd_comb_col.combination_id')
                 ->where('comb_for_col.deleted_at', '=', NULL);
        });
        //
        //for size
       /*$query->leftJoin('product_combinations as prd_comb_sz', function ($join) {
            $join->on('products.id', '=', 'prd_comb_sz.product_id')
                 ->where('prd_comb_sz.combination_type_id', '=', DB::raw(SIZE_ID));
        });
       $query->leftJoin('combinations as comb_for_sz', function ($join) {
            $join->on('comb_for_sz.id', '=', 'prd_comb_sz.combination_id')
                 ->where('comb_for_sz.deleted_at', '=', NULL);
        });*/
       $query->leftJoin('brands', 'product_brands.brand_id', '=', 'brands.id');
       $query->leftJoin('tags', 'product_tags.tag_id', '=', 'tags.id');
       $query->leftJoin('categories', 'product_categories.category_id', '=', 'categories.id');


        if (isset($search_array) && !empty($search_array)) {

            foreach ($search_array as $search) {
                if ($search['value'] !== NULL) {
                    if ($search['name'] == 'name_description') {
                       $query->where(function ($inner_query) use ($search) {
                            $inner_query->where('products.name', 'like', '%' . $search['value'] . '%')
                                        ->orWhere('products.description', 'like', '%' . $search['value'] . '%');
                        });
                    }elseif ($search['name'] == 'category_id') {
                        $query->where('product_categories.category_id', $search['value']);
                    }else if ($search['name'] == 'special_case') {
                        if ($search['value'] == 'n_ct') {
                           $query->where('product_categories.category_id', NULL);
                        } else if ($search['value'] == 'n_t') {
                           $query->where('product_tags.tag_id', NULL);
                        } else if ($search['value'] == 'n_cr') {
                           $query->whereNotIn('products.id', function ($subQuery) use ($search) {
                                $subQuery->select(DB::raw('DISTINCT product_id'))
                                         ->from('product_combinations')
                                         ->where('combination_type_id', COLOR_ID);
                            });
                        } else if ($search['value'] == 'n_m') {
                           $query->whereNotIn('products.id', function ($subQuery) use ($search) {
                                $subQuery->select(DB::raw('DISTINCT product_id'))
                                         ->from('product_combinations')
                                         ->where('media_type', '!=', NULL);
                            });
                        }
                    } else {
                       $query->where('products.' . $search['name'], 'like', '%' . $search['value'] . '%');
                    }
                }
            }
        }
        return $query->groupBy('products.id');
    }


    public function changeStatus($requestArray) {
        return $this->model::where('id', $requestArray['id'])->update(['status' => $requestArray['status']]);
    }

    public function getSlug($slug, $id) {
        $slug = $this->model::where('slug', $slug);
        $slug = !empty($id) ? $slug->where('id', '!=', $id) : $slug;
        return $slug->first();
    }

    public function getReference($reference, $id) {
        $reference = $this->model::where('reference', $reference);
        $reference = !empty($id) ? $reference->where('id', '!=', $id) : $reference;
        return $reference->first();
    }

    public function getProductSearchedData($searchItem) {
        return $this->model::select('products.id', 'products.name', 'products.description', 'products.reference', 'product_combinations.media_url')
                           ->leftJoin('product_combinations', function ($join) {
                               $join->on('products.id', '=', 'product_combinations.product_id');
                               $join->where('product_combinations.is_featured', '=', ACTIVE);
                           })->where('products.status', ACTIVE)
                           ->where(function ($query) use ($searchItem) {
                               $query->where('products.name', 'like', '%' . $searchItem . '%')
                                     ->orWhere('products.reference', 'like', '%' . $searchItem . '%')
                                     ->orWhere('products.slug', 'like', '%' . $searchItem . '%');
                           })->limit(10)->get();
    }

    public function getProductSearchByCategory($category_id, $searchItem) {
        return $this->model::select('products.id', 'products.name', 'products.description', 'products.reference', 'products.slug', 'product_combinations.media_url')
                           ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
                           ->leftJoin('product_combinations', function ($join) {
                               $join->on('products.id', '=', 'product_combinations.product_id');
                               $join->where('product_combinations.is_featured', '=', ACTIVE);
                           })->where('products.status', ACTIVE)
                           ->where('product_categories.category_id', '=', $category_id)
                           ->where(function ($query) use ($searchItem) {
                               $query->where('products.name', 'like', '%' . $searchItem . '%')
                                     ->orWhere('products.reference', 'like', '%' . $searchItem . '%')
                                     ->orWhere('products.slug', 'like', '%' . $searchItem . '%');
                           })->get();

    }

    public function getProductByCategory($category_id) {
        return $this->model::select('products.id', 'products.name', 'products.description', 'products.reference', 'products.slug')
                           ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
                           ->where('products.status', ACTIVE)
                           ->whereIn('product_categories.category_id', $category_id)
                           ->get();
    }

    public function getProductListByWhereIn($id = [], $slug = []) {
        $query = $this->model::select('products.id', 'products.name', 'products.description', 'products.reference', 'products.slug', 'product_combinations.media_url')
                             ->leftJoin('product_combinations', function ($join) {
                                 $join->on('products.id', '=', 'product_combinations.product_id');
                                 $join->where('product_combinations.is_featured', '=', ACTIVE);
                             })->where('products.status', ACTIVE);
        $query = !empty($id) ? $query->whereIn('products.id', $id) : $query;
        $query = !empty($slug) ? $query->whereIn('products.slug', $slug) : $query;

        return $query->get();

    }


}
