<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\ProductTag;
use Illuminate\Support\Facades\DB;

class ProductTagRepository extends BaseRepository
{
    /**
     * ProductTagRepository constructor.
     * @param ProductTag $model
     */
    public function __construct(ProductTag $model)
    {
        parent::__construct($model);
    }

    public function duplicateProductTags($newProductId, $oldProductId) {
        $query = "INSERT INTO `product_tags` (`product_id`, `tag_id`) SELECT :product_id as `product_id`,
                        `tag_id` FROM `product_tags` WHERE `product_id`=:old_product_id;";
        return DB::statement($query, ['product_id'=>$newProductId, 'old_product_id'=>$oldProductId]);
    }
}
