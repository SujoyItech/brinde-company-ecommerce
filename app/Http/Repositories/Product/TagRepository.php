<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Tag;

class TagRepository extends BaseRepository
{

    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
    public function getTagDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }
}
