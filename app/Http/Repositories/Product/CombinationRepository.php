<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Category;
use App\Models\Product\Combination;
use App\Models\Product\Product;
use App\Models\Product\ProductCombination;
use Illuminate\Support\Facades\DB;

class CombinationRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/Combination $model
       */
    public function __construct(Combination $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getCombinationDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }

    public function getCombinationQuery() {
        return $this->model::leftJoin('combination_types','combinations.combination_type_id','=','combination_types.id')->select('combinations.*','combination_types.name as combination_type')->get();
    }

    public function getColorDataApi($parent_category_id){
        $categories = [];
        if ($parent_category_id !== NULL){
            $categories = Category::where(function ($query) use ($parent_category_id){
                $query->where('id',$parent_category_id)->orWhere('parent_id',$parent_category_id);
            })->where('status',STATUS_ACTIVE)->get()->toArray();
        }
        $query = ProductCombination::select('product_combinations.combination_id',
            DB::raw("GROUP_CONCAT(DISTINCT(combinations.name)) AS color_name,
                GROUP_CONCAT(DISTINCT(combinations.class_name)) AS class_name,
                GROUP_CONCAT(DISTINCT(combinations.color_code)) AS color_code
                "))
            ->join('combinations',function ($join){
                $join->on('product_combinations.combination_id','=','combinations.id');
                $join->on('product_combinations.combination_type_id','=',DB::raw(COLOR_ID));
            })
            ->join('products','product_combinations.product_id','products.id')
            ->join('product_categories','products.id','product_categories.product_id')
            ->where('combinations.deleted_at','=',NULL);
            if(!empty($parent_category_id)){
                $query = $query->whereIn('product_categories.category_id',$categories);
            }
        $query = $query->groupBy('product_combinations.combination_id')->get();

        return $query;

    }


}
