<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Brand;

class BrandRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/Store $model
       */
    public function __construct(Brand $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

   public function getBrandDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }
}
