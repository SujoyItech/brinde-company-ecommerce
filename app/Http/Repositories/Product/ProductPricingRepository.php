<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\ProductPricing;
use Illuminate\Support\Facades\DB;

class ProductPricingRepository extends BaseRepository
{
    public function __construct(ProductPricing $model)
    {
        parent::__construct($model);
    }

    public function duplicateProductPricings($newProductId, $oldProductId) {
        $query = "INSERT INTO `product_pricings` (`product_id`, `compare`, `quantity`, `price`, `status`, `created_at`,`updated_at`)
                    SELECT :product_id, `compare`, `quantity`, `price`, `status`, now(), now() FROM `product_pricings` WHERE `product_id`=:old_product_id;";
        return DB::statement($query, ['product_id'=>$newProductId, 'old_product_id'=>$oldProductId]);
    }
}
