<?php

namespace App\Http\Repositories\User;

use App\Http\Repositories\BaseRepository;
use App\Http\Services\MailService;
use App\Models\User;
use App\Models\UserBrand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param User/User $model
       */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getUserList(){
       return $this->model::select('users.*','roles.title as role_name')
                          ->leftJoin('roles','users.role','roles.id')
                          ->where('users.module_id','<>',MODULE_SUPER_ADMIN)
                          ->where('users.id','<>',Auth::user()->id)
                          ->orderBy('users.created_at','desc')->get();
    }
    public function getUserBrandList(){
       return $this->model::select('users.*','roles.title as role_name',DB::raw("GROUP_CONCAT(user_brands.brand_id) AS brand_id"))
                          ->leftJoin('user_brands',function ($join){
                              $join->on('users.id','=','user_brands.user_id')->where('user_brands.status','=',STATUS_ACTIVE);
                          })
                          ->leftJoin('roles','users.role','roles.id')
                          ->whereIn('users.module_id',[MODULE_CRM_MANAGEMENT,MODULE_PRODUCT_MANAGEMENT,MODULE_WEBSITE_MANAGEMENT])
                          ->groupBy('users.id')
                          ->orderBy('users.created_at','desc')
                          ->get();
    }
    public function getSalesmanList(){
       return $this->model::where('status', STATUS_ACTIVE)
                            ->where(function ($query) {
                              $query->where('role', '=', ROLE_SALES_MANAGER)
                                  ->orWhere('role', '=', ROLE_SALESMAN);
                          })->select('users.*')
                          ->orderBy('users.created_at','desc');
    }

    public function getUserDetails($id){
        return $this->model::where('id',$id)->first();
    }

    public function userPasswordChangeMail($user) {
        if ($user) {
            $userName = $user->name;
            $userEmail = $user->email;
            $subject = __('Reset Password');
            $data['name'] = $userName;
            $data['remember_token'] = $user->remember_token;
            MailService::sendResetPasswordMailProcess($userEmail, $data, $subject);
        }
    }

    public function userBrandCheck(array $requestArray){
        $check_brand = UserBrand::where(['user_id'=>$requestArray['user_id'],'brand_id'=>$requestArray['brand_id']])->first();

        if (isset($check_brand)){
            if ($requestArray['checked'] == 1){
                $response =  UserBrand::where('id',$check_brand->id)->update(['status'=>STATUS_ACTIVE]);
            }else{
                $response = UserBrand::where('id',$check_brand->id)->update(['status'=>INACTIVE]);
            }
        }else{
            $response = UserBrand::create(['user_id' =>$requestArray['user_id'],'brand_id' =>$requestArray['brand_id'],'status'=>STATUS_ACTIVE]);
        }
        return $response;
    }
}
