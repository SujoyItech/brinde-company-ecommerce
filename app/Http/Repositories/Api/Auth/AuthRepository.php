<?php

namespace App\Http\Repositories\Api\Auth;


use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmCustomer;

class AuthRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Api/Auth/Auth $model
       */
    public function __construct(CrmCustomer $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
