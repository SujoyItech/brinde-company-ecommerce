<?php

namespace App\Http\Repositories\Api\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Brand;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductCombination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductRepository extends BaseRepository {
    /**
     * Instantiate repository
     *
     * @param Api/Product/Product $model
     */
    public function __construct(Product $model) {
        parent::__construct($model);
    }

    // Your methods for repository
    public function getAllProductListApi($data, $pagination = 20,$brand_slug = '') {
        if (!empty($brand_slug)) {
            $brand = Brand::where('slug',$brand_slug)->first();
        }
        $query = DB::table('products')->select('products.id',
            'products.name',
            'products.description',
            'products.slug',
            'products.reference',
            'products.min_price',
            'products.additional_info',
            'products.supplier_id',
            'products.delivery_type_id',
            'products.status',
            'delivery_types.name as delivery_type_name',
            'delivery_types.icon as delivery_type_icon',
            DB::raw("GROUP_CONCAT(DISTINCT product_image.media_type) AS media_type,
             GROUP_CONCAT(DISTINCT product_image.media_url) AS featured_image,
	        GROUP_CONCAT(
	            DISTINCT (CASE WHEN comb.combination_type_id = 1 THEN
	            CONCAT( combination_types.id,'==',combinations.id,'==',combination_types.name,'==',combinations.name,'==',
                        IFNULL(combinations.class_name,'NO_CLASS'),'==',
                        IFNULL(combinations.color_code,'NO_COLOR'),'==')
                        END
	        ) SEPARATOR '|||') AS combination_color"));
        if (isset($brand)) {
            $query->join('product_brands', 'products.id', '=', 'product_brands.product_id');
        }
        $query->leftJoin('product_combinations as comb', function ($join) {
            $join->on('products.id', '=', 'comb.product_id')->where('comb.status',ACTIVE);
        });
        $query->leftJoin('product_combinations as product_image', function ($join) {
            $join->on('products.id', '=', 'product_image.product_id')
                 ->where('product_image.is_featured', '=', DB::raw(ACTIVE));
        });
        $query->leftJoin('combinations', function ($join) {
            $join->on('comb.combination_id', '=', 'combinations.id')
                 ->where('combinations.deleted_at', '=', NULL);
        });
        $query->leftJoin('combination_types', 'comb.combination_type_id', '=', 'combination_types.id');

        $query->leftJoin('product_categories', 'product_categories.product_id', '=', 'products.id');
        $query->leftJoin('categories', 'product_categories.category_id', '=', 'categories.id');
        $query->leftJoin('product_tags', 'product_tags.product_id', '=', 'products.id');
        $query->leftJoin('tags', 'product_tags.tag_id', '=', 'tags.id');
        $query->leftJoin('delivery_types', function ($join) {
            $join->on('products.delivery_type_id', '=', 'delivery_types.id')
                 ->where('delivery_types.deleted_at', '=', NULL);
        });
        $query->where(['products.deleted_at' => NULL, 'products.status' => ACTIVE]);
        if (isset($brand->id)) {
            $query->where(['product_brands.brand_id'=>$brand->id]);
        }

        if (isset($data['keywords']) && !empty($data['keywords'])) {
            $keywords = strtolower($data['keywords']);
            $query->where(function ($search_query) use ($keywords) {
                $search_query->where('products.name', 'like', '%' . $keywords . '%')
                             ->orWhere('products.reference', 'like', '%' . $keywords . '%')
                             ->orWhere(function ($category_query) use ($keywords) {
                                 $locals = config('app.locales');
                                 if (!empty($locals)) {
                                     foreach ($locals as $key => $local) {
                                         if ($key == 0) {
                                             $category_query->whereRaw("LOWER(json_unquote( json_extract( categories.name, '$.${local}' ))) LIKE '%${keywords}%' ");
                                         } else {
                                             $category_query->orWhereRaw("LOWER(json_unquote( json_extract( categories.name, '$.${local}' ))) LIKE '%${keywords}%' ");
                                         }
                                     }
                                 } else {
                                     $category_query->where("LOWER(json_unquote( json_extract( categories.name, '$.en' ))) LIKE '%${keywords}%' ");
                                 }
                             })
                             ->orWhere(function ($tag_query) use ($keywords) {
                                 $locals = config('app.locales');
                                 if (!empty($locals)) {
                                     foreach ($locals as $key => $local) {
                                         if ($key == 0) {
                                             $tag_query->whereRaw("LOWER(json_unquote( json_extract( tags.name, '$.${local}' ))) LIKE '%${keywords}%' ");
                                         } else {
                                             $tag_query->orWhereRaw("LOWER(json_unquote( json_extract( tags.name, '$.${local}' ))) LIKE '%${keywords}%' ");
                                         }
                                     }
                                 } else {
                                     $tag_query->whereRaw("LOWER(json_unquote( json_extract( tags.name, '$.en' ))) LIKE '%${keywords}%' ");;
                                 }
                             });
            });
        }
        if (isset($data['max_price']) && !empty($data['max_price'])) {
            $query->where('products.min_price','<=', $data['max_price']);
        }
        if (isset($data['product_id']) && !empty($data['product_id'][0])) {
            $query->whereIn('products.id', $data['product_id']);
        }
        if (isset($data['product_slug']) && !empty($data['product_slug'][0])) {
            $query->whereIn('products.slug', $data['product_slug']);
        }
        if (isset($data['category_id']) && !empty($data['category_id'][0])) {
            $query->whereIn('product_categories.category_id', $data['category_id']);
        }
        if (isset($data['tags_id']) && !empty($data['tags_id'][0])) {
            $query->whereIn('product_tags.tag_id', $data['tags_id']);
        }
        if (isset($data['product_combination_id']) && !empty($data['product_combination_id'][0])) {
            $query->whereIn('comb.combination_id', $data['product_combination_id']);
        }
        if (isset($data['delivery_type_id']) && !empty($data['delivery_type_id'][0])) {
            $query->whereIn('products.delivery_type_id', $data['delivery_type_id']);
        }
        if (isset($data['exclude_id']) && !empty($data['exclude_id'])){
            $query->whereNotIn('products.id',$data['exclude_id']);
        }
        $query->groupBy('products.id');

        if (isset($data['order_by']) && !empty($data['order_by'])){
            $order = $data['order_by'];
            if ($order == SORT_BY_LOWEST_PRICE){
                $query->orderBy('products.min_price', 'asc');
            }elseif ($order == SORT_BY_HIGHEST_PRICE){
                $query->orderBy('products.min_price', 'desc');
            }elseif ($order == SORT_BY_NEWEST){
                $query->orderBy('products.created_at', 'desc');
            }

        }else{
            $query->orderBy('products.id', 'desc');
        }

        return $query->paginate($pagination);
    }

    public function getProductDataApiById($slug) {
        $query = DB::table('products')->select('products.*');
        $query->addSelect(DB::raw("GROUP_CONCAT(
                        DISTINCT (CONCAT(product_pricings.compare,'==',product_pricings.quantity,'==',
                        product_pricings.price)
                   ) SEPARATOR '|||') AS pricing"));
        $query->addSelect(DB::raw("GROUP_CONCAT(DISTINCT product_image.media_type) AS media_type"));
        $query->addSelect(DB::raw("GROUP_CONCAT(DISTINCT product_image.media_url) AS featured_image"));
        $query->addSelect(DB::raw("GROUP_CONCAT(
                   	    DISTINCT (CONCAT(product_pricings.compare,'==',product_pricings.quantity,'==',product_pricings.price))
                   	    SEPARATOR '|||') AS pricing"));
        $query->addSelect(DB::raw("GROUP_CONCAT(DISTINCT delivery_types.name) AS delivery_type_name"));
        $query->addSelect(DB::raw("GROUP_CONCAT(DISTINCT delivery_types.icon) AS delivery_type_icon"));

        $query->leftJoin('product_pricings', 'products.id', '=', 'product_pricings.product_id');
        $query->leftJoin('product_tags', 'products.id', '=', 'product_tags.product_id');
        $query->leftJoin('product_combinations as product_image', function ($join) {
            $join->on('products.id', '=', 'product_image.product_id')
                 ->where('product_image.is_featured', '=', DB::raw(ACTIVE));
        });

        $query->leftJoin('delivery_types', function ($join) {
            $join->on('products.delivery_type_id', '=', 'delivery_types.id')
                 ->where('delivery_types.deleted_at', '=', NULL);
        });
        $query->groupBy('products.id');
        $query->where('products.slug', $slug);
        return $query->first();
    }

    public function getProductListData($products) {
        foreach ($products as $key => $product) {
            /*$product_list[$key]['id'] = $product->id;
            $product_list[$key]['name'] = $product->name;
            $product_list[$key]['description'] = $product->description;
            $product_list[$key]['slug'] = $product->slug;
            $product_list[$key]['reference'] = $product->reference;
            $product_list[$key]['status'] = $product->status;
            $product_list[$key]['price'] = $product->min_price;
            $product_list[$key]['featured_image'] = !empty($product->featured_image) ? $product->media_type == 'internal_image' ? Storage::url($product->featured_image)  : $product->featured_image : '';
            $product_list[$key]['delivery_type_name'] = $product->delivery_type_name;
            $product_list[$key]['delivery_type_icon'] = !empty($product->delivery_type_icon) ? asset(get_image_path('delivery') . $product->delivery_type_icon) : '';
            $product_list[$key]['combination_color'] = $this->getProductListColor($product, 'pt');*/
            $products[$key]->featured_image = !empty($product->featured_image) ? $product->media_type == 'internal_image' ? Storage::url($product->featured_image)  : $product->featured_image : '';
            $products[$key]->delivery_type_icon = !empty($product->delivery_type_icon) ? asset(get_image_path('delivery') . $product->delivery_type_icon) : '';
            $products[$key]->combination_color = $this->getProductListColor($product, 'pt');
        }
        return $products;
    }

    public function getProductListDataArray($products) {
        $product_list = [];
        foreach ($products as $key => $product) {
            $product_list[$key]['id'] = $product->id;
            $product_list[$key]['name'] = $product->name;
            $product_list[$key]['description'] = $product->description;
            $product_list[$key]['slug'] = $product->slug;
            $product_list[$key]['reference'] = $product->reference;
            $product_list[$key]['status'] = $product->status;
            $product_list[$key]['min_price'] = $product->min_price;
            $product_list[$key]['featured_image'] = !empty($product->featured_image) ? $product->media_type == 'internal_image' ? Storage::url($product->featured_image)  : $product->featured_image : '';
            $product_list[$key]['delivery_type_name'] = $product->delivery_type_name;
            $product_list[$key]['delivery_type_icon'] = !empty($product->delivery_type_icon) ? asset(get_image_path('delivery') . $product->delivery_type_icon) : '';
            $product_list[$key]['combination_color'] = $this->getProductListColor($product, 'pt');
        }
        return $product_list;
    }

    private function getProductListColor($product, $language) {
        $combinations = !empty($product->combination_color) ? explode('|||', $product->combination_color) : '';
        $combination_array = array();
        if (!empty($combinations[0])) {
            foreach ($combinations as $combination_key => $combination) {
                $combination_data = explode('==', $combination);
                if (!empty($combination_data[0])) {
                    foreach ($combination_data as $key => $value) {
                        if ($key == 0) {
                            $combination_array[$combination_key]['combination_type_id'] = $value;
                        } if ($key == 1) {
                            $combination_array[$combination_key]['combination_id'] = $value;
                        } if ($key == 2) {
                            $combination_array[$combination_key]['combination_type'] = json_decode($value)->en ?? '';
                        } else if ($key == 3) {
                            $combination_array[$combination_key]['combination'] = json_decode($value)->en ?? '';
                        } else if ($key == 4) {
                            $combination_array[$combination_key]['class_name'] = $value;
                        } else if ($key == 5) {
                            $combination_array[$combination_key]['color_code'] = $value;
                        }
/*                        else if ($key == 6) {
                            $combination_array[$combination_key]['media_type'] = $value;
                        } else if ($key == 7) {
                            $combination_array[$combination_key]['media_url'] = !empty($value) && $value != 'NO_MEDIA_URL' ? Storage::url($value) : '';
                        } else if ($key == 8) {
                            $combination_array[$combination_key]['is_featured'] = $value;
                        }*/
                    }
                }
            }
        }
        return $combination_array;
    }

    public function getProductDataById($product) {
        $product_media_combination = $this->getProductColor($product->id);
        $product_array = [];
        $product_array['id'] = $product->id;
        $product_array['name'] = $product->name;
        $product_array['description'] = $product->description;
        $product_array['slug'] = $product->slug;
        $product_array['reference'] = $product->reference;
        $product_array['additional_info'] = $product->additional_info;
        $product_array['delivery_type_id'] = $product->delivery_type_id;
        $product_array['status'] = $product->status;
        $product_array['pricing'] = $this->getPricingData($product->pricing);
        $product_array['featured_image'] = !empty($product->featured_image) ? $product->media_type == 'internal_image' ? Storage::url($product->featured_image) : $product->featured_image: '';
        $product_array['combination_color'] = $product_media_combination['media_with_combination'];
        $product_array['media_extra'] = $product_media_combination['media_extra'];
        $product_array['combination_size'] = $this->getProductSize($product->id);
        $product_array['delivery_type_icon'] = !empty($product->delivery_type_icon) ? Storage::url($product->delivery_type_icon) : '';
        $product_array['delivery_type_name'] = $product->delivery_type_name;
        $product_category = ProductCategory::where('product_id','=',$product->id)->pluck('category_id')->toArray();
        $product_array['categories'] = Category::whereIn('id',$product_category)->get()->toArray();

        return $product_array;
    }

    private function getPricingData($pricing) {
        $pricing_list_data = [];
        if (!empty($pricing)) {
            $pricing_list = explode('|||', $pricing);
            if (!empty($pricing_list[0])) {
                foreach ($pricing_list as $key => $list) {
                    $pricing_list_data[] = explode('==', $list);
                }
            }
        }
        return $pricing_list_data;
    }

    private function getProductColor($product_id) {
        $product_color_data = [];
        $product_extra_data = [];
        $product_colors = ProductCombination::select('product_combinations.*','combinations.name as combination_name',
            'combinations.class_name','combinations.color_code', 'combination_types.name as combination_type_name')
            ->leftJoin('combination_types','product_combinations.combination_type_id','combination_types.id')
            ->leftJoin('combinations','product_combinations.combination_id','combinations.id')
            ->where(function ($query){
                $query->where('product_combinations.combination_type_id',COLOR_ID)->orWhere('product_combinations.combination_type_id',NULL);
            })
            ->where('product_combinations.product_id','=',$product_id)
            ->where('product_combinations.status','=',ACTIVE)
            ->get();
        if (!empty($product_colors)) {
            $index_combination = 0;
            $index_extra = 0;
            foreach ($product_colors as $key => $product_color){
                if (!empty($product_color->combination_type_id) && !empty($product_color->combination_id)){
                    $product_color_data[$index_combination]['combination_type_id'] = $product_color->combination_type_id;
                    $product_color_data[$index_combination]['combination_id'] = $product_color->combination_id;
                    $product_color_data[$index_combination]['combination_type'] = !empty($product_color->combination_type_name) ? json_decode($product_color->combination_type_name)->en ?? '' : '';
                    $product_color_data[$index_combination]['combination'] = !empty($product_color->combination_name) ? json_decode($product_color->combination_name)->en ?? '' : '';
                    $product_color_data[$index_combination]['class_name'] = $product_color->class_name;
                    $product_color_data[$index_combination]['color_code'] = $product_color->color_code;
                    $product_color_data[$index_combination]['media_type'] = $product_color->media_type;
                    $product_color_data[$index_combination]['media_url'] = !empty($product_color->media_url) && $product_color->media_url != 'NO_MEDIA_URL' ? $product_color->media_type == 'internal_image' ? Storage::url($product_color->media_url) : $product_color->media_url : '';
                    $product_color_data[$index_combination]['is_featured'] = $product_color->is_featured;
                    $index_combination ++;
                }else{
                    $product_extra_data[$index_extra]['media_type'] = $product_color->media_type;
                    $product_extra_data[$index_extra]['media_url'] = !empty($product_color->media_url) && $product_color->media_url != 'NO_MEDIA_URL' ? $product_color->media_type == 'internal_image' ? Storage::url($product_color->media_url) : $product_color->media_url : '';
                    $product_extra_data[$index_extra]['is_featured'] = $product_color->is_featured;
                }

            }
        }
        $media['media_with_combination'] = $product_color_data;
        $media['media_extra'] = $product_extra_data;
        return $media;
    }

    private function getProductSize($product_id) {
        $product_size_data = [];
        $product_sizes = ProductCombination::select('product_combinations.*','combinations.name as combination_name','combination_types.name as combination_type_name')
                                            ->leftJoin('combinations','product_combinations.combination_id','combinations.id')
                                            ->leftJoin('combination_types','product_combinations.combination_type_id','combination_types.id')
                                            ->where('product_combinations.combination_type_id','=',SIZE_ID)
                                            ->where('product_combinations.status','=',ACTIVE)
                                            ->where('product_combinations.product_id','=',$product_id)
                                            ->get();
        if (!empty($product_sizes)) {
            foreach ($product_sizes as $key=>$product_size){
                if (!empty($product_size->combination_name)){
                    $product_size_data[] = json_decode($product_size->combination_name)->en ?? '';
                }
            }
        }
        return $product_size_data;
    }
}
