<?php

namespace App\Http\Repositories\Production;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmQuotation;
use Illuminate\Support\Facades\DB;

class ProductionRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CrmQuotation/quotation $model
       */
    public function __construct(CrmQuotation $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getDashboardData(){
        $query = $this->model::select(
            DB::raw("
            SUM(CASE WHEN crm_quotations.expired_at = CURRENT_DATE THEN 1 ELSE 0 END ) AS expire_today,
	        SUM(CASE WHEN crm_quotations.expired_at = CURRENT_DATE+1 THEN 1 ELSE 0 END ) AS expire_tomorrow,
	        SUM(CASE WHEN crm_quotations.status IN (33,41,42,43) THEN 1 ELSE 0 END ) AS production_running,
	        SUM(CASE WHEN crm_quotations.status IN (44) THEN 1 ELSE 0 END ) AS production_completed,
	        SUM(CASE WHEN crm_quotations.status IN (51,52) THEN 1 ELSE 0 END ) AS in_shipment,
	        SUM(CASE WHEN crm_quotations.expired_at < CURRENT_DATE THEN 1 ELSE 0 END ) AS expired")
        )->whereBetween('crm_quotations.status',[16,52])->first();

        return $query;
    }

    public function getOrderData($search_array)
    {
        $query = $this->model::select('*')->where('in_staging', QUOTATION_STAGE_IN_PRODUCTION)
            ->where('status', '>=', STATUS_ORDER_SENT_TO_PRODUCTION)
            ->where('status', '<', STATUS_PRODUCTION_COMPLETED);
        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['value'] !== NULL){
                    $query =  $query->where('crm_quotations.'.$search['name'],$search['value']);
                }
            }
        }
        return $query;
    }

    public function getDataFromOrderToProduction($where=[],$search_array=[])
    {
        $query = $this->model::select('*')->where('status', '>=', STATUS_BUDGET_APPROVED)
            ->where('status', '<=', STATUS_SHIPPED_COMPLETED);
        if (!empty($where)) {
            foreach($where as $key => $value) {
                if(is_array($value)){
                    $query = $query->where($key,$value[0],$value[1]);
                }else{
                    $query = $query->where($key,'=',$value);
                }
            }
        }
        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['value'] !== NULL){
                    $query =  $query->where('crm_quotations.'.$search['name'],$search['value']);
                }
            }
        }
        return $query->get();
    }

    public function getProductionCompleteData($search_array)
    {
        $query = $this->model::select('*')->where('in_staging', QUOTATION_STAGE_IN_PRODUCTION)
            ->where('status', '=', STATUS_PRODUCTION_COMPLETED);

        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['value'] !== NULL){
                    $query =  $query->where('crm_quotations.'.$search['name'],$search['value']);
                }
            }
        }
        return $query->get();
    }

    public function changeStatus($requestArray){
        return $this->model::where('id',$requestArray['id'])->update(['status'=>$requestArray['status']]);
    }
}
