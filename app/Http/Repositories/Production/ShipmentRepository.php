<?php

namespace App\Http\Repositories\Production;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmQuotation;

class ShipmentRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CrmQuotation/quotation $model
       */
    public function __construct(CrmQuotation $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getOrderData($search_array)
    {
        $query = $this->model::select('*')->where('in_staging', QUOTATION_STAGE_IN_SHIPMENT)->where('status', '>=', STATUS_PRODUCTION_COMPLETED);
        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['value'] !== NULL){
                    $query =  $query->where('crm_quotations.'.$search['name'],$search['value']);
                }
            }
        }
        return $query;
    }

    public function changeStatus($requestArray){
        return $this->model::where('id',$requestArray['id'])->update(['status'=>$requestArray['status']]);
    }
}
