<?php

namespace App\Http\Repositories\Production;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmQuotationProductArrivalLog;

class CrmQuotationProductArrivalLogRepository extends BaseRepository
{

    public function __construct(CrmQuotationProductArrivalLog $model)
    {
        parent::__construct($model);
    }

}
