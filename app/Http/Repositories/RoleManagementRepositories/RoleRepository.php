<?php


namespace App\Http\Repositories\RoleManagementRepositories;

use App\Http\Repositories\BaseRepository;
use App\Models\Role\Role;


class RoleRepository extends BaseRepository {

    /**
     * RoleRepository constructor.
     *
     * @param Role $model
     */
    public function __construct(Role $model) {
        parent::__construct($model);
    }
}
