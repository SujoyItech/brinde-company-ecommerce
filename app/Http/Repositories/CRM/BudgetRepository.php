<?php

namespace App\Http\Repositories\CRM;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmQuotation;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;

class BudgetRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CrmQuotation/quotation $model
       */
    public function __construct(CrmQuotation $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getBudgetData($search_array)
    {
        $query = $this->model::select('*')->where('assigned_salesman','<>', null)->where('status', '<', STATUS_BUDGET_ORDERED);
        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['value'] !== NULL){
                    $query =  $query->where('crm_quotations.'.$search['name'],$search['value']);
                }
            }
        }
        return $query;
    }

    // get created budget
    public function getCreatedBudget($code) {
        return $this->model->where(['budget_reference' => $code])->where('status', ">", STATUS_QUOTATION_SALESMAN_ASSIGNED)->first();
    }

    public function changeStatus($requestArray){
        return $this->model::where('id',$requestArray['id'])->update(['status'=>$requestArray['status']]);
    }
}
