<?php

namespace App\Http\Repositories\CRM;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmQuotation;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;

class QuotationRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CrmQuotation/quotation $model
       */
    public function __construct(CrmQuotation $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getQuotationData($search_array)
    {
        return $this->model::leftJoin('brands','crm_quotations.brand_id','brands.id')
                             ->select('crm_quotations.*','brands.name as brand_name')
                             ->whereIn('crm_quotations.status',[STATUS_QUOTATION_NEW,STATUS_QUOTATION_SALESMAN_ASSIGNED]);
    }

    public function getAllQuotationList($search_array)
    {
        $query = $this->model::select('*')->where('status', '>=', STATUS_BUDGET_APPROVED)->where('status','<=',STATUS_SHIPPED_COMPLETED);
        if (isset($search_array) && !empty($search_array)){
            foreach ($search_array as $search){
                if ($search['name'] == 'reference') {
                    if (!empty($search['value'])) {
                        $query = $query->where(function ($sub_query) use ($search){
                            $sub_query->where('quote_reference',$search['value'])
                                ->orWhere('budget_reference',$search['value'])
                                ->orWhere('order_reference',$search['value'])
                                ->orWhere('purchase_reference',$search['value'])
                                ->orWhere('production_reference',$search['value']);
                        });
                    }
                }else{
                    if ($search['value'] !== NULL){
                        $query =  $query->where('crm_quotations.'.$search['name'],$search['value']);
                    }
                }
            }
        }
        return $query;
    }

    public function changeStatus($requestArray){
        return $this->model::where('id',$requestArray['id'])->update(['status'=>$requestArray['status']]);
    }

    public function getDashboardData(array $brand_id){
        $query = $this->model::select('brands.id','brands.name','brands.icon',
        DB::raw("SUM(CASE WHEN crm_quotations.`status` = 11 THEN 1 ELSE 0 END ) AS new_quotation,
            SUM(CASE WHEN crm_quotations.`status` = 12 THEN 1 ELSE 0 END ) AS quotation_assigned,
            SUM(CASE WHEN crm_quotations.`status` IN (13,14,15,16) THEN 1 ELSE 0 END ) AS budget_process")
        )->join('brands','crm_quotations.brand_id','=','brands.id');
        if (!empty($brand_id)) {
            $query = $query->whereIn('crm_quotations.brand_id',$brand_id);
        }
        $query = $query->groupBy('crm_quotations.brand_id')
         ->orderBy('crm_quotations.brand_id','ASC')
         ->get();

        return $query;
    }

//    public function getDashboardData(){
//        $query = $this->model::select('brands.id','brands.name',
//        DB::raw("SUM(CASE WHEN crm_quotations.`status` = 11 THEN 1 ELSE 0 END ) AS new_quotation,
//            SUM(CASE WHEN crm_quotations.`status` = 12 THEN 1 ELSE 0 END ) AS quotation_assigned,
//            SUM(CASE WHEN crm_quotations.`status` IN (13,14,15,16) THEN 1 ELSE 0 END ) AS budget_process,
//            SUM(CASE WHEN crm_quotations.`status` = 17 THEN 1 ELSE 0 END ) AS order_number,
//            SUM(CASE WHEN crm_quotations.`status` IN (21,22) THEN 1 ELSE 0 END ) AS in_purchase,
//            SUM(CASE WHEN crm_quotations.`status` IN (31,32,33) THEN 1 ELSE 0 END ) AS in_logistic,
//            SUM(CASE WHEN crm_quotations.`status` IN (41,42,43) THEN 1 ELSE 0 END ) AS in_production,
//            SUM(CASE WHEN crm_quotations.`status` = 44 THEN 1 ELSE 0 END ) AS production_completed,
//            SUM(CASE WHEN crm_quotations.`status` = 51 THEN 1 ELSE 0 END ) AS in_shipment,
//            SUM(CASE WHEN crm_quotations.`status` = 52 THEN 1 ELSE 0 END ) AS completed")
//        )->join('brands','crm_quotations.brand_id','=','brands.id')
//         ->groupBy('crm_quotations.brand_id')
//         ->orderBy('crm_quotations.brand_id','ASC')
//         ->get();
//        dd($query);
//    }
}
