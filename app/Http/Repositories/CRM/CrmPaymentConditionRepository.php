<?php

namespace App\Http\Repositories\CRM;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmPaymentCondition;

class CrmPaymentConditionRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CRM/CrmPaymentCondition $model
       */
    public function __construct(CrmPaymentCondition $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

}
