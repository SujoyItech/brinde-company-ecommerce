<?php

namespace App\Http\Repositories\CRM;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmQuotationProduct;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;

class QuotationProductRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/Product $model
       */
    public function __construct(CrmQuotationProduct $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getBudgetProducts($quotation_id)
    {
        return $this->model::select('*')->where(['quotation_id' => $quotation_id, 'status' => ACTIVE])->get();
    }

}
