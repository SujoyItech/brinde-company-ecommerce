<?php

namespace App\Http\Repositories\CRM;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmPrintingOption;

class CrmPrintingOptionRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CRM/CrmPrintingOption $model
       */
    public function __construct(CrmPrintingOption $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
