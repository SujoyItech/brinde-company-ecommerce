<?php

namespace App\Http\Repositories\CRM;

use App\Http\Repositories\BaseRepository;
use App\Models\CRM\CrmCustomer;

class CrmCustomerRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param CRM/CrmCustomer $model
       */
    public function __construct(CrmCustomer $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

}
