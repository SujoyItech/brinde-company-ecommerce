<?php

namespace App\Http\Repositories;

use App\Models\IfthenpayRecord;

class IfthenpayRecordRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param IfthenpayRecord $model
       */
    public function __construct(IfthenpayRecord $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
