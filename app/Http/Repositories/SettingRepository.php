<?php

namespace App\Http\Repositories;

use App\Models\Setting;

class SettingRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Setting $model
       */
    public function __construct(Setting $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
