<?php

namespace App\Http\Repositories\Website;

use App\Http\Repositories\BaseRepository;
use App\Models\Website\WebsiteCategoriesMenu;

class WebsiteCategoriesMenuRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Website/WebsiteCategories $model
       */
    public function __construct(WebsiteCategoriesMenu $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
