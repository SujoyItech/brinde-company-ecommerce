<?php

namespace App\Http\Repositories\Website;

use App\Http\Repositories\BaseRepository;

use App\Models\Website\WebsitePageLayerDetails;

class WebsitePageLayerDetailsRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Website/WebsitePageLayerContent $model
       */
    public function __construct(WebsitePageLayerDetails $model)
    {
        parent::__construct($model);
    }

    public function getLayerContentData($where=[]){
        $query = $this->model::select('website_pages_layers_details.*','categories.name as category_name')
            ->leftJoin('categories','website_pages_layers_details.category_id','=','categories.id');
        if (!empty($where)){
            $query = $query->where($where);
        }
        return $query->get();
    }
    public function getLayerContentSingleData($where=[]){
        $query = $this->model::select('website_pages_layers_details.*','categories.name as category_name')
            ->leftJoin('categories','website_pages_layers_details.category_id','=','categories.id');
        if (!empty($where)){
            $query = $query->where($where);
        }
        return $query->first();
    }

    public function layerDeatailsWithLayer($layer_details_id){
        return $this->model::select('website_pages_layers_details.id','website_pages_layers_details.featured_image',
            'website_pages_layers.id as layer_id','website_pages_layers.layer_type')
            ->join('website_pages_layers','website_pages_layers_details.website_pages_layers_id','=','website_pages_layers.id')
            ->where('website_pages_layers_details.id','=',$layer_details_id)
            ->first();
    }

}
