<?php

namespace App\Http\Repositories\Website;

use App\Http\Repositories\BaseRepository;
use App\Models\Website\WebsitePage;

class WebsitePageRepository extends BaseRepository {
    /**
     * Instantiate repository
     *
     * @param Website/WebsitePage $model
     */
    public function __construct(WebsitePage $model) {
        parent::__construct($model);
    }

    public function getWebPageList($brand_id){
        return $this->model::where('brand_id','=',$brand_id)->where('status','=',STATUS_ACTIVE)->orderBy('order','asc')->get();
    }

    public function getWebPageContent($page_slug = NULL, $page_type = NULL, $brand_id = NULL){
        $query = $this->model::select(
            'website_pages.id',
            'website_pages.page_type',
            'website_pages.page_name',
            'website_pages.url',
            'website_pages.title',
            'website_pages.subtitle',
            'website_pages.description',

            'website_pages_layers.id as layer_id',
            'website_pages_layers.layer_name',
            'website_pages_layers.layer_type',
            'website_pages_layers.layer_title',
            'website_pages_layers.layer_subtitle',
            'website_pages_layers.layout_number',
            'website_pages_layers.padding_x',
            'website_pages_layers.padding_top',
            'website_pages_layers.padding_bottom',
            'website_pages_layers.order as layer_order',

            'website_pages_layers_details.id as layer_details_id',
            'website_pages_layers_details.layer_details_title',
            'website_pages_layers_details.description_title',
            'website_pages_layers_details.description_body',
            'website_pages_layers_details.order as layer_details_order',
            'website_pages_layers_details.category_id',
            'website_pages_layers_details.category_product_slugs',
            'website_pages_layers_details.featured_image',
            'website_pages_layers_details.url_slug',
            'website_pages_layers_details.url_target_type',
            'website_pages_layers_details.layout_is_image'
        )->leftJoin('website_pages_layers',function ($join){
            $join->on('website_pages.id','=','website_pages_layers.website_pages_id')->where('website_pages_layers.status','=',STATUS_ACTIVE);
        })
         ->leftJoin('website_pages_layers_details','website_pages_layers.id','=','website_pages_layers_details.website_pages_layers_id');

        $query = !empty($page_slug) ? $query->where('website_pages.url', '=', $page_slug) : $query;
        $query = !empty($page_type) ? $query->where('website_pages.page_type', '=', $page_type) : $query;
        $query = !empty($brand_id) ? $query->where('website_pages.brand_id', '=', $brand_id) : $query;

        $query = $query->get();
        return $query;
    }

    public function getPageQuery($brand_array,$brand_id) {
        $query = $this->model::leftJoin('brands', 'website_pages.brand_id', 'brands.id')
                           ->select('website_pages.*', 'brands.name as brand_name','brands.icon as brand_icon');
        if (!empty($brand_id)) {
            $query = $query->where('brands.id',$brand_id);
        }else{
            $query = $query->whereIn('brands.id',$brand_array);
        }
       return $query->get();
    }

    public function getPageDetails($id) {
        return $this->model::leftJoin('brands', 'website_pages.brand_id', 'brands.id')
                           ->select('website_pages.*', 'brands.name as brand_name','brands.icon as brand_icon')
                           ->where(['website_pages.id' => $id])->first();
    }

    public function websitePageStatusChange($requestArray){
        $page = $this->model::where('id',$requestArray['id'])->first();
        if ($requestArray['status'] == STATUS_ACTIVE){
            return $this->model::where('id',$requestArray['id'])->update(['status'=>INACTIVE]);
        }else{
            if ($page->page_type == 'home') {
                $this->model::where(['brand_id'=>$requestArray['brand_id'],'page_type'=>'home'])->update(['status'=>INACTIVE]);
            }
            $this->model::where('id',$requestArray['id'])->update(['status'=>STATUS_ACTIVE]);
            return TRUE;
        }
    }
}
