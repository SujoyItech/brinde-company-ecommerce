<?php

namespace App\Http\Repositories\Website;

use App\Http\Repositories\BaseRepository;
use App\Models\Website\WebsitePageLayer;
use App\Models\Website\WebsitePageLayerDetails;

class WebsitePageLayerRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Website/WebsitePageLayer $model
       */
    public function __construct(WebsitePageLayer $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
    public function updateOrder($requestArray){
        if(!empty($requestArray['data'])){
            foreach ($requestArray['data'] as $key=>$value){
                $this->model::where('id',$value)->update(['order'=>$key]);
            }
        }
        return TRUE;
    }



}
