<?php

// role
const SUPER_ADMIN = 0;
const USER_ADMIN = 1;
const PRODUCT_ADMIN = 20;
const PRODUCT_ENTRY_MANAGER = 21;

const CRM_ADMIN = 30;
const ROLE_SALES_MANAGER = 31;
const ROLE_SALESMAN = 32;

const PRODUCTION_ADMIN = 40;
const PURCHASE_MANAGER = 41;
const LOGISTIC_MANAGER = 42;
const PRODUCTION_MANAGER = 43;


// Module
const MODULE_SUPER_ADMIN = 0;
const MODULE_USER_ADMIN = 1;
const MODULE_PRODUCT_MANAGEMENT = 2;
const MODULE_CRM_MANAGEMENT = 3;
const MODULE_PRODUCTION_MANAGEMENT = 4;
const MODULE_WEBSITE_MANAGEMENT = 5;

const MODULES = array(
    MODULE_SUPER_ADMIN => 'Super Admin Module',
    MODULE_USER_ADMIN => 'Admin Module',
    MODULE_PRODUCT_MANAGEMENT => 'Product Management Module',
    MODULE_PRODUCTION_MANAGEMENT => 'Production Management Module',
    MODULE_CRM_MANAGEMENT => 'CRM Management Module',
    MODULE_WEBSITE_MANAGEMENT => 'Website Management Module'
);

// Brand
const BRAND_BRINDE_COMPANHIA = 1;
const BRAND_TOP_BRINDE = 2;
const BRAND_BRINDE_BRINDES = 3;
const BRAND_BEST_OF_GIFT = 4;

const BRAND_COLORS = [
    BRAND_BRINDE_COMPANHIA => '#c5007b',
    BRAND_TOP_BRINDE => '#e94e1b',
    BRAND_BRINDE_BRINDES => '#3791d6',
    BRAND_BEST_OF_GIFT => '#c5007b',
];


const GLOBAL_PAGINATION = 20;
const MYSQL_DATE_FORMAT = 'Y-m-d h:i:s';
const MYSQL_DATE_WITHOUT_TIME = 'Y-m-d';

//users table created_by column value definition
const SEEDUSER = 0;

//user status
const INACTIVE = 0;
const ACTIVE = 1;
const USER_SUSPENDED = 2;
const USER_BLOCKED = 3;

//2fa type
const NONE = 0;
const GAUTH = 1;
const EMAIL = 2;

//roles depends on role seeding
const MERCHANT = 2;

const BLOCK = 0;
const UNBLOCK = 1;

// Referral bonus type constants
const FIXED = 1;
const PERCENTAGE = 2;
// API key type
const WEB_API_KEY = 1;
const APP_API_KEY = 2;



const STATUS_PROCESSING = 0;
const STATUS_ACTIVE = 1;
const STATUS_SUCCESS = 1;
const STATUS_ADMIN_APPROVAL = 2;
const STATUS_CANCELLED = 3;
const STATUS_FAILED = 4;

const STATUS_APPROVED_BY_CUSTOMER = 1;
const STATUS_APPROVED_BY_MANAGER = 2;

//payout mode
const TO_BALANCE = 1;



//coin amount decimal scale
const COIN_DECIMAL_SCALE = 8;

//transaction status
const PROCESSING = 0;
const CONFIRMED = 1;
const NOTIFIED = 2;
const COMPLETED = 3;
const FAILED = 4;
const REJECTED = 5;
const EXPIRED = 6;
const BLOCKED = 7;
const REFUNDED = 8;
const VOIDED = 9;

//product statuses
const PRODUCT_DRAFT = 2;


//product pricing compare enums
const MORE = 'more';
const EQUAL = 'equal';
const LESS = 'less';


//quotation status
const STATUS_QUOTATION_CANCELLED = 2;
const STATUS_QUOTATION_EXPIRED = 3;
const STATUS_QUOTATION_REJECTED = 4;
const STATUS_QUOTATION_NEW = 11;
const STATUS_QUOTATION_SALESMAN_ASSIGNED = 12;
const STATUS_QUOTATION_BUDGET_CREATED = 13;
const STATUS_BUDGET_SENT = 14;
const STATUS_BUDGET_MODIFY_REQUEST = 15;
const STATUS_BUDGET_APPROVED = 16;
const STATUS_BUDGET_ORDERED = 17;
const STATUS_ORDER_PARTIALLY_PURCHASED = 21;
const STATUS_ORDER_PURCHASED_DONE = 22;
const STATUS_ORDER_PARTIALLY_ARRIVED = 31;
const STATUS_ORDER_ARRIVED_DONE = 32;
const STATUS_ORDER_SENT_TO_PRODUCTION = 33;
const STATUS_ORDER_IN_PRODUCTION = 41;
const STATUS_PRODUCTION_NOT_COMPLETED = 42;
const STATUS_PRODUCTION_PARTIALLY_COMPLETED = 43;
const STATUS_PRODUCTION_COMPLETED = 44;

const STATUS_IN_SHIPMENT = 51;
const STATUS_SHIPPED_COMPLETED = 52;

// Quotation stage
const QUOTATION_STAGE_IN_SALES = 1;
const QUOTATION_STAGE_IN_PURCHASE = 2;
const QUOTATION_STAGE_IN_ARRIVAL = 3;
const QUOTATION_STAGE_IN_PRODUCTION = 4;
const QUOTATION_STAGE_IN_SHIPMENT = 5;

//Quotation payment statuses
const PAYMENT_DUE = 0;
const PAYMENT_PAID = 1;
const REFERENCE_GENERATED = 2;


//system combination type
const COLOR_ID = 1;
const COLORS_EN = 'Colors';
const COLORS_PT = 'Cores';
const SIZE_ID = 2;
const SIZE_EN = 'Size';
const SIZE_PT = 'Tamanho';

//product combination media type enums
const INTERNAL_IMAGE = 'internal_image';
const EXTERNAL_IMAGE = 'external_image';
const VIDEO_URL = 'video_url';
const _360_URL = '360_url';

// shipped product
const IS_SHIPPED_ITEM = 1;

// payment status

const PAYMENT_NOT_DONE = 0;
const PAYMENT_DONE = 1;
const PAYMENT_IN_PROGRESS = 2;
const PAYMENT_CANCEL = 3;

//Ifthenpay callback status
const CALLED = 1;
const NOT_CALLED_YET = 0;

// purchase status
const STATUS_NOT_PURCHASED = 0;
const STATUS_PURCHASED = 1;
const STATUS_PARTIALLY_PURCHASED = 2;

// quotation product arrival status
const STATUS_NOT_ARRIVED = 0;
const STATUS_ARRIVED = 1;
const STATUS_PARTIALLY_ARRIVED = 2;

// quotation product complete status
const STATUS_NOT_COMPLETED = 0;
const STATUS_COMPLETED = 1;
const STATUS_PARTIALLY_COMPLETED = 2;

const SORT_BY_LOWEST_PRICE = 1;
const SORT_BY_HIGHEST_PRICE = 2;
const SORT_BY_NEWEST = 3;

const STATUS_PRODUCT_PROCESSING_NEW = 0;
const STATUS_PRODUCT_PROCESSING_START= 1;
const STATUS_PRODUCT_PROCESSING_PROCESSING = 2;
const STATUS_PRODUCT_PROCESSING_COMPLETED = 3;

const STATUS_PRODUCT_UPLOAD_UNREAD = 0;
const STATUS_PRODUCT_UPLOAD_READ= 1;
const STATUS_PRODUCT_UPLOAD_FAILED = 2;

//--------------------Website Manager

const LAYER_BANNER = 'banner';
const LAYER_DESCRIPTION = 'description';
const LAYER_CATEGORY = 'category';
const LAYER_PRODUCT = 'product';
const LAYER_GALLERY = 'gallery';

const VAT_AMOUNT = 23;

const MAIN_CONTACT = 1;
const SUB_CONTACT = 2;
