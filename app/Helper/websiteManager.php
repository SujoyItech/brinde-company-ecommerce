<?php

use Illuminate\Support\Facades\Storage;

/**
 * @param string $layout
 *
 * @return array|bool
 */
function getLayouts($layout = ''){
    $all_layouts = Storage::disk('admin_images')->allFiles('website_manager/layouts');
    $layout_file_names = [];
    foreach ($all_layouts as $all_layout) {
        $layout_file_names[pathinfo($all_layout, PATHINFO_FILENAME)] = $all_layout;
    }
    if($layout == ''){
        return $layout_file_names;
    }else {
        if(isset($layout_file_names[$layout])){
            return $layout_file_names[$layout];
        }else {
            return FALSE;
        }
    }
}

