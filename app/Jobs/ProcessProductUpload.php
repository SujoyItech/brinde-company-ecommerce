<?php

namespace App\Jobs;

use App\Http\Services\Product\ProductImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessProductUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $batch_payload,$upload_products,$supplier_id;
    public function __construct($upload_products,$batch_payload,$supplier_id)
    {
       $this->upload_products = $upload_products;
       $this->batch_payload = $batch_payload;
       $this->supplier_id = $supplier_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product_import_service = new ProductImportService();
        $product_import_service->importProducts($this->upload_products,$this->batch_payload,$this->supplier_id);
    }
}
