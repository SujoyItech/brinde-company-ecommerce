<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * If specified, this namespace is automatically applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('web')->namespace($this->namespace)->group(base_path('routes/web.php'));
            Route::prefix('api')->middleware('api')->namespace($this->namespace)->group(base_path('routes/api.php'));
            Route::prefix('admin')->middleware(['web','auth'])->namespace($this->namespace)->group(base_path('routes/app.php'));
            Route::prefix('admin')->middleware('web')->namespace($this->namespace)->group(base_path('routes/auth.php'));

            Route::prefix('admin')
                 ->middleware(['web','auth','language','auth_permission','module_permission:'.MODULE_SUPER_ADMIN])
                 ->namespace($this->namespace)
                 ->group(base_path('routes/admin/super_admin.php'));

            Route::prefix('admin')
                 ->middleware(['web','auth','language','auth_permission','module_permission:'.MODULE_USER_ADMIN,'role_permission'])
                 ->namespace($this->namespace)
                 ->group(base_path('routes/admin/admin.php'));

            Route::prefix('admin')
                 ->middleware(['web','auth','language','auth_permission','module_permission:'.MODULE_PRODUCT_MANAGEMENT,'role_permission'])
                 ->namespace($this->namespace)
                 ->group(base_path('routes/admin/product_manager.php'));

            Route::prefix('admin')
                 ->middleware(['web','auth','language', 'auth_permission','verified','module_permission:'.MODULE_PRODUCTION_MANAGEMENT,'role_permission'])
                 ->namespace($this->namespace)
                 ->group(base_path('routes/admin/production_manager.php'));

            Route::prefix('admin')
                 ->middleware(['web','auth','language','auth_permission','module_permission:'.MODULE_CRM_MANAGEMENT,'role_permission'])
                 ->namespace($this->namespace)
                 ->group(base_path('routes/admin/crm_manager.php'));

            Route::prefix('admin')
                 ->middleware(['web','auth','language','auth_permission','module_permission:'.MODULE_WEBSITE_MANAGEMENT,'role_permission'])
                 ->namespace($this->namespace)
                 ->group(base_path('routes/admin/website_manager.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60);
        });
    }
}
