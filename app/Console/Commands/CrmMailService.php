<?php

namespace App\Console\Commands;

use App\Http\Services\CrmProductionMailService;
use Illuminate\Console\Command;

class CrmMailService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm_mail:service';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crm Mail Send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        CrmProductionMailService::newQuotation();
        CrmProductionMailService::newlyAssignedQuotation();
//        CrmProductionMailService::newOrder();
    }
}
