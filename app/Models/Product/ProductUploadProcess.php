<?php

namespace App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductUploadProcess extends Model
{
    protected $table = 'product_upload_process';
    public $timestamps = FALSE;
    protected $fillable = array('id','product','product_column','batch_payload','status');
}
