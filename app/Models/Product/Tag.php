<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{

    protected $table = 'tags';
    public $timestamps = true;
    use HasTranslations;
    protected $fillable = array('name', 'status');
    public $translatable = ['name'];

}
