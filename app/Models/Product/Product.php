<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'slug', 'reference', 'additional_info', 'supplier_id','min_price', 'delivery_type_id', 'status', 'meta_title', 'meta_keywords', 'meta_description');

    public function product_categories(){
        return $this->hasMany(ProductCategory::class);
    }

    public function product_tags(){
        return $this->hasMany(ProductTag::class);
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class);
    }

    public function pricings() {
        return $this->hasMany(ProductPricing::class);
    }

    public function product_combinations() {
        return $this->hasMany(ProductCombination::class);
    }

}
