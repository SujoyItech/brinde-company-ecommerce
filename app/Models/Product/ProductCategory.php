<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';
    public $timestamps = false;
    protected $fillable = array('product_id', 'category_id');

}
