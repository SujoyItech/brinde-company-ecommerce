<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryType extends Model
{

    protected $table = 'delivery_types';
    public $timestamps = true;
    protected $fillable = array('name', 'time', 'icon', 'status');

}
