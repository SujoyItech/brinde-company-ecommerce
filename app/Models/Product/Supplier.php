<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{

    protected $table = 'suppliers';
    public $timestamps = true;
    protected $fillable = array('name', 'discount', 'status','created_by');

}
