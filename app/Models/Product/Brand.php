<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{

    protected $table = 'brands';
    public $timestamps = true;
    protected $fillable = array('name', 'slug', 'brand_url', 'email', 'phone', 'address', 'privacy_policy_link', 'faq_link', 'icon', 'color_code', 'is_reseller', 'status');
}
