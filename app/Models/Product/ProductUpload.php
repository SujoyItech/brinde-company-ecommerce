<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductUpload extends Model
{
    protected $table = 'product_uploads';
    public $timestamps = false;
    protected $fillable = array('supplier_id', 'file','uploader_id','upload_time','total_product','uploaded_product','status');
}
