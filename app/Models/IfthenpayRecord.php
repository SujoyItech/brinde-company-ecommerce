<?php

namespace App\Models;

use App\Models\CRM\CrmQuotation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IfthenpayRecord extends Model
{
    use HasFactory;

    protected $fillable = [
        'crm_quotation_id',
        'order_id',
        'entity',
        'sub_entity',
        'amount',
        'reference',
        'callback_status',
        'notice',
        'showable_reference',
        'key_backoffice',
        'budget_reference'
    ];

    public function crm_quotation() {
        return $this->belongsTo(CrmQuotation::class);
    }
}
