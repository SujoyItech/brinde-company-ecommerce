<?php

namespace App\Models\Website;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class WebsitePageLayerDetails extends Model
{
    use HasTranslations;
    protected $table = 'website_pages_layers_details';
    public $timestamps = FALSE;
    protected $fillable = array(
        'website_pages_layers_id',
        'description_title',
        'description_body',
        'featured_image',
        'category_id',
        'category_product_slugs',
        'url_slug',
        'layout_is_image',
        'layout_number',
        'order'
    );
    public $translatable = ['category_name'];
}
