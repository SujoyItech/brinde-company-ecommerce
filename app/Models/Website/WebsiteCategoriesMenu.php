<?php

namespace App\Models\Website;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebsiteCategoriesMenu extends Model
{
    protected $table = 'website_categories_menus';
    public $timestamps = FALSE;

    protected $fillable = array('category_id', 'brand_id', 'image','url');
}
