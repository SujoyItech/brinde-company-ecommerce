<?php

namespace App\Models\Website;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebsitePageLayer extends Model {
    protected $table = 'website_pages_layers';
    public $timestamps = FALSE;
    protected $fillable = array(
        'website_pages_id',
        'layer_name',
        'layer_type',
        'layer_title',
        'layer_subtitle',
        'layout_number',
        'order',
        'padding_x',
        'padding_top',
        'padding_bottom',
        'status'
    );
}
