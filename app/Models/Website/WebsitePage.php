<?php

namespace App\Models\Website;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebsitePage extends Model {
    protected $table = 'website_pages';
    public $timestamps = TRUE;

    protected $fillable = array('brand_id', 'page_type', 'page_name', 'url', 'title', 'subtitle', 'description', 'status', 'created_by','order');
}
