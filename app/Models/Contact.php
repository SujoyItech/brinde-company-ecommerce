<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    public $timestamps = FALSE;
    protected $fillable = array('contact_type','title','address','business_days','email','phone','gps','fax');
}
