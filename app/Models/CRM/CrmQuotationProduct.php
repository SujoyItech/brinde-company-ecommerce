<?php

namespace App\Models\CRM;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotationProduct extends Model
{
    use HasFactory;
    protected $fillable = [
        'quotation_id',
        'is_shipped_item',
        'product_id',
        'product_name',
        'product_reference',
        'product_mask_reference',
        'combination_id',
        'combination_type_id',
        'quantity',
        'unit_price',
        'vat',
        'total_price',
        'total_vat',
        'additional_info',
        'comments',
        'status',
        'arrived',
        'completed',
        'purchased',
    ];

    public function quotation()
    {
        return $this->belongsTo(CrmQuotation::class,'quotation_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function arrival_logs()
    {
        return $this->hasMany(CrmQuotationProductArrivalLog::class);
    }
}
