<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class CrmPaymentCondition extends Model
{
    protected $table = 'crm_payment_conditions';
    public $timestamps = true;

    use SoftDeletes,HasTranslations;

    protected $fillable = array('name','status');
    public $translatable = ['name'];
}
