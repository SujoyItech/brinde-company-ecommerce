<?php

namespace App\Models\CRM;

use App\Models\Product\Brand;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrmQuotation extends Model
{
    use HasFactory;
    protected $fillable = [
        'quote_reference',
        'budget_reference',
        'order_reference',
        'purchase_reference',
        'production_reference',
        'customer_id',
        'customer_code',
        'company_name',
        'contact_name',
        'email',
        'phone',
        'attached_file',
        'comments',
        'brand_id',
        'assignee_sales_admin',
        'assigned_salesman',
        'budget_show_total_enable',
        'budget_masking_enable',
        'budget_printing_enable',
        'budget_printing_option',
        'budget_printing_comments',
        'mb_reference',
        'need_payment',
        'budget_model',
        'expired_at',
        'in_staging',
        'payment_status',
        'status',
        'budget_modification_comments',
        'total_price',
        'total_vat',
        'grand_total',
        'approved_by'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class,'brand_id');
    }

    public function salesman()
    {
        return $this->belongsTo(User::class,'assigned_salesman');
    }

    public function quotation_products()
    {
        return $this->hasMany(CrmQuotationProduct::class,'quotation_id')
                    ->withSum('arrival_logs as total_arrival', 'arrival_quantity');
    }
}
