<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerVerificationCode extends Model
{
    protected $fillable = ['customer_id','type','code','expired_at','status'];
}
