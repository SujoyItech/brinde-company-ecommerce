<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class CrmCustomer extends Authenticatable
{
    use HasApiTokens, Notifiable, HasFactory;

    protected $fillable = [
      'customer_code',
      'name',
      'contact_name',
      'email',
      'phone',
      'password',
      'remember_token',
      'crm_payment_condition_id',
      'address',
      'postcode',
      'locality',
      'country',
      'tin_number',
      'tax_address',
      'fiscal_postcode',
      'tax_location',
      'tax_country',
      'image',
      'status',
      'email_verified',
      'reset_password_code',
      'salesman_id'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
