<?php

namespace App\Models\CRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotationProductArrivalLog extends Model
{
    use HasFactory;
    protected $fillable= ['crm_quotation_product_id', 'arrival_quantity', 'arrived_at', 'note'];
}
