<?php

namespace App\Imports;

use App\Models\Product\ProductUpload;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductImport implements ToCollection
{
    private $product_upload_id;

    public function __construct($product_upload_id){
        $this->product_upload_id = $product_upload_id;
    }
    public function collection(Collection $rows) {
        try {
            $columnList = [];
            $product_array = [];
            $index = 0;
            foreach ($rows as $key=>$row){
                if ($row[0] == NULL || $row[1] == NULL) {
                    break;
                }

                if ($key > 0) {
                    $insert_array = [];
                    foreach ($columnList as $column_key=>$column_name){
                        $insert_array[] = $row[$column_key];
                    }
                    $product_array[$index]['id'] = (int)Carbon::now()->timestamp+$index+(int)randomNumber(10);
                    $product_array[$index]['product'] = json_encode($insert_array);
                    $product_array[$index]['product_column'] = json_encode($columnList);
                    $product_array[$index]['batch_payload'] = $this->product_upload_id;
                    $product_array[$index]['status'] = PROCESSING;
                    $index ++;
                }else{
                    foreach ($row as $header_key => $header){
                        if ($header == NULL) {
                            break;
                        }
                        $columnList[$header_key] = $header;
                    }
                }
            }
            DB::table('product_upload_process')->insert($product_array);
            ProductUpload::where('id',$this->product_upload_id)->update(['total_product'=>$index,'status'=>STATUS_PRODUCT_PROCESSING_START]);
        }catch (\Exception $exception){

        }
    }


}
