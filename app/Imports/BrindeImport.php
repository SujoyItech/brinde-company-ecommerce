<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BrindeImport implements WithMultipleSheets
{
    private $product_upload_id;
    public function __construct($product_upload_id) {
        $this->product_upload_id = $product_upload_id;
    }

    public function sheets(): array {
        // TODO: Implement sheets() method.
        return [
            new ProductImport($this->product_upload_id)
        ];
    }
}
