<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BrindeExport implements WithMultipleSheets
{
    use Exportable;
    public function sheets(): array {
        $sheets = [
            new ProductExport(),
            new CategoryExport(),
            new TagExport(),
            new DeliveryTypeExport(),
            new ProductDescription()
        ];
        return $sheets;
    }
}
