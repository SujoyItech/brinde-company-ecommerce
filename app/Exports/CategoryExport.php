<?php

namespace App\Exports;

use App\Models\Product\Category;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CategoryExport implements FromArray,WithHeadings,WithTitle
{
    public function headings(): array {
        // TODO: Implement headings() method.
        return ['Category','Id'];
    }

    public function title(): string {
        // TODO: Implement title() method.
        return 'category_list';
    }

    public function array(): array {
        // TODO: Implement array() method.
        $query =  Category::select('name','id')->where('status',STATUS_ACTIVE)->get();
        $category = [];
        foreach ($query as $key =>$category_result){
            $category[$key]['name'] = $category_result->name;
            $category[$key]['id'] = $category_result->id;
        }
        return $category;
    }
}
