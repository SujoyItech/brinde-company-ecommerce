<?php

namespace App\Exports;

use App\Models\Product\Combination;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProductExport implements WithHeadings,WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array {

        $headings = ['name','reference','description','additional_info','category_id','delivery_type_id','tags_id',
                     'meta_title','meta_keywords','meta_description','status','equal_price','less_price','more_price',
                     'media_default','media_360','media_video'];
        $colors = Combination::where('combination_type_id',COLOR_ID)->pluck('name')->toArray();
        $colors_array = array_map(function ($item){
            return 'media_color_'.$item;
        },$colors);

        $size = Combination::where('combination_type_id',SIZE_ID)->pluck('name')->toArray();

        $size_array=array_map(function ($item){
            return 'ask_size_'.$item;
        },$size);

        $headings = array_merge($headings,$colors_array,$size_array);
        return $headings;
    }

    public function title(): string {
        // TODO: Implement title() method.
        return 'product_list';
    }
}
