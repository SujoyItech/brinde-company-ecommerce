<?php

namespace App\Exports;

use App\Models\Product\DeliveryType;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class DeliveryTypeExport implements FromArray, WithHeadings, WithTitle
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array {
        // TODO: Implement headings() method.
        return ['Delivery Type','id'];
    }

    public function title(): string {
        // TODO: Implement title() method.
        return 'delivery_type_list';
    }

    public function array(): array {
        // TODO: Implement array() method.
        $query =  DeliveryType::select('name','id')->where(['status'=>ACTIVE])->get();
        $delivery_type = [];
        foreach ($query as $key =>$delivery_type_result){
            $delivery_type[$key]['name'] = $delivery_type_result->name;
            $delivery_type[$key]['id'] = $delivery_type_result->id;
        }
        return $delivery_type;
    }
}
