<?php

namespace App\Exports;

use App\Models\Product\Tag;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class TagExport implements FromArray,WithHeadings,WithTitle
{

    public function headings(): array {
        // TODO: Implement headings() method.
        return ['Tag','Id'];
    }

    public function title(): string {
        // TODO: Implement title() method.
        return 'tag_list';
    }

    public function array(): array {
        // TODO: Implement array() method.
        $query = Tag::select('name','id')->where(['status'=>ACTIVE])->get();
        $tag = [];
        foreach ($query as $key =>$tag_result){
            $tag[$key]['name'] = $tag_result->name;
            $tag[$key]['id'] = $tag_result->id;
        }
        return $tag;
    }
}
