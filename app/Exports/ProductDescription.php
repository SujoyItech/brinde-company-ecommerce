<?php

namespace App\Exports;

use App\Models\Product\Combination;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProductDescription implements FromArray,WithHeadings,WithTitle
{
    public function headings(): array {
        // TODO: Implement headings() method.
        $headings = ['name','reference','description','additional_info','category_id','delivery_type_id','tags_id',
                     'meta_title','meta_keywords','meta_description','status','equal_price','less_price','more_price',
                     'media_default','media_360','media_video'];
        $colors = Combination::where('combination_type_id',COLOR_ID)->pluck('name')->toArray();
        $colors_array = array_map(function ($item){
            return 'media_color_'.$item;
        },$colors);

        $size = Combination::where('combination_type_id',SIZE_ID)->pluck('name')->toArray();

        $size_array=array_map(function ($item){
            return 'ask_size_'.$item;
        },$size);

        $headings = array_merge($headings,$colors_array,$size_array);
        return $headings;
    }

    public function title(): string {
        // TODO: Implement title() method.
        return __('product_upload_sample');
    }

    private function productDataUpdate(){
        $headings = $this->headings();
        $insert_data = [];
        $ask_array = ['Y',NULL];
        $media_array = ['https://externer-url/product-image-1.jpg','https://externer-url/product-image-2.png','https://externer-url/product-image-3.jpg',NULL];
        $equal_price = ['100=500,200=400,300=350,400=300','5000=500,10000=400'];
        $less_price = ['500=1000,200=1400'];
        $more_price = ['50=500,100=400','5000=500,10000=400'];
        $category_id = ['1,2','3,4'];
        $delivery_type_id = [1,2];
        $tags_id = ['1,2,3,4','3,4,5,6'];

        for ($index=0;$index<2;$index++){
            $insert_data[$index][0] = $index == 0 ? 'GUARDA-CHUVA AUTOMÁTICO' : 'CASACO CRIANÇA';
            $insert_data[$index][1] = $index == 0 ? 'guarda-chuva': 'casaco-crianca';
            $insert_data[$index][2] = $index == 0 ? 'This super long-lasting battery empowers you to face the challenges of life head-on' :
                'Power your day for longer with this 5000mAh massive battery.';
            $insert_data[$index][3] = $index == 0 ? 'A powerful 13MP AI Triple Camera captures the details of our brilliant world.' :
                'The primary 13MP camera enables sharper and brighter photos, while
            the 2MP Macro lens is perfect for detailed close-ups. Landscapes, portraits, macro shots...the possibilities are endless.';
            $insert_data[$index][4] = $category_id[$index];
            $insert_data[$index][5] = $delivery_type_id[$index];
            $insert_data[$index][6] = $tags_id[$index];
            $insert_data[$index][7] = 'meta title';
            $insert_data[$index][8] = 'meta key';
            $insert_data[$index][9] = 'meta description';
            $insert_data[$index][10] = 1;
            $insert_data[$index][11] = $equal_price[rand(0,1)];
            $insert_data[$index][12] = $less_price[0];
            $insert_data[$index][13] = $more_price[rand(0,1)];
            $insert_data[$index][14] = $media_array[rand(0,3)];
            $insert_data[$index][15] = $media_array[rand(0,3)];
            $insert_data[$index][16] = $media_array[rand(0,3)];
            foreach ($headings as $key => $header){
                if ($key > 16) {
                    if (str_contains($header,'media_color')) {
                        $insert_data[$index][$key] = $media_array[rand(0,3)];
                    }elseif (str_contains($header,'ask_size')){
                        $temp = rand(0,1);
                        $insert_data[$index][$key] = $ask_array[$temp];
                    }
                }
            }
        }

        return $insert_data;
    }
    public function array(): array {
        // TODO: Implement array() method.
        return $this->productDataUpdate();
    }
}
